package com.lianbao.dao;

import java.util.List;

import com.lianbao.vo.PoundageDetailsVO;
import com.vrc.vo.common.PageInfo;

/**
* @ClassName
* @Description
* @author 
* @date 2018年8月23日 上午2:43:01
*/
public interface PoundageDetailsDao {

	boolean addPoundageDetails(PoundageDetailsVO vo);
	
	List<PoundageDetailsVO> getPdList(PageInfo page, String keyword, String startTime, String endTime, String type);
	
	boolean deletePd(String[] ids, String type);
}
