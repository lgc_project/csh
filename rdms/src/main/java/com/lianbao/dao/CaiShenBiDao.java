package com.lianbao.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.lianbao.vo.CSBGuaDanVO;
import com.lianbao.vo.CaiShenBiVO;
import com.vrc.vo.common.PageInfo;

/**
* @ClassName
* @Description
* @author 
* @date 2018年6月21日 上午9:40:19
*/
public interface CaiShenBiDao {

	/**
	 * 获取财神币列表
	 * 
	 * @param page
	 * @param keyword
	 * @return
	 */
	public List<CaiShenBiVO> getCaiShenBisPage(PageInfo page, String keyword);
	
	
	/**
	 * 添加财神币
	 * 
	 * @param vo
	 * @return
	 */
	public boolean saveCaiShenBi(CaiShenBiVO vo);
	
	/**
	 * 修改财神币
	 * 
	 * @param vo
	 * @return
	 */
	public boolean updateCaiShenBi(CaiShenBiVO vo);
	
	/**
	 * 批量删除财神币
	 * 
	 * @param ids
	 * @return
	 */
	public boolean deleteCaiShenBi(String[] ids);
	
	/**
	 * 获取财神币
	 * @param id
	 * @return
	 */
	public CaiShenBiVO getCaiShenBiById(String id);
	
	/**
	 * 获取最大的id
	 * 
	 * @param tableName
	 * @return
	 */
	public long getMaxId(String tableName);
	
	/**
	 * 获取用户的余额
	 * 
	 * @param userId
	 * @return
	 */
	public Double getUserMoneySum(String userId);
	
	/**
	 * 获取用户财神币数量
	 * 
	 * @param userId
	 * @return
	 */
	public BigDecimal getUserCsbSum(String userId);
	
	/**
	 * 获取财神币的数量
	 * 
	 * @return
	 */
	public Map<String, Object> getCaiShenBiAmount();
	
	/**
	 * 获取最大的挂单数
	 * 
	 * @return
	 */
	public long getMaxGuaDanId();

	/**
	 * 增加一张挂单
	 * 
	 * @return
	 */
	public boolean addGuaDan(CSBGuaDanVO vo);
	
	/**
	 * 修改挂单
	 * 
	 * @param vo
	 * @return
	 */
	public boolean updateGuaDan(CSBGuaDanVO vo);
	
	/**
	 * 获取挂单
	 * 
	 * @param oddId
	 * @return
	 */
	public CSBGuaDanVO getGuaDanByOddId(String oddId);
	
	public CSBGuaDanVO getGuaDanByOddIdAndStau(String oddId, Integer status);
}
