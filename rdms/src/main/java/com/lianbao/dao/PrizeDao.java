/**
 * 
 */
package com.lianbao.dao;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.lianbao.vo.PrizeVO;
import com.vrc.vo.common.PageInfo;

/**
* @ClassName
* @Description
* @author 
* @date 2018年6月11日 下午2:09:52
*/
public interface PrizeDao {

	/**
	 * 获取奖品列表
	 * @param page
	 * @param keyword
	 * @return
	 */
	public List<PrizeVO> getPrizesPage(PageInfo page, String keyword);
	
	/**
	 * 添加奖品
	 * @param vo
	 * @return
	 */
	public boolean savePrize(PrizeVO vo);
	
	/**
	 * 修改奖品
	 * @param vo
	 * @return
	 */
	public boolean updatePrize(PrizeVO vo);
	
	/**
	 * 批量删除奖品
	 * @param ids
	 * @return
	 */
	public boolean deletePrize(String[] ids);
	
	/**
	 * 判断是否可以抽奖
	 * @param curTime
	 * @return
	 */
	public boolean judgePrizeDraw(Timestamp curTime);
	
	/**
	 * 判断是否中奖
	 * 
	 * @param random
	 * @return
	 */
	public Map<String, Object> isBingo(double random);
	
	/**
	 * 更新用户奖品信息
	 * 
	 * @param noToName
	 * @param userId
	 * @return
	 */
	public boolean updateUserPrize(String prize, String userId, Integer bingoNum);
	
	
	/**
	 * 更新奖品的数量
	 * 
	 * @param bingoNum
	 * @return
	 */
	public boolean updatePrizeCount(String prizeId, Integer bingoNum);
	
	/**
	 * 获取最大的用户id
	 * 
	 * @return
	 */
	public long getMaxId();
}
