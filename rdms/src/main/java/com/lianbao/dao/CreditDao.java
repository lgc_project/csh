package com.lianbao.dao;

import java.util.List;

import com.lianbao.vo.CreditVO;
import com.vrc.vo.common.PageInfo;

/**
* @ClassName
* @Description
* @author 
* @date 2018年8月20日 下午4:11:38
*/
public interface CreditDao {

	/**
	 * 新增充值记录
	 * 
	 * @param vo
	 * @return
	 */
	boolean addCreditVO(CreditVO vo);
	
	/**
	 * 更新充值记录
	 * 
	 * @param vo
	 * @return
	 */
	boolean updateCreditVO(CreditVO vo);
	
	/**
	 * 根据订单号获取充值记录
	 * 
	 * @param outTradeNo
	 * @return
	 */
	CreditVO getCreditVO(String outTradeNo);
	
	/**
	 * 获取最大的充值编号
	 * 
	 * @return
	 */
	long getMaxCreditNo();
	
	/**
	 * 获取充值记录页面
	 * 
	 * @param page
	 * @param keyword
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	List<CreditVO> getCreditPage(PageInfo page, String keyword, String startTime, String endTime);
}
