package com.lianbao.dao;

import java.sql.Timestamp;
import java.util.List;

import com.lianbao.vo.CSBRecoverVO;
import com.vrc.vo.common.PageInfo;

/**
* @ClassName
* @Description
* @author 
* @date 2018年6月25日 下午7:43:53
*/
public interface CSBRecoverDao {

	/**
	 * 获取系统回收列表
	 * 
	 * @param page
	 * @param keyword
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public List<CSBRecoverVO> getCSBRecoverInfoPage(PageInfo page, String keyword, Timestamp startTime, Timestamp endTime);
	
	/**
	 * 增加一条系统回收的信息
	 * 
	 * @param vo
	 * @return
	 */
	public boolean addCSBRecoverVO(CSBRecoverVO vo);

}
