package com.lianbao.dao;

import com.lianbao.vo.GameProVO;

/**
* @ClassName
* @Description
* @author 
* @date 2018年7月21日 下午6:13:48
*/
public interface GameProDao {

	boolean saveOrUpdate(GameProVO vo);
	
	GameProVO getGamePro();
}
