package com.lianbao.dao;

import java.util.List;

import com.lianbao.vo.TakeCashVO;
import com.vrc.vo.common.PageInfo;

/**
* @ClassName
* @Description
* @author 
* @date 2018年7月9日 上午9:37:04
*/
public interface TakeCashDao {

	public List<TakeCashVO> getCashPage(PageInfo page, String keyword, String startTime, String endTime);

	public boolean saveOrUpdateTakeCash(TakeCashVO vo);
	
	public TakeCashVO getCashById(String id);
}
