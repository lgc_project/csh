/**
 * 
 */
package com.lianbao.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.lianbao.vo.AuctionUserVO;
import com.lianbao.vo.AuctionVO;
import com.vrc.vo.common.PageInfo;

/**
* @ClassName 
* @Description
* @author LGC 
* @date 2018年6月18日 上午1:18:09
*/
public interface AuctionDao {

	/**
	 * 获取竞拍活动列表 
	 * 
	 * @param page
	 * @param keyword
	 * @return
	 */
	public List<AuctionVO> getAuctionsPage(PageInfo page, String keyword);

	/**
	 * 添加竞拍活动
	 * 
	 * @param vo
	 * @return
	 */
	public boolean saveAuction(AuctionVO vo);
	
	/**
	 * 修改竞拍活动
	 * 
	 * @param vo
	 * @return
	 */
	public boolean updateAuction(AuctionVO vo);
	
	/**
	 * 批量删除竞拍活动
	 * 
	 * @param ids
	 * @return
	 */
	public boolean deleteAuction(String[] ids);
	
	
	/**
	 * 判断是否为竞拍活动时间
	 * 
	 * @param curTime
	 * @return
	 */
	public Map<String, Object> judgeIsAuction(Timestamp curTime);
	
	/**
	 * 获得竞拍前五名
	 * 
	 * @return
	 */
	public List<Map<String, Object>> getTop5AuctionInfo(String auctionId);
	
	/**
	 * 增加竞拍的信息
	 * 
	 * @param userId
	 * @param auctionId
	 * @param auctionAmount
	 * @return
	 */
	public boolean addAuctionInfo(AuctionUserVO vo);
	
	/**
	 * 获取最大的用户id
	 * 
	 * @return
	 */
	public long getMaxId(String tableName);
}
