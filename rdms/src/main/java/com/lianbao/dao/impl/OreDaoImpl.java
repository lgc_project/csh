package com.lianbao.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.lianbao.dao.OreDao;
import com.lianbao.vo.OreVO;
import com.vrc.util.InsertUtil;
import com.vrc.vo.common.PageInfo;

/**
* @ClassName
* @Description
* @author 
* @date 2018年7月23日 上午10:34:59
*/
@Repository("oreDao")
public class OreDaoImpl implements OreDao {

	@Resource
	private JdbcTemplate template;
	
	@Override
	public List<OreVO> getOrePage(PageInfo page, String keyword, Timestamp startTime, Timestamp endTime) {
		String sql = "SELECT * FROM lb_ore where 1 = 1 \n"; // 使用where 1=1 使sql的查询条件拼装起来更优雅
		String whereSql = "";
		if(!StringUtils.isNotBlank(keyword)) {
			whereSql += String.format("and (id LIKE '%%%s%%' \n", keyword);
			whereSql += String.format("OR user_id LIKE '%%%s%%' \n", keyword);
			whereSql += String.format("OR user_name LIKE '%%%s%%') \n", keyword);
		}
		
		if(startTime != null) {
			whereSql += String.format(" and create_time >= %s \n", startTime);
		}
		
		if(endTime != null) {
			whereSql += String.format(" and create_time < %s \n", endTime);
		}
		
		sql += whereSql;
		sql += "ORDER BY create_time DESC \n";
		
		page.pageTotal(sql, template);
		sql = page.decorateSql(sql);
		SqlRowSet rs = template.queryForRowSet(sql);
		List<OreVO> list = new ArrayList<>();
		if(rs != null) {
			while(rs.next()) {
				OreVO vo = createRealOreInfo(rs);
				list.add(vo);
			}
		}
		
		return list;
	}

	@Override
	public boolean saveOre(OreVO vo) {
		InsertUtil insUtil = new InsertUtil("lb_ore");
		insUtil.setColumn("id", vo.getId());
		insUtil.setColumn("user_id", vo.getUserId());
		insUtil.setColumn("user_name", vo.getUsername());
		insUtil.setColumn("ore_sum", vo.getOreSum());
		insUtil.setColumn("csb_sum", vo.getCsbSum());
		insUtil.setColumn("create_time", vo.getCreateTime().toString());
		
		String sql = insUtil.getInsertSql();
		int count = template.update(sql);
		return count > 0;
	}


	private OreVO createRealOreInfo(SqlRowSet rs) {
		OreVO vo = new OreVO();
		vo.setId(rs.getLong("id"));
		vo.setUserId(rs.getString("user_id"));
		vo.setUsername(rs.getString("user_name"));
		vo.setOreSum(rs.getInt("ore_sum"));
		vo.setCsbSum(rs.getInt("csb_sum"));
		vo.setCreateTime(rs.getTimestamp("create_time"));
		return vo;
	}

	@Override
	public Long getMaxId() {
		String sql = "SELECT MAX(id) FROM lb_ore \n";
		Long max = template.queryForObject(sql, Long.class);
		if(max == null) {
			return 0L;
		}
		return max;
	}
}
