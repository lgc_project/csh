package com.lianbao.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.alibaba.druid.util.StringUtils;
import com.lianbao.dao.PoundageDetailsDao;
import com.lianbao.vo.PoundageDetailsVO;
import com.vrc.util.InsertUtil;
import com.vrc.vo.common.PageInfo;

/**
* @ClassName
* @Description
* @author 
* @date 2018年8月23日 上午2:45:48
*/
@Repository("poundageDetalsDao")
public class PoundageDetailsDaoImpl implements PoundageDetailsDao {

	@Resource
	private JdbcTemplate template;
	
	@Override
	public boolean addPoundageDetails(PoundageDetailsVO vo) {
		InsertUtil insUtil = new InsertUtil("lb_poundage_details");
		insUtil.setColumn("id", vo.getId());
		insUtil.setColumn("pd_no", vo.getPdNo());
		insUtil.setColumn("trade_price", vo.getTradePrice());
		insUtil.setColumn("trade_poundage", vo.getTradePoundage());
		insUtil.setColumn("pd_price", vo.getPdPrice());
		insUtil.setColumn("user_id", vo.getUserId());
		insUtil.setColumn("type", vo.getType());
		insUtil.setColumn("create_time", vo.getCreateTime());
		insUtil.setColumn("guadan_type", vo.getGuadanType());
		String sql = insUtil.getInsertSql();
		int count = template.update(sql);
		return count > 0;
	}

	@Override
	public List<PoundageDetailsVO> getPdList(PageInfo page, String keyword, String startTime, String endTime,
			String type) {
		String whereSql = "";
		if(!StringUtils.isEmpty(keyword)) {
			whereSql += " and user_id like '%" + keyword + "%' ";
		}
		
		if(!StringUtils.isEmpty(startTime)) {
			whereSql += " and create_time >= '" + startTime + "' ";
		}
		
		if(!StringUtils.isEmpty(endTime)) {
			whereSql += " and create_time < '" + endTime + "' ";
		}
		
		whereSql += " and type = '" + type + "' ";
		String sql = "select * from lb_poundage_details ";
		sql += "where 1=1 " + whereSql + " "; 
		sql += "order by create_time desc";
		
		page.pageTotal(sql, template);
		sql = page.decorateSql(sql);
		SqlRowSet rs = template.queryForRowSet(sql);
		List<PoundageDetailsVO> list = new ArrayList<PoundageDetailsVO>();
		if(rs != null) {
			while(rs.next()) {
				PoundageDetailsVO vo = createPdVO(rs);
				list.add(vo);
			}
		}
		return list;
	}
	
	private PoundageDetailsVO createPdVO(SqlRowSet rs) {
		PoundageDetailsVO vo = new PoundageDetailsVO();
		vo.setId(rs.getString("id"));
		vo.setPdNo(rs.getString("pd_no"));
		vo.setPdPrice(rs.getDouble("pd_price"));
		vo.setUserId(rs.getString("user_id"));
		vo.setType(rs.getString("type"));
		vo.setCreateTime(rs.getString("create_time"));
		vo.setGuadanType(rs.getString("guadan_type"));
		vo.setTradePrice(rs.getDouble("trade_price"));
		vo.setTradePoundage(rs.getDouble("trade_poundage"));
		return vo;
	}

	@Override
	public boolean deletePd(String[] ids, String type) {
		if(ids == null || ids.length == 0) {
			return false;
		}
		
		String delSql = "";
		for(int i = 0; i < ids.length; i++) {
			if(i == 0) {
				delSql += String.format("'%s'", ids[i]);
			} else {
				delSql += String.format(", '%s'", ids[i]);
			}
		}
		String whereSql = " and type = ?";
		
		String sql = "";
		sql += String.format("delete from %s \n", "lb_poundage_details");
		sql += String.format("where 1 = 1 and id IN (%s) \n", delSql);
		sql += whereSql;
		int count = template.update(sql, type);
		return count > 0;
	}

}
