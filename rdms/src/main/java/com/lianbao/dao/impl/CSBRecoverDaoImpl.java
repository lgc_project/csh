package com.lianbao.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.alibaba.druid.util.StringUtils;
import com.lianbao.dao.CSBRecoverDao;
import com.lianbao.vo.CSBRecoverVO;
import com.vrc.util.InsertUtil;
import com.vrc.util.TableNameUtil;
import com.vrc.vo.common.PageInfo;

/**
* @ClassName
* @Description
* @author 
* @date 2018年6月25日 下午7:52:22
*/
@Repository("csbRecoverDao")
public class CSBRecoverDaoImpl implements CSBRecoverDao {

	@Resource
	private JdbcTemplate template;
	
	@Override
	public List<CSBRecoverVO> getCSBRecoverInfoPage(PageInfo page, String keyword, Timestamp startTime,
			Timestamp endTime) {
		List<CSBRecoverVO> list = new ArrayList<CSBRecoverVO>();
		String whereSql = "";
		if(!StringUtils.isEmpty(keyword)) {
			whereSql += String.format(" and user_id like '%%%s%%' \n", keyword);
		}
		if(startTime != null) {
			whereSql += String.format(" and create_time >= %s \n", startTime);
		}
		if(endTime != null) {
			whereSql += String.format(" and create_time <= %s \n", endTime);
		}
		
		String sql = "";
		sql += String.format("select * from %s where 1 = 1 \n", TableNameUtil.LB_CSB_RECOVER);
		sql += whereSql;
		sql += "order by create_time desc \n";
		page.pageTotal(sql, template);
		sql = page.decorateSql(sql);
		SqlRowSet rs = template.queryForRowSet(sql);
		if(rs != null) {
			while(rs.next()) {
				CSBRecoverVO vo = new CSBRecoverVO();
				vo.setId(rs.getString("id"));
				vo.setUserId(rs.getString("user_id"));
				vo.setCsbSum(rs.getDouble("csb_sum"));
				vo.setMoneySum(rs.getDouble("money_sum"));
				vo.setPoundage(rs.getDouble("poundage"));
				vo.setMoneyPd(rs.getDouble("money_pd"));
				vo.setCreateTime(rs.getTimestamp("create_time"));
				list.add(vo);
			}
		}
		return list;
	}

	@Override
	public boolean addCSBRecoverVO(CSBRecoverVO vo) {
		InsertUtil insUtil = new InsertUtil(TableNameUtil.LB_CSB_RECOVER);
		insUtil.setColumn("id", vo.getId());
		insUtil.setColumn("user_id", vo.getUserId());
		insUtil.setColumn("csb_sum", vo.getCsbSum());
		insUtil.setColumn("money_sum", vo.getMoneySum());
		insUtil.setColumn("poundage", vo.getPoundage());
		insUtil.setColumn("money_pd", vo.getMoneyPd());
		insUtil.setColumn("create_time", vo.getCreateTime().toString());
		String sql = insUtil.getInsertSql();
		int count = template.update(sql);
		return count > 0;
	}

}
