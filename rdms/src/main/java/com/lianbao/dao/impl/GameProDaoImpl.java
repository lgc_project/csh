package com.lianbao.dao.impl;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.lianbao.dao.GameProDao;
import com.lianbao.vo.GameProVO;
import com.vrc.util.InsertUtil;

/**
* @ClassName
* @Description
* @author 
* @date 2018年7月21日 下午7:25:32
*/
@Repository("gameProDao")
public class GameProDaoImpl implements GameProDao {

	@Resource
	private JdbcTemplate template;
	
	@Override
	public boolean saveOrUpdate(GameProVO vo) {
		String sql = "select * from lb_gamepro order by id desc limit 1 \n";
		SqlRowSet rs = template.queryForRowSet(sql);
		
		if(rs != null && rs.next()) {
			InsertUtil insUtil = new InsertUtil("lb_gamepro");
			insUtil.setColumn("cq_pro", vo.getCaiQuanPro());
			insUtil.setColumn("sgly_pro", vo.getShuiGuoLeYuanPro());
			insUtil.setColumn("xydb_pro", vo.getXingYunDuoBaoPro());
			String updateSql = insUtil.getUpdateSql();
			updateSql += " where id = ?";
			int count = template.update(updateSql, rs.getLong("id"));
			return count > 0;
		} else {
			InsertUtil insUtil = new InsertUtil("lb_gamepro");
			insUtil.setColumn("id", 10000L);
			insUtil.setColumn("cq_pro", vo.getCaiQuanPro());
			insUtil.setColumn("sgly_pro", vo.getShuiGuoLeYuanPro());
			insUtil.setColumn("xydb_pro", vo.getXingYunDuoBaoPro());
			String insertSql = insUtil.getInsertSql();
			int count = template.update(insertSql);
			return count > 0;
		}
	}

	@Override
	public GameProVO getGamePro() {
		String sql = "SELECT * FROM lb_gamepro \n";
		sql += "ORDER BY id DESC \n";
		sql += "LIMIT 1 \n";
		
		SqlRowSet rs = template.queryForRowSet(sql);
		while(rs != null && rs.next()) {
			GameProVO vo = createGameProRealInfo(rs);
			return vo;
		}
		return null;
	}
	
	private GameProVO createGameProRealInfo(SqlRowSet rs) {
		GameProVO vo = new GameProVO();
		vo.setId(rs.getLong("id"));
		vo.setCaiQuanPro(rs.getDouble("cq_pro"));
		vo.setShuiGuoLeYuanPro(rs.getDouble("sgly_pro"));
		vo.setXingYunDuoBaoPro(rs.getDouble("xydb_pro"));
		return vo;
	}

}
