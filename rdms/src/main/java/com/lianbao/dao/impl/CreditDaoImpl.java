package com.lianbao.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.alibaba.druid.util.StringUtils;
import com.lianbao.dao.CreditDao;
import com.lianbao.vo.CreditVO;
import com.vrc.util.InsertUtil;
import com.vrc.vo.common.PageInfo;

/**
* @ClassName
* @Description
* @author 
* @date 2018年8月20日 下午4:14:06
*/
@Repository("creditDao")
public class CreditDaoImpl implements CreditDao {

	@Resource
	private JdbcTemplate template;
	
	@Override
	public boolean addCreditVO(CreditVO vo) {
		InsertUtil insUtil = new InsertUtil("lb_credit");
		insUtil.setColumn("id", vo.getId());
		insUtil.setColumn("credit_no", vo.getCreditNo());
		insUtil.setColumn("out_trade_no", vo.getOutTradeNo());
		insUtil.setColumn("credit_money", String.valueOf(vo.getCreditMoney()));
		insUtil.setColumn("user_id", vo.getUserId());
		insUtil.setColumn("credit_time", vo.getCreditTime());
		insUtil.setColumn("status", vo.getStatus());
		insUtil.setColumn("credit_type", vo.getCreditType());
		String sql = insUtil.getInsertSql();
		int count = template.update(sql); 
		return count > 0;
	}

	@Override
	public long getMaxCreditNo() {
		String sql = "select max(credit_no) from lb_credit";
		Long max = template.queryForObject(sql, Long.class);
		if(max == null) {
			return 0L;
		}
		
		return max.longValue();
	}
	
	@Override
	public CreditVO getCreditVO(String outTradeNo) {
		String sql = "select * from lb_credit where out_trade_no = ?";
		SqlRowSet rs = template.queryForRowSet(sql, outTradeNo);
		if(rs != null && rs.next()) {
			CreditVO vo = createCreditVO(rs);
			return vo;
		}
		
		return null;
	}

	@Override
	public boolean updateCreditVO(CreditVO vo) {
		InsertUtil insUtil = new InsertUtil("lb_credit");
		insUtil.setColumn("credit_no", vo.getCreditNo());
		insUtil.setColumn("out_trade_no", vo.getOutTradeNo());
		insUtil.setColumn("user_id", vo.getUserId());
		insUtil.setColumn("credit_type", vo.getCreditType());
		insUtil.setColumn("credit_money", vo.getCreditMoney());
		insUtil.setColumn("credit_time", vo.getCreditTime());
		insUtil.setColumn("update_time", vo.getUpdateTime());
		insUtil.setColumn("status", vo.getStatus());
		String sql = insUtil.getUpdateSql();
		sql += " where id = ?";
		int count = template.update(sql, vo.getId());
		return count > 0;
	}
	
	private CreditVO createCreditVO(SqlRowSet rs) {
		CreditVO vo = new CreditVO();
		vo.setId(rs.getString("id"));
		vo.setCreditNo(rs.getString("credit_no"));
		vo.setOutTradeNo(rs.getString("out_trade_no"));
		vo.setUserId(rs.getString("user_id"));
		vo.setCreditType(rs.getString("credit_type"));
		vo.setCreditMoney(rs.getDouble("credit_money"));
		vo.setCreditTime(rs.getString("credit_time"));
		vo.setStatus(rs.getInt("status"));
		return vo;
	}

	@Override
	public List<CreditVO> getCreditPage(PageInfo page, String keyword, String startTime, String endTime) {
		String whereSql = "";
		if(!StringUtils.isEmpty(keyword)) {
			whereSql += " (and user_id like '%" + keyword + "%' ";
			whereSql += " or credit_no like '%" + keyword + "%') ";
		}
		
		if(!StringUtils.isEmpty(startTime)) {
			whereSql += " and credit_time >= " + startTime + " ";
		}
		
		if(!StringUtils.isEmpty(endTime)) {
			whereSql += " and credit_time < " + endTime + " ";
		}
		
		whereSql += " and status = '2' ";
		String sql = "select * from lb_credit where 1=1 " + whereSql + "order by credit_time desc";
		page.pageTotal(sql, template);
		sql = page.decorateSql(sql);
		SqlRowSet rs = template.queryForRowSet(sql);
		List<CreditVO> list = new ArrayList<>();
		while(rs != null && rs.next()) {
			CreditVO vo = createCreditVO(rs);
			list.add(vo);
		}
		
		return list;
	}
}
