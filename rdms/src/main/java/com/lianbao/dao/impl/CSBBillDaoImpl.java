package com.lianbao.dao.impl;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.lianbao.dao.CSBBillDao;
import com.lianbao.vo.CSBBillVO;
import com.vrc.util.InsertUtil;
import com.vrc.util.TableNameUtil;

/**
* @ClassName
* @Description
* @author 
* @date 2018年6月22日 下午10:10:08
*/
@Repository("csbBillDao")
public class CSBBillDaoImpl implements CSBBillDao {

	@Resource
	private JdbcTemplate template;
	
	@Override
	public boolean addCSBBill(CSBBillVO bill) {
		InsertUtil insUtil = new InsertUtil(TableNameUtil.LB_CSB_BILL);
		insUtil.setColumn("id", bill.getId());
		insUtil.setColumn("type", bill.getType());
		insUtil.setColumn("user_id", bill.getUserId());
		insUtil.setColumn("price", bill.getPrice());
		insUtil.setColumn("create_time", bill.getCreateTime().toString());
		
		String sql = insUtil.getInsertSql();
		int count = template.update(sql);
		return count > 0;
	}

}
