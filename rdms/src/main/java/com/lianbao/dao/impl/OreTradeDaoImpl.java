package com.lianbao.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.lianbao.dao.OreTradeDao;
import com.lianbao.vo.OreGuaDanVO;
import com.vrc.common.Const;
import com.vrc.util.InsertUtil;

/**
* @ClassName
* @Description
* @author 
* @date 2018年8月8日 下午10:52:29
*/
@Repository("oreTradeDao")
public class OreTradeDaoImpl implements OreTradeDao {
	
	@Resource
	private JdbcTemplate template;

	@Override
	public long getMaxGuaDanNo() {
		String sql = "select max(odd_no) from lb_ore_guadan";
		Long max = template.queryForObject(sql, Long.class);
		if(max != null) {
			return max.longValue();
		}
		return 0;
	}
	
	@Override
	public OreGuaDanVO getGuaDanByIdAndStau(String oddId, int status) {
		String sql = "select * from lb_ore_guadan where odd_no = ? and status = ?";
		SqlRowSet rs = template.queryForRowSet(sql, oddId, status);
		if(rs != null & rs.next()) {
			return createOreGuaDan(rs);
		}
		return null;
	}
	

	@Override
	public OreGuaDanVO getGuaDanById(String oddId) {
		String sql = "select * from lb_ore_guadan where odd_no = ?";
		SqlRowSet rs = template.queryForRowSet(sql, oddId);
		if(rs != null && rs.next()) {
			return createOreGuaDan(rs);
		}
		return null;
	}
	
	@Override
	public boolean addOreGuaDan(OreGuaDanVO vo) {
		InsertUtil insUtil = new InsertUtil("lb_ore_guadan");
		insUtil.setColumn("id", vo.getId());
		insUtil.setColumn("odd_no", vo.getOddId());
		insUtil.setColumn("buy_id", vo.getBuyId());
		insUtil.setColumn("sell_id", vo.getSellId());
		insUtil.setColumn("count", vo.getCount());
		insUtil.setColumn("unit_price", vo.getUnitPrice());
		insUtil.setColumn("price_sum", vo.getPriceSum());
		insUtil.setColumn("status", vo.getStatus());
		insUtil.setColumn("isvalid", vo.getIsValid());
		insUtil.setColumn("level", vo.getLevel());
		insUtil.setColumn("guadan_time", vo.getGuaDanTime());
		insUtil.setColumn("operate_time", vo.getOperateTime());
		insUtil.setColumn("type", vo.getType());
		String sql = insUtil.getInsertSql();
		int count = template.update(sql);
		
		return count > 0;
	}

	@Override
	public boolean updateGuaDan(OreGuaDanVO vo) {
		InsertUtil insUtil = new InsertUtil("lb_ore_guadan");
		insUtil.setColumn("buy_id", vo.getBuyId());
		insUtil.setColumn("sell_id", vo.getSellId());
		insUtil.setColumn("count", vo.getCount());
		insUtil.setColumn("unit_price", vo.getUnitPrice());
		insUtil.setColumn("price_sum", vo.getPriceSum());
		insUtil.setColumn("status", vo.getStatus());
		insUtil.setColumn("isvalid", vo.getIsValid());
		insUtil.setColumn("level", vo.getLevel());
		insUtil.setColumn("operate_time", vo.getOperateTime());
		
		String sql = insUtil.getUpdateSql();
		String whereSql = " where odd_no = ?";
		sql += whereSql;
		int count = template.update(sql, vo.getOddId());
		
		return count > 0;
	}

	@Override
	public List<OreGuaDanVO> getOreGuaDanList(String userId, String level, int type) {
		List<OreGuaDanVO> list = new ArrayList<>();
		if(userId == null) {
			userId = "";
		}
		
		String levelWhere = "";
		if(level != null) {
			levelWhere = " and level = " + level;
		}	

		String idWhere = "";
		int oddStatus = 0;
		if(type == 0) {
			idWhere = " and buy_id != ? ";
			oddStatus = Const.oreOddPost;
		} else if(type == 1) {
			idWhere = " and sell_id != ? ";
			oddStatus = Const.oreOddSellSure;
		}
		
		String sql = "select * from lb_ore_guadan where status = ? and type = ? "
						+ idWhere + levelWhere + " order by unit_price desc, count desc";
		SqlRowSet rs = template.queryForRowSet(sql, oddStatus, type, userId);
		if(rs != null) {
			while(rs.next()) {
				OreGuaDanVO vo = createOreGuaDan(rs);
				list.add(vo);
			}
		}
		return list;
	}

	@Override
	public List<OreGuaDanVO> getOreGuaDanByBuyOrSell(String userId, String level, int type) {
		String levelWhere = "";
		if(level != null) {
			levelWhere = " and level = " + level;
		} else {
			levelWhere = " and level is null";
		}
		
		// 查询状态为0，1，2。且买家或卖家是该用户的挂单
		String sql = "select * from lb_ore_guadan where status in (0, 1, 2) " 
						+ " and (buy_id = ? or sell_id = ?) and type = ? " + levelWhere + " order by operate_time desc";
		SqlRowSet rs = template.queryForRowSet(sql, userId, userId, type);
		
		List<OreGuaDanVO> list = new ArrayList<>();
		if(rs != null) {
			while(rs.next()) {
				OreGuaDanVO vo = createOreGuaDan(rs);
				list.add(vo);
			}
		}
		return list;
	}
	
	private OreGuaDanVO createOreGuaDan(SqlRowSet rs) {
		OreGuaDanVO vo = new OreGuaDanVO();
		vo.setId(rs.getString("id"));
		vo.setOddId(rs.getString("odd_no"));
		vo.setBuyId(rs.getString("buy_id"));
		vo.setSellId(rs.getString("sell_id"));
		vo.setCount(rs.getDouble("count"));
		vo.setUnitPrice(rs.getDouble("unit_price"));
		vo.setPriceSum(rs.getDouble("price_sum"));
		vo.setStatus(rs.getInt("status"));
		vo.setLevel(rs.getString("level"));
		vo.setIsValid(rs.getInt("isvalid"));
		vo.setGuaDanTime(rs.getString("guadan_time"));
		vo.setOperateTime(rs.getString("operate_time"));
		return vo;
	}

}
