package com.lianbao.dao.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.alibaba.druid.util.StringUtils;
import com.lianbao.dao.CaiShenBiDao;
import com.lianbao.vo.CSBGuaDanVO;
import com.lianbao.vo.CaiShenBiVO;
import com.vrc.util.InsertUtil;
import com.vrc.util.TableNameUtil;
import com.vrc.vo.common.PageInfo;

/**
 * @ClassName
 * @Description
 * @author
 * @date 2018年6月21日 上午9:51:19
 */
@Repository("CaiShenBiDao")
public class CaiShenBiDaoImpl implements CaiShenBiDao {

	@Resource
	private JdbcTemplate template;

	@Override
	public List<CaiShenBiVO> getCaiShenBisPage(PageInfo page, String keyword) {
		List<CaiShenBiVO> results = new ArrayList<CaiShenBiVO>();

		String whereSql = "";
		if (!StringUtils.isEmpty(keyword)) {
			whereSql += String.format("WHERE id LIKE '%%%s%%' \n", keyword);
		}

		String sql = "";
		sql += String.format("SELECT * FROM %s \n", TableNameUtil.LB_CAISHENGBI);
		sql += whereSql;
		sql += "ORDER BY id ASC \n";

		page.pageTotal(sql, template);
		sql = page.decorateSql(sql);
		SqlRowSet rs = template.queryForRowSet(sql);
		if (rs != null) {
			while (rs.next()) {
				CaiShenBiVO vo = createCaiShenBiRealInfo(rs);
				results.add(vo);
			}
		}
		return results;
	}

	/**
	 * @param rs
	 * @return
	 */
	private CaiShenBiVO createCaiShenBiRealInfo(SqlRowSet rs) {
		CaiShenBiVO vo = new CaiShenBiVO();
		vo.setId(rs.getString("id"));
		vo.setAmount(rs.getDouble("amount"));
		vo.setUnitPrice(rs.getDouble("unit_price"));
		vo.setPoundage(rs.getDouble("poundage"));
		vo.setBeginTime(rs.getTimestamp("begin_time"));
		vo.setEndTime(rs.getTimestamp("end_time"));
		vo.setCreateTime(rs.getTimestamp("create_time"));
		vo.setUpdateTime(rs.getTimestamp("update_time"));
		vo.setSurplus(rs.getDouble("surplus"));
		return vo;
	}

	@Override
	public boolean saveCaiShenBi(CaiShenBiVO vo) {
		String sql = "";
		sql += "INSERT INTO \n";
		sql += String.format("%s(id, amount, unit_price, poundage, begin_time, end_time, create_time, surplus) \n",
				TableNameUtil.LB_CAISHENGBI);
		sql += "VALUE (?, ?, ?, ?, ?, ?, ?, ?) \n";

		int count = template.update(sql, vo.getId(), vo.getAmount(), vo.getUnitPrice(), vo.getPoundage(), vo.getBeginTime(),
				vo.getEndTime(), vo.getCreateTime(), vo.getAmount());
		return count > 0;
	}

	@Override
	public boolean updateCaiShenBi(CaiShenBiVO vo) {
		Date curDate = new Date();
		Timestamp curTime = new Timestamp(curDate.getTime());
		InsertUtil insUtil = new InsertUtil(TableNameUtil.LB_CAISHENGBI);
		insUtil.setColumn("amount", vo.getAmount());
		insUtil.setColumn("surplus", vo.getSurplus());
		insUtil.setColumn("unit_price", vo.getUnitPrice());
		insUtil.setColumn("begin_time", vo.getBeginTime().toString());
		insUtil.setColumn("end_time", vo.getEndTime().toString());
		insUtil.setColumn("update_time", curTime.toString());
		String sql = insUtil.getUpdateSql();
		sql = String.format("%s WHERE id = ? \n", sql);
		int count = template.update(sql, vo.getId());
		return count > 0;
	}

	@Override
	public boolean deleteCaiShenBi(String[] ids) {
		if(ids == null || ids.length == 0) {
			return false;
		}
		
		String whereSql = "";
		for(int i = 0; i < ids.length; i++) {
			if(i == 0) {
				whereSql += String.format("%s", ids[i]);
			} else {
				whereSql += String.format(", %s", ids[i]);
			}
		}
		
		String delSql = "";
		delSql += String.format("DELETE FROM %s \n", TableNameUtil.LB_CAISHENGBI);
		delSql += String.format("WHERE id IN (%s) \n", whereSql);
		int count = template.update(delSql);
		return count > 0;
	}

	@Override
	public long getMaxId(String tableName) {
		String sql = String.format("select max(id) from %s", tableName);
		Long max = template.queryForObject(sql, Long.class);
		if(max == null) {
			return 0L;
		}
		return max.longValue();
	}

	@Override
	public Double getUserMoneySum(String userId) {
		String sql = String.format("select money_sum from %s \n", TableNameUtil.TB_USER);
		sql += String.format("WHERE id = %s \n", userId);
		Double moneySum = template.queryForObject(sql, Double.class);
		return moneySum;
	}

	@Override
	public Map<String, Object> getCaiShenBiAmount() {
		Map<String, Object> rstMap = new HashMap<String, Object>();
		String sql = "";
		sql += "SELECT id, amount, poundage, unit_price, surplus \n";
		sql += String.format("FROM %s \n", TableNameUtil.LB_CAISHENGBI);
		sql += "ORDER BY create_time DESC \n";
		sql += "LIMIT 1 \n";
		
		rstMap = template.queryForMap(sql);
		return rstMap;
	}

	@Override
	public BigDecimal getUserCsbSum(String userId) {
		String sql = String.format("select csb_sum from %s \n", TableNameUtil.TB_USER);
		sql += String.format("WHERE id = %s \n", userId);
		BigDecimal csbSum = template.queryForObject(sql, BigDecimal.class);
		return csbSum;
	}

	@Override
	public long getMaxGuaDanId() {
        String sql = String.format("select max(odd_id) from %s \n", TableNameUtil.LB_CSB_GUADAN);
        long max = template.queryForObject(sql,Long.class);
        return max;
	}

	@Override
	public boolean addGuaDan(CSBGuaDanVO vo) {
		InsertUtil insUtil = new InsertUtil(TableNameUtil.LB_CSB_GUADAN);
		insUtil.setColumn("id", vo.getId());
		insUtil.setColumn("odd_id", vo.getOddId());
		insUtil.setColumn("buy_id", vo.getBuyId());
		insUtil.setColumn("sell_id", vo.getSellId());
		insUtil.setColumn("count", vo.getCount());
		insUtil.setColumn("price", vo.getPrice());
		insUtil.setColumn("price_sum", vo.getPriceSum());
		insUtil.setColumn("status", vo.getStatus());
		insUtil.setColumn("create_time", vo.getCreateTime().toString());
		
		String sql = insUtil.getInsertSql();
		int count = template.update(sql);
		return count > 0;
	}

	@Override
	public boolean updateGuaDan(CSBGuaDanVO vo) {
		InsertUtil insUtil = new InsertUtil(TableNameUtil.LB_CSB_GUADAN);
		insUtil.setColumn("status", vo.getStatus());
		insUtil.setColumn("update_time", vo.getUpdateTime().toString());
		
		String sql = insUtil.getUpdateSql();
		sql += " where odd_id = ? \n";
		int count = template.update(sql, vo.getOddId());
		return count > 0;
	}

	@Override
	public CSBGuaDanVO getGuaDanByOddId(String oddId) {
		String sql = String.format("SELECT * from %s \n", TableNameUtil.LB_CSB_GUADAN);
		sql += "WHERE odd_id = ?";
		SqlRowSet rs = template.queryForRowSet(sql, oddId);
		CSBGuaDanVO vo = null;
		if(rs != null && rs.next()){
            vo = createGuadanVO(rs);
        }
        return vo;
	}

	private CSBGuaDanVO createGuadanVO(SqlRowSet rs){
		CSBGuaDanVO vo = new CSBGuaDanVO();
        vo.setId(rs.getString("id"));
        vo.setOddId(rs.getString("odd_no"));
        vo.setBuyId(rs.getString("buy_id"));
        vo.setSellId(rs.getString("sell_id"));
        vo.setCount(rs.getDouble("count"));
        vo.setPrice(rs.getDouble("price"));
        vo.setPriceSum(rs.getDouble("price_sum"));
        vo.setStatus(rs.getInt("status"));
        vo.setCreateTime(rs.getTimestamp("create_time"));
        vo.setUpdateTime(rs.getTimestamp("update_time"));
        return vo;
    }

	@Override
	public CSBGuaDanVO getGuaDanByOddIdAndStau(String oddId, Integer status) {
		String sql = String.format("SELECT * from %s \n", TableNameUtil.LB_CSB_GUADAN);
		sql += "WHERE odd_id = ?";
		sql += "AND status = ?";
		SqlRowSet rs = template.queryForRowSet(sql, oddId, status);
		CSBGuaDanVO vo = null;
		if(rs != null && rs.next()){
            vo = createGuadanVO(rs);
        }
        return vo;
	}

	@Override
	public CaiShenBiVO getCaiShenBiById(String id) {
		String sql = String.format("SELECT * from %s \n", TableNameUtil.LB_CAISHENGBI);
		sql += "WHERE id = ?";
		SqlRowSet rs = template.queryForRowSet(sql, id);
		CaiShenBiVO vo = null;
		if(rs != null && rs.next()) {
			vo = createCaiShenBiRealInfo(rs);
		}
		return vo;
	}
}
