package com.lianbao.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.alibaba.druid.util.StringUtils;
import com.lianbao.dao.PrizeDao;
import com.lianbao.vo.PrizeVO;
import com.vrc.util.InsertUtil;
import com.vrc.util.TableNameUtil;
import com.vrc.vo.common.PageInfo;

/**
 * @ClassName
 * @Description
 * @author
 * @date 2018年6月11日 下午2:11:28
 */
@Repository("prizeDao")
public class PrizeDaoImpl implements PrizeDao {

	@Resource
	private JdbcTemplate template;

	@Override
	public boolean savePrize(PrizeVO vo) {
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO \n");
		sql.append(String.format("%s(id, prize_no, prize_name, prize_count, prize_sum, prize_prob, begin_time, end_time, create_time) \n", TableNameUtil.LB_PRIZE));
		sql.append("VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?) \n");

		int count = template.update(sql.toString(), vo.getId(), vo.getPrizeNo(), vo.getPrizeName(), vo.getPrizeCount(),
				vo.getPrizeSum(), vo.getPrizeProb(), vo.getBeginTime(), vo.getEndTime(), vo.getCreateTime());
		return count > 0;
	}

	@Override
	public long getMaxId() {
		String sql = "select max(id) from lb_prize";
		Long max = template.queryForObject(sql, Long.class);
		if(max == null) {
			return 0L;
		}
		return max.longValue();
	}

	@Override
	public List<PrizeVO> getPrizesPage(PageInfo page, String keyword) {
		List<PrizeVO> results = new ArrayList<PrizeVO>();
		
		String whereSql = "";
		if(!StringUtils.isEmpty(keyword)) {
			whereSql = String.format("WHERE id like '%%%s%%' \n", keyword);
			whereSql += String.format("OR prize_no like '%%%s%%' \n", keyword);
			whereSql += String.format("OR prize_name like '%%%s%%' \n", keyword);
		}
		
		String sql = "";
		sql += String.format("SELECT * FROM %s \n", TableNameUtil.LB_PRIZE);
		sql += whereSql;
		sql += "ORDER BY id \n";
		
		page.pageTotal(sql, template);
		sql = page.decorateSql(sql);
		SqlRowSet rs = template.queryForRowSet(sql);
		if(rs != null) {
			while(rs.next()) {
				PrizeVO vo = createPrizeRealInfo(rs);
				results.add(vo);
			}
		}
		
		return results;
	}
	
	/**
	 * 获取奖品真是信息
	 * @param rs
	 * @return
	 */
	private PrizeVO createPrizeRealInfo(SqlRowSet rs) {
		PrizeVO vo = new PrizeVO();
		vo.setId(rs.getString("id"));
		vo.setPrizeNo(rs.getInt("prize_no"));
		vo.setPrizeName(rs.getString("prize_name"));
		vo.setPrizeCount(rs.getInt("prize_count"));
		vo.setPrizeSum(rs.getInt("prize_sum"));
		vo.setPrizeProb(rs.getFloat("prize_prob"));
		vo.setBeginTime(rs.getTimestamp("begin_time"));
		vo.setEndTime(rs.getTimestamp("end_time"));
		vo.setCreateTime(rs.getTimestamp("create_time"));
		return vo;
	}

	@Override
	public boolean updatePrize(PrizeVO vo) {
		Date curDate = new Date();
		Timestamp curTime = new Timestamp(curDate.getTime());
		InsertUtil insUtil = new InsertUtil(TableNameUtil.LB_PRIZE);
		insUtil.setColumn("prize_no", vo.getPrizeNo());
		insUtil.setColumn("prize_name", vo.getPrizeName());
		insUtil.setColumn("prize_count", vo.getPrizeCount());
		insUtil.setColumn("prize_sum", vo.getPrizeSum());
		insUtil.setColumn("prize_prob", vo.getPrizeProb());
		insUtil.setColumn("begin_time", vo.getBeginTime().toString());
		insUtil.setColumn("end_time", vo.getEndTime().toString());
		insUtil.setColumn("update_time", curTime.toString());
		String sql = insUtil.getUpdateSql();
		sql = String.format("%s where id = ? \n", sql);
		int count = template.update(sql, vo.getId());
		return count > 0;
	}

	@Override
	public boolean deletePrize(String[] ids) {
		if(ids == null || ids.length == 0) {
			return false;
		}
		
		String whereSql = "";
		for(int i = 0; i < ids.length; i++) {
			if(i == 0) {
				whereSql += String.format("%s", ids[i]);
			} else {
				whereSql += String.format(", %s", ids[i]);
			}
		}
		
		String delSql = "";
		delSql += String.format("DELETE FROM %s \n", TableNameUtil.LB_PRIZE);
		delSql += String.format("WHERE id IN (%s) \n", whereSql);
		int count = template.update(delSql);
		return count > 0;
	}

	@Override
	public boolean judgePrizeDraw(Timestamp curTime) {
		if(curTime == null) {
			return false;
		}
		
		String sql = "";
		sql += String.format("SELECT COUNT(1) FROM %s \n", TableNameUtil.LB_PRIZE);
		sql += String.format("WHERE ('%s' BETWEEN begin_time AND end_time) \n", curTime);
		sql += "ORDER BY create_time DESC \n";
		sql += "LIMIT 1 \n";
		
		int count = template.queryForObject(sql, Integer.class);
		
		return count > 0;
	}

	@Override
	public Map<String, Object> isBingo(double random) {
		Map<String, Object> prizeMap = new HashMap<String, Object>();
		
		String sql = "";
		sql += String.format("SELECT id, prize_no, prize_name, prize_count, prize_sum FROM %s \n", TableNameUtil.LB_PRIZE);
		sql += "WHERE prize_prob >= " + random + "AND " + random + " > 0 \n";
	
		SqlRowSet rs = template.queryForRowSet(sql);
		if(rs != null) {
			while(rs.next()) {
				String prizeId = rs.getString("id");
				Integer prizeNo = rs.getInt("prize_no");
				String prizeName = rs.getString("prize_name");
				Integer prizeCount = rs.getInt("prize_count");
				Integer prizeSum = rs.getInt("prize_sum");
				prizeMap.put("prizeId", prizeId);
				prizeMap.put("prizeNo", prizeNo);
				prizeMap.put("prizeName", prizeName);
				prizeMap.put("prizeCount", prizeCount);
				prizeMap.put("prizeSum", prizeSum);
			}
		}
		return prizeMap;
	}

	@Override
	public boolean updateUserPrize(String prize, String userId, Integer bingoNum) {
		String sql = "";
		sql += String.format("UPDATE %s \n", TableNameUtil.TB_USER);
		sql += String.format("SET %s = IFNULL(%s, 0) + %d \n", prize, prize, bingoNum);
		sql += String.format("WHERE id = '%s' \n", userId);
		
		int count = template.update(sql);
		return count > 0;
	}

	@Override
	public boolean updatePrizeCount(String prizeId, Integer bingoNum) {
		String sql = "";
		sql += String.format("UPDATE %s \n", TableNameUtil.LB_PRIZE);
		sql += String.format("SET prize_sum = prize_sum - %d \n", bingoNum);
		sql += String.format("WHERE id = '%s' \n", prizeId);
		int count = template.update(sql);
		
		return count > 0;
	}
}
