package com.lianbao.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.alibaba.druid.util.StringUtils;
import com.lianbao.dao.TakeCashDao;
import com.lianbao.vo.TakeCashVO;
import com.vrc.common.Const;
import com.vrc.util.DateUtils;
import com.vrc.util.InsertUtil;
import com.vrc.util.TableNameUtil;
import com.vrc.vo.common.PageInfo;

/**
* @ClassName
* @Description
* @author 
* @date 2018年7月9日 上午9:38:33
*/
@Repository("takeCashDao")
public class TakeCashDaoImpl implements TakeCashDao {

	@Resource
	private JdbcTemplate template;
	
	@Override
	public List<TakeCashVO> getCashPage(PageInfo page, String keyword, String startTime, String endTime) {
		String sql = String.format("SELECT * FROM %s WHERE 1=1 \n", TableNameUtil.LB_TAKECASH);
		String whereSql = "";
		if(!StringUtils.isEmpty(keyword)) {
			whereSql += String.format(" and user_id LIKE '%%%s%%' \n", keyword);
		}
		if(!StringUtils.isEmpty(startTime)) {
			whereSql += String.format(" and create_time >= %s \n", startTime);
		}
		if(!StringUtils.isEmpty(endTime)) {
			whereSql += String.format(" and end_time <= %s \n", endTime);
		}
		sql += whereSql;
		sql += "ORDER BY create_time desc \n";
		page.pageTotal(sql, template);
		sql = page.decorateSql(sql);
		SqlRowSet rs = template.queryForRowSet(sql);

		List<TakeCashVO> list = new ArrayList<>();
		if(rs != null) {
			while(rs.next()) {
				TakeCashVO vo = createTakeCashVO(rs);
				list.add(vo);
			}
		}
		
		return list;
	}
	
	private TakeCashVO createTakeCashVO(SqlRowSet rs) {
		TakeCashVO vo = new TakeCashVO();
		vo.setId(rs.getString("id"));
		vo.setCash(rs.getDouble("cash"));
		vo.setStatus(rs.getInt("status"));
		vo.setUserId(rs.getString("user_id"));
		vo.setCreateTime(rs.getString("create_time"));
		vo.setUpdateTime(rs.getString("update_time"));
		vo.setRemitTime(rs.getString("remit_time"));
		return vo;
	}

	@Override
	public boolean saveOrUpdateTakeCash(TakeCashVO vo) {
		InsertUtil insUtil = new InsertUtil(TableNameUtil.LB_TAKECASH);
		insUtil.setColumn("user_id", vo.getUserId());
		insUtil.setColumn("cash", vo.getCash());
		insUtil.setColumn("status", vo.getStatus());
		int count = 0;
		if(!StringUtils.isEmpty(vo.getId())) {
			insUtil.setColumn("update_time", DateUtils.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss"));
			String sql = insUtil.getUpdateSql();
			sql = String.format("%s where id = ? \n", sql);
			count = template.update(sql, vo.getId());
		} else {
			insUtil.setColumn("create_time", DateUtils.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss"));
			long id = getMaxId();
			if(id < Const.takeCashIdStart.longValue()) {
				id = Const.takeCashIdStart;
			} else {
				id += 1;
			}
			insUtil.setColumn("id", String.valueOf(id));
			String sql = insUtil.getInsertSql();
			count = template.update(sql);
		}
		return count > 0;
	}

	@Override
	public TakeCashVO getCashById(String id) {
		String sql = String.format("SELECT * FROM %s \n", TableNameUtil.LB_TAKECASH);
		sql += String.format("WHERE id = %s \n", id);
		SqlRowSet rs = template.queryForRowSet(sql);
		if(rs != null && rs.next()) {
			TakeCashVO vo = createTakeCashVO(rs);
			return vo;
		}
		return null;
	}
	
	private long getMaxId() {
		String sql = String.format("select max(id) from %s \n", TableNameUtil.LB_TAKECASH);
		Long max = template.queryForObject(sql, Long.class);
		if(max == null) {
			return 0L;
		}
		return max.longValue();
	}
	
}
