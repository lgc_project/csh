package com.lianbao.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.lianbao.dao.CSBTradeDao;
import com.lianbao.vo.CSBTradeVO;
import com.vrc.util.InsertUtil;
import com.vrc.util.TableNameUtil;

/**
* @ClassName
* @Description
* @author 
* @date 2018年6月27日 上午11:46:30
*/
@Repository("csbTradeDao")
public class CSBTradeDaoImpl implements CSBTradeDao {

	@Resource
	private JdbcTemplate template;
	
	@Override
	public boolean addCSBTrade(CSBTradeVO vo) {
		InsertUtil insUtil = new InsertUtil(TableNameUtil.LB_CSB_TRADE);
		insUtil.setColumn("id", vo.getId());
//		insUtil.setColumn("odd_no", vo.getOddNo());
		insUtil.setColumn("buy_id", vo.getBuyId());
		insUtil.setColumn("trade_count", vo.getTradeCount());
		insUtil.setColumn("trade_unit_price", vo.getTradeUnitPrice());
		insUtil.setColumn("trade_money_sum", vo.getTradeMoneySum());
		insUtil.setColumn("create_time", vo.getCreateTime().toString());
		
		String sql = insUtil.getInsertSql();
		int count = template.update(sql);
		return count > 0;
	}

	@Override
	public List<Map<String, Object>> getWeekTradeInfo(Timestamp startTime, Timestamp endTime) {
		String sql = "";
		sql += "SELECT a.click_date, IFNULL(b.s,0) ms FROM (\r\n" + 
				"		SELECT curdate() as click_date\r\n" + 
				"    union all\r\n" + 
				"    SELECT date_sub(curdate(), interval 1 day) as click_date\r\n" + 
				"    union all\r\n" + 
				"    SELECT date_sub(curdate(), interval 2 day) as click_date\r\n" + 
				"    union all\r\n" + 
				"    SELECT date_sub(curdate(), interval 3 day) as click_date\r\n" + 
				"    union all\r\n" + 
				"    SELECT date_sub(curdate(), interval 4 day) as click_date\r\n" + 
				"    union all\r\n" + 
				"    SELECT date_sub(curdate(), interval 5 day) as click_date\r\n" + 
				"    union all\r\n" + 
				"    SELECT date_sub(curdate(), interval 6 day) as click_date\r\n" + 
				") a LEFT JOIN (\r\n" + 
				"		SELECT DATE(create_time) ct, SUM(trade_money_sum) s FROM lb_csb_trade GROUP BY ct\r\n" + 
				") b on a.click_date = b.ct order by a.click_date asc";
		SqlRowSet rs = template.queryForRowSet(sql);
		
		List<Map<String, Object>> list = new ArrayList<>();
		if(rs != null) {
			while(rs.next()) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("createTime", rs.getObject(1));
				map.put("tradeMoneySum", rs.getObject(2));
				list.add(map);
			}
		}
		return list;
	}

}
