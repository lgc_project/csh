package com.lianbao.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.lianbao.dao.AuctionDao;
import com.lianbao.vo.AuctionUserVO;
import com.lianbao.vo.AuctionVO;
import com.vrc.util.InsertUtil;
import com.vrc.util.TableNameUtil;
import com.vrc.vo.common.PageInfo;

/**
 * @ClassName
 * @Description
 * @author
 * @date 2018年6月18日 上午1:25:01
 */
@Repository("auctionDao")
public class AuctionDaoImpl implements AuctionDao {

	@Resource
	private JdbcTemplate template;

	@Override
	public List<AuctionVO> getAuctionsPage(PageInfo page, String keyword) {
		List<AuctionVO> results = new ArrayList<AuctionVO>();
		
		String whereSql = "";
		if(!StringUtils.isEmpty(keyword)) {
			whereSql += String.format("WHERE id LIKE '%%%s%%' \n", keyword);
			whereSql += String.format("OR auction_name LIKE '%%%s%%' \n", keyword);
		}
		
		String sql = "";
		sql += String.format("SELECT * FROM %s \n", TableNameUtil.LB_AUCTION);
		sql += whereSql;
		sql += "ORDER BY id ASC \n";
		
		page.pageTotal(sql, template);
		sql = page.decorateSql(sql);
		SqlRowSet rs = template.queryForRowSet(sql);
		if(rs != null) {
			while(rs.next()) {
				AuctionVO vo = createAuctionRealInfo(rs);
				results.add(vo);
			}
		}
		
		return results;
	
	}
	
	private AuctionVO createAuctionRealInfo(SqlRowSet rs) {
		AuctionVO vo = new AuctionVO();
		vo.setId(rs.getString("id"));
		vo.setAuctionName(rs.getString("auction_name"));
		vo.setAuctionMoney(rs.getBigDecimal("auction_money"));
		vo.setAuctionLowMoney(rs.getBigDecimal("auction_lowmoney"));
		vo.setAuctionPoundage(rs.getDouble("auction_poundage"));
		vo.setBeginTime(rs.getTimestamp("begin_time"));
		vo.setEndTime(rs.getTimestamp("end_time"));
		vo.setCreateTime(rs.getTimestamp("create_time"));
		vo.setUpdateTime(rs.getTimestamp("update_time"));
		return vo;
	}

	@Override
	public boolean saveAuction(AuctionVO vo) {
		String sql = "";
		sql += "INSERT INTO \n";
		sql += String.format(
				"%s(id, auction_name, auction_money, auction_lowmoney, auction_poundage, begin_time, end_time, create_time \n)",
				TableNameUtil.LB_AUCTION);
		sql += "VALUE (?, ?, ?, ?, ?, ?, ?, ?) \n";

		int count = template.update(sql, vo.getId(), vo.getAuctionName(), vo.getAuctionMoney(), vo.getAuctionLowMoney(),
				vo.getAuctionPoundage(), vo.getBeginTime(), vo.getEndTime(), vo.getCreateTime());
		return count > 0;
	}

	@Override
	public boolean updateAuction(AuctionVO vo) {
		Date curDate = new Date();
		Timestamp curTime = new Timestamp(curDate.getTime());
		InsertUtil insUtil = new InsertUtil(TableNameUtil.LB_AUCTION);
		insUtil.setColumn("auction_name", vo.getAuctionName());
		insUtil.setColumn("auction_money", vo.getAuctionMoney().toPlainString());
		insUtil.setColumn("auction_lowmoney", vo.getAuctionLowMoney().toPlainString());
		insUtil.setColumn("auction_poundage", vo.getAuctionPoundage());
		insUtil.setColumn("begin_time", vo.getBeginTime().toString());
		insUtil.setColumn("end_time", vo.getEndTime().toString());
		insUtil.setColumn("update_time", curTime.toString());
		String sql = insUtil.getUpdateSql();
		sql = String.format("%s where id = ? \n", sql);
		int count = template.update(sql, vo.getId());
		return count > 0;
	}

	@Override
	public boolean deleteAuction(String[] ids) {
		if(ids == null || ids.length == 0) {
			return false;
		}
		
		String whereSql = "";
		for(int i = 0; i < ids.length; i++) {
			if(i == 0) {
				whereSql += String.format("%s", ids[i]);
			} else {
				whereSql += String.format(", %s", ids[i]);
			}
		}
		
		String delSql = "";
		delSql += String.format("DELETE FROM %s \n", TableNameUtil.LB_AUCTION);
		delSql += String.format("WHERE id IN (%s) \n", whereSql);
		int count = template.update(delSql);
		return count > 0;
	}

	@Override
	public long getMaxId(String tableName) {
		String sql = String.format("select max(id) from %s", tableName);
		Long max = template.queryForObject(sql, Long.class);
		if(max == null) {
			return 0L;
		}
		return max.longValue();
	}

	@Override
	public Map<String, Object> judgeIsAuction(Timestamp curTime) {
		if(curTime == null) {
			return null;
		}
		
		
		Map<String, Object> timeMap = new HashMap<String, Object>();
		String sql = "";
		sql += "SELECT begin_time, end_time, id, auction_money, auction_lowmoney, auction_poundage \n";
		sql += String.format("FROM %s \n", TableNameUtil.LB_AUCTION);
		sql += "ORDER BY create_time DESC \n";
		sql += "LIMIT 1 \n";
		
		SqlRowSet rs = template.queryForRowSet(sql);
		if(rs != null) {
			while(rs.next()) {
				Timestamp beginTime = rs.getTimestamp("begin_time");
				Timestamp endTime = rs.getTimestamp("end_time");
				String auctionId = rs.getString("id");
				Double auctionMoney = rs.getDouble("auction_money");
				Double auctionLowmoney = rs.getDouble("auction_lowmoney");
				Double auctionPoundage = rs.getDouble("auction_poundage");
				
				timeMap.put("beginTime", beginTime);
				timeMap.put("endTime", endTime);
				timeMap.put("auctionId", auctionId);
				timeMap.put("auctionMoney", auctionMoney);
				timeMap.put("auctionLowmoney", auctionLowmoney);
				timeMap.put("auctionPoundage", auctionPoundage);
			}
		}
		return timeMap;
	}

	@Override
	public List<Map<String, Object>> getTop5AuctionInfo(String auctionId) {
		String sql = "";
		sql += "SELECT user_id, auction_amount, create_time \n";
		sql += String.format("FROM %s \n", TableNameUtil.LB_AUCTION_USER);
		sql += String.format("WHERE auction_id = %s \n", auctionId);
		sql += "ORDER BY auction_amount DESC \n";
		sql += "LIMIT 5";
		
		List<Map<String, Object>> rstMap = template.queryForList(sql);
		return rstMap;
	}

	@Override
	public boolean addAuctionInfo(AuctionUserVO vo) {
		Date curDate = new Date();
		Timestamp curTime = new Timestamp(curDate.getTime());
		InsertUtil insUtil = new InsertUtil(TableNameUtil.LB_AUCTION_USER);
		insUtil.setColumn("id", vo.getId());
		insUtil.setColumn("user_id", vo.getUserId());
		insUtil.setColumn("auction_id", vo.getAuctionId());
		insUtil.setColumn("auction_amount", vo.getAuctionAmount().toPlainString());
		insUtil.setColumn("create_time", curTime.toString());
		String sql = insUtil.getInsertSql();
		int count = template.update(sql);
		return count > 0;
	}

}
