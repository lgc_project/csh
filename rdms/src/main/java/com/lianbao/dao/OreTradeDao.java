package com.lianbao.dao;

import java.util.List;

import com.lianbao.vo.OreGuaDanVO;

/**
* @ClassName
* @Description
* @author 
* @date 2018年8月8日 下午10:50:31
*/
public interface OreTradeDao {

	/**
	 * 获取最大的挂单编号
	 * @return
	 */
	long getMaxGuaDanNo();
	
	/**
	 * 根据挂单编号和状态获取挂单
	 * 
	 * @param oddId
	 * @param status
	 * @return
	 */
	OreGuaDanVO getGuaDanByIdAndStau(String oddId, int status);
	
	/**
	 * 根据挂单编号获取挂单
	 * 
	 * @param oddId
	 * @return
	 */
	OreGuaDanVO getGuaDanById(String oddId);
	
	/**
	 * 新增一个矿石挂单记录
	 * 
	 * @param vo
	 * @return
	 */
	boolean addOreGuaDan(OreGuaDanVO vo);
	
	/**
	 * 更新挂单
	 * 
	 * @param vo
	 * @return
	 */
	boolean updateGuaDan(OreGuaDanVO vo);
	
	/**
	 * 获取矿石挂单列表
	 * 
	 * @param userId
	 * @param level
	 * @param type
	 * @return
	 */
	List<OreGuaDanVO> getOreGuaDanList(String userId, String level, int type);

	/**
	 * 获取信箱信息
	 * 
	 * @param userId
	 * @param level
	 * @param type
	 * @return
	 */
	List<OreGuaDanVO> getOreGuaDanByBuyOrSell(String userId, String level, int type);
}
