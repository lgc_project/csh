package com.lianbao.dao;

import com.lianbao.vo.CSBBillVO;

/**
* @ClassName 财神币账单操作
* @Description
* @author LGC
* @date 2018年6月22日 下午10:08:54
*/
public interface CSBBillDao {

	/**
	 * 添加一个账单
	 * 
	 * @param bill
	 * @return
	 */
	public boolean addCSBBill(CSBBillVO bill);
	
}
