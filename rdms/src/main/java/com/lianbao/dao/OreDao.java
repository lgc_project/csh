package com.lianbao.dao;

import java.sql.Timestamp;
import java.util.List;

import com.lianbao.vo.OreVO;
import com.vrc.vo.common.PageInfo;

/**
* @ClassName
* @Description
* @author 
* @date 2018年7月23日 上午10:32:52
*/
public interface OreDao {

	List<OreVO> getOrePage(PageInfo page, String keyword, Timestamp startTime, Timestamp endTime);
	
	boolean saveOre(OreVO vo);
	
	Long getMaxId();
}
