package com.lianbao.dao;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.lianbao.vo.CSBTradeVO;

/**
* @ClassName
* @Description
* @author 
* @date 2018年6月27日 上午11:44:49
*/
public interface CSBTradeDao {

	/**
	 * 增加财神币交易记录
	 * 
	 * @param vo
	 * @return
	 */
	public boolean addCSBTrade(CSBTradeVO vo);
	
	/**
	 * 获取一周的交易记录
	 * 
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public List<Map<String, Object>> getWeekTradeInfo(Timestamp startTime, Timestamp endTime);
}
