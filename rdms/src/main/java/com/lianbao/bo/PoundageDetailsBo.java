package com.lianbao.bo;

import com.vrc.vo.common.PageInfo;

/**
* @ClassName
* @Description
* @author 
* @date 2018年8月23日 上午3:03:10
*/
public interface PoundageDetailsBo {

	PageInfo getPoundageDetailsPage(PageInfo page, String keyword, String startTime, String endTime, String type);
	
	boolean deletePd(String ids, String type);
}
