package com.lianbao.bo;

import java.math.BigDecimal;

import com.lianbao.vo.AuctionVO;
import com.vrc.vo.common.PageInfo;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
* @ClassName
* @Description
* @author 
* @date 2018年6月18日 上午1:59:42
*/
public interface AuctionBo {

	/**
	 * 获取竞拍活动列表
	 * 
	 * @param page
	 * @param keyword
	 * @return
	 */
	public PageInfo getAuctionsPage(PageInfo page, String keyword);
	
	/**
	 * 添加竞拍活动
	 * 
	 * @param vo
	 * @return
	 */
	public boolean saveAuction(AuctionVO vo);

	/**
	 * 修改竞拍活动
	 * 
	 * @param vo
	 * @return
	 */
	public boolean updateAuction(AuctionVO vo);

	/**
	 * 批量删除竞拍活动
	 * 
	 * @param ids
	 * @return
	 */
	public boolean deleteAuction(String ids);
	
	/**
	 * 判断是否为竞拍活动时间
	 * 
	 * @return
	 */
	public JSONObject judgeIsAuction();
	
	/**
	 * 获取竞拍前5名
	 * 
	 * @return
	 */
	public JSONArray getTop5AuctionInfo(String auctionId);
	
	/**
	 * 保存竞拍活动信息
	 * 
	 * @param userId
	 * @param auctionId
	 * @param auctionAmount
	 * @return
	 */
	public boolean addAuctionInfo(String userId, String auctionId, BigDecimal auctionAmount);
}
