/**
 * 
 */
package com.lianbao.bo;

import java.util.Date;

import com.lianbao.vo.CreditVO;
import com.vrc.vo.common.PageInfo;

/**
* @ClassName
* @Description
* @author 
* @date 2018年8月20日 下午4:26:00
*/
public interface CreditBo {
	boolean addCreditVO(String userId, double payMoney, String outTradeNo, Date curDate);
	
	boolean updateCreditVO(CreditVO vo);
	
	/**
	 * 根据订单号查找充值记录
	 * 
	 * @param outTradeNo
	 * @return
	 */
	CreditVO getCreditVO(String outTradeNo);
	
	long getMaxCreditNo();
	
	PageInfo getCreditPage(PageInfo page, String keyword, String startTime, String endTime);
}
