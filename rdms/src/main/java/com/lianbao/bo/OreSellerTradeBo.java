package com.lianbao.bo;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
* @ClassName
* @Description
* @author 
* @date 2018年8月14日 下午6:09:25
*/
/**
 * @author admin
 *
 */
/**
 * @author admin
 *
 */
public interface OreSellerTradeBo {

	/**
	 * 卖家挂单
	 * 
	 * @param userId
	 * @param count
	 * @param unitPrice
	 * @param level
	 * @return
	 */
	JSONObject orePostOdd(String userId, double count, double unitPrice, String level);

	/**
	 * 买家购买
	 * 
	 * @param userId
	 * @param oddId
	 * @return
	 */
	JSONObject buy(String userId, String oddId);

	/**
	 * 卖家第二次确认，交易成功
	 * 
	 * @param userId
	 * @param oddId
	 * @return
	 */
	JSONObject sureOreTrade(String userId, String oddId);

	/**
	 * 撤销挂单
	 * 
	 * @param oddId
	 * @return
	 */
	JSONObject cancelOdd(String oddId);
	
	
	/**
	 * 查看信箱
	 * 
	 * @param userId
	 * @param level
	 * @return
	 */
	JSONArray oreMailBox(String userId, String level);
	
	/**
	 * 查看挂单列表
	 * 
	 * @param userId
	 * @param level
	 * @return
	 */
	JSONArray getOreGuaDanList(String userId, String level);
}
