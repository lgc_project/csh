/**
 * 
 */
package com.lianbao.bo;

import com.lianbao.vo.PrizeVO;
import com.vrc.vo.common.PageInfo;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * @ClassName
 * @Description
 * @author
 * @date 2018年6月11日 下午2:45:04
 */
public interface PrizeBo {

	public PageInfo getPrizesPage(PageInfo page, String keyword);

	/**
	 * 手动添加奖品信息
	 * 
	 * @param vo
	 * @return
	 */
	public boolean savePrize(PrizeVO vo);

	/**
	 * 修改奖品信息
	 * 
	 * @param vo
	 * @return
	 */
	public boolean updatePrize(PrizeVO vo);

	/**
	 * 批量删除奖品
	 * 
	 * @param ids
	 * @return
	 */
	public boolean deletePrize(String ids);

	/**
	 * 判断当前时间是否可以抽奖
	 * 
	 * @return
	 */
	public boolean judgePrizeDraw();
	
	/**
	 * 抽奖
	 * 
	 * @return
	 */
	public JSONObject prizeDraw(String userId);
}
