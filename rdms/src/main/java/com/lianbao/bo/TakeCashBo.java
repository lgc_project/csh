package com.lianbao.bo;

import com.vrc.vo.common.PageInfo;

import net.sf.json.JSONObject;

/**
* @ClassName
* @Description
* @author 
* @date 2018年7月9日 上午9:47:14
*/
public interface TakeCashBo {
	
	public PageInfo getCashPage(PageInfo page, String keyword, String startTime, String endTime);

	public JSONObject takeCash(String userId, Double cash);
	
	public JSONObject sureSubmit(String id, int status);
}
