package com.lianbao.bo.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lianbao.bo.OreTradeBo;
import com.lianbao.dao.OreTradeDao;
import com.lianbao.dao.PoundageDetailsDao;
import com.lianbao.vo.OreGuaDanVO;
import com.lianbao.vo.PoundageDetailsVO;
import com.vrc.common.Const;
import com.vrc.dao.sys.UserDAO;
import com.vrc.dao.sys.VrcConfigDAO;
import com.vrc.dao.trade.BillDAO;
import com.vrc.dao.trade.TradeDAO;
import com.vrc.util.DateUtils;
import com.vrc.util.KeyUtil;
import com.vrc.util.StringUtils;
import com.vrc.vo.sys.UserVO;
import com.vrc.vo.trade.BillVO;
import com.vrc.vo.trade.TradeVO;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
* @ClassName
* @Description
* @author 
* @date 2018年8月8日 下午11:13:50
*/
@Service("oreTradeBo")
@Transactional
public class OreTradeBoImpl implements OreTradeBo {
	
	@Resource
	private OreTradeDao oreTradeDao;
	@Resource
	private UserDAO userDao;
	@Resource
	private VrcConfigDAO configDao;
	@Resource
	private TradeDAO tradeDao;
	@Resource
	private BillDAO billDao;
	@Resource
	private PoundageDetailsDao poundageDetailsDao;
	
	@Override
	public boolean orePostOdd(String userId, double count, double unitPrice, String level) {
		if(userId == null) {
			return false;
		}
		
		OreGuaDanVO guaDan = new OreGuaDanVO();
		long maxGuaDanNo = oreTradeDao.getMaxGuaDanNo();
		if(maxGuaDanNo < 1800000L) {
			maxGuaDanNo = 1800000L;
		} else {
			maxGuaDanNo += 1L;
		}
		
		guaDan.setId(KeyUtil.getKey());
		guaDan.setOddId(String.valueOf(maxGuaDanNo));
		guaDan.setBuyId(userId);
		guaDan.setCount(count);
		guaDan.setUnitPrice(unitPrice);
		guaDan.setPriceSum(count * unitPrice);
		guaDan.setStatus(Const.oreOddPost);
		guaDan.setIsValid(1);
		guaDan.setLevel(level);
		guaDan.setGuaDanTime(DateUtils.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss"));
		guaDan.setOperateTime(DateUtils.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss"));
		guaDan.setType(0);
		boolean flag = oreTradeDao.addOreGuaDan(guaDan);
		return flag;
	}

	@Override
	public JSONObject sellFirstSure(String userId, String oddId) {
		JSONObject jo = new JSONObject();
		boolean flag = false;
		Date date = new Date();
		// 1.判断改单是否需要确认，只有状态为0的单才能被确认
		OreGuaDanVO guaDan = oreTradeDao.getGuaDanByIdAndStau(oddId, Const.oreOddPost);
		if(guaDan == null) {
			jo.put("code", "203");
			jo.put("codeDesc", "改单已经确认");
			return jo;
		}
		
		// 2.判断卖家资金是否足够
		UserVO sell = userDao.getUserById(userId);
		double guaDanOreCount = guaDan.getCount(); // 挂单中的矿石
		
		double oreSum = sell.getOreSum(); // 卖家的矿石余额
		double oreFreeze = sell.getOreSumFreeze(); // 卖家冻结的矿石
		if(oreSum - oreFreeze < guaDanOreCount) {
			jo.put("code", "202");
			jo.put("codeDesc", "矿石余额不足");
			return jo;
		}
		
		// 3.冻结卖家矿石
		sell.setOreSum(oreSum);
		sell.setOreSumFreeze(oreFreeze + guaDanOreCount);
		flag = userDao.updateUser(sell);
		
		// 写入账单表（类型：冻结，子类型：冻结，账单类型：矿石）
		BillVO bill = new BillVO();
		bill.setId(KeyUtil.getKey());
		bill.setType(Const.billType_freeze);
		bill.setActionType(Const.actionType_freeze);
		bill.setUserId(userId);
		bill.setPrice(guaDanOreCount);
		bill.setTime(DateUtils.dateToString(date));
		bill.setBillType("0");
		billDao.addBill(bill);
		
		// 4.在挂单中填入卖家，修改挂单状态为1 （卖家第一次确认）
		if(flag) {
			OreGuaDanVO vo = oreTradeDao.getGuaDanById(oddId);
			vo.setSellId(userId);
			vo.setStatus(Const.oreOddSellSure);
			vo.setOperateTime(DateUtils.dateToString(date, "yyyy-MM-dd HH:mm:ss"));
			flag = oreTradeDao.updateGuaDan(vo);
		}
		
		if(flag) {
			jo.put("code", Const.success);
			jo.put("codeDesc", "操作成功");
		} else {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
		}
		return jo;
	}

	@Override
	public JSONObject sureOreTrade(String userId, String oddId) {
		JSONObject jo = new JSONObject();
		if(StringUtils.isEmpty(userId) || StringUtils.isEmpty(oddId)) {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
			return jo;
		}
		
		boolean flag = false;
		OreGuaDanVO guaDan = oreTradeDao.getGuaDanById(oddId);
		// 如果为买家，则只需要修改挂单状态为2
		if(userId.equalsIgnoreCase(guaDan.getBuyId()) && guaDan.getStatus() == Const.oreOddSellSure) {
			flag = buySure(guaDan);
		}
		
		// 如果为卖家，则修改挂单状态为3，卖家解冻，转入百源币，买家转入矿石，扣除百源币和手续费
		if(userId.equalsIgnoreCase(guaDan.getSellId()) && guaDan.getStatus() == Const.oreOddBuySure) {
			Date date = new Date();
			flag = sellSecondSure(guaDan, date);
			
			// 写入到矿石交易简易表
			if(flag) {
				TradeVO trade = new TradeVO();
				trade.setId(KeyUtil.getKey());
				trade.setBuyId(guaDan.getBuyId());
				trade.setSellId(guaDan.getSellId());
				trade.setOddNo(guaDan.getOddId());
				trade.setPrice(guaDan.getUnitPrice());
				trade.setTradeCount(guaDan.getCount());
				trade.setTradeMoney(guaDan.getPriceSum());
				trade.setTradeTime(DateUtils.dateToString(date));
				tradeDao.addTrade(trade);
				
				// 修改简易交易表中矿石的最高价和数量
				tradeDao.updateTradeSimpleCount(DateUtils.dateToString(date, "yyyy-MM-dd"), guaDan.getCount(), guaDan.getUnitPrice());
			}
		}
		
		if(flag) {
			jo.put("code", Const.success);
			jo.put("codeDesc", "操作成功");
		} else {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
		}
		
		return jo;
	}
	
	private boolean buySure(OreGuaDanVO guaDan) {
		guaDan.setStatus(Const.oddBuySure);
		guaDan.setOperateTime(DateUtils.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss"));
		boolean flag = oreTradeDao.updateGuaDan(guaDan);
		return flag;
	}

	private boolean sellSecondSure(OreGuaDanVO guaDan, Date date) {
		boolean flag = false;
		// 1.修改挂单状态为3
		guaDan.setStatus(Const.oreOddSuccess);
		guaDan.setOperateTime(DateUtils.dateToString(date, "yyyy-MM-dd HH:mm:ss"));
		flag = oreTradeDao.updateGuaDan(guaDan);
		
		if(flag) {
			double guaDanOreCount = guaDan.getCount(); // 挂了单的矿石数
			
			double orePoundage = 0.0; // 矿石交易手续费
			try {
				String str = configDao.getConfigByKey("orePoundage");
				orePoundage = Double.valueOf(str);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			double priceSum = guaDan.getPriceSum(); // 挂了单的交易总金额
			double pricePd = priceSum * orePoundage; // 手续费
			
			// 2.卖家增加金额
			String sellId = guaDan.getSellId();
			UserVO seller = userDao.getUserById(sellId);
			seller.setMoneySum(seller.getMoneySum() + priceSum - pricePd);
			seller.setOreSum(seller.getOreSum() - guaDanOreCount); // 卖家解冻
			seller.setOreSumFreeze(seller.getOreSumFreeze() - guaDanOreCount);
			flag = userDao.updateUser(seller);
			// 写入账单表（类型：转入，子类型：转入，账单类型：百源币）
			BillVO bill = new BillVO();
			bill.setId(KeyUtil.getKey());
			bill.setType(Const.billType_roolin);
			bill.setActionType(Const.actionType_roolin);
			bill.setUserId(sellId);
			bill.setPrice(priceSum);
			bill.setTime(DateUtils.dateToString(date));
			bill.setBillType("2");
			billDao.addBill(bill);
			
			// 写入账单表（类型：转出，子类型：手续费，账单类型：百源币）
			bill.setId(KeyUtil.getKey());
			bill.setType(Const.billType_roolout);
			bill.setActionType(Const.actionType_fee);
			bill.setUserId(sellId);
			bill.setPrice(pricePd);
			bill.setTime(DateUtils.dateToString(date));
			bill.setBillType("2");
			billDao.addBill(bill);
			
			// 记录矿石扣手续费记录
			PoundageDetailsVO pd = new PoundageDetailsVO();
			pd.setId(KeyUtil.getKey());
			pd.setType("0");
			pd.setTradePrice(priceSum);
			pd.setTradePoundage(orePoundage);
			pd.setPdPrice(pricePd);
			pd.setUserId(sellId);
			pd.setCreateTime(DateUtils.dateToString(date, "yyyy-MM-dd HH:mm:ss"));
			pd.setGuadanType("0");
			poundageDetailsDao.addPoundageDetails(pd);
			
			// 写入账单表（类型：冻结，子类型：解冻，账单类型：矿石）
			bill.setId(KeyUtil.getKey());
			bill.setType(Const.billType_freeze);
			bill.setActionType(Const.actionType_refreeze);
			bill.setUserId(sellId);
			bill.setPrice(guaDanOreCount);
			bill.setTime(DateUtils.dateToString(date));
			bill.setBillType("0");
			billDao.addBill(bill);
			
			// 写入账单表（类型：转出，子类型：转出，账单类型：矿石）
			bill.setId(KeyUtil.getKey());
			bill.setType(Const.billType_roolout);
			bill.setActionType(Const.actionType_roolout);
			bill.setUserId(sellId);
			bill.setPrice(guaDanOreCount);
			bill.setTime(DateUtils.dateToString(date));
			bill.setBillType("0");
			billDao.addBill(bill);
			
			if(flag) {
				// 3.转入买家账户
				String buyId = guaDan.getBuyId();
				UserVO buyer = userDao.getUserById(buyId);
				buyer.setOreSum(buyer.getOreSum() + guaDanOreCount);
				buyer.setMoneySum(buyer.getMoneySum() - priceSum);
				flag = userDao.updateUser(buyer);
				
				// 写入账单表（类型：转入，子类型：转入，账单类型：矿石）
				bill.setId(KeyUtil.getKey());
				bill.setType(Const.billType_roolin);
				bill.setActionType(Const.actionType_roolin);
				bill.setUserId(buyId);
				bill.setPrice(guaDanOreCount);
				bill.setTime(DateUtils.dateToString(date));
				bill.setBillType("0");
				billDao.addBill(bill);
				
				// 写入账单表（类型：转出，子类型：转出，账单类型：百源币）
				bill.setId(KeyUtil.getKey());
				bill.setType(Const.billType_roolout);
				bill.setActionType(Const.actionType_roolout);
				bill.setUserId(buyId);
				bill.setPrice(priceSum);
				bill.setTime(DateUtils.dateToString(date));
				bill.setBillType("2");
				billDao.addBill(bill);
			}
		}
		
		return flag;
	}

	@Override
	public JSONObject cancelOdd(String oddId) {
		JSONObject jo = new JSONObject();
		boolean flag = false;
		
		OreGuaDanVO guaDan = oreTradeDao.getGuaDanByIdAndStau(oddId, Const.oreOddPost);
		if(guaDan == null) {
			jo.put("code", "202");
			jo.put("codeDesc", "撤销失败，该单已有卖家确认");
			return jo;
		}
		
		guaDan.setStatus(Const.oreOddCancel);
		guaDan.setOperateTime(DateUtils.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss"));
		flag = oreTradeDao.updateGuaDan(guaDan);
		
		if(flag) {
			jo.put("code", Const.success);
			jo.put("codeDesc", "撤销成功");
		} else {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
		}		
		return jo;
	}

	@Override
	public JSONObject orePointSell(String buyId, String sellId, double count, double price) {
		JSONObject jo = new JSONObject();
		boolean flag = false;
		Date date = new Date();
		// 1.判断卖家矿石是否足够
		UserVO seller = userDao.getUserById(sellId);
		double oreSum = seller.getOreSum();
		double oreFreeze = seller.getOreSumFreeze();
		
		double oddOreCount = count; // 交易的矿石数量
		
		if(oreSum - oreFreeze < oddOreCount) {
			jo.put("code", "202");
			jo.put("codeDesc", "矿石余额不足");
			return jo;
		}
		
		// 2.判断买家是否存在
		UserVO buyer = userDao.getUserById(buyId);
		if(buyer == null) {
			jo.put("code", "204");
			jo.put("codeDesc", "买家不存在");
			return jo;
		}
		
		// 3.冻结卖家矿石
		seller.setOreSum(oreSum);
		seller.setOreSumFreeze(oreFreeze + oddOreCount);
		flag = userDao.updateUser(seller);
		
		// 写入账单表（类型：冻结，子类型：冻结，账单类型：矿石）
		BillVO bill = new BillVO();
		bill.setId(KeyUtil.getKey());
		bill.setType(Const.billType_freeze);
		bill.setActionType(Const.actionType_freeze);
		bill.setUserId(sellId);
		bill.setPrice(oddOreCount);
		bill.setTime(DateUtils.dateToString(date));
		bill.setBillType("0");
		billDao.addBill(bill);
		
		// 4.创建一个挂单，并设置状态为1
		if(flag) {
			OreGuaDanVO vo = new OreGuaDanVO();
			// 获取最大的id
			long maxOddNo = oreTradeDao.getMaxGuaDanNo();
			if(maxOddNo < 1800000L) {
				maxOddNo = 1800000L;
			} else {
				maxOddNo += 1L;
			}
			vo.setId(KeyUtil.getKey());
			vo.setOddId(String.valueOf(maxOddNo));
			vo.setBuyId(buyId);
			vo.setSellId(sellId);
			vo.setUnitPrice(price);
			vo.setPriceSum(price * count);
			vo.setCount(oddOreCount);
			vo.setStatus(Const.oreOddSellSure);
			vo.setIsValid(1);
			vo.setGuaDanTime(DateUtils.dateToString(date, "yyyy-MM-dd HH:mm:ss"));
			vo.setOperateTime(DateUtils.dateToString(date, "yyyy-MM-dd HH:mm:ss"));
			vo.setType(0);
			flag = oreTradeDao.addOreGuaDan(vo);
		}
		
		if(flag) {
			jo.put("code", Const.success);
			jo.put("codeDesc", "操作成功");
		} else {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
		}
		
		return jo;
	}

	@Override
	public JSONArray oreMailBox(String userId, String level) {
		JSONArray ja = new JSONArray();
		if(StringUtils.isEmpty(userId)) {
			return ja;
		}
		
		List<OreGuaDanVO> list = oreTradeDao.getOreGuaDanByBuyOrSell(userId, level, 0);
		if(list != null) {
			for(OreGuaDanVO vo : list) {
				String buyId = vo.getBuyId();
				String sellId = vo.getSellId();
				int status = vo.getStatus();
				
				// 交易状态为0，且是买家，提示 您挂单买入XX矿石，单价XX，总价XXX（撤单）
				if(status == 0 && userId.equals(buyId)) {
					JSONObject jo = new JSONObject();
					jo.put("oddId", StringUtils.nullToEmpty(vo.getOddId()));
					jo.put("time", StringUtils.nullToEmpty(vo.getOperateTime()));
					jo.put("opponentId", StringUtils.nullToEmpty(sellId));
					
					double count = vo.getCount();
					double unitPrice = vo.getUnitPrice();
					double priceSum = vo.getPriceSum();
					String content = "您挂单买入" + count + "矿石，单价"+unitPrice+"，总价"+priceSum;
					jo.put("content", StringUtils.nullToEmpty(content));
					jo.put("cancel", "1");
					jo.put("account", "0");
					jo.put("sure", "0");
					ja.add(jo);
				}
				
				// 交易状态为1，且是买家，提示 收到XXX转入的XX矿石，请确认接收（交易账号，确认交易）
				if(status == 1 && userId.equals(buyId)) {
					JSONObject jo = new JSONObject();
					jo.put("oddId", StringUtils.nullToEmpty(vo.getOddId()));
					jo.put("time", StringUtils.nullToEmpty(vo.getOperateTime()));
					jo.put("opponentId", StringUtils.nullToEmpty(sellId));
					
					double count = vo.getCount();
					String content = "收到"+sellId+"转入"+count+"矿石，请确认接收";
					jo.put("content", StringUtils.nullToEmpty(content));
					jo.put("cancel", "0");
					jo.put("account", "1");
					jo.put("sure", "1");
					ja.add(jo);
				}
				
				// 交易状态为1，且是卖家，提示 转给XXX20矿石，等待对方确认（交易账号）
				if(status == 1 && userId.equals(sellId)) {
					JSONObject jo = new JSONObject();
					jo.put("oddId", StringUtils.nullToEmpty(vo.getOddId()));
					jo.put("time", StringUtils.nullToEmpty(vo.getOperateTime()));
					jo.put("opponentId", StringUtils.nullToEmpty(buyId));
					
					double count = vo.getCount();
					String content = "转给"+buyId+"用户"+count+"矿石，等待对方确认";
					jo.put("content", StringUtils.nullToEmpty(content));
					jo.put("cancel", "0");
					jo.put("account", "1");
					jo.put("sure", "0");
					ja.add(jo);
				}
				
				// 交易状态为2，且是买家，提示 等待卖家二次确认，卖家二次确认后即可收到矿石（交易账号）
				if(status == 2 && userId.equals(buyId)) {
					JSONObject jo = new JSONObject();
					jo.put("oddId", StringUtils.nullToEmpty(vo.getOddId()));
					jo.put("time", StringUtils.nullToEmpty(vo.getOperateTime()));
					jo.put("opponentId", StringUtils.nullToEmpty(sellId));
					
					String content = "等待卖家二次确认，卖家二次确认后即可收到矿石";
					jo.put("content", StringUtils.nullToEmpty(content));
					jo.put("cancel", "0");
					jo.put("account", "1");
					jo.put("sure", "0");
					ja.add(jo);
				}
				
				// 交易状态为2，且是卖家，提示 转给XXX矿石，等待您的二次确认（确认交易，交易账号）
				if(status == 2 && userId.equals(sellId)) {
					JSONObject jo = new JSONObject();
					jo.put("oddId", StringUtils.nullToEmpty(vo.getOddId()));
					jo.put("time", StringUtils.nullToEmpty(vo.getOperateTime()));
					jo.put("opponentId", StringUtils.nullToEmpty(buyId));
					
					double count = vo.getCount();
					String content = "转给"+buyId+"用户"+count+"矿石，等待您的二次确认";
					jo.put("content", StringUtils.nullToEmpty(content));
					jo.put("button", "account,sure");
					jo.put("cancel", "0");
					jo.put("account", "1");
					jo.put("sure", "1");
					ja.add(jo);
				}
				
			}
		}
		
		return ja;
	}

	@Override
	public JSONArray getOreGuaDanList(String userId, String level) {
		JSONArray ja = new JSONArray();
		List<OreGuaDanVO> list = oreTradeDao.getOreGuaDanList(userId, level, 0);
		if(list != null) {
			for(OreGuaDanVO vo : list) {
				JSONObject jo = new JSONObject();
				jo.put("oddId", StringUtils.nullToEmpty(vo.getOddId()));
				jo.put("time", StringUtils.nullToEmpty(vo.getOperateTime()));
				jo.put("buyId", StringUtils.nullToEmpty(vo.getBuyId()));
				jo.put("count", vo.getCount());
				jo.put("price", vo.getUnitPrice());
				jo.put("priceSum", vo.getPriceSum());
				ja.add(jo);
			}
		}
		
		return ja;
	}

	@Override
	public JSONObject getOrePoundageMsg() {
		String tips = configDao.getConfigByKey("orepoundagetip");
		JSONObject jo = new JSONObject();
		jo.put("tips", tips);
		return jo;
	}
}
