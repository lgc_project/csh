package com.lianbao.bo.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lianbao.bo.PoundageDetailsBo;
import com.lianbao.dao.PoundageDetailsDao;
import com.lianbao.vo.PoundageDetailsVO;
import com.vrc.vo.common.PageInfo;

/**
* @ClassName
* @Description
* @author 
* @date 2018年8月23日 上午3:05:25
*/
@Service("poundageDetailsBo")
@Transactional
public class PoundageDetailsBoImpl implements PoundageDetailsBo {

	@Resource
	private PoundageDetailsDao poundageDetailsDao;

	@Override
	public PageInfo getPoundageDetailsPage(PageInfo page, String keyword, String startTime, String endTime,
			String type) {
		List<PoundageDetailsVO> list = poundageDetailsDao.getPdList(page, keyword, startTime, endTime, type);
		page.setData(list);
		return page;
	}

	@Override
	public boolean deletePd(String ids, String type) {
		if(ids == null) {
			return false;
		}
		
		String[] idArray = ids.split(",");
		return poundageDetailsDao.deletePd(idArray, type);
	}

}
