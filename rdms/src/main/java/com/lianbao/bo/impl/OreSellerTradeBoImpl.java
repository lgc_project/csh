package com.lianbao.bo.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lianbao.bo.OreSellerTradeBo;
import com.lianbao.dao.CaiShenBiDao;
import com.lianbao.dao.OreTradeDao;
import com.lianbao.dao.PoundageDetailsDao;
import com.lianbao.vo.OreGuaDanVO;
import com.lianbao.vo.PoundageDetailsVO;
import com.vrc.common.Const;
import com.vrc.dao.sys.UserDAO;
import com.vrc.dao.sys.VrcConfigDAO;
import com.vrc.dao.trade.BillDAO;
import com.vrc.dao.trade.TradeDAO;
import com.vrc.util.DateUtils;
import com.vrc.util.KeyUtil;
import com.vrc.util.StringUtils;
import com.vrc.vo.sys.UserVO;
import com.vrc.vo.trade.BillVO;
import com.vrc.vo.trade.TradeVO;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
* @ClassName
* @Description
* @author 
* @date 2018年8月14日 下午6:14:46
*/
@Service("oreSellerTradeBo")
@Transactional
public class OreSellerTradeBoImpl implements OreSellerTradeBo {

	@Resource
	private OreTradeDao oreTradeDao;
	@Resource
	private UserDAO userDao;
	@Resource
	private VrcConfigDAO configDao;
	@Resource
	private TradeDAO tradeDao;
	@Resource
	private CaiShenBiDao caiShenBiDao;
	@Resource
	private BillDAO billDao;
	@Resource
	private PoundageDetailsDao poundageDetailsDao;
	
	@Override
	public JSONObject orePostOdd(String userId, double count, double unitPrice, String level) {
		JSONObject jo = new JSONObject();
		boolean flag;
		Date date = new Date();
		if(userId == null) {
			jo.put("code", "202");
			jo.put("codeDesc", "用户不存在");
			return jo;
		}
		
		UserVO seller = userDao.getUserById(userId);
		double oreSum = seller.getOreSum();
		double oreFreeze = seller.getOreSumFreeze();
		if(oreSum - oreFreeze < count) {
			jo.put("code", "203");
			jo.put("codeDesc", "矿石余额不足");
			return jo;
		}
		
		// 1.冻结卖家挂卖的矿石
		seller.setOreSum(oreSum);
		seller.setOreSumFreeze(oreFreeze + count);
		flag = userDao.updateUser(seller);
		
		// 写入账单表（类型：冻结，子类型：冻结，账单类型：矿石）
		BillVO bill = new BillVO();
		bill.setId(KeyUtil.getKey());
		bill.setType(Const.billType_freeze);
		bill.setActionType(Const.actionType_freeze);
		bill.setUserId(userId);
		bill.setPrice(count);
		bill.setTime(DateUtils.dateToString(date));
		bill.setBillType("0");
		billDao.addBill(bill);
		
		if(flag) {
			OreGuaDanVO guaDan = new OreGuaDanVO();
			long maxGuaDanNo = oreTradeDao.getMaxGuaDanNo();
			if(maxGuaDanNo < 1800000L) {
				maxGuaDanNo = 1800000L;
			} else {
				maxGuaDanNo += 1L;
			}
			
			guaDan.setId(KeyUtil.getKey());
			guaDan.setOddId(String.valueOf(maxGuaDanNo));
			guaDan.setSellId(userId);
			guaDan.setCount(count);
			guaDan.setUnitPrice(unitPrice);
			guaDan.setPriceSum(count * unitPrice);
			guaDan.setStatus(Const.oreOddSellSure);
			guaDan.setIsValid(1);
			guaDan.setLevel(level);
			guaDan.setGuaDanTime(DateUtils.dateToString(date, "yyyy-MM-dd HH:mm:ss"));
			guaDan.setOperateTime(DateUtils.dateToString(date, "yyyy-MM-dd HH:mm:ss"));
			guaDan.setType(1);
			flag = oreTradeDao.addOreGuaDan(guaDan);
		}
		
		if(flag) {
			jo.put("code", Const.success);
			jo.put("codeDesc", "挂单成功"); 
		} else {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
		}
		
		return jo;
	}

	@Override
	public JSONObject buy(String userId, String oddId) {
		JSONObject jo = new JSONObject();
		boolean flag = false;
		Date date = new Date();
		// 1.判断该单是否已经成交
		OreGuaDanVO guadan = oreTradeDao.getGuaDanByIdAndStau(oddId, Const.oreOddSellSure);
		if(guadan == null) {
			jo.put("code", "202");
			jo.put("codeDesc", "该单已经成交");
			return jo;
		}
		
		// 2.判断买家资金是否足够
		UserVO user = userDao.getUserById(userId);
		
		double moneySum = user.getMoneySum(); // 买家的百源币
		double moneyFreeze = user.getMoneyFreeze(); // 买家冻结的百源币
		double orePoundage = 0.0; // 矿石交易手续费
		try {
			String str = configDao.getConfigByKey("orePoundage");
			orePoundage = Double.valueOf(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		double priceSum = guadan.getPriceSum(); // 交易金额
//		double pricePd = priceSum * orePoundage; // 手续费
		
		if(moneySum - moneyFreeze < priceSum) {
			jo.put("code", "203");
			jo.put("codeDesc", "买家余额不足");
			return jo;
		}
		
		// 3.冻结买家资金
		user.setMoneySum(moneySum);
		user.setMoneyFreeze(moneyFreeze + priceSum); // 冻结交易金额和手续费
		flag = userDao.updateUser(user);
		
		// 4.写入账单表（类型：冻结， 子类型：冻结，账单类型：百源币）
		if(flag) {
			BillVO bill = new BillVO();
			bill.setId(KeyUtil.getKey());
			bill.setType(Const.billType_freeze);
			bill.setActionType(Const.actionType_freeze);
			bill.setUserId(userId);
			bill.setPrice(priceSum);
			bill.setTime(DateUtils.dateToString(date));
			bill.setBillType("2");
			flag = billDao.addBill(bill);
		}
		
        // 5.在挂单中填入买家，修改挂单状态为2（买家确认）
        if(flag){
            OreGuaDanVO vo = oreTradeDao.getGuaDanById(oddId);
            vo.setBuyId(userId);
            vo.setStatus(Const.oreOddBuySure);
            vo.setOperateTime(DateUtils.dateToString(date, "yyyy-MM-dd HH:mm:ss"));
            flag = oreTradeDao.updateGuaDan(vo);
        }
        
        if(flag) {
        	jo.put("code", Const.success);
            jo.put("codeDesc", "操作成功");
        } else {
        	jo.put("code", Const.failed);
            jo.put("codeDesc", "系统错误");
        }
		
		return null;
	}

	@Override
	public JSONObject sureOreTrade(String userId, String oddId) {
		JSONObject jo = new JSONObject();
		if(StringUtils.isEmpty(userId) || StringUtils.isEmpty(oddId)) {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
			return jo;
		}
		
		boolean flag = false;
		// 卖家确认，修改挂单状态为3，卖家解冻，转入百源币，买家转入矿石，扣除百源币和手续费
		Date date = new Date();
		OreGuaDanVO guaDan = oreTradeDao.getGuaDanById(oddId);
		flag = sellSecondSure(guaDan, date);
		
		// 写入到矿石交易简易表
		if(flag) {
			TradeVO trade = new TradeVO();
			trade.setId(KeyUtil.getKey());
			trade.setBuyId(guaDan.getBuyId());
			trade.setSellId(guaDan.getSellId());
			trade.setOddNo(guaDan.getOddId());
			trade.setPrice(guaDan.getUnitPrice());
			trade.setTradeCount(guaDan.getCount());
			trade.setTradeMoney(guaDan.getPriceSum());
			trade.setTradeTime(DateUtils.dateToString(date));
			tradeDao.addTrade(trade);
			// 修改简易交易表中矿石的最高价和数量
			tradeDao.updateTradeSimpleCount(DateUtils.dateToString(date, "yyyy-MM-dd"), guaDan.getCount(), guaDan.getUnitPrice());
		}
		
		if(flag) {
			jo.put("code", Const.success);
			jo.put("codeDesc", "操作成功");
		} else {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
		}
		
		return jo;
	}
	
	private boolean sellSecondSure(OreGuaDanVO guaDan, Date date) {
		boolean flag = false;
		// 1.修改挂单状态为3
		guaDan.setStatus(Const.oreOddSuccess);
		guaDan.setOperateTime(DateUtils.dateToString(date, "yyyy-MM-dd HH:mm:ss"));
		flag = oreTradeDao.updateGuaDan(guaDan);
		
		if(flag) {
			double guaDanOreCount = guaDan.getCount(); // 挂了单的矿石数
			
			double orePoundage = 0.0; // 矿石交易手续费
			try {
				String str = configDao.getConfigByKey("orePoundage");
				orePoundage = Double.valueOf(str);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			double priceSum = guaDan.getPriceSum(); // 挂了单的交易总金额
			double pricePd = priceSum * orePoundage; // 手续费
			
			// 2.卖家增加金额
			String sellId = guaDan.getSellId();
			UserVO seller = userDao.getUserById(sellId);
			seller.setMoneySum(seller.getMoneySum() + priceSum - pricePd);
			seller.setOreSum(seller.getOreSum() - guaDanOreCount); // 卖家解冻
			seller.setOreSumFreeze(seller.getOreSumFreeze() - guaDanOreCount);
			flag = userDao.updateUser(seller);
			if(flag) {
				// 写入账单表（类型：转入，子类型：转入，账单类型：百源币）
				BillVO bill = new BillVO();
				bill.setId(KeyUtil.getKey());
                bill.setType(Const.billType_roolin);
                bill.setActionType(Const.actionType_roolin);
                bill.setUserId(sellId);
                bill.setPrice(priceSum);
                bill.setTime(DateUtils.dateToString(date));
                bill.setBillType("2");
                billDao.addBill(bill);
                
                // 写入账单表（类型：转出，子类型：手续费，账单类型：百源币）
                bill.setId(KeyUtil.getKey());
                bill.setType(Const.billType_roolout);
                bill.setActionType(Const.actionType_fee);
                bill.setUserId(sellId);
                bill.setPrice(pricePd);
                bill.setTime(DateUtils.dateToString(date));
                bill.setBillType("2");
                billDao.addBill(bill);
                
                // 记录矿石扣手续费记录
                PoundageDetailsVO pd = new PoundageDetailsVO();
                pd.setId(KeyUtil.getKey());
                pd.setType("0");
                pd.setTradePrice(priceSum);
                pd.setTradePoundage(orePoundage);
                pd.setPdPrice(pricePd);
                pd.setUserId(sellId);
                pd.setCreateTime(DateUtils.dateToString(date, "yyyy-MM-dd HH:mm:ss"));
                pd.setGuadanType("1");
                poundageDetailsDao.addPoundageDetails(pd);
                
                // 写入账单表（类型：转出，子类型：转出，账单类型：矿石）
                bill.setId(KeyUtil.getKey());
                bill.setType(Const.billType_roolout);
                bill.setActionType(Const.actionType_roolout);
                bill.setUserId(sellId);
                bill.setPrice(guaDanOreCount);
                bill.setTime(DateUtils.dateToString(date));
                bill.setBillType("0");
                billDao.addBill(bill);
                
                // 写入账单表（类型：冻结，子类型：解冻，账单类型：矿石）
                bill.setId(KeyUtil.getKey());
                bill.setType(Const.billType_freeze);
                bill.setActionType(Const.actionType_refreeze);
                bill.setUserId(sellId);
                bill.setPrice(guaDanOreCount);
                bill.setTime(DateUtils.dateToString(date));
                bill.setBillType("0");
                billDao.addBill(bill);
			}
			
			if(flag) {
				// 3.转入买家账户
				String buyId = guaDan.getBuyId();
				UserVO buyer = userDao.getUserById(buyId);
				buyer.setOreSum(buyer.getOreSum() + guaDanOreCount);
				buyer.setMoneySum(buyer.getMoneySum() - priceSum);
				buyer.setMoneyFreeze(buyer.getMoneyFreeze() - priceSum);
				flag = userDao.updateUser(buyer);
				if(flag) {
					// 写入账单表（类型：冻结，子类型：解冻，账单类型：百源币）
					BillVO bill = new BillVO();
					bill.setId(KeyUtil.getKey());
					bill.setType(Const.billType_freeze);
					bill.setActionType(Const.actionType_refreeze);
					bill.setUserId(buyId);
					bill.setPrice(priceSum);
					bill.setTime(DateUtils.dateToString(date));
					bill.setBillType("2");
					billDao.addBill(bill);
					
					// 写入账单表（类型：转出，子类型：转出，账单类型：百源币）
					bill.setId(KeyUtil.getKey());
	                bill.setType(Const.billType_roolout);
	                bill.setActionType(Const.actionType_roolout);
	                bill.setUserId(buyId);
	                bill.setPrice(priceSum);
	                bill.setTime(DateUtils.dateToString(date));
	                bill.setBillType("2");
	                billDao.addBill(bill);
	                
	                // 写入账单表（类型：转入，子类型：转入，账单类型：矿石）
	                bill.setId(KeyUtil.getKey());
	                bill.setType(Const.billType_roolin);
	                bill.setActionType(Const.actionType_roolin);
	                bill.setUserId(buyId);
	                bill.setPrice(guaDanOreCount);
	                bill.setTime(DateUtils.dateToString(date));
	                bill.setBillType("0");
	                billDao.addBill(bill);
				}
			}
		}
		
		return flag;
	}

	@Override
	public JSONObject cancelOdd(String oddId) {
		JSONObject jo = new JSONObject();
		boolean flag = false;
		Date date = new Date();
		OreGuaDanVO vo = oreTradeDao.getGuaDanByIdAndStau(oddId, Const.oddSellSure);
		if(vo == null) {
			jo.put("code", "202");
			jo.put("codeDesc", "撤销失败，该单已有买家确认");
			return jo;
		}
		
		// 1.卖家解冻
		String sellId = vo.getSellId();
		UserVO seller = userDao.getUserById(sellId);
		seller.setOreSum(seller.getOreSum());
		seller.setOreSumFreeze(seller.getOreSumFreeze() - vo.getCount());
		flag = userDao.updateUser(seller);
		
		// 写入账单表（类型：冻结，子类型：解冻，账单类型：矿石）
		BillVO bill = new BillVO();
		bill.setId(KeyUtil.getKey());
		bill.setType(Const.billType_freeze);
		bill.setActionType(Const.actionType_refreeze);
		bill.setUserId(sellId);
		bill.setPrice(vo.getCount());
		bill.setTime(DateUtils.dateToString(date));
		bill.setBillType("0");
		billDao.addBill(bill);
		
		// 2.修改挂单状态为撤销状态
		if(flag) {
			OreGuaDanVO guadan = oreTradeDao.getGuaDanById(oddId);
			guadan.setOddId(oddId);
			guadan.setStatus(Const.oddCancel);
			guadan.setOperateTime(DateUtils.dateToString(date, "yyyy-MM-dd HH:mm:ss"));
			flag = oreTradeDao.updateGuaDan(guadan);
		}
		
		if(flag) {
			jo.put("code", Const.success);
			jo.put("codeDesc", "撤销成功");
		} else {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
		}		
		return jo;
	}

	@Override
	public JSONArray oreMailBox(String userId, String level) {
		JSONArray ja = new JSONArray();
		if(StringUtils.isEmpty(userId)) {
			return ja;
		}
		
		List<OreGuaDanVO> list = oreTradeDao.getOreGuaDanByBuyOrSell(userId, level, 1);
		if(list != null) {
			for(OreGuaDanVO vo : list) {
				String buyId = vo.getBuyId();
				String sellId = vo.getSellId();
				int status = vo.getStatus();
				
				// 交易状态为1，且是卖家，提示 您挂单卖出XX矿石，单价XX，总价XXX（撤单）
				if(status == 1 && userId.equals(sellId)) {
					JSONObject jo = new JSONObject();
					jo.put("oddId", StringUtils.nullToEmpty(vo.getOddId()));
					jo.put("time", StringUtils.nullToEmpty(vo.getOperateTime()));
					jo.put("opponentId", StringUtils.nullToEmpty(buyId));
					
					double count = vo.getCount();
					double unitPrice = vo.getUnitPrice();
					double priceSum = vo.getPriceSum();
					String content = "您挂单卖出" + count + "矿石，单价"+unitPrice+"，总价"+priceSum;
					jo.put("content", StringUtils.nullToEmpty(content));
					jo.put("cancel", "1");
					jo.put("account", "0");
					jo.put("sure", "0");
					ja.add(jo);
				}
				
				// 交易状态为2，且是买家，提示 等待卖家二次确认，卖家二次确认后即可收到矿石（交易账号）
				if(status == 2 && userId.equals(buyId)) {
					JSONObject jo = new JSONObject();
					jo.put("oddId", StringUtils.nullToEmpty(vo.getOddId()));
					jo.put("time", StringUtils.nullToEmpty(vo.getOperateTime()));
					jo.put("opponentId", StringUtils.nullToEmpty(sellId));
					
					String content = "等待卖家二次确认，卖家二次确认后即可收到矿石";
					jo.put("content", StringUtils.nullToEmpty(content));
					jo.put("cancel", "0");
					jo.put("account", "1");
					jo.put("sure", "0");
					ja.add(jo);
				}
				
				// 交易状态为2，且是卖家，提示 转给XXX矿石，等待您的二次确认（确认交易，交易账号）
				if(status == 2 && userId.equals(sellId)) {
					JSONObject jo = new JSONObject();
					jo.put("oddId", StringUtils.nullToEmpty(vo.getOddId()));
					jo.put("time", StringUtils.nullToEmpty(vo.getOperateTime()));
					jo.put("opponentId", StringUtils.nullToEmpty(buyId));
					
					double count = vo.getCount();
					String content = "转给"+buyId+"用户"+count+"矿石，等待您的二次确认";
					jo.put("content", StringUtils.nullToEmpty(content));
					jo.put("button", "account,sure");
					jo.put("cancel", "0");
					jo.put("account", "1");
					jo.put("sure", "1");
					ja.add(jo);
				}
				
			}
		}
		
		return ja;
	}

	@Override
	public JSONArray getOreGuaDanList(String userId, String level) {
		JSONArray ja = new JSONArray();
		List<OreGuaDanVO> list = oreTradeDao.getOreGuaDanList(userId, level, 1);
		if(list != null) {
			for(OreGuaDanVO vo : list) {
				JSONObject jo = new JSONObject();
				jo.put("oddId", StringUtils.nullToEmpty(vo.getOddId()));
				jo.put("time", StringUtils.nullToEmpty(vo.getOperateTime()));
				jo.put("sellId", StringUtils.nullToEmpty(vo.getSellId()));
				jo.put("count", vo.getCount());
				jo.put("price", vo.getUnitPrice());
				jo.put("priceSum", vo.getPriceSum());
				ja.add(jo);
			}
		}
		
		return ja;
	}

}
