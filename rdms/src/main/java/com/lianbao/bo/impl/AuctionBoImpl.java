package com.lianbao.bo.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.lianbao.bo.AuctionBo;
import com.lianbao.dao.AuctionDao;
import com.lianbao.vo.AuctionUserVO;
import com.lianbao.vo.AuctionVO;
import com.rdms.common.utils.DateUtils;
import com.vrc.common.Const;
import com.vrc.dao.sys.UserDAO;
import com.vrc.util.TableNameUtil;
import com.vrc.vo.common.PageInfo;
import com.vrc.vo.sys.UserVO;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
* @ClassName
* @Description
* @author 
* @date 2018年6月18日 上午2:02:36
*/
@Service("auctionBo")
@Transactional
public class AuctionBoImpl implements AuctionBo {

	@Resource
	private AuctionDao auctionDao;
	@Resource
	private UserDAO userDao;
	
	@Override
	public PageInfo getAuctionsPage(PageInfo page, String keyword) {
		List<AuctionVO> results = auctionDao.getAuctionsPage(page, keyword);
		page.setData(results);
		return page;
	}

	@Override
	public boolean saveAuction(AuctionVO vo) {
		if(vo == null) {
			return false;
		}
		long id = auctionDao.getMaxId(TableNameUtil.LB_AUCTION);
		if(id < Const.auctionIdStart.longValue()) {
			id = Const.auctionIdStart;
		} else {
			id += 1;
		}
		
		Date curDate = new Date();
		
		vo.setId(String.valueOf(id));
		vo.setCreateTime(new Timestamp(curDate.getTime()));
		return auctionDao.saveAuction(vo);
	}

	@Override
	public boolean updateAuction(AuctionVO vo) {
		if(vo == null || StringUtils.isEmpty(vo.getId())) {
			return false;
		}
		return auctionDao.updateAuction(vo);
	}

	@Override
	public boolean deleteAuction(String ids) {
		if(StringUtils.isEmpty(ids)) {
			return false;
		}
		String[] idArray = ids.split(",");
		
		return auctionDao.deleteAuction(idArray);
	}

	@Override
	public JSONObject judgeIsAuction() {
		JSONObject jo = new JSONObject();
		
		Date curDate = new Date();
		Timestamp curTime = new Timestamp(curDate.getTime());
		Map<String, Object> timeMap = auctionDao.judgeIsAuction(curTime);
		if(timeMap == null || timeMap.isEmpty()) {
			return null;
		}
		
		Timestamp beginTime = (Timestamp) timeMap.get("beginTime");
		Timestamp endTime = (Timestamp) timeMap.get("endTime");
		String auctionId = (String) timeMap.get("auctionId");
		Double auctionMoney = (Double) timeMap.get("auctionMoney");
		Double auctionLowmoney = (Double) timeMap.get("auctionLowmoney");
		Double auctionPoundage = (Double) timeMap.get("auctionPoundage");
		
		if(curTime.before(beginTime)) {
			long distance = (curTime.getTime() - beginTime.getTime()) / 1000;
			if(distance >= 3600) { // 如果距离时间超过一个小时
				jo.put("outOneHourTime", beginTime);
				return jo;
			}
			jo.put("beginTime", distance);
			jo.put("auctionId", auctionId);
			return jo;
		} else if(curTime.after(beginTime) && curTime.before(endTime)) {
			long distance = (endTime.getTime() - curTime.getTime()) / 1000;
			jo.put("curTime", distance);
			jo.put("auctionId", auctionId);
			jo.put("auctionMoney", auctionMoney);
			jo.put("auctionLowmoney", auctionLowmoney);
			jo.put("auctionPoundage", auctionPoundage);
			return jo;
		}
		
		return null;
	}

	@Override
	public JSONArray getTop5AuctionInfo(String auctionId) {
		List<Map<String, Object>> rstMap = auctionDao.getTop5AuctionInfo(auctionId);
		if(rstMap == null || rstMap.isEmpty()) {
			return null;
		}
		JSONArray ja = new JSONArray();
		
		for(Map<String, Object> map : rstMap) {
			JSONObject jo = new JSONObject();
			
			String userId = (String) map.get("user_id");
			Double auctionAmount = (Double) map.get("auction_amount");
			Timestamp createTime = (Timestamp) map.get("create_time");
			
			UserVO userVo = userDao.getUserById(userId);
			jo.put("userName", userVo.getUsername());
			jo.put("auctionAmount", auctionAmount);
			jo.put("createTime", DateUtils.format(createTime, DateUtils.DATE_TIME_PATTERN));
			ja.add(jo);
		}
		
		return ja;
	}

	@Override
	public boolean addAuctionInfo(String userId, String auctionId, BigDecimal auctionAmount) {
		long id = auctionDao.getMaxId(TableNameUtil.LB_AUCTION_USER);
		if(id < Const.auctionUserIdStart.longValue()) {
			id = Const.auctionUserIdStart;
		} else {
			id += 1;
		}
		
		AuctionUserVO vo = new AuctionUserVO();
		vo.setId(String.valueOf(id));
		vo.setUserId(userId);
		vo.setAuctionId(auctionId);
		vo.setAuctionAmount(auctionAmount);
		return auctionDao.addAuctionInfo(vo);
	}

}
