package com.lianbao.bo.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lianbao.bo.CreditBo;
import com.lianbao.dao.CreditDao;
import com.lianbao.vo.CreditVO;
import com.vrc.common.Const;
import com.vrc.dao.sys.UserDAO;
import com.vrc.dao.trade.BillDAO;
import com.vrc.util.DateUtils;
import com.vrc.util.KeyUtil;
import com.vrc.vo.common.PageInfo;
import com.vrc.vo.sys.UserVO;
import com.vrc.vo.trade.BillVO;

/**
* @ClassName
* @Description
* @author 
* @date 2018年8月20日 下午4:27:18
*/
@Service("creditBo")
@Transactional
public class CreditBoImpl implements CreditBo {

	@Resource
	private CreditDao creditDao;
	@Resource
	private BillDAO billDao;
	@Resource
	private UserDAO userDao;
	
	@Override
	public boolean addCreditVO(String userId, double payMoney, String outTradeNo, Date curDate) {
		CreditVO vo = new CreditVO();
		long maxCreditNo = creditDao.getMaxCreditNo();
		if(maxCreditNo < 10000L) {
			maxCreditNo = 10000L;
		} else {
			maxCreditNo += 1L;
		}
		
		vo.setId(KeyUtil.getKey());
		vo.setUserId(userId);
		vo.setCreditMoney(payMoney);
		vo.setCreditNo(String.valueOf(maxCreditNo));
		vo.setOutTradeNo(outTradeNo);
		vo.setCreditType("0");
		vo.setCreditTime(DateUtils.dateToString(curDate, "yyyy-MM-dd HH:mm:ss"));
		vo.setStatus(1);
		boolean flag = creditDao.addCreditVO(vo);
		return flag;
	}

	@Override
	public long getMaxCreditNo() {
		return creditDao.getMaxCreditNo();
	}

	@Override
	public boolean updateCreditVO(CreditVO vo) {
		boolean flag = false;
		
		// 更新用户百源币
		String userId = vo.getUserId();
		UserVO user = userDao.getUserById(userId);
		user.setMoneySum(user.getMoneySum() + vo.getCreditMoney());
		flag = userDao.updateUser(user);
		
		// 写入账单表
		BillVO bill = new BillVO();
		bill.setId(KeyUtil.getKey());
		bill.setType(Const.billType_roolin);
		bill.setActionType(Const.actionType_roolin);
		bill.setUserId(userId);
		bill.setPrice(vo.getCreditMoney());
		bill.setTime(DateUtils.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss"));
		bill.setBillType("2");
		billDao.addBill(bill);
		
		// 更新充值记录
		if(flag) {
			vo.setStatus(2);
			flag = creditDao.updateCreditVO(vo);
		}
		return flag;
	}

	@Override
	public CreditVO getCreditVO(String outTradeNo) {
		return creditDao.getCreditVO(outTradeNo);
	}

	@Override
	public PageInfo getCreditPage(PageInfo page, String keyword, String startTime, String endTime) {
		List<CreditVO> list = creditDao.getCreditPage(page, keyword, startTime, endTime);
		page.setData(list);
		return page;
	}

}
