package com.lianbao.bo.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lianbao.bo.GameProBo;
import com.lianbao.dao.GameProDao;
import com.lianbao.vo.GameProVO;
import com.vrc.common.Const;

import net.sf.json.JSONObject;

/**
* @ClassName
* @Description
* @author 
* @date 2018年7月21日 下午7:49:03
*/
@Service("gameProBo")
@Transactional
public class GameProBoImpl implements GameProBo {

	@Resource
	private GameProDao gameProDao;
	
	@Override
	public boolean saveOrUpdate(GameProVO vo) {
		if(vo == null) {
			return false;
		}
		return gameProDao.saveOrUpdate(vo);
	}



	@Override
	public JSONObject getGamePro() {
		GameProVO vo = gameProDao.getGamePro();
		if(vo == null) {
			JSONObject jo = new JSONObject();
			jo.put("code", "202");
			jo.put("codeDesc", "概率不存在");
			return jo;
		}
		
		JSONObject jo = new JSONObject();
		jo.put("code", Const.success);
		jo.put("codeDesc", "获取成功");
		jo.put("data", vo);
		return jo;
	}

}
