package com.lianbao.bo.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lianbao.bo.TakeCashBo;
import com.lianbao.dao.TakeCashDao;
import com.lianbao.vo.TakeCashVO;
import com.vrc.common.Const;
import com.vrc.dao.sys.UserDAO;
import com.vrc.dao.trade.BillDAO;
import com.vrc.util.DateUtils;
import com.vrc.util.KeyUtil;
import com.vrc.vo.common.PageInfo;
import com.vrc.vo.sys.UserVO;
import com.vrc.vo.trade.BillVO;

import net.sf.json.JSONObject;

/**
* @ClassName
* @Description
* @author 
* @date 2018年7月9日 上午9:48:39
*/
@Service("takeCashBo")
@Transactional
public class TakeCashBoImpl implements TakeCashBo {

	@Resource
	private TakeCashDao takeCashDao;
	@Resource
	private UserDAO userDao;
	@Resource
	private BillDAO billDao;
	
	@Override
	public PageInfo getCashPage(PageInfo page, String keyword, String startTime, String endTime) {
		List<TakeCashVO> list = takeCashDao.getCashPage(page, keyword, startTime, endTime);
		for(TakeCashVO vo : list) {
			String userId = vo.getUserId();
			UserVO user = userDao.getUserById(userId);
			if(user == null) {
				continue;
			}
			vo.setBankCard(user.getBankcard());
			vo.setAlipayAccount(user.getAlipayAccount());
			int status = vo.getStatus();
			if(status == 0) {
				vo.setStatusTip("未审理");
			} else if(status == 1) {
				vo.setStatusTip("已审理，未汇款");
			} else {
				vo.setStatusTip("已汇款");
			}
		}
		
		page.setData(list);
		return page;
	}

	@Override
	public JSONObject takeCash(String userId, Double cash) {
		JSONObject jo = new JSONObject();
		UserVO user = userDao.getUserById(userId);
		Double money = user.getMoneySum();
		Double moneyFreeze = user.getMoneyFreeze();
		if(money.doubleValue() - moneyFreeze < cash.doubleValue()) {
			jo.put("code", "202");
			jo.put("codeDesc", "余额不足");
			return jo;
		}
		
		TakeCashVO vo = new TakeCashVO();
		vo.setUserId(userId);
		vo.setCash(cash);
		vo.setStatus(0);
		boolean flag = takeCashDao.saveOrUpdateTakeCash(vo);
		if(flag) {
			jo.put("code", Const.success);
			jo.put("codeDesc", "操作成功");
		} else {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
		}
		
		return jo;
	}

	@Override
	public JSONObject sureSubmit(String id, int status) {
		JSONObject jo = new JSONObject();
		boolean flag = false;
		
		TakeCashVO vo = takeCashDao.getCashById(id);
		if(vo == null) {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
			return jo;
		}
		
		if(vo.getStatus() == 2) {
			jo.put("code", "202");
			jo.put("codeDesc", "已汇款，不需要重复确认");
			return jo;
		}
		
		if(status == 0) {
			vo.setStatus(1);
			flag = takeCashDao.saveOrUpdateTakeCash(vo);
		}
		
		if(status == 1) {
			vo.setStatus(2);
			flag = takeCashDao.saveOrUpdateTakeCash(vo);
			
			if(flag) {
				UserVO user = userDao.getUserById(vo.getUserId());
				user.setMoneySum(user.getMoneySum() - vo.getCash());
				flag = userDao.updateUser(user);
				
				// 写入账单（类型：转出，子类型：转出，账单类型：百源币）
				BillVO bill = new BillVO();
				bill.setId(KeyUtil.getKey());
				bill.setType(Const.billType_roolout);
				bill.setActionType(Const.actionType_roolout);
				bill.setUserId(user.getId());
				bill.setPrice(vo.getCash().doubleValue());
				bill.setTime(DateUtils.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss"));
				bill.setBillType("2");
				billDao.addBill(bill);
			}
			
		}
		
		if(flag) {
			jo.put("code", Const.success);
			jo.put("codeDesc", "操作成功");
		} else {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
		}
		
		return jo;
	}

}
