package com.lianbao.bo.impl;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lianbao.bo.OreBo;
import com.lianbao.dao.OreDao;
import com.lianbao.vo.OreVO;
import com.vrc.vo.common.PageInfo;

/**
* @ClassName
* @Description
* @author 
* @date 2018年7月23日 上午11:04:18
*/
@Service("oreBo")
@Transactional
public class OreBoImpl implements OreBo {

	@Resource
	private OreDao oreDao;
	
	@Override
	public PageInfo getOrePage(PageInfo page, String keyword, Timestamp startTime, Timestamp endTime) {
		List<OreVO> list = oreDao.getOrePage(page, keyword, startTime, endTime);
		page.setData(list);
		return page;
	}
}
