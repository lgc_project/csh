package com.lianbao.bo.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.druid.util.StringUtils;
import com.lianbao.bo.CaiShenBiBo;
import com.lianbao.dao.CSBBillDao;
import com.lianbao.dao.CSBRecoverDao;
import com.lianbao.dao.CSBTradeDao;
import com.lianbao.dao.CaiShenBiDao;
import com.lianbao.dao.OreDao;
import com.lianbao.vo.CSBGuaDanVO;
import com.lianbao.vo.CSBRecoverVO;
import com.lianbao.vo.CSBTradeVO;
import com.lianbao.vo.CaiShenBiVO;
import com.lianbao.vo.OreVO;
import com.vrc.common.Const;
import com.vrc.dao.sys.UserDAO;
import com.vrc.dao.sys.VrcConfigDAO;
import com.vrc.dao.trade.BillDAO;
import com.vrc.dao.trade.TradeDAO;
import com.vrc.util.DateUtils;
import com.vrc.util.KeyUtil;
import com.vrc.util.TableNameUtil;
import com.vrc.vo.common.PageInfo;
import com.vrc.vo.sys.UserVO;
import com.vrc.vo.trade.BillVO;

import net.sf.json.JSONObject;

/**
* @ClassName
* @Description
* @author 
* @date 2018年6月21日 上午10:58:11
*/
@Service("caiShenBiBo")
@Transactional
public class CaiShenBiBoImpl implements CaiShenBiBo {

	@Resource
	private CaiShenBiDao caiShenBiDao;
	@Resource
	private UserDAO userDao;
	@Resource
	private CSBBillDao csbBillDao;
	@Resource
	private BillDAO billDao;
	@Resource
	private CSBRecoverDao csbRecoverDao;
	@Resource
	private CSBTradeDao csbTradeDao;
	@Resource
	private OreDao oreDao;
	@Resource
	private TradeDAO tradeDao;
	@Resource
	private VrcConfigDAO configDao;
	
	@Override
	public PageInfo getCaiShenBisPage(PageInfo page, String keyword) {
		List<CaiShenBiVO> results = caiShenBiDao.getCaiShenBisPage(page, keyword);
		page.setData(results);
		return page;
	}

	@Override
	public boolean saveCaiShenBi(CaiShenBiVO vo) {
		if(vo == null) {
			return false;
		}
		
		long id = caiShenBiDao.getMaxId(TableNameUtil.LB_CAISHENGBI);
		if(id < Const.caiShenBiIdStart.longValue()) {
			id = Const.caiShenBiIdStart;
		} else {
			id += 1;
		}
		
		Date curDate = new Date();
		
		vo.setId(String.valueOf(id));
		vo.setCreateTime(new Timestamp(curDate.getTime()));
		return caiShenBiDao.saveCaiShenBi(vo);
	}

	@Override
	public boolean updateCaiShenBi(CaiShenBiVO vo) {
		if(vo == null || StringUtils.isEmpty(vo.getId())) {
			return false;
		}
		return caiShenBiDao.updateCaiShenBi(vo);
	}

	@Override
	public boolean deleteCaiShenBi(String ids) {
		if(StringUtils.isEmpty(ids)) {
			return false;
		}
		String[] idArray = ids.split(",");
		return caiShenBiDao.deleteCaiShenBi(idArray);
	}

	@Override
	public synchronized JSONObject sellIn(String userId, double amount, double unitPrice) {
		JSONObject jo = new JSONObject();
		Date curDate = new Date();
		Timestamp curTime = new Timestamp(curDate.getTime());

		Map<String, Object> rstMap = caiShenBiDao.getCaiShenBiAmount();
		if(rstMap.isEmpty()) {
			jo.put("code", "203");
			jo.put("codeDesc", "财神币数量不足");
			return jo;
		}
		String csbId = (String) rstMap.get("id");
		Double csbSurplus = (Double) rstMap.get("surplus"); // 剩余量
//		Double poundage = (Double) rstMap.get("poundage"); // 手续费率
		// 总金额（数量 * 单价）
		Double totalMoney = amount * unitPrice;
		// 计算手续费
//		Double tolPoundage = totalMoney * poundage;

		UserVO user = userDao.getUserById(userId);
		double moneySum = user.getMoneySum();
		double moneyFreeze = user.getMoneyFreeze();
		// 财神币总额大于用户余额时
		if(totalMoney.doubleValue() > (moneySum - moneyFreeze)) {
			jo.put("code", "202");
			jo.put("codeDesc", "余额不足");
			return jo;
		}
		
		if(csbSurplus.doubleValue() < amount) {
			jo.put("code", "203");
			jo.put("codeDesc", "财神币数量不足");
			return jo;
		}
		
		// 更新发布的财神币数量
		CaiShenBiVO vo = caiShenBiDao.getCaiShenBiById(csbId);
		vo.setSurplus(csbSurplus - amount);
		vo.setUpdateTime(curTime);
		
		boolean flag = caiShenBiDao.updateCaiShenBi(vo);
		if(flag) {
			// 更新用户的财神币数量和余额
			user.setMoneySum(moneySum - totalMoney.doubleValue());
			user.setCaiShenBiSum(user.getCaiShenBiSum() + amount);
			flag = userDao.updateUser(user);
			
			// 写入账单表（类型：转出，子类型：转出，账单类型：百源币）
			BillVO bill = new BillVO();
			bill.setId(KeyUtil.getKey());
			bill.setType(Const.billType_roolout);
			bill.setActionType(Const.actionType_roolout);
			bill.setUserId(userId);
			bill.setPrice(totalMoney.doubleValue());
			bill.setTime(DateUtils.dateToString(curDate, "yyyy-MM-dd HH:mm:ss"));
			bill.setBillType("2");
			billDao.addBill(bill);
				
			// 写入账单表（类型：转入，子类型：转入，账单类型：财神币）
			bill.setId(KeyUtil.getKey());
			bill.setType(Const.billType_roolin);
			bill.setActionType(Const.actionType_roolin);
			bill.setUserId(userId);
			bill.setPrice(amount);
			bill.setTime(DateUtils.dateToString(curDate, "yyyy-MM-dd HH:mm:ss"));
			bill.setBillType("1");
			billDao.addBill(bill);
		
			// 更新财神币购入的交易详情
			CSBTradeVO csbTradeVO = new CSBTradeVO();
			csbTradeVO.setId(KeyUtil.getKey());
			csbTradeVO.setBuyId(userId);
			csbTradeVO.setTradeCount(amount);
			csbTradeVO.setTradeUnitPrice(unitPrice);
			csbTradeVO.setTradeMoneySum(totalMoney.doubleValue());
			csbTradeVO.setCreateTime(new Timestamp(curDate.getTime()));
			csbTradeDao.addCSBTrade(csbTradeVO);
		}
		
		if(flag) {
			jo.put("code", Const.success);
			jo.put("codeDesc", "操作成功");
		} else {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
		}
		
		return jo;
	}

	@Override
	public synchronized JSONObject sellOut(String buyId, String sellId, double amount, double unitPrice) {
		JSONObject jo = new JSONObject();
		Date curDate = new Date();
		Timestamp curTime = new Timestamp(curDate.getTime());
		boolean flag = false;
		// 1.判断卖家财神币数量是否足够
		UserVO sellUser = userDao.getUserById(sellId);
//		double sellCsbSum = sellUser.getCaiShenBiSum();
		double sellOreSum = sellUser.getOreSum(); // 获取用户现有的矿石数
		if(sellOreSum < amount) {
			jo.put("code", "202");
			jo.put("codeDesc", "卖家财神币数量不足");
			return jo;
		} else {
			// 2.判断买家是否存在
			UserVO buyer = userDao.getUserById(buyId);
			if(buyer == null) {
				jo.put("code", "204");
				jo.put("codeDesc", "买家不存在");
				return jo;
			}
		}
		
		// 3.扣除卖家矿石（当交易取消的时候，会返还） 
//		sellUser.setCaiShenBiSum(sellCsbSum - amount);
		sellUser.setOreSum(sellOreSum - amount);
		flag = userDao.updateUser(sellUser);
		
		// 4.创建一个挂单，并设置状态为1
		if(flag) {
			CSBGuaDanVO vo = new CSBGuaDanVO();
			// 获取最大的挂单id
			long maxId = caiShenBiDao.getMaxGuaDanId();
			if(maxId < Const.caiShenBiIdStart.longValue()) {
				maxId = Const.caiShenBiIdStart;
			} else {
				maxId += 1;
			}
			
			vo.setId(KeyUtil.getKey());
			vo.setOddId(String.valueOf(maxId));
			vo.setBuyId(buyId);
			vo.setSellId(sellId);
			vo.setPrice(unitPrice);
			vo.setCount(amount);
			vo.setPriceSum(amount * unitPrice);
			vo.setCreateTime(curTime);
			vo.setStatus(Const.oddSellSure);
			caiShenBiDao.addGuaDan(vo);
			jo.put("code", Const.success);
			jo.put("codeDesc", "操作成功");
			return jo;
		} else {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
			return jo;
		}
	}
	
	/**
	 * 买家确认，修改挂单状态为2
	 * 
	 * @param vo
	 * @return
	 */
	private JSONObject buySure(CSBGuaDanVO vo, Timestamp curTime) {
		JSONObject jo = new JSONObject();
		// 1.判断买家资金是否足够，冻结资金
		String userId = vo.getBuyId();
		UserVO user = userDao.getUserById(userId);
		Double moneySum = user.getMoneySum();
		Double moneyFreeze = user.getMoneyFreeze();
		Double canuse = moneySum.doubleValue() - moneyFreeze.doubleValue(); // 用户现有资金
		Double poundage = 0.0;
		
		Map<String, Object> rstMap = caiShenBiDao.getCaiShenBiAmount();
		if(!rstMap.isEmpty()) {
			poundage = (Double) rstMap.get("poundage"); // 发布财神币的手续费率
		}
		Double realPriceSum = vo.getPriceSum() * (1 + poundage);
		
		boolean flag = false;
		if(canuse < realPriceSum) {
			jo.put("code", "202");
			jo.put("codeDesc", "余额不足");
			return jo;
		} else {
			// 2.冻结资金
			moneyFreeze = moneyFreeze.doubleValue() + realPriceSum.doubleValue();
			user.setId(userId);
			user.setMoneySum(moneySum);
			user.setMoneyFreeze(moneyFreeze);
			flag = userDao.updateUserMoney(user);
			if(flag) {
				// 3.写入账单表（冻结）
				BillVO bill = new BillVO();
				bill.setId(KeyUtil.getKey());
				bill.setType(Const.billType_freeze);
				bill.setActionType(Const.actionType_freeze);
				bill.setUserId(userId);
				bill.setPrice(moneyFreeze);
				bill.setTime(DateUtils.dateToString(new Date()));
				billDao.addBill(bill);
				// 4.生产买家确认的挂单
				CSBGuaDanVO guaDan = new CSBGuaDanVO();
				guaDan.setOddId(vo.getOddId());
				guaDan.setStatus(Const.oddBuySure);
				guaDan.setUpdateTime(curTime);
				caiShenBiDao.updateGuaDan(guaDan);
			}
		}
		
		if(flag) {
			jo.put("code", Const.success);
			jo.put("codeDesc", "操作成功");
		} else {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
		}
		return jo;
	}
	
	/**
	 * 卖家确认，修改挂单状态为3
	 * 
	 * @param vo
	 * @param curTime
	 * @return
	 */
	private boolean sellSure(CSBGuaDanVO vo, Timestamp curTime) {
		Date date = new Date();
		
		Double poundage = 0.0;
		Map<String, Object> rstMap = caiShenBiDao.getCaiShenBiAmount();
		if(!rstMap.isEmpty()) {
			poundage = (Double) rstMap.get("poundage"); // 发布财神币的手续费率
		}
		
		// 1.更新挂单状态
		CSBGuaDanVO guaDan = new CSBGuaDanVO();
		guaDan.setOddId(vo.getOddId());
		guaDan.setStatus(Const.oddSuccess);
		guaDan.setUpdateTime(curTime);
		boolean flag = caiShenBiDao.updateGuaDan(vo);
		
		double count = vo.getCount(); // 获得交易的财神币数量
		
		Double csbPriceSum = vo.getPriceSum(); // 获得交易的总金额
		Double poundageSum = csbPriceSum * poundage;
		
		if(flag) {
			// 2.买家解冻，并扣除金额
			String buyId = vo.getBuyId();
			UserVO buyer = userDao.getUserById(buyId);
			buyer.setMoneySum(buyer.getMoneySum() - csbPriceSum - poundageSum);
			buyer.setMoneyFreeze(buyer.getMoneyFreeze() - csbPriceSum - poundageSum);
			flag = userDao.updateUser(buyer);
			if(flag) {
				// 写入账单表（解冻）
				BillVO bill = new BillVO();
				bill.setId(KeyUtil.getKey());
				bill.setType(Const.billType_freeze);
				bill.setActionType(Const.actionType_refreeze);
				bill.setUserId(buyId);
				bill.setPrice(csbPriceSum + poundageSum);
				bill.setTime(DateUtils.dateToString(date));
				billDao.addBill(bill);
				
				// 写入账单表（转出）
				bill.setId(KeyUtil.getKey());
				bill.setType(Const.billType_pay);
				bill.setActionType(Const.actionType_pay);
				bill.setUserId(buyId);
				bill.setPrice(csbPriceSum + poundageSum);
				billDao.addBill(bill);
			}
			// 3.转入到买家账户
			buyer.setCaiShenBiSum(buyer.getCaiShenBiSum() + count);
			flag = userDao.updateUser(buyer);
			
			if(flag) {
				// 4.修改卖家的金额
				String sellId = vo.getSellId();
				UserVO seller = userDao.getUserById(sellId);
				Double sellMoneySum = caiShenBiDao.getUserMoneySum(sellId);
				seller.setMoneySum(sellMoneySum.doubleValue() + csbPriceSum - poundageSum);
				flag = userDao.updateUser(seller);
			}
		}
		
		if(flag) {
			// 5.写入账单表(收益)
			BillVO bill = new BillVO();
			bill.setId(KeyUtil.getKey());
			bill.setUserId(vo.getSellId());
			bill.setType(Const.billType_income);
			bill.setActionType(Const.actionType_income);
			bill.setUserId(vo.getSellId());
			bill.setPrice(csbPriceSum - poundageSum);
			bill.setTime(DateUtils.dateToString(date));
			billDao.addBill(bill);
		}
		
		return flag;
	}

	@Override
	public JSONObject sureSell(String userId, String oddId) {
		Date curDate = new Date();
		Timestamp curTime = new Timestamp(curDate.getTime());
		JSONObject jo = new JSONObject();
		if(StringUtils.isEmpty(userId) || StringUtils.isEmpty(oddId)) {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
			return jo;
		}
		
		boolean flag = false;
		CSBGuaDanVO vo = caiShenBiDao.getGuaDanByOddId(oddId);
		// 买家确认，只需修改挂单状态
		if(userId.equalsIgnoreCase(vo.getBuyId()) && vo.getStatus() == Const.oddSellSure) {
			jo = buySure(vo, curTime);
			return jo;
		}
		// 卖家确认
		if(userId.equalsIgnoreCase(vo.getSellId()) && vo.getStatus() == Const.oddBuySure) {
			flag = sellSure(vo, curTime);
			
			CSBTradeVO trade = new CSBTradeVO();
			trade.setId(KeyUtil.getKey());
			trade.setBuyId(userId);
			trade.setTradeCount(vo.getCount());
			trade.setTradeUnitPrice(vo.getPrice());
			trade.setTradeMoneySum(vo.getPriceSum());
			trade.setCreateTime(new Timestamp(curDate.getTime()));
			csbTradeDao.addCSBTrade(trade);
			
			// 修改简易交易表中财神币的最高价和数量
			tradeDao.updateTradeSimpleCsbCount(DateUtils.dateToString(curDate, "yyyy-MM-dd"), vo.getCount(), vo.getPrice());
		}
		
		if(flag) {
			jo.put("code", Const.success);
			jo.put("codeDesc", "操作成功");
		} else {
			jo.put("code", Const.failed);
			jo.put("code", "系统错误");
		}
		
		return jo;
	}

	@Override
	public JSONObject cancelOdd(String oddId) {
		Date curDate = new Date();
		Timestamp curTime = new Timestamp(curDate.getTime());
		
		JSONObject jo = new JSONObject();
		boolean flag = false;
		// 获取单号为oddId，状态为卖家确认的单
		CSBGuaDanVO vo = caiShenBiDao.getGuaDanByOddIdAndStau(oddId, Const.oddBuySure);
		if(vo == null) {
			jo.put("code", "202");
			jo.put("codeDesc", "撤销失败，该单已有买家确认");
			return jo;
		} else {
			CSBGuaDanVO guaDan = new CSBGuaDanVO();
			guaDan.setOddId(oddId);
			guaDan.setStatus(Const.oddCancel);
			guaDan.setUpdateTime(curTime);
			flag = caiShenBiDao.updateGuaDan(guaDan);
		}
		
		if(flag) {
			// 撤销成功，要退还卖家财神币
			Double csbAmount = vo.getCount();
			userDao.addCsbSum(vo.getSellId(), new BigDecimal(csbAmount));
			jo.put("code", Const.success);
			jo.put("codeDesc", "撤销成功");
			return jo;
		} else {
			jo.put("code", Const.failed);
            jo.put("codeDesc", "系统错误");
            return jo;
		}
	}

	@Override
	public JSONObject exchangeCsb(String userId, double oreSum) {
		JSONObject jo = new JSONObject();
		Date date = new Date();
		
		UserVO user = userDao.getUserById(userId);
		double userOreSum = user.getOreSum();
		double userOreFreeze = user.getOreSumFreeze();
		if(userOreSum - userOreFreeze < oreSum) {
			jo.put("code", "202");
			jo.put("codeDesc", "用户矿石数不足");
			return jo;
		}
		
		// 1.减去用户的矿石数
//		boolean flag = userDao.subOreSum(userId, oreSum);
		user.setOreSum(userOreSum - oreSum);
		boolean flag = userDao.updateUser(user);
		
		// 写入账单表（类型：转出，子类型：转出，账单类型：矿石）
		BillVO bill = new BillVO();
		bill.setId(KeyUtil.getKey());
		bill.setType(Const.billType_roolout);
		bill.setActionType(Const.actionType_roolout);
		bill.setUserId(userId);
		bill.setPrice(oreSum);
		bill.setTime(DateUtils.dateToString(date, "yyyy-MM-dd HH:mm:ss"));
		bill.setBillType("0");
		billDao.addBill(bill);
		
		
		if(flag) {			
			// 2.增加用户的财神币数（5换1）			
			double exchange = oreSum / 5;
			user.setCaiShenBiSum(user.getCaiShenBiSum() + exchange);
			userDao.updateUser(user);
			// 写入账单表（类型：转入，子类型：转入，账单类型：财神币）
			bill.setId(KeyUtil.getKey());
			bill.setType(Const.billType_roolin);
			bill.setActionType(Const.actionType_roolin);
			bill.setUserId(userId);
			bill.setPrice(exchange);
			bill.setTime(DateUtils.dateToString(date, "yyyy-MM-dd HH:mm:ss"));
			bill.setBillType("1");
			billDao.addBill(bill);
			
			// 3.记录矿石合成
			Long oreId = oreDao.getMaxId();
			if(oreId.longValue() < 10000L) {
				oreId = 10000L;
			} else {
				oreId += 1;
			}
			
			Date curDate = new Date();
			Timestamp curTime = new Timestamp(curDate.getTime());
			
			OreVO oreVO = new OreVO();
			oreVO.setId(oreId);
			oreVO.setCreateTime(curTime);
			oreVO.setUserId(userId);
			oreVO.setUsername(user.getUsername());
			oreVO.setOreSum(oreSum);
			oreVO.setCsbSum(exchange);
			oreDao.saveOre(oreVO);
		}
		
		if(flag) {
			jo.put("code", Const.success);
			jo.put("codeDesc", "操作成功");
		} else {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
		}
		return jo;
	}

	@Override
	public JSONObject recoverCsb(String userId, double csbAmount) {
		Date date = new Date();
		
		Map<String, Object> rstMap = caiShenBiDao.getCaiShenBiAmount();
		Double unitPrice = (Double) rstMap.get("unit_price"); // 发布的财神币单位价格
		Double poundage = 0.0; // 手续费率
		try {
			String str = configDao.getConfigByKey("recoverpoundage");
			poundage = Double.valueOf(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Double csbPriceSum = unitPrice * csbAmount; // 财神币总额
		Double csbRealAmount = csbPriceSum * (1 - poundage); // 扣除手续费
		Double csbPricePd = csbPriceSum * poundage; // 手续费
		
		
		JSONObject jo = new JSONObject();
		UserVO user = userDao.getUserById(userId);
		double userCsbSum = user.getCaiShenBiSum();
		if(userCsbSum < 0) {
			csbAmount = userCsbSum;
		}
		
		// 1.减去用户财神币数量
//		boolean flag = userDao.subCsbSum(userId, csbSum);
		user.setCaiShenBiSum(userCsbSum - csbAmount);
		user.setMoneySum(user.getMoneySum() + csbRealAmount.doubleValue());
		boolean flag = userDao.updateUser(user);
		// 写入账单表（类型：转出，子类型：转出，账单类型：财神币）
		BillVO bill = new BillVO();
		bill.setId(KeyUtil.getKey());
		bill.setType(Const.billType_roolout);
		bill.setActionType(Const.actionType_roolout);
		bill.setUserId(userId);
		bill.setPrice(csbAmount);
		bill.setTime(DateUtils.dateToString(date, "yyyy-MM-dd HH:mm:ss"));
		bill.setBillType("1");
		billDao.addBill(bill);
		
		// 写入账单表（类型：转入，子类型：转入，账单类型：百源币）
		bill.setId(KeyUtil.getKey());
		bill.setType(Const.billType_roolin);
		bill.setActionType(Const.actionType_roolin);
		bill.setUserId(userId);
		bill.setPrice(csbPriceSum.doubleValue());
		bill.setTime(DateUtils.dateToString(date, "yyyy-MM-dd HH:mm:ss"));
		bill.setBillType("2");
		billDao.addBill(bill);
		
		// 写入账单表（类型：转出，子类型：手续费，账单类型：百源币）
		bill.setId(KeyUtil.getKey());
		bill.setType(Const.billType_roolout);
		bill.setActionType(Const.actionType_fee);
		bill.setUserId(userId);
		bill.setPrice(csbPricePd.doubleValue());
		bill.setTime(DateUtils.dateToString(date, "yyyy-MM-dd HH:mm:ss"));
		bill.setBillType("2");
		billDao.addBill(bill);
		
		if(flag) {
			Date curDate = new Date();
			Timestamp curTime = new Timestamp(curDate.getTime());
			
			CSBRecoverVO vo = new CSBRecoverVO();
			vo.setId(KeyUtil.getKey());
			vo.setUserId(userId);
			vo.setCsbSum(csbAmount);
			vo.setMoneySum(csbRealAmount);
			vo.setPoundage(poundage);
			vo.setMoneyPd(csbPricePd);
			vo.setCreateTime(curTime);
			csbRecoverDao.addCSBRecoverVO(vo);
			
			jo.put("code", Const.success);
			jo.put("codeDesc", "操作成功");
		} else {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
		}
		
		return jo;
	}

	@Override
	public PageInfo getCSBRecoverInfoPage(PageInfo page, String keyword, Timestamp startTime, Timestamp endTime) {
		List<CSBRecoverVO> list = csbRecoverDao.getCSBRecoverInfoPage(page, keyword, startTime, endTime);
		page.setData(list);
		return page;
	}

	@Override
	public JSONObject getCSBTradeInfo() {
		JSONObject jo = new JSONObject();
		
		Calendar ca = Calendar.getInstance();
        Date endDate = ca.getTime();
        
        Calendar ca2 = Calendar.getInstance();
        ca2.add(Calendar.DAY_OF_MONTH, -Const.chartDayLength);
        Date startDate = ca2.getTime();
        
        String startTime = DateUtils.dateToString(startDate, "yyyy-MM-dd 00:00:00");
        String endTime = DateUtils.dateToString(endDate, "yyyy-MM-dd 23:59:59");
		
		List<Map<String, Object>> list = tradeDao.getChartDate(startTime, endTime);
		
		double[] d = new double[] {0, 0, 0, 0, 0, 0, 0};
		
		double weekTradeSum = 0;
		for(int i = 0; i < list.size(); i++) {
			Map<String, Object> map = list.get(i);
			double tradeMoneySum = (double) map.get("csbCount");
			
			weekTradeSum = weekTradeSum + tradeMoneySum;
			d[i] = tradeMoneySum;
		}
		
		Map<String, Object> rstMap = caiShenBiDao.getCaiShenBiAmount();
		
		jo.put("code", Const.success);
		jo.put("codeDesc", "操作成功");
		jo.put("caiShenBiCount", rstMap.get("amount"));
		jo.put("tradeSum", rstMap.get("surplus"));
		jo.put("tradeDay", d[d.length - 1]);
		jo.put("unitPrice", rstMap.get("unit_price"));
		jo.put("weekData", d);
		return jo;
	}

	@Override
	public JSONObject recoverCsbTips() {
		String tips = configDao.getConfigByKey("recoverpoundagetip");
		JSONObject jo = new JSONObject();
		jo.put("tips", tips);
		return jo;
	}
}
