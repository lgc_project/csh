package com.lianbao.bo.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.lianbao.bo.PrizeBo;
import com.lianbao.dao.PrizeDao;
import com.lianbao.vo.PrizeVO;
import com.vrc.common.Const;
import com.vrc.vo.common.PageInfo;

import net.sf.json.JSONObject;

/**
 * @ClassName
 * @Description
 * @author
 * @date 2018年6月11日 下午2:50:44
 */
@Service("prizeBo")
@Transactional
public class PrizeBoImpl implements PrizeBo {

	@Resource
	private PrizeDao prizeDao;

	@Override
	public boolean savePrize(PrizeVO vo) {
		if (vo == null) {
			return false;
		}

		// 1.获取最大id, 加一
		long id = prizeDao.getMaxId();
		if (id < Const.prizeIdStart.longValue()) {
			id = Const.prizeIdStart;
		} else {
			id += 1;
		}

		Date curDate = new Date();

		vo.setId(String.valueOf(id));
		vo.setCreateTime(new Timestamp(curDate.getTime()));
		return prizeDao.savePrize(vo);
	}

	@Override
	public PageInfo getPrizesPage(PageInfo page, String keyword) {
		List<PrizeVO> results = prizeDao.getPrizesPage(page, keyword);
		page.setData(results);
		return page;
	}

	@Override
	public boolean updatePrize(PrizeVO vo) {
		if (vo == null || vo.getId() == null) {
			return false;
		}
		return prizeDao.updatePrize(vo);
	}

	@Override
	public boolean deletePrize(String ids) {
		if (StringUtils.isEmpty(ids)) {
			return false;
		}
		String[] idArray = ids.split(",");
		return prizeDao.deletePrize(idArray);
	}

	@Override
	public boolean judgePrizeDraw() {
		Date curDate = new Date();
		Timestamp curTime = new Timestamp(curDate.getTime());
		boolean result = prizeDao.judgePrizeDraw(curTime);
		return result;
	}

	@Override
	public synchronized JSONObject prizeDraw(String userId) {
		double random = Math.random();
		Map<String, Object> prizeMap = prizeDao.isBingo(random);
		if(prizeMap == null || prizeMap.isEmpty()) {
			return null;
		}
		
		String prize = "";
		String prizeId = (String) prizeMap.get("prizeId");
		Integer prizeNo = (Integer) prizeMap.get("prizeNo");
		String prizeName = (String) prizeMap.get("prizeName");
		Integer prizeCount = (Integer) prizeMap.get("prizeCount");
		Integer prizeSum = (Integer) prizeMap.get("prizeSum");
		if(prizeSum.intValue() <= 0) {
			return null;
		}
		
		switch (prizeNo.intValue()) {
		case 0:
			prize = "ore_sum";
			break;
		case 1:
			prize = "yb_sum";
			break;
		case 2:
			prize = "csb_sum";
			break;
		default:
			break;
		}
		
		JSONObject jo = null;
		boolean result = prizeDao.updateUserPrize(prize, userId, prizeCount);
		
		if(result) {
			boolean updatePrizeCountRst = prizeDao.updatePrizeCount(prizeId, prizeCount);
			if(updatePrizeCountRst) {
				jo = new JSONObject();
				jo.put("prizeName", prizeName);
				jo.put("prizeCount", prizeCount);
				return jo;
			}
		}
		return jo;
	}

}
