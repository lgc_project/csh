package com.lianbao.bo;

import com.lianbao.vo.GameProVO;

import net.sf.json.JSONObject;

/**
* @ClassName
* @Description
* @author 
* @date 2018年7月21日 下午7:46:49
*/
public interface GameProBo {

	boolean saveOrUpdate(GameProVO vo);
	
	JSONObject getGamePro();
}
