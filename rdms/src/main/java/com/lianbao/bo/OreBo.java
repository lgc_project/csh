/**
 * 
 */
package com.lianbao.bo;

import java.sql.Timestamp;

import com.vrc.vo.common.PageInfo;

/**
* @ClassName
* @Description
* @author 
* @date 2018年7月23日 上午10:58:03
*/
public interface OreBo {
	
	PageInfo getOrePage(PageInfo page, String keyword, Timestamp startTime, Timestamp endTime);
}
