package com.lianbao.bo;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
* @ClassName
* @Description
* @author 
* @date 2018年8月8日 下午11:11:29
*/
public interface OreTradeBo {

	/**
	 * 矿石挂单
	 * 
	 * @param userId 挂单的买家
	 * @param count 数量
	 * @param unitPrice 单价
	 * @param level 买家等级（前端会判断，后台不需要做处理）
	 * @return
	 */
	boolean orePostOdd(String userId, double count, double unitPrice, String level);
	
	/**
	 * 卖家第一次确认
	 * 
	 * @param userId
	 * @param oddId
	 * @return
	 */
	JSONObject sellFirstSure(String userId, String oddId);
	
	/**
	 * userId为买家时，则买家确认交易，修改挂单状态为2
	 * userId为卖家时，则卖家第二次确认交易，修改挂单状态为3
	 * 
	 * @param userId
	 * @param oddId
	 * @return
	 */
	JSONObject sureOreTrade(String userId, String oddId);
	
	/**
	 * 撤销交易
	 * 
	 * @param oddId
	 * @return
	 */
	JSONObject cancelOdd(String oddId);
	
	/**
	 * 矿石点对点交易
	 * 
	 * @param buyId
	 * @param sellId
	 * @param count
	 * @param price
	 * @return
	 */
	JSONObject orePointSell(String buyId, String sellId, double count, double price);

	/**
	 * 信箱
	 * 
	 * @param userId
	 * @param level
	 * @return
	 */
	JSONArray oreMailBox(String userId, String level);
	
	/**
	 * 获取挂单列表
	 * 
	 * @param userId
	 * @param level
	 * @return
	 */
	JSONArray getOreGuaDanList(String userId, String level);

	/**
	 * 获取矿石交易手续费提示信息
	 * 
	 * @return
	 */
	JSONObject getOrePoundageMsg();
}
