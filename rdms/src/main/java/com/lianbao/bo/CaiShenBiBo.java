/**
 * 
 */
package com.lianbao.bo;

import java.sql.Timestamp;

import com.lianbao.vo.CaiShenBiVO;
import com.vrc.vo.common.PageInfo;

import net.sf.json.JSONObject;

/**
* @ClassName
* @Description
* @author 
* @date 2018年6月21日 上午10:50:38
*/
public interface CaiShenBiBo {
	
	/**
	 * 获取财神币列表
	 * 
	 * @param page
	 * @param keyword
	 * @return
	 */
	public PageInfo getCaiShenBisPage(PageInfo page, String keyword);

	/**
	 * 添加财神币
	 * 
	 * @param vo
	 * @return
	 */
	public boolean saveCaiShenBi(CaiShenBiVO vo);
	
	/**
	 * 修改财神币
	 * 
	 * @param vo
	 * @return
	 */
	public boolean updateCaiShenBi(CaiShenBiVO vo);

	/**
	 * 批量删除财神币
	 * 
	 * @param ids
	 * @return
	 */
	public boolean deleteCaiShenBi(String ids);
	
	/**
	 * 买入
	 * 
	 * @param userId
	 * @param amount
	 * @return
	 */
	public JSONObject sellIn(String userId, double amount, double unitPrice);

	/**
	 * 卖出
	 * 
	 * @param buyId
	 * @param sellId
	 * @param amount
	 * @param unitPrice
	 * @return
	 */
	public JSONObject sellOut(String buyId, String sellId, double amount, double unitPrice);
	
	/**
	 * 确认交易
	 * 
	 * @param userId
	 * @param oddId
	 * @return
	 */
	public JSONObject sureSell(String userId, String oddId);
	
	/**
	 * 取消交易
	 * 
	 * @param oddId
	 * @return
	 */
	public JSONObject cancelOdd(String oddId);
	
	/**
	 * 五个矿石交换一个财神币
	 * 
	 * @param userId
	 * @param oreSum
	 * @return
	 */
	public JSONObject exchangeCsb(String userId, double oreSum);
	
	/**
	 * 回收财神币
	 * 
	 * @param userId
	 * @param csbSum
	 * @return
	 */
	public JSONObject recoverCsb(String userId, double csbAmount);
	
	/**
	 * 回收财神币手续费提示信息
	 * 
	 * @return
	 */
	public JSONObject recoverCsbTips();
	
	/**
	 * 获取系统回收列表
	 * 
	 * @param page
	 * @param userId
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public PageInfo getCSBRecoverInfoPage(PageInfo page, String keyword, Timestamp startTime, Timestamp endTime);

	/**
	 * 获取财神币交易记录
	 * 
	 * @return
	 */
	public JSONObject getCSBTradeInfo();
}
