package com.lianbao.vo;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
* @ClassName
* @Description
* @author 
* @date 2018年6月18日 下午2:53:01
*/
public class AuctionUserVO {

	private String id;
	private String userId;
	private String auctionId;
	private BigDecimal auctionAmount;
	private Timestamp createTime;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getAuctionId() {
		return auctionId;
	}
	public void setAuctionId(String auctionId) {
		this.auctionId = auctionId;
	}
	public BigDecimal getAuctionAmount() {
		return auctionAmount;
	}
	public void setAuctionAmount(BigDecimal auctionAmount) {
		this.auctionAmount = auctionAmount;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	
}
