/**
 * 
 */
package com.lianbao.vo;

import java.sql.Timestamp;

/**
* @ClassName 奖品实体类
* @Description 记录奖品信息
* @author LGC
* @date 2018年6月11日 上午11:34:31
*/
public class PrizeVO {

	private String id;
	private String prizeName;
	private int prizeNo;
	private int prizeSum; // 奖品总数
	private int prizeCount; // 抽奖数量
	private double prizeProb;
	private Timestamp beginTime;
	private Timestamp endTime;
	private Timestamp createTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPrizeName() {
		return prizeName;
	}

	public void setPrizeName(String prizeName) {
		this.prizeName = prizeName;
	}
	
	public int getPrizeNo() {
		return prizeNo;
	}

	public void setPrizeNo(int prizeNo) {
		this.prizeNo = prizeNo;
	}

	public int getPrizeSum() {
		return prizeSum;
	}

	public void setPrizeSum(int prizeSum) {
		this.prizeSum = prizeSum;
	}

	public int getPrizeCount() {
		return prizeCount;
	}

	public void setPrizeCount(int prizeCount) {
		this.prizeCount = prizeCount;
	}

	public double getPrizeProb() {
		return prizeProb;
	}

	public void setPrizeProb(double prizeProb) {
		this.prizeProb = prizeProb;
	}

	public Timestamp getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Timestamp beginTime) {
		this.beginTime = beginTime;
	}

	public Timestamp getEndTime() {
		return endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	
}
