package com.lianbao.vo;
/**
* @ClassName 矿石挂单表
* @Description
* @author LGC
* @date 2018年8月8日 下午10:35:00
*/
public class OreGuaDanVO {

	private String id;
	private String oddId;
	private String buyId;
	private String sellId;
	private double count; // 交易的矿石数量
	private double unitPrice; // 单价
	private double priceSum; // 总额
	private int status; // 交易状态：0：买家挂单 1：卖家第一次确认 2：买家确认 3：卖家第二次确认，成功交易 4：撤单
	private String guaDanTime; // 挂单时间
	private String operateTime; // 最新操作时间
	private int isValid; // 是否有效 0：无效 1：有效
	private String level; // 挂单等级 0：没有矿机或有免费矿机 1：新手 2：进阶 3：高手 4：王牌 5：国际
	private int type; // 挂单类型 0：买家挂单， 1：卖家挂单
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOddId() {
		return oddId;
	}
	public void setOddId(String oddId) {
		this.oddId = oddId;
	}
	public String getBuyId() {
		return buyId;
	}
	public void setBuyId(String buyId) {
		this.buyId = buyId;
	}
	public String getSellId() {
		return sellId;
	}
	public void setSellId(String sellId) {
		this.sellId = sellId;
	}
	public double getCount() {
		return count;
	}
	public void setCount(double count) {
		this.count = count;
	}
	public double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public double getPriceSum() {
		return priceSum;
	}
	public void setPriceSum(double priceSum) {
		this.priceSum = priceSum;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getGuaDanTime() {
		return guaDanTime;
	}
	public void setGuaDanTime(String guaDanTime) {
		this.guaDanTime = guaDanTime;
	}
	public String getOperateTime() {
		return operateTime;
	}
	public void setOperateTime(String operateTime) {
		this.operateTime = operateTime;
	}
	public int getIsValid() {
		return isValid;
	}
	public void setIsValid(int isValid) {
		this.isValid = isValid;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
}
