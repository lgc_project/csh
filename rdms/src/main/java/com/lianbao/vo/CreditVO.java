package com.lianbao.vo;
/**
* @ClassName
* @Description
* @author 
* @date 2018年8月20日 下午4:02:48
*/
public class CreditVO {

	private String id;
	private String creditNo;
	private String outTradeNo;
	private String userId;
	private String creditType; // 充值类型0：微信 1：支付宝
	private double creditMoney; // 充值金额
	private String creditTime; // 充值时间
	private String updateTime; // 更新状态时间
	private int status; // 支付状态0：取消支付 1：等待支付 2：支付成功
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreditNo() {
		return creditNo;
	}
	public void setCreditNo(String creditNo) {
		this.creditNo = creditNo;
	}
	public String getOutTradeNo() {
		return outTradeNo;
	}
	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getCreditType() {
		return creditType;
	}
	public void setCreditType(String creditType) {
		this.creditType = creditType;
	}
	public double getCreditMoney() {
		return creditMoney;
	}
	public void setCreditMoney(double creditMoney) {
		this.creditMoney = creditMoney;
	}
	public String getCreditTime() {
		return creditTime;
	}
	public void setCreditTime(String creditTime) {
		this.creditTime = creditTime;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
