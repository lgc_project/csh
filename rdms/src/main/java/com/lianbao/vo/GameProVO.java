package com.lianbao.vo;
/**
* @ClassName 游戏概率
* @Description 
* @author LGC
* @date 2018年7月21日 下午5:13:34
*/
public class GameProVO {

	private Long id;
	private Double caiQuanPro; // 猜拳游戏概率
	private Double shuiGuoLeYuanPro; // 水果乐园概率
	private Double xingYunDuoBaoPro; // 幸运夺宝概率
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Double getCaiQuanPro() {
		return caiQuanPro;
	}
	public void setCaiQuanPro(Double caiQuanPro) {
		this.caiQuanPro = caiQuanPro;
	}
	public Double getShuiGuoLeYuanPro() {
		return shuiGuoLeYuanPro;
	}
	public void setShuiGuoLeYuanPro(Double shuiGuoLeYuanPro) {
		this.shuiGuoLeYuanPro = shuiGuoLeYuanPro;
	}
	public Double getXingYunDuoBaoPro() {
		return xingYunDuoBaoPro;
	}
	public void setXingYunDuoBaoPro(Double xingYunDuoBaoPro) {
		this.xingYunDuoBaoPro = xingYunDuoBaoPro;
	}
	
	
}
