package com.lianbao.vo;

import java.sql.Timestamp;

/**
* @ClassName 财神币回收
* @Description
* @author 
* @date 2018年6月25日 下午7:36:59
*/
public class CSBRecoverVO {
	private String id;
	private String userId;
	private double csbSum;
	private double moneySum;
	private double poundage; // 手续费率
	private double moneyPd; // 手续费
	private Timestamp createTime;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public double getCsbSum() {
		return csbSum;
	}
	public void setCsbSum(double csbSum) {
		this.csbSum = csbSum;
	}
	public double getMoneySum() {
		return moneySum;
	}
	public void setMoneySum(double moneySum) {
		this.moneySum = moneySum;
	}
	public double getPoundage() {
		return poundage;
	}
	public void setPoundage(double poundage) {
		this.poundage = poundage;
	}
	public double getMoneyPd() {
		return moneyPd;
	}
	public void setMoneyPd(double moneyPd) {
		this.moneyPd = moneyPd;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	
}
