package com.lianbao.vo;
/**
* @ClassName 手续费详情
* @Description
* @author 
* @date 2018年8月23日 上午2:38:44
*/
public class PoundageDetailsVO {

	private String id;
	private String pdNo;
	private double tradePrice; // 交易金额
	private double tradePoundage; // 交易手续费率
	private double pdPrice; // 扣除的手续费
	private String userId;
	private String type; // 0：矿石交易 1：财神币交易 2：回收
	private String createTime;
	private String guadanType; // 0：矿石挂买 1：矿石挂卖 2：财神币挂买 3：财神币挂卖
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPdNo() {
		return pdNo;
	}
	public void setPdNo(String pdNo) {
		this.pdNo = pdNo;
	}
	public double getTradePrice() {
		return tradePrice;
	}
	public void setTradePrice(double tradePrice) {
		this.tradePrice = tradePrice;
	}
	public double getTradePoundage() {
		return tradePoundage;
	}
	public void setTradePoundage(double tradePoundage) {
		this.tradePoundage = tradePoundage;
	}
	public double getPdPrice() {
		return pdPrice;
	}
	public void setPdPrice(double pdPrice) {
		this.pdPrice = pdPrice;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getGuadanType() {
		return guadanType;
	}
	public void setGuadanType(String guadanType) {
		this.guadanType = guadanType;
	}
	
}
