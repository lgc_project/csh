package com.lianbao.vo;

import java.sql.Timestamp;

/**
* @ClassName 矿石合成系统
* @Description
* @author LGC
* @date 2018年7月23日 上午10:28:36
*/
public class OreVO {

	private Long id;
	private String userId;
	private String username;
	private double oreSum; // 合成时矿石数量
	private double csbSum; // 合成了财神币的数量 （5换1）
	private Timestamp createTime;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public double getOreSum() {
		return oreSum;
	}
	public void setOreSum(double oreSum) {
		this.oreSum = oreSum;
	}
	public double getCsbSum() {
		return csbSum;
	}
	public void setCsbSum(double csbSum) {
		this.csbSum = csbSum;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	
}
