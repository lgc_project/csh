package com.lianbao.vo;

import java.sql.Timestamp;

/**
* @ClassName 财神币账单
* @Description
* @author LGC
* @date 2018年6月22日 下午10:05:20
*/
public class CSBBillVO {

	public String id;
	public String type;
	public String userId;
	public Double price;
	public Timestamp createTime;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
}
