package com.lianbao.vo;

import java.sql.Timestamp;

/**
* @ClassName
* @Description
* @author 
* @date 2018年6月24日 下午11:02:39
*/
public class CSBGuaDanVO {
	private String id;
	private String oddId;
	private String buyId;
	private String sellId;
	private double count;
	private double price;
	private double priceSum;
	private int status; //交易状态：0：挂单中  1：卖家已确认  2：买家已确认  3：成功  4：撤单   5：取消交易
	private Timestamp createTime;
	private Timestamp updateTime;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOddId() {
		return oddId;
	}
	public void setOddId(String oddId) {
		this.oddId = oddId;
	}
	public String getBuyId() {
		return buyId;
	}
	public void setBuyId(String buyId) {
		this.buyId = buyId;
	}
	public String getSellId() {
		return sellId;
	}
	public void setSellId(String sellId) {
		this.sellId = sellId;
	}
	public double getCount() {
		return count;
	}
	public void setCount(double count) {
		this.count = count;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getPriceSum() {
		return priceSum;
	}
	public void setPriceSum(double priceSum) {
		this.priceSum = priceSum;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public Timestamp getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}
	
}
