package com.lianbao.vo;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
* @ClassName 竞拍活动实体类
* @Description
* @author LGC
* @date 2018年6月18日 上午1:15:40
*/
public class AuctionVO {

	private String id;
	private String auctionName;
	private BigDecimal auctionMoney;
	private BigDecimal auctionLowMoney;
	private Double auctionPoundage; 
	private Timestamp beginTime;
	private Timestamp endTime;
	private Timestamp createTime;
	private Timestamp updateTime;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAuctionName() {
		return auctionName;
	}
	public void setAuctionName(String auctionName) {
		this.auctionName = auctionName;
	}
	public BigDecimal getAuctionMoney() {
		return auctionMoney;
	}
	public void setAuctionMoney(BigDecimal auctionMoney) {
		this.auctionMoney = auctionMoney;
	}
	public BigDecimal getAuctionLowMoney() {
		return auctionLowMoney;
	}
	public void setAuctionLowMoney(BigDecimal auctionLowMoney) {
		this.auctionLowMoney = auctionLowMoney;
	}
	public Double getAuctionPoundage() {
		return auctionPoundage;
	}
	public void setAuctionPoundage(Double auctionPoundage) {
		this.auctionPoundage = auctionPoundage;
	}
	public Timestamp getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Timestamp beginTime) {
		this.beginTime = beginTime;
	}
	public Timestamp getEndTime() {
		return endTime;
	}
	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public Timestamp getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}
	
	
}
