package com.lianbao.vo;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
* @ClassName 财神币交易记录
* @Description
* @author 
* @date 2018年6月27日 上午11:39:32
*/
public class CSBTradeVO {

	private String id;
	private String oddNo;
	private String buyId;
	private double tradeCount; // 交易数量
	private double tradeUnitPrice; // 单价
	private double tradeMoneySum; // 总额
	private Timestamp createTime;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOddNo() {
		return oddNo;
	}
	public void setOddNo(String oddNo) {
		this.oddNo = oddNo;
	}
	public String getBuyId() {
		return buyId;
	}
	public void setBuyId(String buyId) {
		this.buyId = buyId;
	}
	public double getTradeCount() {
		return tradeCount;
	}
	public void setTradeCount(double tradeCount) {
		this.tradeCount = tradeCount;
	}
	public double getTradeUnitPrice() {
		return tradeUnitPrice;
	}
	public void setTradeUnitPrice(double tradeUnitPrice) {
		this.tradeUnitPrice = tradeUnitPrice;
	}
	public double getTradeMoneySum() {
		return tradeMoneySum;
	}
	public void setTradeMoneySum(double tradeMoneySum) {
		this.tradeMoneySum = tradeMoneySum;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	
}
