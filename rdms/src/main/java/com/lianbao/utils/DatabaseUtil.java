package com.lianbao.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
* @ClassName 数据库配置信息
* @Description
* @author LGC
* @date 2018年7月27日 上午11:30:45
*/
@Component
public class DatabaseUtil {

	@Value("${spring.datasource.druid.first.url}")
	private String url;
	
	@Value("${spring.datasource.druid.first.username}")
	private String username;
	
	@Value("${spring.datasource.druid.first.password}")
	private String password;

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	/**
	 * 从Url中解析出IP地址
	 * @return
	 */
	public String getHostIP() {
		String[] strs = url.split(":");
		if(strs == null || strs.length == 0) {
			return "";
		}
		return strs[2].replace("//", "");
	}
	
	/**
	 * 从Url中解析出数据库名称
	 * @return
	 */
	public String getDatabaseName() {
		return url.substring(url.lastIndexOf("/") + 1, url.indexOf("?"));
	}
	
	
}
