package com.lianbao.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lianbao.bo.OreTradeBo;
import com.vrc.common.Const;
import com.vrc.common.ResponseObject;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
* @ClassName 矿石交易相关类
* @Description
* @author LGC
* @date 2018年8月8日 下午10:31:53
*/
@Controller
@RequestMapping("oreTrade")
public class OreTradeController {

	@Resource
	private OreTradeBo oreTradeBo;
	
	/**
	 * 买家挂单
	 * 
	 * @param userId
	 * @param price
	 * @param count
	 * @param level
	 * @return
	 */
	@RequestMapping("orePostOdd")
	@ResponseBody
	public ResponseObject orePostOdd(String userId, double price, double count, String level) {
		boolean flag = oreTradeBo.orePostOdd(userId, count, price, level);
		if(flag) {
			return ResponseObject.editObject(Const.success, "挂单成功", null);
		}
		return ResponseObject.editObject(Const.failed, "挂单失败", null);
	}
	
	/**
	 * 卖家第一次确认
	 * 
	 * @param oddId
	 * @param userId
	 * @return
	 */
	@RequestMapping("sellFirstSure")
	@ResponseBody
	public ResponseObject sellFirstSure(String oddId, String userId) {
		JSONObject jo = oreTradeBo.sellFirstSure(userId, oddId);
		return ResponseObject.editObject(jo.getString("code"), jo.getString("codeDesc"), null);
	}
	
	/**
	 * 如果userId为买家，则修改挂单状态为2
	 * 如果userId为卖家，则交易成功
	 * 
	 * @param oddId
	 * @param userId
	 * @return
	 */
	@RequestMapping("sureOreTrade")
	@ResponseBody
	public JSONObject sureOreTrade(String oddId, String userId) {
		JSONObject jo = oreTradeBo.sureOreTrade(userId, oddId);
		return jo;
	}
	
	/**
	 * 撤销挂单
	 * 
	 * @param oddId
	 * @return
	 */
	@RequestMapping("cancelOreOdd")
	@ResponseBody
	public JSONObject cancelOdd(String oddId) {
		JSONObject jo = oreTradeBo.cancelOdd(oddId);
		return jo;
	}
	
	/**
	 * 矿石点对点交易
	 * 
	 * @param buyId
	 * @param sellId
	 * @param count
	 * @param price
	 * @return
	 */
	@RequestMapping("orePointSell")
	@ResponseBody
	public JSONObject orePointSell(String buyId, String sellId, double count, double price) {
		JSONObject jo = oreTradeBo.orePointSell(buyId, sellId, count, price);
		return jo;
	}
	
	/**
	 * 查看挂单列表
	 * 
	 * @param userId
	 * @param level
	 * @return
	 */
	@RequestMapping("oreOddList")
	@ResponseBody
	public ResponseObject oreOddList(String userId, String level) {
		JSONArray ja = oreTradeBo.getOreGuaDanList(userId, level);
		return ResponseObject.editObject(Const.success, "操作成功", ja);
	}
	
	/**
	 * 查看信箱
	 * 
	 * @param userId
	 * @param level
	 * @return
	 */
	@RequestMapping("oreMailBox")
	@ResponseBody
	public ResponseObject oreMailBox(String userId, String level) {
		JSONArray ja = oreTradeBo.oreMailBox(userId, level);
		return ResponseObject.editObject(Const.success, "操作成功", ja);
	}
	
	@RequestMapping("orePoundageMsg")
	@ResponseBody
	public ResponseObject orePoundageMsg() {
		JSONObject jo = oreTradeBo.getOrePoundageMsg();
		return ResponseObject.editObject(Const.success, "操作成功", jo);
	}
	
}
