package com.lianbao.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lianbao.bo.TakeCashBo;
import com.rdms.common.annotation.SysLog;
import com.vrc.common.ResponseObject;
import com.vrc.vo.common.PageInfo;

import net.sf.json.JSONObject;

/**
* @ClassName
* @Description
* @author 
* @date 2018年7月9日 上午10:05:18
*/
@Controller
@RequestMapping("takeCash")
public class TakeCashController {

	@Resource
	private TakeCashBo takeCashBo;

	@RequestMapping("getCashPage")
	@ResponseBody
	@SysLog("获取取现列表")
	public PageInfo getCashPage(PageInfo page, String keyword, String startTime, String endTime) {
		PageInfo p = takeCashBo.getCashPage(page, keyword, startTime, endTime);
		return p;
	}
	
	@RequestMapping("submitTakeCash")
	@ResponseBody
	public ResponseObject takeCash(String userId, Double cash) {
		JSONObject jo = takeCashBo.takeCash(userId, cash);
		return ResponseObject.editObject(jo.getString("code"), jo.getString("codeDesc"), null);
	}
	
	@RequestMapping("sureSubmit")
	@ResponseBody
	@SysLog("确认用户取现请求")
	public ResponseObject sureSubmit(String id, int status) {
		JSONObject jo = takeCashBo.sureSubmit(id, status);
		return ResponseObject.editObject(jo.getString("code"), jo.getString("codeDesc"), null);
	}
}
