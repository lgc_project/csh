package com.lianbao.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lianbao.bo.PoundageDetailsBo;
import com.vrc.common.Const;
import com.vrc.common.ResponseObject;
import com.vrc.vo.common.PageInfo;

/**
* @ClassName 交易扣手续费明细
* @Description
* @author 
* @date 2018年8月23日 上午9:11:08
*/
@RestController
@RequestMapping("poundageDetails")
public class PoundageDetailsController {

	@Resource
	private PoundageDetailsBo poundageDetailsBo;
	
	@RequestMapping("getPdPage")
	public PageInfo getPdPage(PageInfo page, String keyword, String startTime, String endTime, String type) {
		return poundageDetailsBo.getPoundageDetailsPage(page, keyword, startTime, endTime, type);
	}
	
	@RequestMapping("deletePd")
	public ResponseObject deletePd(String ids, String type) {
		if(poundageDetailsBo.deletePd(ids, type)) {
			return ResponseObject.editObject(Const.success, "操作成功", null);
		}
		return ResponseObject.editObject(Const.failed, "系统错误", null);
	}
}
