package com.lianbao.controller;

import java.sql.Timestamp;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lianbao.bo.CaiShenBiBo;
import com.lianbao.vo.CaiShenBiVO;
import com.rdms.common.annotation.SysLog;
import com.vrc.common.Const;
import com.vrc.common.ResponseObject;
import com.vrc.vo.common.PageInfo;

import net.sf.json.JSONObject;

/**
* @ClassName
* @Description
* @author 
* @date 2018年6月21日 上午11:14:02
*/
@Controller
@RequestMapping("caiShenBi")
public class CaiShenBiController {

	@Resource
	private CaiShenBiBo caiShenBiBo;

	@RequestMapping("saveCaiShenBi")
	@ResponseBody
	@SysLog("添加财神币")
	public ResponseObject saveCaiShenBi(CaiShenBiVO vo) {
		if(vo == null) {
			return ResponseObject.editObject(Const.failed, "保存失败", null);
		}
		
		boolean flag = caiShenBiBo.saveCaiShenBi(vo);
		if(flag) {
			return ResponseObject.editObject(Const.success, "保存成功", null);
		}
		return ResponseObject.editObject(Const.failed, "保存失败", null);
	}
	
	@RequestMapping("updateCaiShenBi")
	@ResponseBody
	@SysLog("修改财神币")
	public ResponseObject updateCaiShenBi(CaiShenBiVO vo) {
		boolean flag = caiShenBiBo.updateCaiShenBi(vo);
		if(flag) {
			return ResponseObject.editObject(Const.success, "修改成功", null);
		}
		return ResponseObject.editObject(Const.failed, "修改失败", null);
	}
	
	@RequestMapping("deleteCaiShenBi")
	@ResponseBody
	@SysLog("批量删除财神币")
	public ResponseObject deleteCaiShenBi(String ids) {
		boolean flag = caiShenBiBo.deleteCaiShenBi(ids);
		if(flag) {
			return ResponseObject.editObject(Const.success, "操作成功", null);
		}
		return ResponseObject.editObject(Const.failed, "操作失败", null);
	}
	
	@RequestMapping("getCaiShenBisPage")
	@ResponseBody
	@SysLog("获取财神币信息列表")
	public PageInfo getCaiShenBisPage(PageInfo page, String keyword) {
		PageInfo p = caiShenBiBo.getCaiShenBisPage(page, keyword);
		return p;
		
	}
	
	@RequestMapping("sellIn")
	@ResponseBody
	public ResponseObject sellIn(String userId, double amount, double unitPrice) {
		JSONObject jo = caiShenBiBo.sellIn(userId, amount, unitPrice);
		return ResponseObject.editObject(jo.getString("code"), jo.getString("codeDesc"), null);
	}
	
	@RequestMapping("sellOut")
	@ResponseBody
	public ResponseObject sellOut(String buyId, String sellId, double amount, double unitPrice) {
		JSONObject jo = caiShenBiBo.sellOut(buyId, sellId, amount, unitPrice);
		return ResponseObject.editObject(jo.getString("code"), jo.getString("codeDesc"), null);
	}
	
	@RequestMapping("sureSell")
	@ResponseBody
	public JSONObject sureSell(String userId, String oddId) {
		JSONObject jo = caiShenBiBo.sureSell(userId, oddId);
		return jo;
	}
	
	@RequestMapping("cancelOdd")
	@ResponseBody
	public JSONObject cancelOdd(String oddId) {
		JSONObject jo = caiShenBiBo.cancelOdd(oddId);
		return jo;
	}
	
	/**
	 * 矿石合成
	 * 
	 * @param userId
	 * @param oreSum
	 * @return
	 */
	@RequestMapping("exchangeCsb")
	@ResponseBody
	public JSONObject exchangeCsb(String userId, double oreSum) {
		JSONObject jo = caiShenBiBo.exchangeCsb(userId, oreSum);
		return jo;
	}
	
	/**
	 * 财神币回收
	 * 
	 * @param userId
	 * @param csbSum
	 * @return
	 */
	@RequestMapping("recoverCsb")
	@ResponseBody
	public JSONObject recoverCsb(String userId, double csbSum) {
		JSONObject jo = caiShenBiBo.recoverCsb(userId, csbSum);
		return jo;
	}
	
	@RequestMapping("recoverCsbTips")
	@ResponseBody
	public JSONObject recoverCsbTips() {
		return caiShenBiBo.recoverCsbTips();
	}
	
	@RequestMapping("getCsbRecoverInfo")
	@ResponseBody
	public PageInfo getCsbRecoverInfoPage(PageInfo page, String keyword, Timestamp startTime, Timestamp endTime) {
		PageInfo p = caiShenBiBo.getCSBRecoverInfoPage(page, keyword, startTime, endTime);
		return p;
	}
	
	@RequestMapping("getCsbTradeInfo")
	@ResponseBody
	public JSONObject getCsbTradeInfo() {
		JSONObject jo = caiShenBiBo.getCSBTradeInfo();
		return jo;
	}
}
