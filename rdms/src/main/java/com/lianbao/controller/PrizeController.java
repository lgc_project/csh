package com.lianbao.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lianbao.bo.PrizeBo;
import com.lianbao.vo.PrizeVO;
import com.rdms.common.annotation.SysLog;
import com.vrc.common.Const;
import com.vrc.common.ResponseObject;
import com.vrc.vo.common.PageInfo;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
* @ClassName
* @Description
* @author 
* @date 2018年6月11日 下午2:32:43
*/
@Controller
@RequestMapping("prize")
public class PrizeController {

	@Resource
	private PrizeBo prizeBo;
	
	@RequestMapping("savePrize")
	@ResponseBody
	@SysLog("添加奖品")
	public ResponseObject savePrize(PrizeVO vo) {
		if(vo == null) {
			return ResponseObject.editObject(Const.failed, "操作失败", null);
		}
		
		boolean flag = prizeBo.savePrize(vo);
		if(flag) {
			return ResponseObject.editObject(Const.success, "保存成功", null);
		} 
		return ResponseObject.editObject(Const.failed, "保存失败", null);
	}
	
	@RequestMapping("updatePrize")
	@ResponseBody
	@SysLog("修改奖品")
	public ResponseObject updatePrize(PrizeVO vo) {
		boolean flag = prizeBo.updatePrize(vo);
		if(flag) {
			return ResponseObject.editObject(Const.success, "修改成功", null);
		}
		return ResponseObject.editObject(Const.failed, "修改失败", null);
	}
	
	@RequestMapping("deletePrize")
	@ResponseBody
	@SysLog("批量删除奖品")
	public ResponseObject deletePrize(String ids) {
		boolean flag = prizeBo.deletePrize(ids);
		if(flag) {
			return ResponseObject.editObject(Const.success, "删除成功", null);
		} 
		return ResponseObject.editObject(Const.failed, "删除失败", null);
	}
	
	@RequestMapping("getPrizesPage")
	@ResponseBody
	@SysLog("获取奖品信息列表")
	public PageInfo getPrizesPage(PageInfo page, String keyword) {
		PageInfo p = prizeBo.getPrizesPage(page, keyword);
		return p;
	}
	
	@RequestMapping("canPrizeDraw")
	@ResponseBody
	public ResponseObject canPrizeDraw() {
		boolean result = prizeBo.judgePrizeDraw();
		if(result) {
			return ResponseObject.editObject(Const.success, "", null);
		}
		return ResponseObject.editObject(Const.failed, "目前不是抽奖时间", null);
	}
	
	@RequestMapping("prizeDraw")
	@ResponseBody
	public ResponseObject prizeDraw(String userId) {
		JSONObject result = prizeBo.prizeDraw(userId);
		if(result != null && !result.isEmpty()) {
			return ResponseObject.editObject(Const.success, "", result);
		}
		return ResponseObject.editObject(Const.failed, "", null);
	}
}
