package com.lianbao.controller;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.util.DruidDataSourceUtils;
import com.lianbao.utils.DatabaseUtil;
import com.vrc.common.Const;
import com.vrc.common.ResponseObject;
import com.vrc.util.DateUtils;

/**
* @ClassName 数据库备份与恢复
* @Description
* @author LGC
* @date 2018年7月27日 上午10:08:10
*/
@RestController
@RequestMapping("database")
public class DatabaseController {
	
	@Autowired
	private DatabaseUtil dbUtil;
	
	@RequestMapping("backup")
	public void backup(HttpServletRequest request, HttpServletResponse response) {
		// mysql 备份命令
		String sqlBackup = String.format("C:\\Program Files\\MySQL\\MySQL Server 5.7\\bin\\mysqldump -h%s -u%s -p%s %s", 
				dbUtil.getHostIP(), dbUtil.getUsername(), dbUtil.getPassword(), dbUtil.getDatabaseName());
		InputStream inputStream = null;
		OutputStream outputStream = null;
		try {
			Process process = Runtime.getRuntime().exec(sqlBackup);
			inputStream = process.getInputStream();
			
			String dbFileName = DateUtils.dateToString(new Date(), "yyyy-MM-dd") + ".sql";
			response.setCharacterEncoding(request.getCharacterEncoding());
			response.setContentType("application/octet-stream"); // 设置为二进制流
			response.setHeader("Content-Disposition", "attachment;filename=" + dbFileName);
			outputStream = response.getOutputStream();
			IOUtils.copy(inputStream, outputStream);
			response.flushBuffer();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				inputStream.close();
				outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	@RequestMapping("recover")
	public ResponseObject recover(@RequestParam("file") MultipartFile file) {
		// mysql 还原命令
		String sqlRecover = String.format("C:\\Program Files\\MySQL\\MySQL Server 5.7\\bin\\mysql -h%s -u%s -p%s %s", 
				dbUtil.getHostIP(), dbUtil.getUsername(), dbUtil.getPassword(), dbUtil.getDatabaseName());
		OutputStream outputStream = null;
		BufferedReader bf = null;
		OutputStreamWriter writer = null;
		try {
			Process process = Runtime.getRuntime().exec(sqlRecover);
			outputStream = process.getOutputStream();
			bf = new BufferedReader(new InputStreamReader(file.getInputStream(), "utf-8"));
			String line = null;
			StringBuffer sb = new StringBuffer();
			while((line = bf.readLine()) != null) {
				sb.append(line + "\r\n");
			}
			line = sb.toString();
			writer = new OutputStreamWriter(outputStream, "utf-8");
			writer.write(line);
			// 因为是远程还原，必须要flush，不然不会从缓存中读取写数据
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
			return ResponseObject.editObject(Const.failed, "系统错误", null);
		} finally {
			try {
				outputStream.close();
				bf.close();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return ResponseObject.editObject(Const.success, "操作成功", null);
	}
	
}
