package com.lianbao.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lianbao.bo.GameProBo;
import com.lianbao.vo.GameProVO;
import com.vrc.common.Const;
import com.vrc.common.ResponseObject;

import net.sf.json.JSONObject;

/**
* @ClassName
* @Description
* @author 
* @date 2018年7月21日 下午7:54:30
*/
@RestController
@RequestMapping("gamePro")
public class GameProController {

	@Resource
	private GameProBo gameProBo;
	
	@RequestMapping("saveOrUpdate")
	public ResponseObject saveOrUpdateGamePro(GameProVO vo) {
		boolean flag = gameProBo.saveOrUpdate(vo);
		if(flag) {
			return ResponseObject.editObject(Const.success, "操作成功", null);
		} 
		return ResponseObject.editObject(Const.failed, "系统错误", null);
	}
	
	@RequestMapping("getGamePro")
	public ResponseObject getGamePro() {
		JSONObject jo = gameProBo.getGamePro();
		return ResponseObject.editObject(jo.getString("code"), jo.getString("codeDesc"), jo.get("data"));
	}
}
