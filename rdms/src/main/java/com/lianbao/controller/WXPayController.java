package com.lianbao.controller;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.druid.util.StringUtils;
import com.lianbao.bo.CreditBo;
import com.lianbao.utils.HttpRequest;
import com.lianbao.utils.WXPayUtil;
import com.lianbao.vo.CreditVO;
import com.vrc.common.Const;
import com.vrc.common.ResponseObject;
import com.vrc.common.WXPayConfig;
import com.vrc.util.DateUtils;

import net.sf.json.JSONObject;

/**
* @ClassName 微信支付
* @Description
* @author LGC
* @date 2018年8月20日 上午10:49:55
*/
@Controller
@RequestMapping("wxpay")
public class WXPayController {
	
	private static Logger logger = LoggerFactory.getLogger(WXPayController.class);
	

	@Resource
	private CreditBo creditBo;
	
	/**
	 * 统一下单
	 * 
	 * @param userId
	 * @param payMoney
	 * @return
	 */
	@RequestMapping("getWxPayOrder")
	@ResponseBody
	public ResponseObject getWxPayOrder(HttpServletRequest request, String userId, double payMoney) {
		logger.info("-------------------------------getWxPayOrder start-------------------------------------");
		Date curDate = new Date();
		Timestamp curTime = new Timestamp(curDate.getTime());
		logger.info(String.valueOf(payMoney / 100));
		JSONObject jo = new JSONObject();
		String outTradeNo = "";
		try {
			String appId = WXPayConfig.appId;
			String mchId = WXPayConfig.mchId;
			String paternerKey = WXPayConfig.paternerKey;
			
			String body = "BaiYuanCredit";
			outTradeNo = Const.getOutTradeNo();
			String totalFee = String.valueOf((int)payMoney);
//			String spbillCreateIp = HttpRequest.getAddressIp(request);
			String spbillCreateIp = "114.115.223.199";
			String notifyUrl = "https://chaintreasure.cn/lianbao/wxpay/notify";
			logger.info("IP:     " + spbillCreateIp);
			logger.info("notifyUrl:    " + notifyUrl);
			String tradeType = "APP";
			Map<String, String> params = new HashMap<String, String>(); 
			params.put("appid", appId);
			params.put("mch_id", mchId);
			params.put("nonce_str", WXPayUtil.generateNonceStr());
			params.put("body", body);
			params.put("out_trade_no", outTradeNo);
			params.put("total_fee", totalFee);
			params.put("spbill_create_ip", spbillCreateIp);
			params.put("notify_url", notifyUrl);
			params.put("trade_type", tradeType);
			String sign = WXPayUtil.generateSignature(params, paternerKey);
			params.put("sign", sign);
			
			String reqParam = WXPayUtil.mapToXml(params);
			String resMsg = HttpRequest.sendPost("https://api.mch.weixin.qq.com/pay/unifiedorder", reqParam);
			logger.info(resMsg);
			if(StringUtils.isEmpty(resMsg)) {
				return ResponseObject.editObject("201", "请求接口错误", null);
			}
			
			Map<String, String> retMap = WXPayUtil.xmlToMap(resMsg);
			if(!retMap.get("return_code").equals("SUCCESS")) {
				logger.info(retMap.get("return_msg"));
				return ResponseObject.editObject("202", retMap.get("return_msg"), null);
			}
			
			// 判断签名是否正确
			boolean isSignValid = WXPayUtil.isSignatureValid(retMap, paternerKey);
			if(!isSignValid) {
				logger.info(retMap.get("SignValid is not true"));
				return ResponseObject.editObject("201", "请求接口错误", null);
			}
			// 再次签名（必须要有这一步）
			Map<String, String> startPayParams = new HashMap<String, String>();
			String startPayNonceStr = WXPayUtil.generateNonceStr();
			startPayParams.put("appid", appId);
			startPayParams.put("partnerid", mchId);
			startPayParams.put("prepayid", retMap.get("prepay_id"));
			startPayParams.put("package", "Sign=WXPay");
			startPayParams.put("noncestr", startPayNonceStr);
			startPayParams.put("timestamp", String.valueOf(curTime.getTime() / 1000));
			String startPaySign = WXPayUtil.generateSignature(startPayParams, paternerKey);
			
			jo.put("appid", appId);
			jo.put("partnerid", mchId);
			jo.put("prepayid", retMap.get("prepay_id"));
			jo.put("package", "Sign=WXPay");
			jo.put("noncestr", startPayNonceStr);
			jo.put("timestamp", curTime.getTime() / 1000);
			jo.put("timestamp2", curTime.getTime());
			jo.put("sign", startPaySign);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		boolean flag = creditBo.addCreditVO(userId, payMoney / 100, outTradeNo, curDate);
		if(!flag) {
			logger.info("fail to add credit");
			return ResponseObject.editObject(Const.failed, "系统错误", null);
		}
		logger.info("-------------------------------getWxPayOrder end-------------------------------------");
		return ResponseObject.editObject("200", "操作成功", jo);
	}
	
	@RequestMapping("notify")
	@ResponseBody
	public String notify(HttpServletRequest request, HttpServletResponse response) {
		Map<String, String> resParams = new HashMap<String, String>();
		logger.info("-------------------------------微信回调函数开始-------------------------------------");
		try {
			InputStream inputStream = request.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
			String line = null;
			StringBuffer sb = new StringBuffer();
			while((line = br.readLine()) != null) {
				sb.append(line);
			}
	        br.close();
	        inputStream.close();
			logger.info("微信回调函数返回值：" + sb.toString());
			Map<String, String> retParams = WXPayUtil.xmlToMap(sb.toString());
			if(retParams.get("return_code").equals("SUCCESS")) { // 返回成功
				if(retParams.get("result_code").equals("SUCCESS")) { // 业务返回成功
					if(WXPayUtil.isSignatureValid(retParams, WXPayConfig.paternerKey)) { // 验证签名是否正确
						String outTradeNo = retParams.get("out_trade_no");
						CreditVO credit = creditBo.getCreditVO(outTradeNo);
						double creditMoney = credit.getCreditMoney() * 100;
						String totalFee = retParams.get("total_fee");
						if(creditMoney == Double.valueOf(totalFee).doubleValue()) {
							String timeEnd = retParams.get("time_end");
							Date updateDate = DateUtils.stringToDate(timeEnd, "yyyy-MM-dd HH:mm:ss");
							credit.setUpdateTime(DateUtils.dateToString(updateDate));
							boolean flag = creditBo.updateCreditVO(credit);
							if(flag) {
								resParams.put("return_code", "SUCCESS");
								resParams.put("return_msg", "OK");
								return WXPayUtil.mapToXml(resParams);
							}
						}
					} else {
						resParams.put("return_code", "FAIL");
						resParams.put("return_msg", "签名验证失败");
						return WXPayUtil.mapToXml(resParams);
					}
				}
			}
			
			resParams.put("return_code", "FAIL");
			resParams.put("return_msg", "参数验证失败");
			return WXPayUtil.mapToXml(resParams);
		} catch (Exception e) {
			e.printStackTrace();
			try {
				resParams.put("return_code", "FAIL");
				resParams.put("return_msg", e.getMessage());
				return WXPayUtil.mapToXml(resParams);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
        
		logger.info("-------------------------------微信回调函数结束-------------------------------------");
        
		return null;
	}
}
