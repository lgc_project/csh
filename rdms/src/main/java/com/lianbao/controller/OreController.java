package com.lianbao.controller;

import java.sql.Timestamp;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lianbao.bo.OreBo;
import com.rdms.common.annotation.SysLog;
import com.vrc.vo.common.PageInfo;

/**
* @ClassName
* @Description
* @author 
* @date 2018年7月23日 上午11:18:00
*/
@RestController
@RequestMapping("ore")
public class OreController {

	@Resource
	private OreBo oreBo;
	
	@RequestMapping("getOrePage")
	@SysLog("获取矿石合成列表")
	public PageInfo getOrePage(PageInfo page, String keyword, Timestamp startTime, Timestamp endTime) {
		PageInfo p = oreBo.getOrePage(page, keyword, startTime, endTime);
		return p;
	}
}
