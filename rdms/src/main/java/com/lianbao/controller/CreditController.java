package com.lianbao.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lianbao.bo.CreditBo;
import com.vrc.vo.common.PageInfo;

/**
* @ClassName 充值
* @Description
* @author LGC
* @date 2018年8月27日 上午10:28:55
*/
@RestController
@RequestMapping("credit")
public class CreditController {

	@Resource
	private CreditBo creditBo;
	
	@RequestMapping("getCreditPage")
	public PageInfo getCreditPage(PageInfo page, String keyword, String startTime, String endTime) {
		return creditBo.getCreditPage(page, keyword, startTime, endTime);
	}
	
}
