package com.lianbao.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lianbao.bo.OreSellerTradeBo;
import com.vrc.common.Const;
import com.vrc.common.ResponseObject;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
* @ClassName
* @Description
* @author 
* @date 2018年8月14日 下午6:08:21
*/
@Controller
@RequestMapping("oreSellerTrade")
public class OreSellerTradeController {

	@Resource
	private OreSellerTradeBo oreSellerTradeBo;
	
	/**
	 * 卖家挂单
	 * 
	 * @param userId
	 * @param price
	 * @param count
	 * @param level
	 * @return
	 */
	@RequestMapping("orePostOdd")
	@ResponseBody
	public JSONObject orePostOdd(String userId, double price, double count, String level) {
		JSONObject jo = oreSellerTradeBo.orePostOdd(userId, count, price, level);
		return jo;
	}
	
	/**
	 * 买家购入矿石
	 * 
	 * @param oddId
	 * @param userId
	 * @return
	 */
	@RequestMapping("oreBuy")
	@ResponseBody
	public JSONObject buy(String oddId, String userId) {
		JSONObject jo = oreSellerTradeBo.buy(userId, oddId);
		return jo;
	}
	
	/**
	 * 卖家二次确认，交易成功
	 * 
	 * @param oddId
	 * @param userId
	 * @return
	 */
	@RequestMapping("sureOreTrade")
	@ResponseBody
	public JSONObject sureOreTrade(String oddId, String userId) {
		JSONObject jo = oreSellerTradeBo.sureOreTrade(userId, oddId);
		return jo;
	}
	
	/**
	 * 撤销挂单
	 * 
	 * @param oddId
	 * @return
	 */
	@RequestMapping("cancelOreOdd")
	@ResponseBody
	public JSONObject cancelOdd(String oddId) {
		JSONObject jo = oreSellerTradeBo.cancelOdd(oddId);
		return jo;
	}
	
	/**
	 * 查看挂单列表
	 * 
	 * @param userId
	 * @param level
	 * @return
	 */
	@RequestMapping("oreOddList")
	@ResponseBody
	public ResponseObject oreOddList(String userId, String level) {
		JSONArray ja = oreSellerTradeBo.getOreGuaDanList(userId, level);
		return ResponseObject.editObject(Const.success, "操作成功", ja);
	}
	
	/**
	 * 查看信箱
	 * 
	 * @param userId
	 * @param level
	 * @return
	 */
	@RequestMapping("oreMailBox")
	@ResponseBody
	public ResponseObject oreMailBox(String userId, String level) {
		JSONArray ja = oreSellerTradeBo.oreMailBox(userId, level);
		return ResponseObject.editObject(Const.success, "操作成功", ja);
	}
}
