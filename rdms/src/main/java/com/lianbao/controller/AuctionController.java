package com.lianbao.controller;

import java.math.BigDecimal;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lianbao.bo.AuctionBo;
import com.lianbao.vo.AuctionVO;
import com.rdms.common.annotation.SysLog;
import com.vrc.common.Const;
import com.vrc.common.ResponseObject;
import com.vrc.vo.common.PageInfo;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
* @ClassName
* @Description
* @author 
* @date 2018年6月18日 上午2:10:22
*/
@Controller
@RequestMapping("auction")
public class AuctionController {

	@Resource
	private AuctionBo auctionBo;
	
	@RequestMapping("saveAuction")
	@ResponseBody
	@SysLog("添加奖品")
	public ResponseObject savePrize(AuctionVO vo) {
		if(vo == null) {
			return ResponseObject.editObject(Const.failed, "操作失败", null);
		}
		
		boolean flag = auctionBo.saveAuction(vo);
		if(flag) {
			return ResponseObject.editObject(Const.success, "保存成功", null);
		} 
		return ResponseObject.editObject(Const.failed, "保存失败", null);
	}
	
	@RequestMapping("updateAuction")
	@ResponseBody
	@SysLog("修改奖品")
	public ResponseObject updatePrize(AuctionVO vo) {
		boolean flag = auctionBo.updateAuction(vo);
		if(flag) {
			return ResponseObject.editObject(Const.success, "修改成功", null);
		}
		return ResponseObject.editObject(Const.failed, "修改失败", null);
	}
	
	@RequestMapping("deleteAuction")
	@ResponseBody
	@SysLog("批量删除奖品")
	public ResponseObject deletePrize(String ids) {
		boolean flag = auctionBo.deleteAuction(ids);
		if(flag) {
			return ResponseObject.editObject(Const.success, "删除成功", null);
		} 
		return ResponseObject.editObject(Const.failed, "删除失败", null);
	}
	
	@RequestMapping("getAuctionsPage")
	@ResponseBody
	@SysLog("获取奖品信息列表")
	public PageInfo getPrizesPage(PageInfo page, String keyword) {
		PageInfo p = auctionBo.getAuctionsPage(page, keyword);
		return p;
	}

	@RequestMapping("judgeIsAuction")
	@ResponseBody
	public ResponseObject judgeIsAuction() {
		JSONObject jo = auctionBo.judgeIsAuction();
		if(jo == null) {
			return ResponseObject.editObject("202", "没有竞拍活动", null);
		}
		
		if(jo.containsKey("curTime")) {
			return ResponseObject.editObject(Const.success, "操作成功", jo);
		} else if(jo.containsKey("beginTime")) {
			return ResponseObject.editObject(Const.failed, "不在竞拍时间", jo);
		}
		return null;
	}
	
	@RequestMapping("getTop5AuctionInfo")
	@ResponseBody
	public ResponseObject getTop5AuctionInfo(String auctionId) {
		JSONArray ja = auctionBo.getTop5AuctionInfo(auctionId);
		if(ja != null && !ja.isEmpty()) {
			return ResponseObject.editObject(Const.success, "", ja);
		}
		return ResponseObject.editObject(Const.failed, "", null);
	}
	
	@RequestMapping("addAuctionInfo")
	@ResponseBody
	public ResponseObject addAuctionInfo(String userId, String auctionId, BigDecimal auctionAmount) {
		boolean flag = auctionBo.addAuctionInfo(userId, auctionId, auctionAmount);
		if(flag) {
			return ResponseObject.editObject(Const.success, "", null);
		}
		return ResponseObject.editObject(Const.failed, "", null);
	}
}
