package com.rdms.config;

import com.rdms.common.xss.XssFilter;
import com.vrc.filter.TokenFilter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.DelegatingFilterProxy;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.DispatcherType;

/**
 * Filter配置
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-04-21 21:56
 */
@Configuration
public class FilterConfig {

    @Bean
    public FilterRegistrationBean shiroFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new DelegatingFilterProxy("shiroFilter"));
        //该值缺省为false，表示生命周期由SpringApplicationContext管理，设置为true则表示由ServletContainer管理
        registration.addInitParameter("targetFilterLifecycle", "true");
        registration.setEnabled(true);
        registration.setOrder(Integer.MAX_VALUE - 1);
        registration.addUrlPatterns("/*");
        return registration;
    }

    @Bean
    public FilterRegistrationBean xssFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        
        registration.setDispatcherTypes(DispatcherType.REQUEST);
        registration.setFilter(new XssFilter());
        registration.setName("xssFilter");
        registration.setOrder(Integer.MAX_VALUE);
        
        registration.addUrlPatterns("/*");
        return registration;
    }
    
    /**
     * 会员请求校验，用来控制单终端登录
     * @return
     */
    @Bean
    public FilterRegistrationBean tokenFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        
        registration.setFilter(new TokenFilter());
        registration.setName("tokenFilter");
        registration.setOrder(Integer.MAX_VALUE-2);
        
        List<String> list = new ArrayList<>();
        list.add("/machine/machineType");
//        list.add("/machine/buyMachine");
//        list.add("/machine/machineList");
//        list.add("/machine/maintenanceMachine");
        
        list.add("/trade/postOdd");
        list.add("/trade/sell");
        list.add("/trade/pointSell");
//        list.add("/trade/oddList");
        list.add("/trade/mailBox");
        list.add("/user/setTradePassword");
        list.add("/trade/cancelOdd");
        list.add("/trade/sureTrade");
//        list.add("/trade/getBillList");
//        list.add("/user/getUserInfo");
        list.add("/user/changeTradePass");
        list.add("/user/changeLoginPass");
//        list.add("/user/resetLoginPass");
//        list.add("/user/resetTradePass");
        list.add("/user/getUserPool");
        list.add("/user/setWeixinAccount");
        list.add("/user/setAlipayAccount");
        registration.setUrlPatterns(list);
        return registration;
    }
}
