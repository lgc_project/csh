package com.rdms.config;

import org.quartz.Trigger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import com.vrc.bo.job.JobService;

/**
 * 定时任务配置类
 * 
 * @author liboxing
 *
 */
@Configuration
public class QuartzConfigration {

	/**
	 * attention: Details：配置定时任务
	 */
	@Bean(name = "machineRunDay")
	public MethodInvokingJobDetailFactoryBean firstJobDetail(JobService jobService) {// ScheduleTask为需要执行的任务
		MethodInvokingJobDetailFactoryBean jobDetail = new MethodInvokingJobDetailFactoryBean();

		jobDetail.setConcurrent(false);
		jobDetail.setName("machineRunDayJob");// 设置任务的名字

		jobDetail.setTargetObject(jobService);
		jobDetail.setTargetMethod("machineRunDay");
		return jobDetail;
	}
	
    @Bean(name = "tradeSimpleJob")  
    public MethodInvokingJobDetailFactoryBean secondJobDetail(JobService jobService) {// ScheduleTask为需要执行的任务  
        MethodInvokingJobDetailFactoryBean jobDetail = new MethodInvokingJobDetailFactoryBean();  

        jobDetail.setConcurrent(false);  
        jobDetail.setName("tradeSimpleJob");// 设置任务的名字  
          
        jobDetail.setTargetObject(jobService);  
        jobDetail.setTargetMethod("tradeSimpleJob");
        return jobDetail;  
    } 
    
    @Bean(name = "userIncome")  
    public MethodInvokingJobDetailFactoryBean thirdJobDetail(JobService jobService) {// ScheduleTask为需要执行的任务  
        MethodInvokingJobDetailFactoryBean jobDetail = new MethodInvokingJobDetailFactoryBean();  

        jobDetail.setConcurrent(false);  
        jobDetail.setName("userIncomeJob");// 设置任务的名字  
          
        jobDetail.setTargetObject(jobService);  
        jobDetail.setTargetMethod("userIncome");
        return jobDetail;  
    }
    
	/**
	 * 每天00：13执行
	 */
	@Bean(name = "firstTrigger")
	public CronTriggerFactoryBean firstTrigger(@Qualifier("machineRunDay")MethodInvokingJobDetailFactoryBean firstJobDetail) {
		CronTriggerFactoryBean tigger = new CronTriggerFactoryBean();
		tigger.setJobDetail(firstJobDetail.getObject());
		tigger.setCronExpression("0 13 0 * * ?");// 初始时的cron表达式
		return tigger;
	}
	
	/**
	 * 每天00：10执行
	 */
	@Bean(name = "secondTrigger")
	public CronTriggerFactoryBean secondTrigger(@Qualifier("tradeSimpleJob")MethodInvokingJobDetailFactoryBean secondJobDetail) {
		CronTriggerFactoryBean tigger = new CronTriggerFactoryBean();
		tigger.setJobDetail(secondJobDetail.getObject());
		tigger.setCronExpression("0 10 0 * * ?");// 初始时的cron表达式
		return tigger;
	}
	
	/**
	 * 每天00：01执行
	 */
	@Bean(name = "thirdTrigger")
	public CronTriggerFactoryBean thirdTrigger(@Qualifier("userIncome")MethodInvokingJobDetailFactoryBean secondJobDetail) {
		CronTriggerFactoryBean tigger = new CronTriggerFactoryBean();
		tigger.setJobDetail(secondJobDetail.getObject());
		tigger.setCronExpression("0 1 0 * * ?");// 初始时的cron表达式
//		tigger.setCronExpression("0 * * * * ?"); // 每分钟执行
		return tigger;
	}
	
	/**
	 * attention: Details：定义quartz调度工厂
	 */
	@Bean(name = "scheduler")
	public SchedulerFactoryBean schedulerFactory(Trigger firstTrigger, Trigger secondTrigger, Trigger thirdTrigger) {
		SchedulerFactoryBean bean = new SchedulerFactoryBean();  
        // 延时启动，应用启动1秒后
        bean.setStartupDelay(1);  
        // 注册触发器
        bean.setTriggers(firstTrigger,secondTrigger,thirdTrigger);
//        bean.setTriggers(firstTrigger);
        return bean;
	}
}
