package com.rdms.modules.sys.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rdms.common.utils.R;
import com.rdms.modules.sys.dao.MemberTokenDao;
import com.rdms.modules.sys.dao.SysUserTokenDao;
import com.rdms.modules.sys.entity.SysUserTokenEntity;
import com.rdms.modules.sys.oauth2.TokenGenerator;
import com.rdms.modules.sys.service.MemberTokenService;


@Service("memberTokenService")
public class MemberTokenServiceImpl implements MemberTokenService {
	@Autowired
	private MemberTokenDao memberTokenDao;
	//12小时后过期
	private final static int EXPIRE = 3600 * 12;

	@Override
	public SysUserTokenEntity queryByUserId(Long userId) {
		return memberTokenDao.queryByUserId(userId);
	}

	@Override
	public SysUserTokenEntity queryByToken(String token) {
		return memberTokenDao.queryByToken(token);
	}

	@Override
	public void save(SysUserTokenEntity token){
		memberTokenDao.save(token);
	}
	
	@Override
	public void update(SysUserTokenEntity token){
		memberTokenDao.update(token);
	}

	@Override
	public String createToken(long userId) {
		//生成一个token
		String token = TokenGenerator.generateValue();

		//当前时间
		Date now = new Date();
		//过期时间
		Date expireTime = new Date(now.getTime() + EXPIRE * 1000);

		//判断是否生成过token
		SysUserTokenEntity tokenEntity = queryByUserId(userId);
		if(tokenEntity == null){
			tokenEntity = new SysUserTokenEntity();
			tokenEntity.setUserId(userId);
			tokenEntity.setToken(token);
			tokenEntity.setUpdateTime(now);
			tokenEntity.setExpireTime(expireTime);

			//保存token
			save(tokenEntity);
		}else{
			tokenEntity.setToken(token);
			tokenEntity.setUpdateTime(now);
			tokenEntity.setExpireTime(expireTime);

			//更新token
			update(tokenEntity);
		}

//		R r = R.ok().put("token", token).put("expire", EXPIRE);

		return token;
	}
}
