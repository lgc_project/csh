package com.vrc.common;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
* @ClassName 微信支付参数
* @Description
* @author LGC
* @date 2018年8月20日 上午10:43:42
*/
@Component
@ConfigurationProperties(prefix="wxpayconfig")
public class WXPayConfig {

	public static String appId;
	public static String appSecept;
	public static String mchId;
	public static String paternerKey;
	public static String getAppId() {
		return appId;
	}
	public static void setAppId(String appId) {
		WXPayConfig.appId = appId;
	}
	public static String getAppSecept() {
		return appSecept;
	}
	public static void setAppSecept(String appSecept) {
		WXPayConfig.appSecept = appSecept;
	}
	public static String getMchId() {
		return mchId;
	}
	public static void setMchId(String mchId) {
		WXPayConfig.mchId = mchId;
	}
	public static String getPaternerKey() {
		return paternerKey;
	}
	public static void setPaternerKey(String paternerKey) {
		WXPayConfig.paternerKey = paternerKey;
	}
}
