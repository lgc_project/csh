package com.vrc.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * 系统常量
 * @author libx
 *
 */
public class Const {

    public static String success = "200";
    
    public static String failed = "201";
    
    public static Long verifCodeTime = 300000L;   //验证码有效时间5分钟(5*60*1000)
    
    /**
     * 用户开始编号
     */
    public static Long userIdStart = 10000000L;
    
    /**
     * 单号开始编号
     */
    public static Long oddIdStart = 1800000L;
    
    /**
     * 矿机开始编号
     */
    public static Long machineNoStart = 100000L;
    /**
     * 商品开始编号
     */
    public static Long commodityIdStart = 10000L;
    /**
     * 商品交易中间表开始编号
     */
    public static Long commodityDealIdStart = 10000L;
    /**
     * 商品交易开始编号
     */
    public static Long dealIdStart = 10000L;
    
    public static Long prizeIdStart = 10000L;
    
    public static Long auctionIdStart = 10000L;
    
    public static Long auctionUserIdStart = 10000L;
    
    public static Long caiShenBiIdStart = 10000L;
    
    public static Long takeCashIdStart = 10000L;

    
    public static int oddPost = 0;          //挂单中
    public static int oddSellSure = 1;      //卖家首次确认
    public static int oddBuySure = 2;       //买家已确认
    public static int oddSuccess = 3;       //卖家二次确认，交易成功
    public static int oddRmove = 4;        //撤单
    public static int oddCancel = 5;       //取消交易
    
    public static int oreOddPost = 0;      // 矿石挂单中
    public static int oreOddSellSure = 1;  // 卖家首次确认
    public static int oreOddBuySure = 2;   // 买家确认
    public static int oreOddSuccess = 3;   // 卖家二次确认，交易成功
    public static int oreOddRemove = 4;    // 撤单
    public static int oreOddCancel = 5;    // 取消交易
    
    public static String billType_roolin = "1";  //账单类型：转入
    public static String billType_roolout = "2";  //账单类型：转出
    public static String billType_income = "3";  //账单类型：收益
    public static String billType_reward = "4";  //账单类型：奖励
    public static String billType_pay = "5";     //账单类型：支出
    public static String billType_freeze = "6";  //账单类型：冻结
    
    public static String actionType_roolin = "11";   //账单子类型：转入
    public static String actionType_roolout = "21";  //账单子类型：转出
    public static String actionType_fee = "22";      //账单子类型：手续费
    public static String actionType_income = "31";   //账单子类型：收益 
    public static String actionType_pushReward = "41";  //账单子类型：直推奖励
    public static String actionType_teamReward = "42";  //账单子类型：团队奖励
    public static String actionType_pay = "51";         //账单子类型：支出
    public static String actionType_freeze = "61";      //账单子类型：冻结
    public static String actionType_refreeze = "62";    //账单子类型：解冻
    
    public static int chartDayLength = 7;       //矿市中折线图的天数
    
    public static String giveMachineCode = "A0";  //赠送的矿机编码
    public static int ondDaySell = 3;           //一天最多只能卖3次
    
    public static double firstLevelPerc = 0.1;    //第一代奖励的比例
    public static double secondLevelPerc = 0.02;  //第二代奖励比例
    public static double thirdLevelPerc = 0.01;    //第三代奖励比例
    public static double fourthLevelPerc = 0.005;   //第四代奖励比例
    
    public static double poundage = 0.2;     //手续费
    
    public static double upamount = 0.01;      //价格上涨幅度
    
    public synchronized static String getOutTradeNo() {
    	Date date = new Date();
    	DateFormat format = new SimpleDateFormat("yyyyMMdd");
    	String time = format.format(date);
    	int hashCodeV = UUID.randomUUID().toString().hashCode();
    	if(hashCodeV < 0) {
    		hashCodeV = -hashCodeV;
    	}
    	
    	// 0 代表前面补充0
        // 6 代表长度为6
        // d 代表参数为正数型
    	return time + String.format("%06d", hashCodeV);
    	
    }
}
