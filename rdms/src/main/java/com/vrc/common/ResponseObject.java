package com.vrc.common;

/**
 * 返回数据实体
 * @author libx
 *
 */
public class ResponseObject {

    /**
     * 状态编码
     */
    private String code;
    
    /**
     * 状态信息
     */
    private String codeDesc;
    
    /**
     * 请求数据
     */
    private Object data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeDesc() {
        return codeDesc;
    }

    public void setCodeDesc(String codeDesc) {
        this.codeDesc = codeDesc;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
    
    /**
     * 返回实体格式化
     * 
     * @param code
     * @param codeMsg
     * @param data
     * @return
     */
    public static ResponseObject editObject(String code,String codeDesc,Object data ){
        ResponseObject requestObject = new ResponseObject();
        requestObject.setCode(code);
        requestObject.setCodeDesc(codeDesc);
        requestObject.setData(data);
        return requestObject;
    }
    
    @Override
    public String toString() {
        String str = "Code:"+getCode()+"\n";
        String str1 = "CodeMsg:"+getCodeDesc()+"\n";
        String str2 = "Data:"+getData();
        return str+str1+str2;
    }
}
