package com.vrc.common;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="sysconfig")
public class SysConfig {

	//阿里短信验证码接口
//	public static String aliProduct;
//	public static String aliDomain;
//	public static String aliAccessKeyId;
//	public static String aliAccessKeySecret;
//	public static String templateCode;
//	public static String signName;
	
	//四元素验证
	public static String bankAppKey;
	public static String bankAppSecret;
	public static String bankAppCode;
	public static String bankHost;
	public static String bankPath;
	
	//短信验证
	public static String messUrl;
	public static String messAppKey;
	public static String messAppSecret;
	public static String messTemplateId;
	public static String messSender;
	public static String messStatusCallBack;
	
	//安卓开发包和ios开发包的名称
	public static String androidName;
	public static String iosName;
	
	public static void setBankAppKey(String bankAppKey) {
		SysConfig.bankAppKey = bankAppKey;
	}
	public static void setBankAppSecret(String bankAppSecret) {
		SysConfig.bankAppSecret = bankAppSecret;
	}
	public static void setBankAppCode(String bankAppCode) {
		SysConfig.bankAppCode = bankAppCode;
	}
	public static void setBankHost(String bankHost) {
		SysConfig.bankHost = bankHost;
	}
	public static void setBankPath(String bankPath) {
		SysConfig.bankPath = bankPath;
	}
	public static String getMessUrl() {
		return messUrl;
	}
	public static void setMessUrl(String messUrl) {
		SysConfig.messUrl = messUrl;
	}
	public static String getMessAppKey() {
		return messAppKey;
	}
	public static void setMessAppKey(String messAppKey) {
		SysConfig.messAppKey = messAppKey;
	}
	public static String getMessAppSecret() {
		return messAppSecret;
	}
	public static void setMessAppSecret(String messAppSecret) {
		SysConfig.messAppSecret = messAppSecret;
	}
	public static String getMessTemplateId() {
		return messTemplateId;
	}
	public static void setMessTemplateId(String messTemplateId) {
		SysConfig.messTemplateId = messTemplateId;
	}
	public static String getMessSender() {
		return messSender;
	}
	public static void setMessSender(String messSender) {
		SysConfig.messSender = messSender;
	}
	public static String getMessStatusCallBack() {
		return messStatusCallBack;
	}
	public static void setMessStatusCallBack(String messStatusCallBack) {
		SysConfig.messStatusCallBack = messStatusCallBack;
	}
	public static void setAndroidName(String androidName) {
		SysConfig.androidName = androidName;
	}
	public static void setIosName(String iosName) {
		SysConfig.iosName = iosName;
	}
	
}
