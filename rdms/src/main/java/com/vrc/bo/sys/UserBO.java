package com.vrc.bo.sys;

import com.vrc.vo.common.PageInfo;
import com.vrc.vo.sys.UserVO;

import net.sf.json.JSONObject;

/**
 * 用户管理BO
 * @author libx
 *
 */
public interface UserBO {

    /**
     * 判断电话号码是否存在
     * @param phone
     * @return
     */
    public boolean hasPhone(String phone);
    
    /**
     * 判断用户名是否存在
     * @param username
     * @return
     */
    public boolean hasUserName(String username);
    
    /**
     * 注册用户
     * @param user
     * @return
     */
    public boolean register(UserVO user);
    
    /**
     * 获取用户
     * @param phone
     * @return
     */
    public UserVO getUser(String phone, String password);
    
    /**
     * 设置交易密码
     * @param userId
     * @param password
     * @return
     */
    public boolean updateTradePass(String userId, String password);
    
    /**
     * 修改交易密码
     * @param userId
     * @param oldPass
     * @param newPass
     * @return
     */
    public JSONObject changeTradePass(String userId, String oldPass, String newPass);
    
    /**
     * 修改登录密码
     * @param userId
     * @param oldPass
     * @param newPass
     * @return
     */
    public JSONObject changeLoginPass(String userId, String oldPass, String newPass);
    
    /**
     * 校验交易密码
     * @param userId
     * @param passwrd
     * @return
     */
    public boolean checkTradePass(String userId, String password);
    
    /**
     * 获取用户信息
     * @param userId
     * @return
     */
    public JSONObject getUserInfo(String userId);
    
    /**
     * 获取用户矿池
     * @param userId
     * @return
     */
    public JSONObject getUserPool(String userId);
    
    /**
     * 重设登录密码
     * @param phone
     * @param password
     * @return
     */
    public JSONObject resetLoginPass(String phone, String password);
    
    /**
     * 重设交易密码
     * @param phone
     * @param password
     * @return
     */
    public JSONObject resetTradePass(String phone, String password);
    
    /**
     * 实名认证后，更新用户的真实信息
     * @param user
     * @return
     */
    public JSONObject updateRealInfo(String userId, String info);
    
    /**
     * 分页查询会员
     * @param page
     * @param keyword
     * @return
     */
    public PageInfo getUserPage(PageInfo page, String keyword);
    
    /**
     * 获取直推列表
     * 
     * @param page
     * @param parentId
     * @return
     */
    public PageInfo getChildUserPage(PageInfo page, String parentId);
    
    /**
     * 后台手动添加会员
     * @param vo
     * @return
     */
    public boolean saveUser(UserVO vo);
    
    /**
     * 后台手动修改会员信息
     * @param vo
     * @return
     */
    public boolean updateUserInfo(UserVO vo);
    
    /**
     * 删除会员
     * @param ids
     * @return
     */
    public boolean deleteUser(String ids);
    
    /**
     * 设置微信账号
     * @param id
     * @param account
     * @return
     */
    public boolean setWeixinAccount(String id, String account);
    
    /**
     * 设置支付宝账号
     * @param id
     * @param account
     * @return
     */
    public boolean setAlipayAccount(String id, String account);
    
    /**
     * 认证用户
     * @param userId
     * @param name
     * @param bankcard
     * @param idcard
     * @param phone
     * @return
     */
    public JSONObject authenUser(String userId, String name, String bankcard,String bankName,String idcard,String phone);
    
    /**
     * 
     * @Title: updateMoneySum
     * @Description:更新用户总资产
     * @author: MDS
     * @param bool 判断用户输赢
     * @param money 一次交易的金额
     * @param userId 用户编号
     * @return
     */
	public boolean updateMoneySum(boolean bool, double money,Long userId);
	
	 /** 冻结或解冻用户
     * @param userId
     * @param isvalid
     * @return
     */
    public boolean freOrUnfreUser(String userId, String isvalid);
	
}
