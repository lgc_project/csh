package com.vrc.bo.sys;

import java.util.Map;

import net.sf.json.JSONObject;

/**
 * 系统设置bo
 * @author liboxing
 *
 */
public interface VrcConfigBO {

	/**
	 * 获取所有参数设置
	 * @return
	 */
	public JSONObject getAllConfig();
	
	/**
	 * 保存参数设置
	 * @return
	 */
	public void saveOrUpdate(Map<String, String> map);
}
