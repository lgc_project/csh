package com.vrc.bo.sys.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.vrc.bo.sys.VrcConfigBO;
import com.vrc.dao.sys.VrcConfigDAO;

import net.sf.json.JSONObject;

/**
 * 系统设置表bo
 * @author liboxing
 *
 */
@Service("vrcConfigBO")
public class VrcConfigBOImpl implements VrcConfigBO {

	@Resource
	private VrcConfigDAO vrcConfigDAO;
	
	@Override
	public JSONObject getAllConfig() {
		JSONObject jo = vrcConfigDAO.getAllConfig();
		return jo;
	}

	@Override
	public void saveOrUpdate(Map<String, String> map) {
		if(map != null){
			for(String key : map.keySet()){
				System.out.println("updateConfig :"+key+"="+map.get(key));
				vrcConfigDAO.saveOrUpdate(key, map.get(key));
			}
		}
	}

}
