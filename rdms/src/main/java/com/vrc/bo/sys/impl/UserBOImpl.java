package com.vrc.bo.sys.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vrc.bo.sys.UserBO;
import com.vrc.common.Const;
import com.vrc.dao.machine.MachineDAO;
import com.vrc.dao.sys.UserDAO;
import com.vrc.dao.sys.VrcConfigDAO;
import com.vrc.util.KeyUtil;
import com.vrc.util.StringUtils;
import com.vrc.vo.common.PageInfo;
import com.vrc.vo.machine.MachineTypeVO;
import com.vrc.vo.machine.MachineVO;
import com.vrc.vo.sys.UserVO;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 用户管理BO
 * @author libx
 *
 */
@Service("userBO")
@Transactional
public class UserBOImpl implements UserBO {

    @Resource
    private UserDAO userDAO;
    
    @Resource
    private MachineDAO machineDAO;
    
    @Resource
    private VrcConfigDAO configDao;
    
    @Override
    public boolean hasPhone(String phone) {
        boolean flag = false;
        flag = userDAO.hasPhone(phone);
        return flag;
    }

    @Override
    public boolean hasUserName(String username) {
        boolean flag = false;
        flag = userDAO.hasUsername(username);
        return flag;
    }

    @Override
    @Transactional
    public synchronized boolean register(UserVO user) {
        //1、随机生成8位数id
        String id = randomId();
        
        //根据推荐人手机或id查找推荐人
        String reference = user.getParentId();
        if(!StringUtils.isEmpty(reference)){
        	UserVO u = userDAO.getUserByReference(reference);
        	if(u != null){
        		String parentId = u.getId();
        		user.setParentId(parentId);
        	} else {
        		user.setParentId("");
        	}
        }
        
        //2、添加用户
        user.setId(id);
        user.setLevel("1");
        user.setIsvalid("1");
        boolean flag = userDAO.insertUser(user);
        
        if(flag){
            //3、添加矿机
            MachineVO machine = new MachineVO();
            machine.setId(KeyUtil.getKey());
            machine.setUserId(id);
            
            //获取矿机类型
            MachineTypeVO machineType = machineDAO.getMachineTypeByCode(Const.giveMachineCode);
            if(machineType != null){
            	long maxNo = machineDAO.getMaxMachineNo();
            	if(maxNo < Const.machineNoStart){
            		maxNo = Const.machineNoStart;
            	} else {
            		maxNo = maxNo + 1;
            	}
            	machine.setMachineNo(String.valueOf(maxNo));
            	machine.setStatus(1);
            	machine.setType(machineType.getCode());
            	machine.setWorkDay(0);
            	machine.setWorkSum(machineType.getWorkSum());
            	machine.setMtDay(0);
            	machine.setMtStatus(1);
            	machine.setMtSum(machineType.getMtSum());
            	machine.setMaintenance(machineType.getMaintenance());
            	flag = machineDAO.saveMachine(machine);            	
            }
        }
        
        return flag;
    }

    @Override
    public UserVO getUser(String phone, String password) {
        UserVO user = userDAO.getUser(phone, password);
        return user;
    }

    @Override
    public boolean updateTradePass(String userId, String password) {
        boolean flag = userDAO.updateTradePass(userId, password);
        return flag;
    }
    
    @Override
    public JSONObject changeTradePass(String userId, String oldPass, String newPass) {
        JSONObject jo = new JSONObject();
        if(StringUtils.isEmpty(userId) || StringUtils.isEmpty(oldPass) 
                || StringUtils.isEmpty(newPass)){
            jo.put("code", Const.failed);
            jo.put("codeDesc", "传入参数错误，修改失败");
            return jo;
        }
        
        UserVO user = userDAO.getUserById(userId);
        if(user == null){
            jo.put("code", "202");
            jo.put("codeDesc", "不存在该用户，修改失败");
            return jo;
        }
        if(oldPass == null || !oldPass.equals(user.getTradePassword())){
            jo.put("code", "203");
            jo.put("codeDesc", "密码有误，修改失败");
            return jo;
        }
        
        boolean flag = userDAO.updateTradePass(userId, newPass);
        if(flag){
            jo.put("code", Const.success);
            jo.put("codeDesc", "修改成功");
            return jo;
        } else {
            jo.put("code", Const.failed);
            jo.put("codeDesc", "修改失败");
            return jo;
        }
    }
    
    public JSONObject changeLoginPass(String userId, String oldPass, String newPass) {
        JSONObject jo = new JSONObject();
        if(StringUtils.isEmpty(userId) || StringUtils.isEmpty(oldPass) 
                || StringUtils.isEmpty(newPass)){
            jo.put("code", Const.failed);
            jo.put("codeDesc", "传入参数错误，修改失败");
            return jo;
        }
        
        UserVO user = userDAO.getUserById(userId);
        if(user == null){
            jo.put("code", "202");
            jo.put("codeDesc", "不存在该用户，修改失败");
            return jo;
        }
        if(oldPass == null || !oldPass.equals(user.getLoginPassword())){
            jo.put("code", "203");
            jo.put("codeDesc", "密码有误，修改失败");
            return jo;
        }
        
        boolean flag = userDAO.updateLoginPass(userId, newPass);
        if(flag){
            jo.put("code", Const.success);
            jo.put("codeDesc", "修改成功");
            return jo;
        } else {
            jo.put("code", Const.failed);
            jo.put("codeDesc", "修改失败");
            return jo;
        }
    }

    @Override
    public boolean checkTradePass(String userId, String password) {
        if(StringUtils.isEmpty(userId) || StringUtils.isEmpty(password)){
            return false;
        }
        String str = userDAO.getTradePassById(userId);
        boolean flag = false;
        if(password.equals(str)){
            flag = true;
        }else {
            flag = false;
        }
        return flag;
    }

    @Override
    public JSONObject getUserInfo(String userId){
        if(StringUtils.isEmpty(userId)){
            return null;
        }
        
        JSONObject jo = null;
        UserVO user = userDAO.getUserInfo(userId);
        if(user != null){
            jo = new JSONObject();
            if(StringUtils.isEmpty(user.getTradePassword())){
            	jo.put("hasTradePsd", false);
            } else {
            	jo.put("hasTradePsd", true);
            }
            
            jo.put("id", StringUtils.nullToEmpty(user.getId()));
            jo.put("parentId", StringUtils.nullToEmpty(user.getParentId()));
            jo.put("phone", StringUtils.nullToEmpty(user.getPhone()));
            jo.put("loginPassword", StringUtils.nullToEmpty(user.getLoginPassword()));
            jo.put("tradePassword", StringUtils.nullToEmpty(user.getTradePassword()));
            //TODO
            jo.put("level", StringUtils.nullToEmpty(user.getLevel()));
            jo.put("moneySum", user.getMoneySum());
            jo.put("moneyFreeze", user.getMoneyFreeze());
            
            jo.put("realname", StringUtils.nullToEmpty(user.getRealname()));
            jo.put("idcard", StringUtils.nullToEmpty(user.getIdcard()));
            jo.put("bankcard", StringUtils.nullToEmpty(user.getBankcard()));
            jo.put("bankName", StringUtils.nullToEmpty(user.getBankName()));
            jo.put("bankKind", StringUtils.nullToEmpty(user.getBankKind()));
            jo.put("bankcardType", StringUtils.nullToEmpty(user.getBankcardType()));
            jo.put("bankCode", StringUtils.nullToEmpty(user.getBankCode()));
            jo.put("alipayAccount", StringUtils.nullToEmpty(user.getAlipayAccount()));
            jo.put("weixinAccount", StringUtils.nullToEmpty(user.getWeixinAccount()));
            
            jo.put("oreSum", user.getOreSum());
            jo.put("csbSum", user.getCaiShenBiSum());
            
        }
        return jo;
    }
    
    @Override
	public JSONObject getUserPool(String userId) {
    	JSONObject retJo = new JSONObject();
		List<UserVO> list = userDAO.getUserByParentId(userId);
		
		double calculate = 0;  //总算力
		int memberNum = 0;  //矿工总数
		JSONArray childList = new JSONArray();   //直推列表
		double reward = 0; // 直推奖励（购买矿机时奖励）
		if(list != null && list.size() != 0){
			Map<String, JSONObject> map = userDAO.getMachineNumByParentId(userId);
			Map<String, Integer> map2 = userDAO.getChildPushNum(userId);
			
			for(UserVO vo : list){
				JSONObject userJo = new JSONObject();
				String id = vo.getId();
				// 计算直推奖励（更加用户拥有的非免费矿机等级计算，每个等级都有对应的直推奖励）
				List<Map<String, Object>> retList = machineDAO.getMachineByUid(id);
				for(Map<String, Object> v : retList) {
					String level = (String) v.get("level");
					if("1".equals(level)) {
						double firstreward = 0.0;
                		try {
							String str = configDao.getConfigByKey("firstreward");
							firstreward = Double.valueOf(str);
						} catch (Exception e) {
							e.printStackTrace();
						}
                		reward += firstreward;
					}
					if("2".equals(level)) {
						double secondreward = 0.0;
                		try {
							String str = configDao.getConfigByKey("secondreward");
							secondreward = Double.valueOf(str);
						} catch (Exception e) {
							e.printStackTrace();
						}
                		reward += secondreward;
					}
					if("3".equals(level)) {
						double thirdreward = 0.0;
                		try {
							String str = configDao.getConfigByKey("thirdreward");
							thirdreward = Double.valueOf(str);
						} catch (Exception e) {
							e.printStackTrace();
						}
                		reward += thirdreward;
					}
					if("4".equals(level)) {
						double fourthreward = 0.0;
                		try {
							String str = configDao.getConfigByKey("fourthreward");
							fourthreward = Double.valueOf(str);
						} catch (Exception e) {
							e.printStackTrace();
						}
                		reward += fourthreward;
					}
					if("5".equals(level)) {
						double fifthreward = 0.0;
                		try {
							String str = configDao.getConfigByKey("fifthreward");
							fifthreward = Double.valueOf(str);
						} catch (Exception e) {
							e.printStackTrace();
						}
                		reward += fifthreward;
					}
				}
				
				JSONObject obj = map.get(id);
				if(obj != null){
					userJo.put("machineNum", obj.get("machineNum"));
					userJo.put("calculate", obj.get("income"));
//					calculate = calculate + (double)obj.get("calculate");
				} else {
					userJo.put("machineNum", 0);
					userJo.put("calculate", 0);
				}
				
				int pushNum = map2.get(id)==null ? 0:map2.get(id);
				userJo.put("pushNum", pushNum);
				userJo.put("id", StringUtils.nullToEmpty(id));
				userJo.put("level", vo.getLevel());
				childList.add(userJo);
			}
			memberNum = childList.size();
		}
		retJo.put("calculate", reward);
		retJo.put("memberNum", memberNum);
		retJo.put("childList", childList);
		return retJo;
	}
    
    @Override
    public JSONObject resetLoginPass(String phone, String password){
    	JSONObject jo = new JSONObject();
    	UserVO user = userDAO.getUserByPhone(phone);
    	if(user == null){
    		jo.put("code", "202");
    		jo.put("codeDesc", "用户不存在");
    		return jo;
    	}

    	String userId = user.getId();
    	boolean flag = userDAO.updateLoginPass(userId, password);
    	if(flag){
    		jo.put("code", Const.success);
    		jo.put("codeDesc", "操作成功");
    		return jo;
    	} else {
    		jo.put("code", Const.failed);
    		jo.put("codeDesc", "操作失败");
    		return jo;
    	}
    }
    
    @Override
    public JSONObject resetTradePass(String phone, String password){
    	JSONObject jo = new JSONObject();
    	UserVO user = userDAO.getUserByPhone(phone);
    	if(user == null){
    		jo.put("code", "202");
    		jo.put("codeDesc", "用户不存在");
    		return jo;
    	}

    	String userId = user.getId();
    	boolean flag = userDAO.updateTradePass(userId, password);
    	if(flag){
    		jo.put("code", Const.success);
    		jo.put("codeDesc", "操作成功");
    		return jo;
    	} else {
    		jo.put("code", Const.failed);
    		jo.put("codeDesc", "操作失败");
    		return jo;
    	}
    }

	@Override
	public JSONObject updateRealInfo(String userId, String info) {
//		{
//			  "name": "张三",
//			  "cardNo": "6225756663322156",
//			  "idNo": "34042158962596321",
//			  "phoneNo": "13699995555",
//			  "respMessage": "结果匹配",
//			  "respCode": "0000",
//			  "bankName": "招商银行",
//			  "bankKind": "招商银行信用卡",
//			  "bankType": "信用卡",
//			  "bankCode": "CMB"
//			}

		JSONObject jo = JSONObject.fromObject(info);
		String respCode = jo.getString("respCode");
		//如果认证成功，则入库
		if("0000".equals(respCode)){
			UserVO vo = new UserVO();
			vo.setId(StringUtils.nullToEmpty(userId));
			vo.setRealname(StringUtils.nullToEmpty(jo.getString("name")));
			vo.setIdcard(StringUtils.nullToEmpty(jo.getString("idNo")));
			vo.setBankcard(StringUtils.nullToEmpty(jo.getString("cardNo")));
			vo.setBankName(StringUtils.nullToEmpty(jo.getString("bankName")));
			vo.setBankKind(StringUtils.nullToEmpty(jo.getString("bankKind")));
			vo.setBankcardType(StringUtils.nullToEmpty(jo.getString("bankType")));
			vo.setBankCode(StringUtils.nullToEmpty(jo.getString("bankCode")));
			userDAO.updateRealInfo(vo);
		}
		
		//构造返回实体
		JSONObject retObj = new JSONObject();
		retObj.put("realname", StringUtils.nullToEmpty(jo.getString("name")));
		retObj.put("idcard", StringUtils.nullToEmpty(jo.getString("idNo")));
		retObj.put("bankcard", StringUtils.nullToEmpty(jo.getString("cardNo")));
		retObj.put("bankName", StringUtils.nullToEmpty(jo.getString("bankName")));
		retObj.put("bankKind", StringUtils.nullToEmpty(jo.getString("bankKind")));
		retObj.put("bankcardType", StringUtils.nullToEmpty(jo.getString("bankType")));
		retObj.put("bankCode", StringUtils.nullToEmpty(jo.getString("bankCode")));
		retObj.put("phone", StringUtils.nullToEmpty(jo.getString("phoneNo")));
		retObj.put("code", StringUtils.nullToEmpty(jo.getString("respCode")));
		retObj.put("codeDesc", StringUtils.nullToEmpty(jo.getString("respMessage")));
		
		JSONObject j = new JSONObject();
		j.put("code", Const.success);
		j.put("codeDesc", "操作成功");
		j.put("data", retObj);
		return j;
	}

	@Override
	public PageInfo getUserPage(PageInfo page, String keyword) {
		List<UserVO> list = userDAO.getUserPage(page, keyword);
		page.setData(list);
		return page;
	}

	@Override
	public synchronized boolean saveUser(UserVO vo) {
		if(vo == null){
			return false;
		}
		//1、获取最大id，加一
        String id = randomId();
        vo.setId(id);
        
        //根据推荐人手机或id查找推荐人
        String reference = vo.getParentId();
        if(!StringUtils.isEmpty(reference)){
        	UserVO u = userDAO.getUserByReference(reference);
        	if(u != null){
        		String parentId = u.getId();
        		vo.setParentId(parentId);
        	} else {
        		vo.setParentId("");
        	}
        }
        
        //2、添加用户
        vo.setLevel("1");
        boolean flag = userDAO.saveUser(vo);
        
		return flag;
	}

	@Override
	public boolean updateUserInfo(UserVO vo) {
		if(vo == null || vo.getId() == null){
			return false;
		}
		boolean flag = userDAO.updateUser(vo);
		return flag;
	}

	@Override
	public boolean deleteUser(String ids) {
		if(StringUtils.isEmpty(ids)){
			return false;
		}
		String[] idList = ids.split(",");
		//1、删除用户矿机
		machineDAO.delMachineByUserId(idList);
		
		//2、删除用户
		boolean flag = userDAO.deleteUser(idList);
		return flag;
	}

	@Override
	public boolean setWeixinAccount(String id, String account) {
		boolean flag = userDAO.setWeixinAccount(id, account);
		return flag;
	}

	@Override
	public boolean setAlipayAccount(String id, String account) {
		boolean flag = userDAO.setAlipayAccount(id, account);
		return flag;
	}

	@Override
	public JSONObject authenUser(String userId, String name, String bankcard, String bankName, String idcard, String phone) {
		JSONObject jo = new JSONObject();
		jo.put("code", Const.failed);
		jo.put("codeDesc", "系统错误");
		
		boolean flag = userDAO.hasIdCard(idcard);
		if(flag){
			jo.put("code", Const.failed);
			jo.put("codeDesc", "身份证已存在，认证失败");
			return jo;
		}
		
		flag = userDAO.hasBankCard(bankcard);
		if(flag){
			jo.put("code", Const.failed);
			jo.put("codeDesc", "银行卡已被绑定，认证失败");
			return jo;
		}
		
		UserVO vo = new UserVO();
		vo.setId(StringUtils.nullToEmpty(userId));
		vo.setRealname(StringUtils.nullToEmpty(name));
		vo.setIdcard(StringUtils.nullToEmpty(idcard));
		vo.setBankcard(StringUtils.nullToEmpty(bankcard));
		vo.setBankName(StringUtils.nullToEmpty(bankName));
		flag = userDAO.updateRealInfo(vo);
		
		if(flag){
			jo.put("code", Const.success);
			jo.put("codeDesc", "认证成功");
			//构造返回实体
			JSONObject retObj = new JSONObject();
			retObj.put("realname", StringUtils.nullToEmpty(name));
			retObj.put("idcard", StringUtils.nullToEmpty(idcard));
			retObj.put("bankcard", StringUtils.nullToEmpty(bankcard));
			retObj.put("bankName", StringUtils.nullToEmpty(bankName));
			jo.put("data", retObj);
			return jo;
		} else {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "认证失败");
			return jo;
		}
	}

	@Override
	public boolean updateMoneySum(boolean bool, double money, Long userId) {
		if(money==0||userId==null){
			return false;
		}
		if(!bool){
			money = -money;
		}
		return userDAO.updateMoneySum(money, userId);
	}

	@Override
	public PageInfo getChildUserPage(PageInfo page, String parentId) {
		List<UserVO> list = userDAO.getUserByParentIdPage(page, parentId);
		page.setData(list);
		return page;
	}
	
		@Override
	public boolean freOrUnfreUser(String userId, String isvalid) {
		boolean flag = false;
		if("正常".equals(isvalid)){
			flag = userDAO.freezeUser(userId);
		} else {
			flag = userDAO.reFreezeUser(userId);
		}
		return flag;
	}

	//随机生成8位数的用户id
	private String randomId(){
		String id = "";
		while(true){
			long i = (long) (89999999*Math.random()+10000000);
			id = String.valueOf(i);
			
			if(!userDAO.hasId(id)){
				break;				
			}
		}
		return id;
	}

}
