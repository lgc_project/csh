package com.vrc.bo.machine;

import com.vrc.vo.common.PageInfo;
import com.vrc.vo.machine.MachineTypeVO;
import com.vrc.vo.trade.BuyrecordVO;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 矿机处理BO
 * @author liboxing
 *
 */
public interface MachineBO {

    /**
     * 获取所有的矿机类型
     * @return
     */
    public JSONArray getAllMachineType();
    
    /**
     * 根据userId获取矿机
     * @param userId
     * @return
     */
    public JSONObject getMachine(String userId);
    
    /**
     * 获取所有用户矿机产量
     * 
     * @param page
     * @param keyword
     * @return
     */
    public PageInfo getMachine(PageInfo page, String keyword);
    
    /**
     * 购买矿机
     * @param userId
     * @param machineCode
     * @return
     */
    public JSONObject buyMachine(String userId, String machineTypeCode);
    
    /**
     * 后台手动添加矿机
     * @param userId
     * @param machineTypeCode
     * @return
     */
    public JSONObject addMachine(String userId, String machineTypeCode);
    
    /**
     * 分页获取机器类型
     * @param page
     * @param keyword
     * @return
     */
    public PageInfo getMachineTypePage(PageInfo page, String keyword);
    
    /**
     * 添加矿机类型
     * @param vo
     * @return
     */
    public boolean saveMachineType(MachineTypeVO vo);
    
    /**
     * 修改矿机类型
     * @param vo
     * @return
     */
    public boolean updateMachineType(MachineTypeVO vo);
    
    /**
     * 删除矿机类型
     * @param id
     * @return
     */
    public boolean deleteMachType(String codes);
    
    /**
     * 分页获取矿机
     * @param page
     * @param keyword
     * @return
     */
    public PageInfo getMachinePage(PageInfo page, String keyword); 
    
    /**
     * 删除矿机
     * @param id
     * @return
     */
    public boolean deleteMachine(String codes);
    
    /**
     * 获取购买矿机记录
     * @param page
     * @param keyword
     * @param startTime
     * @param endTime
     * @return
     */
    public PageInfo getBuyRecordVO(PageInfo page, String keyword, String startTime, String endTime);
    
    /**
     * 添加购买矿机记录
     * @param vo
     * @return
     */
    public boolean saveBuyrecordVO(BuyrecordVO vo);
    
    /**
     * 维修矿机
     * 
     * @param userId
     * @param machineNo
     * @return
     */
    public JSONObject maintenanceMachine(String userId, String machineNo);
}
