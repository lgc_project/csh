package com.vrc.bo.machine.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rdms.common.annotation.SysLog;
import com.vrc.bo.machine.MachineBO;
import com.vrc.common.Const;
import com.vrc.dao.machine.MachineDAO;
import com.vrc.dao.sys.UserDAO;
import com.vrc.dao.sys.VrcConfigDAO;
import com.vrc.dao.trade.BillDAO;
import com.vrc.util.DateUtils;
import com.vrc.util.KeyUtil;
import com.vrc.util.StringUtils;
import com.vrc.vo.common.PageInfo;
import com.vrc.vo.machine.MachineTypeVO;
import com.vrc.vo.machine.MachineVO;
import com.vrc.vo.sys.UserVO;
import com.vrc.vo.trade.BillVO;
import com.vrc.vo.trade.BuyrecordVO;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service("machineBO")
@Transactional
public class MachineBOImpl implements MachineBO {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
    @Resource
    private MachineDAO machineDAO;
    
    @Resource
    private UserDAO userDAO;
    
    @Resource
    private BillDAO billDAO;
    
    @Resource
    private VrcConfigDAO configDao;
    
    @Override
    public JSONArray getAllMachineType() {
        List<MachineTypeVO> list = machineDAO.getAllMachineType();
        JSONArray ja = (JSONArray) JSONArray.fromObject(list);
        return ja;
    }

    @Override
    public JSONObject getMachine(String userId) {
        JSONObject retObj = new JSONObject();
        List<MachineVO> list = machineDAO.getMachine(userId);
        
        JSONArray runList = new JSONArray();
        JSONArray stopList = new JSONArray();
        if(list != null && list.size() > 0){
            for(MachineVO mach : list){
                JSONObject jo = new JSONObject();
                jo.put("id", mach.getId());
                jo.put("userId", mach.getUserId());
                jo.put("status", mach.getStatus());
                jo.put("machineNo", mach.getMachineNo());
                jo.put("runDay", mach.getWorkDay() +"/"+mach.getWorkSum());
                jo.put("calculate", mach.getMachineType().getCalculate());
                jo.put("type", mach.getType());
                jo.put("typeName", mach.getMachineType().getName());
                jo.put("mtDay", mach.getMtDay() + "/" + mach.getMtSum());
                jo.put("maintenance", mach.getMaintenance());
                jo.put("mtStatus", mach.getMtStatus());
                
                double incomeSum = mach.getMachineType().getIncomeSum();
//                double workSum = mach.getWorkSum();
                double mtSum = mach.getMtSum();
                double canliang = 0;
                if(mtSum != 0){
                	canliang = incomeSum / mtSum;        
                	//保留2位小数
                    BigDecimal bg = new BigDecimal(canliang);    
                    canliang = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                }
                jo.put("product", canliang);
                
                int status = mach.getStatus();
                if(1 == status){
                    runList.add(jo);
                } else {
                    stopList.add(jo);
                }
            }
        }
        
        double cal = 0;
        double product = 0;
        for(Object obj : runList){
        	JSONObject jo = (JSONObject)obj;
        	cal = cal + jo.getDouble("calculate");
        	product = product + jo.getDouble("product");;
        }
        //保留2位小数
        BigDecimal bg = new BigDecimal(product);    
        product = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        
        retObj.put("runList", runList);
        retObj.put("stopList", stopList);
        retObj.put("calculate", cal);
        retObj.put("product", product);
        return retObj;
    }

    @Override
    public synchronized JSONObject buyMachine(String userId, String machineTypeCode) {
        JSONObject jo = new JSONObject();
        boolean flag = false;
        MachineTypeVO machineType = machineDAO.getMachineTypeByCode(machineTypeCode);
        if(machineType == null){
            jo.put("code", "203");
            jo.put("codeDesc", "无此矿机");
            return jo;
        }
        
        if("A0".equals(machineTypeCode)) {
        	MachineVO mac = machineDAO.getMachineByUidAndCode(userId, machineTypeCode);
            if(mac != null) {
            	jo.put("code", "204");
            	jo.put("codeDesc", "免费矿机只能领取一台");
            	return jo;
            }
        }
        
        double price = machineType.getPrice();
        
        UserVO user = userDAO.getUserById(userId);
        double moneySum = user.getMoneySum();
        double freeze = user.getMoneyFreeze();
        double canuse = moneySum - freeze;
        
        try {
            //判断余额是否足够
            if(canuse < price){
                jo.put("code", "202");
                jo.put("codeDesc", "余额不足");
                return jo;
            } else {
                //先扣钱
                moneySum = moneySum - price;
                user.setMoneySum(moneySum);
                user.setMoneyFreeze(freeze);
                flag = userDAO.updateUserMoney(user);
                
                //给推荐人增加费用
                String parentId = user.getParentId();
                if(!StringUtils.isEmpty(parentId)) {
                	UserVO parent = userDAO.getUserInfo(parentId);
                	String level = machineType.getLevel();
                	if("1".equals(level)) {
                		double firstreward = 0.0;
                		try {
							String str = configDao.getConfigByKey("firstreward");
							firstreward = Double.valueOf(str);
						} catch (Exception e) {
							e.printStackTrace();
						}
                		
                		parent.setMoneySum(parent.getMoneySum() + firstreward);
                		parent.setMoneyFreeze(parent.getMoneyFreeze());
                		flag = userDAO.updateUserMoney(parent);
                		
                		// 写入账单表（类型：奖励，子类型：奖励，账单类型：百源币）
                		BillVO bill = new BillVO();
                		bill.setId(KeyUtil.getKey());
                		bill.setType(Const.billType_reward);
                		bill.setActionType(Const.actionType_pushReward);
                		bill.setUserId(parentId);
                		bill.setPrice(firstreward);
                		bill.setTime(DateUtils.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss"));
                		bill.setBillType("2");
                		billDAO.addBill(bill);
                	}
                	if("2".equals(level)) {
                		double secondreward = 0.0;
                		try {
							String str = configDao.getConfigByKey("secondreward");
							secondreward = Double.valueOf(str);
						} catch (Exception e) {
							e.printStackTrace();
						}
                		
                		parent.setMoneySum(parent.getMoneySum() + secondreward);
                		parent.setMoneyFreeze(parent.getMoneyFreeze());
                		flag = userDAO.updateUserMoney(parent);
                		
                		// 写入账单表（类型：奖励，子类型：奖励，账单类型：百源币）
                		BillVO bill = new BillVO();
                		bill.setId(KeyUtil.getKey());
                		bill.setType(Const.billType_reward);
                		bill.setActionType(Const.actionType_pushReward);
                		bill.setUserId(parentId);
                		bill.setPrice(secondreward);
                		bill.setTime(DateUtils.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss"));
                		bill.setBillType("2");
                		billDAO.addBill(bill);
                	}
                	if("3".equals(level)) {
                		double thirdreward = 0.0;
                		try {
							String str = configDao.getConfigByKey("thirdreward");
							thirdreward = Double.valueOf(str);
						} catch (Exception e) {
							e.printStackTrace();
						}
                		
                		parent.setMoneySum(parent.getMoneySum() + thirdreward);
                		parent.setMoneyFreeze(parent.getMoneyFreeze());
                		flag = userDAO.updateUserMoney(parent);
                		
                		// 写入账单表（类型：奖励，子类型：奖励，账单类型：百源币）
                		BillVO bill = new BillVO();
                		bill.setId(KeyUtil.getKey());
                		bill.setType(Const.billType_reward);
                		bill.setActionType(Const.actionType_pushReward);
                		bill.setUserId(parentId);
                		bill.setPrice(thirdreward);
                		bill.setTime(DateUtils.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss"));
                		bill.setBillType("2");
                		billDAO.addBill(bill);
                	}
                	if("4".equals(level)) {
                		double fourthreward = 0.0;
                		try {
							String str = configDao.getConfigByKey("fourthreward");
							fourthreward = Double.valueOf(str);
						} catch (Exception e) {
							e.printStackTrace();
						}
                		
                		parent.setMoneySum(parent.getMoneySum() + fourthreward);
                		parent.setMoneyFreeze(parent.getMoneyFreeze());
                		flag = userDAO.updateUserMoney(parent);
                		
                		// 写入账单表（类型：奖励，子类型：奖励，账单类型：百源币）
                		BillVO bill = new BillVO();
                		bill.setId(KeyUtil.getKey());
                		bill.setType(Const.billType_reward);
                		bill.setActionType(Const.actionType_pushReward);
                		bill.setUserId(parentId);
                		bill.setPrice(fourthreward);
                		bill.setTime(DateUtils.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss"));
                		bill.setBillType("2");
                		billDAO.addBill(bill);
                	}
                	if("5".equals(level)) {
                		double fifthreward = 0.0;
                		try {
							String str = configDao.getConfigByKey("fifthreward");
							fifthreward = Double.valueOf(str);
						} catch (Exception e) {
							e.printStackTrace();
						}
                		
                		parent.setMoneySum(parent.getMoneySum() + fifthreward);
                		parent.setMoneyFreeze(parent.getMoneyFreeze());
                		flag = userDAO.updateUserMoney(parent);
                		
                		// 写入账单表（类型：奖励，子类型：奖励，账单类型：百源币）
                		BillVO bill = new BillVO();
                		bill.setId(KeyUtil.getKey());
                		bill.setType(Const.billType_reward);
                		bill.setActionType(Const.actionType_pushReward);
                		bill.setUserId(parentId);
                		bill.setPrice(fifthreward);
                		bill.setTime(DateUtils.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss"));
                		bill.setBillType("2");
                		billDAO.addBill(bill);
                	}
                }
                
                //写入账单中（类型：支出，子类型：支出，账单类型：百源币）
                if(flag){
                    BillVO bill = new BillVO();
                    bill.setId(KeyUtil.getKey());
                    bill.setType(Const.billType_roolout);
                    bill.setActionType(Const.actionType_roolout);
                    bill.setUserId(userId);
                    bill.setPrice(price);
                    bill.setTime(DateUtils.dateToString(new Date()));
                    bill.setBillType("2");
                    billDAO.addBill(bill);
                }
                
                if(flag){
                    //添加矿机
                    MachineVO machine = new MachineVO();
                    machine.setId(KeyUtil.getKey());
                    machine.setUserId(userId);
                    
                    long maxNo = machineDAO.getMaxMachineNo();
                    if(maxNo < Const.machineNoStart){
                        maxNo = Const.machineNoStart;
                    } else {
                        maxNo = maxNo + 1;
                    }
                    machine.setMachineNo(String.valueOf(maxNo));
                    machine.setStatus(1);
                    machine.setType(machineType.getCode());
                    machine.setWorkDay(0);
                    machine.setWorkSum(machineType.getWorkSum());
                    machine.setMtDay(0);
                    machine.setMtSum(machineType.getMtSum());
                    machine.setMaintenance(machineType.getMaintenance());
                    machine.setMtStatus(1);
                    flag = machineDAO.saveMachine(machine);
                    
                    //添加购机记录
                    BuyrecordVO buyRecord = new BuyrecordVO();
                    buyRecord.setId(KeyUtil.getKey32());
                    buyRecord.setUserId(userId);
                    buyRecord.setMachineNo(String.valueOf(maxNo));
                    buyRecord.setTime(DateUtils.dateToString(new Date()));
                    buyRecord.setMachineType(machineType.getCode());
                    machineDAO.saveBuyrecordVO(buyRecord);
                    
                    jo.put("code", Const.success);
                    jo.put("codeDesc", "购买成功");
                } else {
                    jo.put("code", Const.failed);
                    jo.put("codeDesc", "系统错误");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            jo.put("code", Const.failed);
            jo.put("codeDesc", "系统错误");
        }
        return jo;
    }

    @Override
    @SysLog(value="赠送矿机")
	public synchronized JSONObject addMachine(String userId, String machineTypeCode) {
    	JSONObject jo = new JSONObject();
    	
    	//添加矿机
        MachineVO machine = new MachineVO();
        machine.setId(KeyUtil.getKey());
        machine.setUserId(userId);
        
        long maxNo = machineDAO.getMaxMachineNo();
        if(maxNo < Const.machineNoStart){
            maxNo = Const.machineNoStart;
        } else {
            maxNo = maxNo + 1;
        }
        machine.setMachineNo(String.valueOf(maxNo));
        machine.setStatus(1);
        machine.setType(machineTypeCode);
        machine.setWorkDay(0);
        machine.setWorkSum(0);
        boolean flag = machineDAO.saveMachine(machine);
        if(flag){
        	jo.put("code", Const.success);
        	jo.put("codeDesc", "操作成功");        	
        } else {
        	jo.put("code", Const.failed);
        	jo.put("codeDesc", "操作失败");     
        }
		return jo;
	}
    
	@Override
	public PageInfo getMachineTypePage(PageInfo page, String keyword) {
		List<MachineTypeVO> list = machineDAO.getMachineTypePage(page, keyword);
		page.setData(list);
		return page;
	}

	@Override
	public boolean saveMachineType(MachineTypeVO vo) {
		String id = KeyUtil.getKey32();
		String code = KeyUtil.getKey32();
		vo.setId(id);
		vo.setCode(code);
		boolean flag = machineDAO.saveMachineType(vo);
		return flag;
	}

	@Override
	public boolean updateMachineType(MachineTypeVO vo) {
		boolean flag = machineDAO.updateMachineType(vo);
		return flag;
	}

	@Override
	public boolean deleteMachType(String codes) {
		if(StringUtils.isEmpty(codes)){
			return false;
		}
		String[] codeList = codes.split(",");
		boolean flag = machineDAO.deleteMachType(codeList);
		return flag;
	}

	@Override
	public PageInfo getMachinePage(PageInfo page, String keyword) {
        List<MachineVO> list = machineDAO.getMachinePage(page, keyword);
        
        JSONArray ja = new JSONArray();
        if(list != null && list.size() > 0){
            for(MachineVO mach : list){
                JSONObject jo = new JSONObject();
                jo.put("id", mach.getId());
                jo.put("userId", mach.getUserId());
                jo.put("status", mach.getStatus());
                jo.put("machineNo", mach.getMachineNo());
                jo.put("runDay", mach.getWorkDay() +"/"+mach.getWorkSum());
                jo.put("calculate", mach.getMachineType().getCalculate());
                jo.put("type", mach.getType());
                jo.put("typeName", mach.getMachineType().getName());
                jo.put("mtDay", mach.getMtDay() + "/" + mach.getMtSum());
                jo.put("maintenance", mach.getMaintenance());
                jo.put("mtStatus", mach.getMtStatus());
                double incomeSum = mach.getMachineType().getIncomeSum();
//                double workSum = mach.getWorkSum();
                double mtSum = mach.getMtSum();
                double canliang = 0;
                if(mtSum != 0){
                	canliang = incomeSum / mtSum;        
                	//保留2位小数
                    BigDecimal bg = new BigDecimal(canliang);    
                    canliang = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                }
                jo.put("canliang", canliang);
                ja.add(jo);
            }
        }
        page.setData(ja);
		return page;
	}

	@Override
	public boolean deleteMachine(String codes) {
		if(StringUtils.isEmpty(codes)){
			return false;
		}
		String[] codeList = codes.split(",");
		boolean flag = machineDAO.deleteMachine(codeList);
		return flag;
	}

	@Override
	public boolean saveBuyrecordVO(BuyrecordVO vo) {
		if(vo == null){
			return false;
		}
		String id = KeyUtil.getKey32();
		vo.setId(id);
		boolean flag = machineDAO.saveBuyrecordVO(vo);
		return flag;
	}
	
	@Override
	public PageInfo getBuyRecordVO(PageInfo page, String keyword, String startTime, String endTime) {
		List<BuyrecordVO> list = machineDAO.getBuyRecordVO(page, keyword, startTime, endTime);
		page.setData(list);
		return page;
	}

	@Override
	public JSONObject maintenanceMachine(String userId, String machineNo) {
		JSONObject jo = new JSONObject();
		logger.info(userId);
		logger.info(machineNo);
		
		MachineVO machine = machineDAO.getMachineByUidAndNo(userId, machineNo);
		if(machine == null) {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
			return jo;
		}
		
		int workDay = machine.getWorkDay();
		int workSum = machine.getWorkSum();
		if(workDay >= workSum) {
			jo.put("code", "202");
			jo.put("codeDesc", "该矿机已经超过运行天数");
			return jo;
		}
		
		BigDecimal maintenance = machine.getMaintenance(); // 维修费用
		
		UserVO user = userDAO.getUserById(userId);
		double moneySum = user.getMoneySum();
		double moneyFreeze = user.getMoneyFreeze();
		int count = (new BigDecimal(moneySum - moneyFreeze)).compareTo(maintenance);
		boolean flag;
		// 如果余额不足
		if(count < 0) {
			jo.put("code", "203");
			jo.put("codeDesc", "用户余额不足，不能维修");
			return jo;
		} else {
			// 1.扣除用户维修费用
			user.setMoneySum(moneySum - maintenance.doubleValue());
			userDAO.updateUser(user);
			
			// 2.修改矿机维修天数为0，维修状态为1
			MachineVO mv = machineDAO.getMachineByUidAndNo(userId, machineNo);
			mv.setId(machine.getId());
			mv.setMtStatus(1);
			mv.setMtDay(0);
			flag = machineDAO.updateMachine(mv);
		}
		
		if(flag) {
			jo.put("code", Const.success);
			jo.put("codeDesc", "操作成功");
		} else {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
		}
		
		return jo;
	}

	@Override
	public PageInfo getMachine(PageInfo page, String keyword) {
		List<Map<String, Object>> list = machineDAO.getMachine(page, keyword);
		page.setData(list);
		return page;
	}

}
