package com.vrc.bo.trade.impl;

import java.math.BigDecimal;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vrc.bo.trade.DealBO;
import com.vrc.common.Const;
import com.vrc.dao.sys.UserDAO;
import com.vrc.dao.trade.BillDAO;
import com.vrc.dao.trade.CommodityDAO;
import com.vrc.dao.trade.CommodityDealDao;
import com.vrc.dao.trade.DealDao;
import com.vrc.util.DateUtils;
import com.vrc.util.KeyUtil;
import com.vrc.vo.sys.UserVO;
import com.vrc.vo.trade.BillVO;
import com.vrc.vo.trade.CommodityDealVO;
import com.vrc.vo.trade.CommodityVO;
import com.vrc.vo.trade.DealVO;

import net.sf.json.JSONObject;

/**
 * 
 * @ClassName: DealBOImpl
 * @Description:商品交易boimpl类
 * @author: MDS
 * @date: 2018年5月29日 上午11:52:04
 */
@Service("dealBO")
@Transactional
public class DealBOImpl implements DealBO {
	@Resource
	private CommodityDealDao commodityDealDao;

	@Resource
	private DealDao dealDao;
	@Resource
	private CommodityDAO commodityDao;
	@Resource
	private UserDAO userDao;
	@Resource
	private BillDAO billDao;

	@Override
	public JSONObject saveDealInfo(Long userId, Long commodityId, String receiver, String address, String phone) {
		JSONObject jo = new JSONObject();
		if(userId == null) {
			jo.put("code", "202");
			jo.put("codeDesc", "用户不存在");
			return jo;
		}
		
		if(commodityId == null) {
			jo.put("code", "203");
			jo.put("codeDesc", "商品不存在");
			return jo;
		}
		
		boolean bool = false;
		CommodityVO cv = commodityDao.getCommodity(String.valueOf(commodityId.longValue()));
		double price = cv.getPrice();
		UserVO user = userDao.getUserById(String.valueOf(userId.longValue()));
		double csbSum = user.getCaiShenBiSum();
		double csbFreeze = user.getCaiShenBiFreeze();
//		Double money = user.getMoneySum();
//		int count = new BigDecimal(money.doubleValue()).compareTo(new BigDecimal(price));
		if(csbSum - csbFreeze < price) {
			jo.put("code", "204");
			jo.put("codeDesc", "用户余额不足");
			return jo;
		}
		user.setCaiShenBiSum(csbSum - price);
		userDao.updateUser(user);
		
		DealVO dealVO = new DealVO();
		// 1、获取最大id，加一
		Long did = dealDao.getMaxId();
		if (did == null || did < Const.dealIdStart.longValue()) {
			did = Const.dealIdStart;
		} else {
			did = did + 1;
		}
		dealVO.setId(did);
		dealVO.setState(1);
		dealVO.setTime(new Date().getTime());
		dealVO.setUserId(userId);
		dealVO.setReceiver(receiver);
		dealVO.setAddress(address);
		dealVO.setPhone(phone);
		if (dealDao.saveDealInfo(dealVO)) {
			CommodityDealVO commodityDealVO = new CommodityDealVO();
			Long cid = dealDao.getMaxId();
			if (cid == null || cid < Const.commodityDealIdStart.longValue()) {
				cid = Const.commodityDealIdStart;
			} else {
				cid = cid + 1;
			}
			commodityDealVO.setId(cid);
			commodityDealVO.setDealId(did);
			commodityDealVO.setCommodityId(commodityId);
			bool = commodityDealDao.saveCommodityDealInfo(commodityDealVO);
		}
		
		if(bool) {
			// 写入账单表（类型：转出，子类型：转出，账单类型：财神币）
			BillVO bill = new BillVO();
			bill.setId(KeyUtil.getKey());
			bill.setType(Const.billType_roolout);
			bill.setActionType(Const.actionType_roolout);
			bill.setUserId(String.valueOf(userId.longValue()));
			bill.setPrice(price);
			bill.setTime(DateUtils.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss"));
			bill.setBillType("1");
			billDao.addBill(bill);
			jo.put("code", Const.success);
			jo.put("codeDesc", "操作成功");
		} else {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
		}
		return jo;
	}

}
