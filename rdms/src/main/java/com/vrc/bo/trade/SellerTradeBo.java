package com.vrc.bo.trade;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
* @ClassName 卖家挂单财神币
* @Description
* @author LGC
* @date 2018年8月13日 下午10:06:47
*/
public interface SellerTradeBo {

	/**
	 * 卖家挂单
	 * 
	 * @param userId
	 * @param price
	 * @param count
	 * @param level
	 * @return
	 */
	JSONObject postOdd(String userId, double price, double count, String level);

	/**
	 * 买家确认
	 * 
	 * @param oddId
	 * @param userId
	 * @return
	 */
	JSONObject buy(String oddId, String userId);

	/**
	 * 卖家确认
	 * 
	 * @param userId
	 * @param oddId
	 * @return
	 */
	JSONObject sureTrade(String userId, String oddId);
	
	/**
	 * 撤销挂单
	 * 
	 * @param oddId
	 * @return
	 */
	JSONObject cancelOdd(String oddId);
	
	/**
	 * 获取挂单列表
	 * 
	 * @param userId
	 * @param level
	 * @return
	 */
	JSONArray getGuadanList(String userId, String level);
	
	/**
	 * 信箱
	 * 
	 * @param userId
	 * @param level
	 * @return
	 */
	JSONArray mailBox(String userId, String level);
}
