package com.vrc.bo.trade;

import com.vrc.vo.common.PageInfo;
import com.vrc.vo.trade.TradeSimpleVO;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 交易相关bo
 * @author liboxing
 *
 */
public interface TradeBO {

    /**
     * 挂单
     * @param userId  用户id
     * @param price    单价
     * @param count    数量
     */
    public boolean postOdd(String userId, double price, double count, String level);
    
    /**
     * 获取挂单列表（用户不能在列表中看到自己挂的单）
     * @param userId  用户id
     * @return
     */
    public JSONArray getGuadanList(String userId, String level);
    
    /**
     * 卖家点击挂单列表，进行卖出操作（该方法必须要同步）
     * @param oddId    单号
     * @param userId     用户id
     * @return
     */
    public JSONObject sell(String oddId, String userId);
    
    /**
     * 点对点交易（卖家输入买家id，进行交易）
     * @param buyId     买家id
     * @param sellId       卖家id
     * @return
     */
    public JSONObject pointSell(String buyId, String sellId, double count, double price);
    
    /**
     * 查看信箱
     * @param userId
     * @return
     */
    public JSONArray mailBox(String userId, String level);
    
    /**
     * 撤单
     * @return
     */
    public JSONObject cancelOdd(String oddId);
    
    /**
     * 确认交易
     * @param userId
     * @param oddId
     * @return
     */
    public JSONObject sureTrade(String userId, String oddId);
    
    /**
     * 获取昨日的平均价，最高价，交易数量
     * @return
     */
    public JSONObject getTradeSimple();
    
    /**
     * 获取矿市页面折线图数据
     * @return
     */
    public JSONObject getChartData();
    
    /**
     * 获取挂单级别的信箱（最大数量，最少数量，描述）
     * @return
     */
    public JSONObject getPostBillMsg(String level);
    
    /**
     * 手续费提示信息
     * @return
     */
    public JSONObject getPoundageMsg();
    
    /**
     * 获取账单列表
     * @param userId
     * @param type
     * @param page
     * @param billType
     * @return
     */
    public JSONObject getBillList(String userId, String type, PageInfo page, String billType);
    
    /**
     * 添加简要交易记录
     * @param vo
     * @return
     */
    public boolean saveTradeSimple(TradeSimpleVO vo);
    
    /**
     * 修改简要交易记录
     * @param vo
     * @return
     */
    public boolean updateTradeSimple(TradeSimpleVO vo);
    
    /**
     * 获取交易记录
     * @param page
     * @param keyword
     * @param startTime
     * @param endTime
     * @return
     */
    public PageInfo getTradePage(PageInfo page, String keyword, String startTime, String endTime);
    
    /**
     * 获取简易交易记录
     * @param page
     * @return
     */
    public PageInfo getSimpleTradePage(PageInfo page);
    
    /**
     * 删除交易记录
     * 
     * @param ids
     * @return
     */
    public boolean deleteTrade(String ids);
    
}
