package com.vrc.bo.trade.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vrc.bo.trade.CommodityBO;
import com.vrc.common.Const;
import com.vrc.dao.sys.UserDAO;
import com.vrc.dao.trade.BillDAO;
import com.vrc.dao.trade.CommodityDAO;
import com.vrc.util.DateUtils;
import com.vrc.util.KeyUtil;
import com.vrc.vo.common.PageInfo;
import com.vrc.vo.sys.UserVO;
import com.vrc.vo.trade.BillVO;
import com.vrc.vo.trade.CommodityVO;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 
 * @ClassName: CommodityBOImpl
 * @Description:商品信息boimpl
 * @author: MDS
 * @date: 2018年5月29日 上午9:21:47
 */
@Service("commodityBOImpl")
public class CommodityBOImpl implements CommodityBO {
	@Resource
	private CommodityDAO commodityDAO;
	@Resource
	private UserDAO userDao;
	@Resource
	private BillDAO billDao;
	
	@Override
	public PageInfo getCommodityPage(PageInfo page, String keyword) {
		List<CommodityVO> list = commodityDAO.getCommodityPage(page, keyword);
		page.setData(list);
		return page;
	}
	
	@Override
	public JSONArray getCommodityAll() {
		JSONArray fromObject = JSONArray.fromObject(commodityDAO
				.getCommodityAll());
		return fromObject;
	}

	@Override
	public JSONObject updateUserMoneySum(String userId, double price, boolean bool, Integer priceType, double pay, Integer payType) {
		JSONObject jo = new JSONObject();
		Date date = new Date();
		
		UserVO payUser = userDao.getUserById(userId);
		switch (payType) {
		case 0:
			double oreSum = payUser.getOreSum();
			double oreFreeze = payUser.getOreSumFreeze();
			if(oreSum  - oreFreeze < pay) {
				jo.put("code", "203");
				jo.put("codeDesc", "用户余额不足");
				return jo;
			}
			payUser.setOreSum(oreSum - pay);
			break;
		case 1:
			double moneySum = payUser.getMoneySum();
			double moneyFreeze = payUser.getMoneyFreeze();
			if(moneySum  - moneyFreeze < pay) {
				jo.put("code", "203");
				jo.put("codeDesc", "用户余额不足");
				return jo;
			}
			payUser.setMoneySum(moneySum - pay);
			break;
		case 2:
			double caiShenBiSum = payUser.getCaiShenBiSum();
			double caiShenBiFreeze = payUser.getCaiShenBiFreeze();
			if(caiShenBiSum - caiShenBiFreeze < pay) {
				jo.put("code", "203");
				jo.put("codeDesc", "用户余额不足");
				return jo;
			}
			payUser.setCaiShenBiSum(caiShenBiSum - pay);
			break;
		default:
			break;
		}
		userDao.updateUser(payUser);
		// 写入账单表（类型：转出，子类型：转出，账单类型：）
		BillVO bill = new BillVO();
		bill.setId(KeyUtil.getKey());
		bill.setType(Const.billType_roolout);
		bill.setActionType(Const.actionType_roolout);
		bill.setUserId(userId);
		bill.setPrice(pay);
		bill.setTime(DateUtils.dateToString(date, "yyyy-MM-dd HH:mm:ss"));
		bill.setBillType(String.valueOf(payType));
		billDao.addBill(bill);
		if(!bool) {
			jo.put("code", "202");
			jo.put("codeDesc", "本次未抽中奖，谢谢参与");
			return jo;
		}
		
		UserVO curUser = userDao.getUserById(userId);
		
		switch (priceType) {
		case 0: // 矿石数
			curUser.setOreSum(curUser.getOreSum() + price);
			break;
		case 1: // 元宝
			curUser.setMoneySum(curUser.getMoneySum() + price);
			break;
		case 2: // 财神币
			curUser.setCaiShenBiSum(curUser.getCaiShenBiSum() + price);
			break;
		default:
			break;
		}
		// 1.更新用户的余额数
		boolean flag = userDao.updateUser(curUser);
		// 写入账单表（类型：转出，子类型：转出，账单类型：）
		bill = new BillVO();
		bill.setId(KeyUtil.getKey());
		bill.setType(Const.billType_reward);
		bill.setActionType(Const.actionType_teamReward);
		bill.setPrice(price);
		bill.setUserId(userId);
		bill.setTime(DateUtils.dateToString(new Date()));
		bill.setBillType(String.valueOf(priceType));
		billDao.addBill(bill);
		if(flag) {
			jo.put("code", Const.success);
			jo.put("codeDesc", "操作成功");
		} else {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
		}
		
		return jo;
	}

	@Override
	public boolean saveCommodity(CommodityVO vo) {
		if(vo == null) {
			return false;
		}
		
		Long id = commodityDAO.getMaxId();
		if(id.longValue() < Const.commodityIdStart) {
			id = Const.commodityIdStart;
		} else {
			id += 1;
		}
		
		vo.setId(id);
		vo.setTime(DateUtils.getTime(new Date()));
		return commodityDAO.saveCommodity(vo);
	}

	@Override
	public boolean updateCommodity(CommodityVO vo) {
		if(vo == null || vo.getId() == null) {
			return false;
		}
	
		return commodityDAO.updateCommodity(vo);
	}

	@Override
	public boolean deleteCommodity(String ids) {
		if(StringUtils.isEmpty(ids)) {
			return false;
		}
		String[] idArray = ids.split(",");
		return commodityDAO.deleteCommodity(idArray);
	}

	@Override
	public boolean updateCommodityImg(String commodityId, String path) {
		return commodityDAO.updateCommodityImg(commodityId, path);
	}
}
