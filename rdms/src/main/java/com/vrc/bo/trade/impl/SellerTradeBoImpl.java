package com.vrc.bo.trade.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vrc.util.StringUtils;
import com.lianbao.dao.CaiShenBiDao;
import com.lianbao.dao.PoundageDetailsDao;
import com.lianbao.vo.PoundageDetailsVO;
import com.vrc.bo.trade.SellerTradeBo;
import com.vrc.common.Const;
import com.vrc.dao.sys.UserDAO;
import com.vrc.dao.trade.BillDAO;
import com.vrc.dao.trade.TradeDAO;
import com.vrc.util.DateUtils;
import com.vrc.util.KeyUtil;
import com.vrc.vo.sys.UserVO;
import com.vrc.vo.trade.BillVO;
import com.vrc.vo.trade.GuadanVO;
import com.vrc.vo.trade.TradeVO;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
* @ClassName
* @Description
* @author 
* @date 2018年8月13日 下午10:12:17
*/
@Service("sellerTradeBo")
@Transactional
public class SellerTradeBoImpl implements SellerTradeBo {
	
	@Resource
	private TradeDAO tradeDao;
	@Resource
	private UserDAO userDao;
	@Resource
	private BillDAO billDao;
	@Resource
	private CaiShenBiDao caiShenBiDao;
	@Resource
	private PoundageDetailsDao poundageDetailsDao;
	
	@Override
	public JSONObject postOdd(String userId, double price, double count, String level) {
		JSONObject jo = new JSONObject();
		Date date = new Date();
		boolean flag;
		if(userId == null) {
			jo.put("code", "202");
			jo.put("codeDesc", "用户不存在");
			return jo;
		}
		
		UserVO seller = userDao.getUserById(userId);
		double csbSum = seller.getCaiShenBiSum();
		double csbFreeze = seller.getCaiShenBiFreeze();
		if(csbSum - csbFreeze < count) {
			jo.put("code", "203");
			jo.put("codeDesc", "财神币余额不足，无法挂卖单");
			return jo;
		}
		
		// 1.冻结卖家挂卖的财神币
		seller.setCaiShenBiSum(csbSum);
		seller.setCaiShenBiFreeze(csbFreeze + count);
		flag = userDao.updateUser(seller);
        // 写入账单表（类型：冻结，子类型：冻结，账单类型：财神币）
        BillVO bill = new BillVO();
		bill.setId(KeyUtil.getKey());
		bill.setType(Const.billType_freeze);
		bill.setActionType(Const.actionType_freeze);
		bill.setUserId(userId);
        bill.setPrice(count);
        bill.setTime(DateUtils.dateToString(date));
		bill.setBillType("1");
		billDao.addBill(bill);
		
		if(flag) {
			GuadanVO vo = new GuadanVO();
			// 获取最大的id
			long maxId = tradeDao.getMaxGuadanId();
			if(maxId < Const.oddIdStart.longValue()) {
				maxId = Const.oddIdStart;
			} else {
				maxId += 1L;
			}
			
			vo.setId(KeyUtil.getKey());
			vo.setOddId(String.valueOf(maxId));
			vo.setSellId(userId);
			vo.setPrice(price);
			vo.setCount(count);
			
			double sum = price * count;        
	    	//保留2位小数
	        BigDecimal bg = new BigDecimal(sum);    
	        sum = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
	        vo.setPriceSum(sum);
	        vo.setGuadanTime(DateUtils.dateToString(date));
	        vo.setOperateTime(DateUtils.dateToString(date));
	        vo.setStatus(Const.oddSellSure);
	        vo.setLevel(level);
	        vo.setType("1");
	        flag = tradeDao.addGuadan(vo);
		}
		
		if(flag) {
			jo.put("code", Const.success);
			jo.put("codeDesc", "挂单成功");
		} else {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
		}
		
		return jo;
    }

	@Override
	public JSONObject buy(String oddId, String userId) {
		JSONObject jo = new JSONObject();
		boolean flag = false;
		// 1.判断该单是否已成交
		GuadanVO guadan = tradeDao.getGuadanByIdAndStau(oddId, Const.oddSellSure);
		if(guadan == null) {
			jo.put("code", "202");
			jo.put("codeDesc", "该单已成交");
			return jo;
		}
		
		// 2.判断买家资金是否足够
		UserVO user = userDao.getUserById(userId);
		
		double moneySum = user.getMoneySum(); // 买家的百源币
		double moneyFreeze = user.getMoneyFreeze(); // 买家冻结的百源币
		double csbPoundage = 0.0; // 手续费
		Map<String, Object> rstMap = caiShenBiDao.getCaiShenBiAmount();
		if(!rstMap.isEmpty()) {
			Double poundage = (Double) rstMap.get("poundage");
			csbPoundage = poundage.doubleValue();
		}
		
		double priceSum = guadan.getPriceSum(); // 交易金额
//		double pricePd = priceSum * csbPoundage; // 手续费
		
		if(moneySum - moneyFreeze < priceSum) {
			jo.put("code", "203");
			jo.put("codeDesc", "买家余额不足");
			return jo;
		}
		
		// 3.冻结买家资金
		user.setMoneySum(moneySum);
		user.setMoneyFreeze(moneyFreeze + priceSum); // 冻结交易金额和手续费
		flag = userDao.updateUser(user);
		// 4.写入账单表（类型：冻结， 子类型：冻结，账单类型：百源币）
		if(flag) {
			BillVO bill = new BillVO();
			bill.setId(KeyUtil.getKey());
			bill.setType(Const.billType_freeze);
			bill.setActionType(Const.actionType_freeze);
			bill.setUserId(userId);
			bill.setPrice(priceSum);
			bill.setTime(DateUtils.dateToString(new Date()));
			bill.setBillType("2");
			flag = billDao.addBill(bill);
		}
		
        // 5.在挂单中填入买家，修改挂单状态为2（买家确认）
        if(flag){
            GuadanVO vo = new GuadanVO();
            vo.setOddId(oddId);
            vo.setBuyId(userId);
            vo.setStatus(Const.oddBuySure);
            vo.setOperateTime(DateUtils.dateToString(new Date()));
            flag = tradeDao.updateGuadan(vo);           
        }
        
        if(flag) {
        	jo.put("code", Const.success);
            jo.put("codeDesc", "操作成功");
        } else {
        	jo.put("code", Const.failed);
            jo.put("codeDesc", "系统错误");
        }

        return jo;
	}

	@Override
	public JSONObject sureTrade(String userId, String oddId) {
		JSONObject jo = new JSONObject();
		if(StringUtils.isEmpty(userId) || StringUtils.isEmpty(oddId)) {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
			return jo;
		}
		
		boolean flag = false;
		GuadanVO vo = tradeDao.getGuadanByIdAndStau(oddId, Const.oddBuySure);
		if(vo == null) {
			jo.put("code", "202");
			jo.put("codeDesc", "该单已经成交");
			return jo;
		}
		
		
		// 卖家第二次家确认（修改挂单状态为3，买家扣除百源币和手续费，卖家解冻财神币，转出，买家转入）
		Date date = new Date();
		// 1.修改挂单状态为3
		GuadanVO guadan = new GuadanVO();
		guadan.setOddId(vo.getOddId());
		guadan.setStatus(Const.oddSuccess);
		guadan.setOperateTime(DateUtils.dateToString(date));
		flag = tradeDao.updateGuadan(guadan);
		
		String buyId = vo.getBuyId();
		if(flag) {
			double guaDanCsbCount = vo.getCount(); // 交易的财神币
			double priceSum = vo.getPriceSum(); // 交易金额
			
			double csbPoundage = 0.0; // 手续费
			Map<String, Object> rstMap = caiShenBiDao.getCaiShenBiAmount();
			if(!rstMap.isEmpty()) {
				Double poundage = (Double) rstMap.get("poundage");
				csbPoundage = poundage.doubleValue();
			}
			
			double pricePd = priceSum * csbPoundage; // 手续费

			// 2.扣除买家的百源币，转入购买的财神币
			UserVO buyer = userDao.getUserById(buyId);
			double moneySum = buyer.getMoneySum(); // 买家的百源币
			double moneyFreeze = buyer.getMoneyFreeze(); // 买家冻结的百源币
			
			buyer.setMoneySum(moneySum - priceSum);
			buyer.setMoneyFreeze(moneyFreeze - priceSum);
			buyer.setCaiShenBiSum(buyer.getCaiShenBiSum() + guaDanCsbCount);
			flag = userDao.updateUser(buyer);
			
			if(flag) {
				// 写入账单表（类型：冻结，子类型：解冻，账单类型：百源币）
				BillVO bill = new BillVO();
				bill.setId(KeyUtil.getKey());
				bill.setType(Const.billType_freeze);
				bill.setActionType(Const.actionType_refreeze);
				bill.setUserId(buyId);
				bill.setPrice(priceSum);
				bill.setTime(DateUtils.dateToString(date));
				bill.setBillType("2");
				billDao.addBill(bill);
				
				// 写入账单表（类型：转出，子类型：转出，账单类型：百源币）
				bill.setId(KeyUtil.getKey());
                bill.setType(Const.billType_roolout);
                bill.setActionType(Const.actionType_roolout);
                bill.setUserId(buyId);
                bill.setPrice(priceSum);
                bill.setTime(DateUtils.dateToString(date));
                bill.setBillType("2");
                billDao.addBill(bill);
                
                // 写入账单表（类型：转入，子类型：转入，账单类型：财神币）
                bill.setId(KeyUtil.getKey());
                bill.setType(Const.billType_roolin);
                bill.setActionType(Const.actionType_roolin);
                bill.setUserId(buyId);
                bill.setPrice(guaDanCsbCount);
                bill.setTime(DateUtils.dateToString(date));
                bill.setBillType("1");
                billDao.addBill(bill);
			}
			
			// 3.百源币转入卖家账号
			if(flag) {
				String sellId = vo.getSellId();
				UserVO seller = userDao.getUserById(sellId);
				seller.setCaiShenBiSum(seller.getCaiShenBiSum() - guaDanCsbCount);
				seller.setCaiShenBiFreeze(seller.getCaiShenBiFreeze() - guaDanCsbCount);
				seller.setMoneySum(seller.getMoneySum() + priceSum - pricePd); // 扣除的卖家的手续费
				flag = userDao.updateUser(seller);
				
				// 写入账单表（类型：转入，子类型：转入，账单类型：百源币）
				BillVO bill = new BillVO();
				bill.setId(KeyUtil.getKey());
                bill.setType(Const.billType_roolin);
                bill.setActionType(Const.actionType_roolin);
                bill.setUserId(sellId);
                bill.setPrice(priceSum);
                bill.setTime(DateUtils.dateToString(date));
                bill.setBillType("2");
                billDao.addBill(bill);
                
                // 写入账单表（类型：转出，子类型：手续费，账单类型：百源币）
                bill.setId(KeyUtil.getKey());
                bill.setType(Const.billType_roolout);
                bill.setActionType(Const.actionType_fee);
                bill.setUserId(sellId);
                bill.setPrice(pricePd);
                bill.setTime(DateUtils.dateToString(date));
                bill.setBillType("2");
                billDao.addBill(bill);
                
                // 记录财神币扣除手续费记录
                PoundageDetailsVO pd = new PoundageDetailsVO();
                pd.setId(KeyUtil.getKey());
                pd.setType("1");
                pd.setTradePrice(priceSum);
                pd.setTradePoundage(csbPoundage);
                pd.setPdPrice(pricePd);
                pd.setUserId(sellId);
                pd.setCreateTime(DateUtils.dateToString(date, "yyyy-MM-dd HH:mm:ss"));
                pd.setGuadanType("3");
                poundageDetailsDao.addPoundageDetails(pd);
                
                // 写入账单表（类型：转出，子类型：转出，账单类型：财神币）
                bill.setId(KeyUtil.getKey());
                bill.setType(Const.billType_roolout);
                bill.setActionType(Const.actionType_roolout);
                bill.setUserId(sellId);
                bill.setPrice(guaDanCsbCount);
                bill.setTime(DateUtils.dateToString(date));
                bill.setBillType("1");
                billDao.addBill(bill);
                
                // 写入账单表（类型：冻结，子类型：解冻，账单类型：财神币）
                bill.setId(KeyUtil.getKey());
                bill.setType(Const.billType_freeze);
                bill.setActionType(Const.actionType_refreeze);
                bill.setUserId(sellId);
                bill.setPrice(guaDanCsbCount);
                bill.setTime(DateUtils.dateToString(date));
                bill.setBillType("1");
                billDao.addBill(bill);
			}
		}
		
		//写入到交易表，便于计算；修改简易交易表中的最高价和数量
        if(flag){
            TradeVO trade = new TradeVO();
            trade.setId(KeyUtil.getKey());
            trade.setBuyId(vo.getBuyId());
            trade.setSellId(vo.getSellId());
            trade.setOddNo(vo.getOddId());
            trade.setPrice(vo.getPrice());
            trade.setTradeCount(vo.getCount());
            trade.setTradeMoney(vo.getPriceSum());
            trade.setTradeTime(DateUtils.dateToString(date));       
            tradeDao.addTrade(trade);
            
            //修改简易交易表中财神币的最高价和数量
            tradeDao.updateTradeSimpleCsbCount(DateUtils.dateToString(date, "yyyy-MM-dd"), vo.getCount(), vo.getPrice());
        }
		
		if(flag) {
			jo.put("code", Const.success);
			jo.put("codeDesc", "操作成功");
		} else {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
		}
		
		return jo;
	}

	@Override
	public JSONObject cancelOdd(String oddId) {
		JSONObject jo = new JSONObject();
		boolean flag = false;
		Date date = new Date();
		GuadanVO vo = tradeDao.getGuadanByIdAndStau(oddId, Const.oddSellSure);
		// 若没有。说明已经有买家确认，提示用户刷新
		if(vo == null) {
			jo.put("code", "202");
			jo.put("codeDesc", "撤销失败，该单已有买家确认");
			return jo;
		}
		
		// 1.卖家解冻
		String sellId = vo.getSellId();
		UserVO seller = userDao.getUserById(sellId);
		seller.setCaiShenBiSum(seller.getCaiShenBiSum());
		seller.setCaiShenBiFreeze(seller.getCaiShenBiFreeze() - vo.getCount());
		flag = userDao.updateUser(seller);
		// 写入账单表（类型：冻结，子类型：解冻，账单类型：财神币）
		BillVO bill = new BillVO();
		bill.setId(KeyUtil.getKey());
		bill.setType(Const.billType_freeze);
		bill.setActionType(Const.actionType_refreeze);
		bill.setUserId(sellId);
		bill.setPrice(vo.getCount());
		bill.setTime(DateUtils.dateToString(date));
		bill.setBillType("1");
		billDao.addBill(bill);
		
		// 2.修改挂单状态为撤销状态
		if(flag) {
			GuadanVO guadan = new GuadanVO();
			guadan.setOddId(oddId);
			guadan.setStatus(Const.oddCancel);
			guadan.setOperateTime(DateUtils.dateToString(date));
			flag = tradeDao.updateGuadan(guadan);
		}
		
		if(flag) {
			jo.put("code", Const.success);
			jo.put("codeDesc", "操作成功");
		} else {
			jo.put("code", Const.failed);
			jo.put("codeDesc", "系统错误");
		}
		
		return jo;
	}

	@Override
	public JSONArray getGuadanList(String userId, String level) {
		JSONArray ja = new JSONArray();
		List<GuadanVO> list = tradeDao.getGuadanList(userId, level, "1");
		if(list != null) {
			for(GuadanVO vo : list) {
				JSONObject jo = new JSONObject();
				jo.put("oddId", StringUtils.nullToEmpty(vo.getOddId()));
                jo.put("time", StringUtils.nullToEmpty(vo.getOperateTime()));
                jo.put("sellId", StringUtils.nullToEmpty(vo.getSellId()));
                jo.put("count", vo.getCount());
                jo.put("price", vo.getPrice());
                jo.put("priceSum", vo.getPriceSum());
                ja.add(jo);
			}
		}
		
		return ja;
	}

	@Override
	public JSONArray mailBox(String userId, String level) {
		JSONArray retArr = new JSONArray();
		if(StringUtils.isEmpty(userId)) {
			return retArr;
		}
		
		List<GuadanVO> list = tradeDao.getGuadanByBuyOrSell(userId, level, "1");
		if(list != null && list.size() > 0) {
			for(GuadanVO vo : list) {
				String buyId = vo.getBuyId();
				String sellId = vo.getSellId();
				int status = vo.getStatus();
				
				// 交易状态为1，且是卖家，提示 您挂单卖出20财神币，单价10，总价200（撤单）
				if(status == 1 && userId.equals(sellId)) {
					JSONObject jo = new JSONObject();
					jo.put("oddId", StringUtils.nullToEmpty(vo.getOddId()));
					jo.put("oddId", StringUtils.nullToEmpty(vo.getOddId()));
                    jo.put("time", StringUtils.nullToEmpty(vo.getOperateTime()));
                    jo.put("opponentId", StringUtils.nullToEmpty(buyId));
                    
                    double count = vo.getCount();
                    double price = vo.getPrice();
                    double priceSum = vo.getPriceSum();
                    String content = "您挂单卖出"+count+"财神币，单价"+price+"，总价"+priceSum;
                    jo.put("content", StringUtils.nullToEmpty(content));
                    jo.put("cancel", "1");
                    jo.put("account", "0");
                    jo.put("sure", "0");
                    retArr.add(jo);
				}
				
				// 交易状态为2，且是买家，提示 等待卖家二次确认，卖家二次确认后即可收到财神币（交易账户）
				if(status == 2 && userId.equals(buyId)) {
					JSONObject jo = new JSONObject();
                    jo.put("oddId", StringUtils.nullToEmpty(vo.getOddId()));
                    jo.put("time", StringUtils.nullToEmpty(vo.getOperateTime()));
                    jo.put("opponentId", StringUtils.nullToEmpty(sellId));
                    
                    String content = "等待卖家二次确认，卖家二次确认后即可收到财神币";
                    jo.put("content", StringUtils.nullToEmpty(content));
                    jo.put("cancel", "0");
                    jo.put("account", "1");
                    jo.put("sure", "0");
                    retArr.add(jo);
				}
				
				//交易状态为2，且是卖家，提示 转给xxx20VRC，等待您的二次确认（确认交易，交易账户）
                if(status == 2 && userId.equals(sellId)){
                    JSONObject jo = new JSONObject();
                    jo.put("oddId", StringUtils.nullToEmpty(vo.getOddId()));
                    jo.put("time", StringUtils.nullToEmpty(vo.getOperateTime()));
                    jo.put("opponentId", StringUtils.nullToEmpty(buyId));
                    
                    double count = vo.getCount();
                    String content = "转给"+buyId+"用户"+count+"财神币，等待您的二次确认";
                    jo.put("content", StringUtils.nullToEmpty(content));
                    jo.put("button", "account,sure");
                    jo.put("cancel", "0");
                    jo.put("account", "1");
                    jo.put("sure", "1");
                    retArr.add(jo);
                }
			}
		}
		
		return retArr;
	}

}
