package com.vrc.bo.trade.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lianbao.dao.CaiShenBiDao;
import com.lianbao.dao.PoundageDetailsDao;
import com.lianbao.vo.PoundageDetailsVO;
import com.vrc.bo.trade.TradeBO;
import com.vrc.common.Const;
import com.vrc.dao.sys.UserDAO;
import com.vrc.dao.sys.VrcConfigDAO;
import com.vrc.dao.trade.BillDAO;
import com.vrc.dao.trade.TradeDAO;
import com.vrc.util.DateUtils;
import com.vrc.util.KeyUtil;
import com.vrc.util.StringUtils;
import com.vrc.vo.common.PageInfo;
import com.vrc.vo.sys.UserVO;
import com.vrc.vo.trade.BillVO;
import com.vrc.vo.trade.GuadanVO;
import com.vrc.vo.trade.TradeSimpleVO;
import com.vrc.vo.trade.TradeVO;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 交易相关bo
 * @author liboxing
 *
 */
@Service("tradeBO")
@Transactional
public class TradeBOImpl implements TradeBO {

    @Resource
    private TradeDAO tradeDAO;
    @Resource
    private UserDAO userDAO;
    @Resource
    private BillDAO billDAO;
    @Resource
    private CaiShenBiDao caiShenBiDao;
    @Resource
    private VrcConfigDAO configDAO;
    @Resource
    private PoundageDetailsDao poundageDetailsDao;
    
    @Override
    public boolean postOdd(String userId, double price, double count, String level) {
        if(userId == null){
            return false;
        }
        
        GuadanVO vo = new GuadanVO();
        //获取最大的id
        long maxId = tradeDAO.getMaxGuadanId();
        if(maxId < Const.oddIdStart.longValue()){
            maxId = Const.oddIdStart;
        } else {
            maxId = maxId + 1;
        }
        
        vo.setId(KeyUtil.getKey());
        vo.setOddId(String.valueOf(maxId));
        vo.setBuyId(userId);
        vo.setPrice(price);
        vo.setCount(count);
        
        double sum = price * count;        
    	//保留2位小数
        BigDecimal bg = new BigDecimal(sum);    
        sum = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        vo.setPriceSum(sum);
        vo.setGuadanTime(DateUtils.dateToString(new Date()));
        vo.setOperateTime(DateUtils.dateToString(new Date()));
        vo.setStatus(Const.oddPost);
        vo.setLevel(level);
        vo.setType("0");
        boolean flag = tradeDAO.addGuadan(vo);
        return flag;
    }

    @Override
    public JSONArray getGuadanList(String userId,String level) {
        JSONArray ja = new JSONArray();
        List<GuadanVO> list = tradeDAO.getGuadanList(userId, level, "0");
        if(list != null){
            for(GuadanVO vo : list){
                JSONObject jo = new JSONObject();
                jo.put("oddId", StringUtils.nullToEmpty(vo.getOddId()));
                jo.put("time", StringUtils.nullToEmpty(vo.getOperateTime()));
                jo.put("buyId", StringUtils.nullToEmpty(vo.getBuyId()));
                jo.put("count", vo.getCount());
                jo.put("price", vo.getPrice());
                jo.put("priceSum", vo.getPriceSum());
                ja.add(jo);
            }
        }
        return ja;
    }

    @Override
    public synchronized JSONObject sell(String oddId, String userId) {
        JSONObject jo = new JSONObject();
        boolean flag = false;
        //1、判断该单是否已成交
        GuadanVO guadan = tradeDAO.getGuadanByIdAndStau(oddId, Const.oddPost);
        if(guadan == null){
            jo.put("code", "203");
            jo.put("codeDesc", "该单已成交");
            return jo;
        }
        
        try {
            //2、判断用户资金是否足够，冻结资金
            UserVO user = userDAO.getUserById(userId);
            double guaDanCsbCount = guadan.getCount();  // 挂单时记录的财神币数量
            
            double csbSum = user.getCaiShenBiSum(); // 用户的财神币余额
            double csbFreeze = user.getCaiShenBiFreeze(); // 用户冻结的财神币         
            if(csbSum - csbFreeze < guaDanCsbCount) {
                jo.put("code", "202");
                jo.put("codeDesc", "财神币余额不足");
                return jo;
            } else {
            	// 卖家第一次确认时，冻结财神币
            	user.setCaiShenBiSum(csbSum);
            	user.setCaiShenBiFreeze(csbFreeze + guaDanCsbCount);
                flag = userDAO.updateUser(user);
                //写入账单表（类型：冻结，子类型：冻结，账单类型：财神币）
                if(flag){
                    BillVO bill = new BillVO();
                    bill.setId(KeyUtil.getKey());
                    bill.setType(Const.billType_freeze);
                    bill.setActionType(Const.actionType_freeze);
                    bill.setUserId(userId);
                    bill.setPrice(guaDanCsbCount); // 这一次交易冻结了的财神币数目（等于挂了单的财神币数目）
                    bill.setTime(DateUtils.dateToString(new Date()));
                    bill.setBillType("1");
                    billDAO.addBill(bill);
                }
            }
            
            //3、在挂单中填入卖家（userId），修改挂单状态为1（卖家已确认）
            if(flag){
                GuadanVO vo = new GuadanVO();
                vo.setOddId(oddId);
                vo.setSellId(userId);
                vo.setStatus(Const.oddSellSure);
                vo.setOperateTime(DateUtils.dateToString(new Date()));
                tradeDAO.updateGuadan(vo);           
                jo.put("code", Const.success);
                jo.put("codeDesc", "操作成功");
                return jo;
            } else {
                jo.put("code", Const.failed);
                jo.put("codeDesc", "系统错误");
            }
        } catch (Exception e) {
            e.printStackTrace();
            jo.put("code", Const.failed);
            jo.put("codeDesc", "系统错误");
        }
        return jo;
    }

    @Override
    public synchronized JSONObject pointSell(String buyId, String sellId, double count, double price) {
        JSONObject jo = new JSONObject();
        boolean flag = false;
        
        try {        	
            //1、判断用户（卖家）资金是否足够，冻结资金
            UserVO sellUser = userDAO.getUserById(sellId);
            double csbSum = sellUser.getCaiShenBiSum();
            double csbFreeze = sellUser.getCaiShenBiFreeze();
            
            double oddMoney = count;       // 交易的财神币数量
            
            if(csbSum - csbFreeze < oddMoney) {
                jo.put("code", "202");
                jo.put("codeDesc", "财神币余额不足");
                return jo;
            } else {
                //2、判断买家是否存在
                UserVO buyer = userDAO.getUserById(buyId);
                if(buyer == null){
                    jo.put("code", "204");
                    jo.put("codeDesc", "买家不存在");
                    return jo;
                } else {
                    //3、冻结卖家财神币
                    sellUser.setCaiShenBiSum(csbSum);
                    sellUser.setCaiShenBiFreeze(csbFreeze + oddMoney);
                    flag = userDAO.updateUser(sellUser);
                    
                    //4、写入账单表（类型：冻结，子类型：冻结，账单类型：财神币）
                    if(flag){
                        BillVO bill = new BillVO();
                        bill.setId(KeyUtil.getKey());
                        bill.setType(Const.billType_freeze);
                        bill.setActionType(Const.actionType_freeze);
                        bill.setUserId(sellId);
                        bill.setPrice(oddMoney);
                        bill.setTime(DateUtils.dateToString(new Date()));
                        bill.setBillType("1");
                        billDAO.addBill(bill);
                    }
                }
            }
            
            //5、创建一个挂单，并设置状态为1
            if(flag){
                GuadanVO vo = new GuadanVO();
                //获取最大的id
                long maxId = tradeDAO.getMaxGuadanId();
                if(maxId < Const.oddIdStart.longValue()){
                    maxId = Const.oddIdStart;
                } else {
                    maxId = maxId + 1;
                }
                vo.setId(KeyUtil.getKey());
                vo.setOddId(String.valueOf(maxId));
                vo.setBuyId(buyId);
                vo.setSellId(sellId);
                vo.setPrice(price);
                vo.setCount(count);
                vo.setPriceSum(price * count);
                vo.setGuadanTime(DateUtils.dateToString(new Date()));
                vo.setOperateTime(DateUtils.dateToString(new Date()));
                vo.setStatus(Const.oddSellSure);
                vo.setType("0");
                flag = tradeDAO.addGuadan(vo);
                jo.put("code", Const.success);
                jo.put("codeDesc", "操作成功");
                return jo;
            } else {
                jo.put("code", Const.failed);
                jo.put("codeDesc", "系统错误");
            }
        } catch (Exception e) {
            e.printStackTrace();
            jo.put("code", Const.failed);
            jo.put("codeDesc", "系统错误");
            return jo;
        }
        return jo;
    }

    @Override
    public JSONArray mailBox(String userId, String level) {
        JSONArray retArr = new JSONArray();
        if(StringUtils.isEmpty(userId)){
            return retArr;
        }
        
        List<GuadanVO> list = tradeDAO.getGuadanByBuyOrSell(userId, level, "0");
        if(list != null && list.size() > 0){
            for(GuadanVO vo : list){
                String buyId = vo.getBuyId();
                String sellId = vo.getSellId();
                int status = vo.getStatus();
                
                //交易状态为0，且是买家，提示 您挂单买入20VRC，单价10，总价200（撤单）
                if(status == 0 && userId.equals(buyId)){
                    JSONObject jo = new JSONObject();
                    jo.put("oddId", StringUtils.nullToEmpty(vo.getOddId()));
                    jo.put("time", StringUtils.nullToEmpty(vo.getOperateTime()));
                    jo.put("opponentId", StringUtils.nullToEmpty(sellId));
                    
                    double count = vo.getCount();
                    double price = vo.getPrice();
                    double priceSum = vo.getPriceSum();
                    String content = "您挂单买入"+count+"财神币，单价"+price+"，总价"+priceSum;
                    jo.put("content", StringUtils.nullToEmpty(content));
                    jo.put("cancel", "1");
                    jo.put("account", "0");
                    jo.put("sure", "0");
                    retArr.add(jo);
                }
                
                //交易状态为1，且是买家，提示 收到xxx转入20VRC，请确认接收（交易账户，确认交易）
                if(status == 1 && userId.equals(buyId)) {
                    JSONObject jo = new JSONObject();
                    jo.put("oddId", StringUtils.nullToEmpty(vo.getOddId()));
                    jo.put("time", StringUtils.nullToEmpty(vo.getOperateTime()));
                    jo.put("opponentId", StringUtils.nullToEmpty(sellId));
                    
                    double count = vo.getCount();
                    String content = "收到"+sellId+"转入"+count+"财神币，请确认接收";
                    jo.put("content", StringUtils.nullToEmpty(content));
                    jo.put("cancel", "0");
                    jo.put("account", "1");
                    jo.put("sure", "1");
                    retArr.add(jo);
                }
                
                //交易状态为1，且是卖家，提示 转给xxx20VRC，等待对方确认（交易账户）
                if(status == 1 && userId.equals(sellId)){
                    JSONObject jo = new JSONObject();
                    jo.put("oddId", StringUtils.nullToEmpty(vo.getOddId()));
                    jo.put("time", StringUtils.nullToEmpty(vo.getOperateTime()));
                    jo.put("opponentId", StringUtils.nullToEmpty(buyId));
                    
                    double count = vo.getCount();
                    String content = "转给"+buyId+"用户"+count+"财神币，等待对方确认";
                    jo.put("content", StringUtils.nullToEmpty(content));
                    jo.put("cancel", "0");
                    jo.put("account", "1");
                    jo.put("sure", "0");
                    retArr.add(jo);
                }
                
                //交易状态为2，且是买家，提示 等待卖家二次确认，卖家二次确认后即可收到VRC（交易账户）
                if(status == 2 && userId.equals(buyId)) {
                    JSONObject jo = new JSONObject();
                    jo.put("oddId", StringUtils.nullToEmpty(vo.getOddId()));
                    jo.put("time", StringUtils.nullToEmpty(vo.getOperateTime()));
                    jo.put("opponentId", StringUtils.nullToEmpty(sellId));
                    
                    String content = "等待卖家二次确认，卖家二次确认后即可收到财神币";
                    jo.put("content", StringUtils.nullToEmpty(content));
                    jo.put("cancel", "0");
                    jo.put("account", "1");
                    jo.put("sure", "0");
                    retArr.add(jo);
                }
                
                //交易状态为2，且是卖家，提示 转给xxx20VRC，等待您的二次确认（确认交易，交易账户）
                if(status == 2 && userId.equals(sellId)){
                    JSONObject jo = new JSONObject();
                    jo.put("oddId", StringUtils.nullToEmpty(vo.getOddId()));
                    jo.put("time", StringUtils.nullToEmpty(vo.getOperateTime()));
                    jo.put("opponentId", StringUtils.nullToEmpty(buyId));
                    
                    double count = vo.getCount();
                    String content = "转给"+buyId+"用户"+count+"财神币，等待您的二次确认";
                    jo.put("content", StringUtils.nullToEmpty(content));
                    jo.put("button", "account,sure");
                    jo.put("cancel", "0");
                    jo.put("account", "1");
                    jo.put("sure", "1");
                    retArr.add(jo);
                }
            }
        }
        
        return retArr;
    }

    @Override
    public JSONObject cancelOdd(String oddId) {
        JSONObject jo = new JSONObject();
        boolean flag = false;
        
        try {
            //获取单号为oddId，状态为挂单中的单
            GuadanVO vo = tradeDAO.getGuadanByIdAndStau(oddId, Const.oddPost);
            //若没有，说明已经有卖家确认，提示用户刷新
            if(vo == null){
                jo.put("code", "202");
                jo.put("codeDesc", "撤销失败，该单已有卖家确认");
                return jo;
            } else {
                GuadanVO guadan = new GuadanVO();
                guadan.setOddId(oddId);
                guadan.setStatus(Const.oddCancel);
                guadan.setOperateTime(DateUtils.dateToString(new Date()));
                flag = tradeDAO.updateGuadan(guadan);
            }
            
            if(flag){
                jo.put("code", Const.success);
                jo.put("codeDesc", "撤销成功");
            } else {
                jo.put("code", Const.failed);
                jo.put("codeDesc", "系统错误");
            }
        } catch (Exception e) {
            e.printStackTrace();
            jo.put("code", Const.failed);
            jo.put("codeDesc", "系统错误");
        }
        return jo;
    }

    @Override
    public JSONObject sureTrade(String userId, String oddId) {
        JSONObject jo = new JSONObject();
        if(StringUtils.isEmpty(userId) || StringUtils.isEmpty(oddId)){
            jo.put("code", Const.failed);
            jo.put("codeDesc", "系统错误");
            return jo;
        }
        
        boolean flag = false;
        GuadanVO vo = tradeDAO.getGuadanByOddId(oddId);
        //买家确认，只需要修改挂单状态
        if(userId.equalsIgnoreCase(vo.getBuyId()) && vo.getStatus() == Const.oddSellSure){
           flag = buySure(vo);
        }
        //卖家确认（修改挂单状态为3，卖家解冻，转出，扣手续费，买家转入）
        if(userId.equalsIgnoreCase(vo.getSellId()) && vo.getStatus() == Const.oddBuySure){
            Date date = new Date();
            flag = sellSure(vo, date);
            
            //写入到交易表，便于计算；修改简易交易表中的最高价和数量
            if(flag){
                TradeVO trade = new TradeVO();
                trade.setId(KeyUtil.getKey());
                trade.setBuyId(vo.getBuyId());
                trade.setSellId(vo.getSellId());
                trade.setOddNo(vo.getOddId());
                trade.setPrice(vo.getPrice());
                trade.setTradeCount(vo.getCount());
                trade.setTradeMoney(vo.getPriceSum());
                trade.setTradeTime(DateUtils.dateToString(date));       
                tradeDAO.addTrade(trade);
                
                //修改简易交易表中财神币的最高价和数量
                tradeDAO.updateTradeSimpleCsbCount(DateUtils.dateToString(date, "yyyy-MM-dd"), vo.getCount(), vo.getPrice());
            }
        }
        
        if(flag){
            jo.put("code", Const.success);
            jo.put("codeDesc", "操作成功");
        } else {
            jo.put("code", Const.failed);
            jo.put("codeDesc", "系统错误");
        }
        return jo;
    }
    
    //买家确认，只需要修改挂单状态为2
    private boolean buySure(GuadanVO vo){
        boolean flag = false;
        GuadanVO guadan = new GuadanVO();
        guadan.setOddId(vo.getOddId());
        guadan.setStatus(Const.oddBuySure);
        guadan.setOperateTime(DateUtils.dateToString(new Date()));
        flag = tradeDAO.updateGuadan(guadan);
        return flag;
    }
    //卖家确认（修改挂单状态为3，卖家解冻，转出，扣手续费，买家转入）
    private boolean sellSure(GuadanVO vo,Date date){
        boolean flag = false;
        //1、修改挂单状态为3
        GuadanVO guadan = new GuadanVO();
        guadan.setOddId(vo.getOddId());
        guadan.setStatus(Const.oddSuccess);
        guadan.setOperateTime(DateUtils.dateToString(date));
        flag = tradeDAO.updateGuadan(guadan);
        
        if(flag){
            double guaDanCsbCount = vo.getCount();     // 挂了单的财神币数目
            
            double csbPoundage = 0.0; // 手续费
            Map<String, Object> rstMap = caiShenBiDao.getCaiShenBiAmount();
            if(!rstMap.isEmpty()) {
            	Double poundage = (Double) rstMap.get("poundage");
                csbPoundage = poundage.doubleValue();
            }
            
            double priceSum = vo.getPriceSum(); // 挂了单的交易金额（交易的是百源币）
            double pricePd = priceSum * csbPoundage; // 扣除的手续费
                        
            //2、卖家解冻，增加金额 
            String sellId = vo.getSellId();
            UserVO seller = userDAO.getUserById(sellId);
            
            seller.setMoneySum(seller.getMoneySum() + (priceSum - pricePd)); // 扣除手续费后实际金额
            seller.setCaiShenBiSum(seller.getCaiShenBiSum() - guaDanCsbCount);
            seller.setCaiShenBiFreeze(seller.getCaiShenBiFreeze() - guaDanCsbCount);
            flag = userDAO.updateUser(seller);
            if(flag){
                //写入账单表（类型：冻结，子类型：解冻，账单类型：财神币）
                BillVO bill = new BillVO();
                bill.setId(KeyUtil.getKey());
                bill.setType(Const.billType_freeze);
                bill.setActionType(Const.actionType_refreeze);
                bill.setUserId(sellId);
                bill.setPrice(guaDanCsbCount);
                bill.setTime(DateUtils.dateToString(date));
                bill.setBillType("1");
                billDAO.addBill(bill);
                
                //写入账单表（类型：转出，子类型：转出，账单类型：财神币）
                bill.setId(KeyUtil.getKey());
                bill.setType(Const.billType_roolout);
                bill.setActionType(Const.actionType_roolout);
                bill.setUserId(sellId);
                bill.setPrice(guaDanCsbCount);
                bill.setTime(DateUtils.dateToString(date));
                bill.setBillType("1");
                billDAO.addBill(bill);
                
                //写入账单表（类型：转出，子类型：手续费，账单类型：百源币）
                bill.setId(KeyUtil.getKey());
                bill.setType(Const.billType_roolout);
                bill.setActionType(Const.actionType_fee);
                bill.setUserId(sellId);
                bill.setPrice(pricePd);
                bill.setTime(DateUtils.dateToString(date));
                bill.setBillType("2");
                billDAO.addBill(bill);
                
                // 记录财神币扣手续费记录
                PoundageDetailsVO pd = new PoundageDetailsVO();
                pd.setId(KeyUtil.getKey());
                pd.setType("1");
                pd.setTradePrice(priceSum);
                pd.setTradePoundage(csbPoundage);
                pd.setPdPrice(pricePd);
                pd.setUserId(sellId);
                pd.setCreateTime(DateUtils.dateToString(date, "yyyy-MM-dd HH:mm:ss"));
                pd.setGuadanType("2");
                poundageDetailsDao.addPoundageDetails(pd);
                
                // 写入账单表（类型：转入，子类型：转入，账单类型：百源币）
                bill.setId(KeyUtil.getKey());
                bill.setType(Const.billType_roolin);
                bill.setActionType(Const.actionType_roolin);
                bill.setUserId(sellId);
                bill.setPrice(priceSum);
                bill.setTime(DateUtils.dateToString(date));
                bill.setBillType("2");
                billDAO.addBill(bill);
            }
            
            //3、转入到买家账户（挂单人）
            if(flag){
                String buyId = vo.getBuyId();
                UserVO buyer = userDAO.getUserById(buyId);
                buyer.setCaiShenBiSum(buyer.getCaiShenBiSum() + guaDanCsbCount); // 增加买入的财神币
                buyer.setMoneySum(buyer.getMoneySum() - priceSum); // 扣除用户金额（此时不需要扣手续费，手续费由卖家负责）
                flag = userDAO.updateUser(buyer);
                
                //写入账单表（类型：转入，子类型：转入，账单类型：财神币）
                BillVO bill = new BillVO();
                bill.setId(KeyUtil.getKey());
                bill.setType(Const.billType_roolin);
                bill.setActionType(Const.actionType_roolin);
                bill.setUserId(buyId);
                bill.setPrice(guaDanCsbCount);
                bill.setTime(DateUtils.dateToString(date));
                bill.setBillType("1");
                billDAO.addBill(bill);
                
                // 写入账单表（类型：转出，子类型：转出，账单类型：百源币）
                bill.setId(KeyUtil.getKey());
                bill.setType(Const.billType_roolout);
                bill.setActionType(Const.actionType_roolout);
                bill.setUserId(buyId);
                bill.setPrice(priceSum);
                bill.setTime(DateUtils.dateToString(date));
                bill.setBillType("2");
                billDAO.addBill(bill);
            }
            
        }
        return flag;
    }

    @Override
    public JSONObject getTradeSimple() {        
        Map<String, Object> map = tradeDAO.getLastSimpleTrade();
        JSONObject jo = JSONObject.fromObject(map);
        return jo;
    }

    @Override
    public JSONObject getChartData() {
        JSONObject jo = new JSONObject();
        
        Calendar ca = Calendar.getInstance();
        Date endDate = ca.getTime();
        
        Calendar ca2 = Calendar.getInstance();
        ca2.add(Calendar.DAY_OF_MONTH, -Const.chartDayLength);
        Date startDate = ca2.getTime();
        
        
        String startTime = DateUtils.dateToString(startDate, "yyyy-MM-dd 00:00:00");
        String endTime = DateUtils.dateToString(endDate, "yyyy-MM-dd 23:59:59");
        List<Map<String, Object>> list = tradeDAO.getChartDate(startTime, endTime);
        
        List<String> dayList = new ArrayList<>();
        List<Double> priceList = new ArrayList<>();
        List<Double> csbPriceList = new ArrayList<>();
        if(list != null && list.size() > 0){
            for(Map<String, Object> map : list){
                String day = (String) map.get("day");
                double price = (double) map.get("price");
                double csbPrice = (double) map.get("csbPrice");
                day = DateUtils.formatDate(day, "yyyy-MM-dd", "MM-dd");
                dayList.add(day);
                priceList.add(price);
                csbPriceList.add(csbPrice);
            }
        }
        
        jo.put("dayList", dayList);
        jo.put("priceList", priceList);
        jo.put("csbPriceList", csbPriceList);
        return jo;
    }
    
    @Override
	public JSONObject getPostBillMsg(String level) {
    	double min = 0;
    	double max = 0;
    	String tips = "";
    	
    	try {
    		if("0".equals(level)) {
    			String minStr = configDAO.getConfigByKey("zeroguadanmin");
    			min = Double.valueOf(minStr);
    			String maxStr = configDAO.getConfigByKey("zeroguadanmax");
    			max = Double.valueOf(maxStr);
    			tips = configDAO.getConfigByKey("zeroguadantip");
    		}
    		
			if("1".equals(level)){
				String minStr = configDAO.getConfigByKey("firstguadanmin");
				min = Double.valueOf(minStr);
				String maxStr = configDAO.getConfigByKey("firstguadanmax");
				max = Double.valueOf(maxStr);
				tips = configDAO.getConfigByKey("firstguadantip");
			}
			
			if("2".equals(level)){
				String minStr = configDAO.getConfigByKey("secondguadanmin");
				min = Double.valueOf(minStr);
				String maxStr = configDAO.getConfigByKey("secondguadanmax");
				max = Double.valueOf(maxStr);
				tips = configDAO.getConfigByKey("secondguadantip");
			}
			
			if("3".equals(level)) {
				String minStr = configDAO.getConfigByKey("thirdguadanmin");
				min = Double.valueOf(minStr);
				String maxStr = configDAO.getConfigByKey("thirdguadanmax");
				max = Double.valueOf(maxStr);
				tips = configDAO.getConfigByKey("thirdguadantip");
			}
			
			if("4".equals(level)) {
				String minStr = configDAO.getConfigByKey("fouthguadanmin");
				min = Double.valueOf(minStr);
				String maxStr = configDAO.getConfigByKey("fouthguadanmax");
				max = Double.valueOf(maxStr);
				tips = configDAO.getConfigByKey("fouthguadantip");
			}
			if("5".equals(level)) {
				String minStr = configDAO.getConfigByKey("fifthguadanmin");
				min = Double.valueOf(minStr);
				String maxStr = configDAO.getConfigByKey("fifthguadanmax");
				max = Double.valueOf(maxStr);
				tips = configDAO.getConfigByKey("fifthguadantip");
			}
			
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
    	
    	JSONObject jo = new JSONObject();
    	jo.put("min", min);
    	jo.put("max", max);
    	jo.put("tips", tips);
		return jo;
	}
    
    @Override
	public JSONObject getPoundageMsg() {
		String tips = configDAO.getConfigByKey("poundagetip");
		JSONObject jo = new JSONObject();
		jo.put("tips", tips);
		return jo;
	}
    
    @Override
    public JSONObject getBillList(String userId, String type, PageInfo page, String billType) {
        JSONObject jo = new JSONObject();
        List<BillVO> list = billDAO.getBillList(userId, type, page, billType);
        jo.put("data", list);
        jo.put("pageNum", page.getPageNum());
        jo.put("pageSize", page.getPageSize());
        jo.put("total", page.getTotal());
        return jo;
    }

	@Override
	public boolean saveTradeSimple(TradeSimpleVO vo) {
		if(vo == null){
			return false;
		}
		String id = KeyUtil.getKey32();
		vo.setId(id);
		boolean flag = tradeDAO.saveTradeSimple(vo);
		return flag;
	}

	@Override
	public boolean updateTradeSimple(TradeSimpleVO vo) {
		if(vo == null){
			return false;
		}
		boolean flag = tradeDAO.updateTradeSimple(vo);
		return flag;
	}

	@Override
	public PageInfo getTradePage(PageInfo page, String keyword, String startTime, String endTime) {
		List<TradeVO> list = tradeDAO.getTradePage(page, keyword, startTime, endTime);
		page.setData(list);
		return page;
	}

	@Override
	public PageInfo getSimpleTradePage(PageInfo page) {
		List<TradeSimpleVO> list = tradeDAO.getSimpleTradePage(page);
		page.setData(list);
		return page;
	}

	@Override
	public boolean deleteTrade(String ids) {
		if(StringUtils.isEmpty(ids)) {
			return false;
		}
		String[] idArray = ids.split(",");
		return tradeDAO.deleteTrade(idArray);
	}
}
