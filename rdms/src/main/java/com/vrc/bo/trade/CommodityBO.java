package com.vrc.bo.trade;

import java.math.BigDecimal;

import org.springframework.transaction.annotation.Transactional;

import com.vrc.vo.common.PageInfo;
import com.vrc.vo.trade.CommodityVO;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 
 * @ClassName:  CommodityBO   
 * @Description:商品信息bo
 * @author: MDS
 * @date:   2018年5月29日 上午9:19:18
 */
@Transactional
public interface CommodityBO {
	
	PageInfo getCommodityPage(PageInfo page, String keyword);
	
	boolean saveCommodity(CommodityVO vo);
	
	boolean updateCommodity(CommodityVO vo);
	
	boolean deleteCommodity(String ids);
	
	/**
	 * 
	 * @Title: getCommodityAll
	 * @Description:获取所有的商品信息
	 * @author: MDS
	 * @return
	 */
	public JSONArray getCommodityAll();
	
	/**
	 * 根据用户游戏输赢增加用户的余额
	 * 
	 * @param userId
	 * @param price
	 * @param bool
	 * @param priceType
	 * @return
	 */
	public JSONObject updateUserMoneySum(String userId, double price, boolean bool, Integer priceType, double pay, Integer payType);
	
	/**
	 * 更新商品图片
	 * 
	 * @param commodityId
	 * @param path
	 * @return
	 */
	public boolean updateCommodityImg(String commodityId, String path);

}
