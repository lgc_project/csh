package com.vrc.bo.trade;

import net.sf.json.JSONObject;

/**
 * 
 * @ClassName:  DealBO   
 * @Description:商品交易bo接口
 * @author: MDS
 * @date:   2018年5月29日 上午11:45:02
 */
public interface DealBO {
	/**
	 * 
	 * @Title: saveDealInfo
	 * @Description:保存商品交易信息
	 * @author: MDS
	 * @param userId 用户编号
	 * @param commodityId 商品编号
	 * @param receiver 收货人
	 * @param address 收货地址
	 * @param phone 电话
	 * @return
	 */
	public JSONObject saveDealInfo(Long userId,Long commodityId, String receiver, String address, String phone);
}
