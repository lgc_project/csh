package com.vrc.bo.job.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Service;

import com.rdms.common.utils.SpringContextUtils;
import com.vrc.bo.job.JobService;
import com.vrc.common.Const;
import com.vrc.dao.sys.UserDAO;
import com.vrc.dao.sys.VrcConfigDAO;
import com.vrc.dao.trade.BillDAO;
import com.vrc.dao.trade.TradeDAO;
import com.vrc.util.DateUtils;
import com.vrc.util.KeyUtil;
import com.vrc.util.StringUtils;
import com.vrc.util.TableNameUtil;
import com.vrc.vo.job.MachineDTO;
import com.vrc.vo.job.UserMachineDTO;
import com.vrc.vo.trade.BillVO;

/**
 * 每天的定时任务
 * @author liboxing
 *
 */
@Service("jobService")
public class JobServiceImpl implements JobService {

	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	public void machineRunDay() {
		logger.info("开始执行machineRunDay任务："+DateUtils.dateToString(new Date()));
		JdbcTemplate template = (JdbcTemplate) SpringContextUtils.getBean("jdbcTemplate");
		
		//1、所有运行中的矿机加运行天数加1
		String sql = "update "+TableNameUtil.TB_MACHINE+" set work_day=work_day+1 where status='1'";
		int count1 = template.update(sql);
		
		//2、把运行天数大于等于总天数的修改状态为0
		String sql2 = "update tb_machine set status='0' where id in ("
				+ "select id from (select a.id from tb_machine a,tb_machine_type b "
				+ "where a.type=b.code and a.work_day>=b.work_sum and a.status='1') c)";
		int count2 = template.update(sql2);
		
		// 3、维修天数加1
		String sql3 = "update " + TableNameUtil.TB_MACHINE + " set mt_day = mt_day + 1 where mt_status = '1' and status = '1'"; 
		int count3 = template.update(sql3);
		
		// 4、维修天数超过维修周期，则修改状态为0
		String sql4 = "update tb_machine set mt_status='0' where id in ("
				+ "select id from (select a.id from tb_machine a,tb_machine_type b "
				+ "where a.type=b.code and a.mt_day>=b.mt_sum and a.mt_status='1' and a.status = '1') c)";
		int count4 = template.update(sql4);
		logger.info("machineRunDay任务执行完毕："+DateUtils.dateToString(new Date())+"  运行天数加一："+count1+"  修改状态为0："+count2+"  维修天数加一："+count3+"  修改状态为0："+count4);
	}

	@Override
	public void tradeSimpleJob() {
		logger.info("开始执行tradeSimpleJob任务："+DateUtils.dateToString(new Date()));		
		//递增数量（矿石）
		double upamount = Const.upamount;
		VrcConfigDAO configDAO = (VrcConfigDAO) SpringContextUtils.getBean("vrcConfigDAO");
		try {
			String str = configDAO.getConfigByKey("upamount");
			upamount = Double.valueOf(str);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		
		double csbupamount = 0.0;
		try {
			String str = configDAO.getConfigByKey("csbupamount");
			csbupamount = Double.valueOf(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//获取昨日的时间
		Calendar ca = Calendar.getInstance();
        ca.add(Calendar.DAY_OF_MONTH, -1);
        Date date = ca.getTime();
        String dateStr = DateUtils.dateToString(date, "yyyy-MM-dd");
        
        JdbcTemplate template = (JdbcTemplate) SpringContextUtils.getBean("jdbcTemplate");
        TradeDAO tradeDao = (TradeDAO) SpringContextUtils.getBean("tradeDAO");

        // 昨日交易数据
        String oreSql = "select * from tb_tradesimple where time = ?";
        SqlRowSet rs = template.queryForRowSet(oreSql, dateStr);
        if(rs != null && rs.next()) {
        	// 修改当天的数据
        	dateStr = DateUtils.dateToString(new Date(), "yyyy-MM-dd");
        	
        	double avg = rs.getDouble("avg");
        	BigDecimal b1 = new BigDecimal(avg);
        	BigDecimal b2 = new BigDecimal(upamount);
        	avg = b1.add(b2).doubleValue();
        	tradeDao.updateTradeSimpleAvg(dateStr, avg);
        	
        	double csbAvg = rs.getDouble("csb_avg");
        	BigDecimal c1 = new BigDecimal(csbAvg);
        	BigDecimal c2 = new BigDecimal(csbupamount);
        	csbAvg = c1.add(c2).doubleValue();
        	tradeDao.updateTradeSimpleCsbAvg(dateStr, csbAvg);
        	
        } else { // 如果没有昨天的矿石交易数据，则找距离今天最近一天的数据
        	oreSql = "select * from tb_tradesimple order by time desc";
        	SqlRowSet r = template.queryForRowSet(oreSql);
        	if(r != null && r.next()) {
        		//修改当天的数据
				dateStr = DateUtils.dateToString(new Date(), "yyyy-MM-dd");
			
				double avg = r.getDouble("avg");				
				BigDecimal b1=new BigDecimal(Double.toString(avg));
		        BigDecimal b2=new BigDecimal(Double.toString(upamount));
		        avg = b1.add(b2).doubleValue();
		    	tradeDao.updateTradeSimpleAvg(dateStr, avg);
		    	
		    	double csbAvg = r.getDouble("csb_avg");
		    	BigDecimal c1 = new BigDecimal(csbAvg);
		    	BigDecimal c2 = new BigDecimal(csbupamount);
		    	csbAvg = c1.add(c2).doubleValue();
		    	tradeDao.updateTradeSimpleCsbAvg(dateStr, csbAvg);
        	}
        }
		logger.info("tradeSimpleJob任务执行完毕："+DateUtils.dateToString(new Date()));
	}

	@Override
	public void userIncome() {
		logger.info("开始执行userIncome任务："+DateUtils.dateToString(new Date()));
		JdbcTemplate template = (JdbcTemplate) SpringContextUtils.getBean("jdbcTemplate");
		UserDAO userDAO = (UserDAO) SpringContextUtils.getBean("userDAO");
		BillDAO billDAO = (BillDAO) SpringContextUtils.getBean("billDAO");
		VrcConfigDAO configDAO = (VrcConfigDAO) SpringContextUtils.getBean("vrcConfigDAO");
		Map<Integer, Double> levelMap = new HashMap<>();   //记录每个等级的奖励
		Date date = new Date();
		String dateStr = DateUtils.dateToString(date);
		
		//1、查出所有正在运行的（不需要维修的）矿机，并关联上用户id，用户父级id
		Map<String, UserMachineDTO> userMachMap = new HashMap<>();
//		String sql = "select um.*, FORMAT(mt.income_sum/mt.mt_sum,2) as day_income, mt.level, mt.maintenance from ( "
//				+ "select a.id as user_id, a.parent_id, b.machine_no, b.type "
//				+ "from tb_user a, tb_machine b where a.id = b.user_id "
//				+ "and b.status='1' and b.mt_status = '1' order by a.id,b.type ) um, tb_machine_type mt where um.type = mt.code ";
		String sql = "SELECT a.id as user_id \n" + 
					 "      ,a.parent_id \n" + 
					 "      ,b.machine_no \n" + 
					 "      ,b.type \n" + 
					 "		,FORMAT(mt.income_sum/mt.mt_sum,2) as day_income \n" + 
					 "      ,mt.level \n" + 
					 "      ,mt.maintenance \n" + 
					 "FROM tb_user a, tb_machine b, tb_machine_type mt  \n" + 
					 "WHERE a.id = b.user_id and b.type = mt.code and b.status='1' and b.mt_status = '1'";
		SqlRowSet rs = template.queryForRowSet(sql);
		if(rs != null){
			while(rs.next()){
				String userId = rs.getString("user_id");
				String parentId = rs.getString("parent_id");
				String machineNo = rs.getString("machine_no");
				String type = rs.getString("type");
				double dayIncome = rs.getDouble("day_income");
				int level = rs.getInt("level");
				
				levelMap.put(level, dayIncome);   //赋值给levelMap
				
				MachineDTO machine = new MachineDTO();
				machine.setUserId(userId);
				machine.setMachineNo(machineNo);
				machine.setType(type);
				machine.setDayIncome(dayIncome);
				machine.setLevel(level);
				
				UserMachineDTO dto = userMachMap.get(userId);
				if(dto == null){
					dto = new UserMachineDTO();
					dto.setUserId(userId);
					dto.setParentId(parentId);
					dto.setMaxLevel(level);
					dto.getMachineList().add(machine);
					userMachMap.put(userId, dto);
				} else {
					//判断最高等级的矿机
					if(level > dto.getMaxLevel()){
						dto.setMaxLevel(level);
					}
					dto.getMachineList().add(machine);
				}
			}
		}
		
		// 遍历用户userMachMap，计算他的收益和团队奖励
		List<UserMachineDTO> allUserList = new ArrayList<>();
		for(String userId : userMachMap.keySet()){
			UserMachineDTO dto = userMachMap.get(userId);
			allUserList.add(dto);
		}
		userMachMap = null;
		
		Iterator<UserMachineDTO> ite = allUserList.iterator();
		while(ite.hasNext()){
			UserMachineDTO dto = ite.next();
			String userId = dto.getUserId();    //用户id
			double income = 0;   //自身的收益
			double reward = 0;   //团队奖励
			int level = dto.getMaxLevel();     //矿机等级（代表他可以收到第几代）
			
			//计算自身收益（所有矿机日产量的和）
			List<MachineDTO> machList = dto.getMachineList();
			for(MachineDTO mach : machList){
				income = income + mach.getDayIncome();
				logger.info("自身收益：" + income);
			}
			
			//如果矿机等级大于等于1，计算第一代的奖励
			List<UserMachineDTO> firstChildList = new ArrayList<>();
			if(level >= 1){
				List<UserMachineDTO> parentList = new ArrayList<>();
				parentList.add(dto);
				firstChildList = getChild(parentList, allUserList);
				
				//第一级孩子的总收益
				double count = 0;
				for(UserMachineDTO us : firstChildList){
					List<MachineDTO> maList = us.getMachineList();
					for(MachineDTO mach : maList){
						//烧伤机制
						if(level < mach.getLevel()){
							double d = levelMap.get(level);
							count = count + d;
						} else {
							count = count + mach.getDayIncome();							
						}
					}
				}
				//当前奖励加上第一级孩子收益的10%
				double percent = Const.firstLevelPerc;
				try {
					String str = configDAO.getConfigByKey("firstReward");
					percent = Double.parseDouble(str);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				reward = reward + count * percent;
			}
			
			//计算第二代的奖励
			List<UserMachineDTO> secondChildList = new ArrayList<>();
			if(level >= 2 && firstChildList != null && firstChildList.size() > 0){
				secondChildList = getChild(firstChildList, allUserList);
				
				//第二级孩子的总收益
				double count = 0;
				for(UserMachineDTO us : secondChildList){
					List<MachineDTO> maList = us.getMachineList();
					for(MachineDTO mach : maList){
						//烧伤机制
						if(level < mach.getLevel()){
							double d = levelMap.get(level);
							count = count + d;
						} else {
							count = count + mach.getDayIncome();							
						}
					}
				}
				//当前奖励加上第二级孩子收益的2%
				double percent = Const.secondLevelPerc;
				try {
					String str = configDAO.getConfigByKey("secondreward");
					percent = Double.parseDouble(str);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				reward = reward + count * percent;
			}
			
			//计算第三代的奖励
			List<UserMachineDTO> thirdChildList = new ArrayList<>();
			if(level >= 3 && secondChildList != null && secondChildList.size() > 0){
				thirdChildList = getChild(secondChildList, allUserList);
				
				//第三级孩子的总收益
				double count = 0;
				for(UserMachineDTO us : thirdChildList){
					List<MachineDTO> maList = us.getMachineList();
					for(MachineDTO mach : maList){
						//烧伤机制
						if(level < mach.getLevel()){
							double d = levelMap.get(level);
							count = count + d;
						} else {
							count = count + mach.getDayIncome();							
						}
					}
				}
				//当前奖励加上第三级孩子收益的1%
				double percent = Const.thirdLevelPerc;
				try {
					String str = configDAO.getConfigByKey("thirdreward");
					percent = Double.parseDouble(str);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				reward = reward + count * percent;
			}
			
			//计算第四代的奖励
			List<UserMachineDTO> fourthChildList = new ArrayList<>();
			if(level >= 4 && thirdChildList != null && thirdChildList.size() > 0){
				fourthChildList = getChild(thirdChildList, allUserList);
				
				//第四级孩子的总收益
				double count = 0;
				for(UserMachineDTO us : fourthChildList){
					List<MachineDTO> maList = us.getMachineList();
					for(MachineDTO mach : maList){
						//烧伤机制
						if(level < mach.getLevel()){
							double d = levelMap.get(level);
							count = count + d;
						} else {
							count = count + mach.getDayIncome();							
						}
					}
				}
				//当前奖励加上第四级孩子收益的0.5%
				double percent = Const.fourthLevelPerc;
				try {
					String str = configDAO.getConfigByKey("fouthreward");
					percent = Double.parseDouble(str);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				reward = reward + count * percent;
			}
			
			//double money = income + reward;
			//boolean flag = userDAO.addUserMoney(userId, money);   //增加用户资金
			double money = income;
			boolean flagOre = userDAO.addOreSum(userId, money); // 增加用户矿石数
			if(flagOre) {
				if(income != 0) {
					// 写入账单表（类型：收益，子类型：收益，账单类型：矿石）
					BillVO bill = new BillVO();
					bill.setId(KeyUtil.getKey());
					bill.setType(Const.billType_income);
					bill.setActionType(Const.actionType_income);
					bill.setUserId(userId);
					bill.setPrice(income);
					bill.setTime(dateStr);
					bill.setBillType("0");
					billDAO.addBill(bill);
				}
			}
			
//			if(flagOre){
//				if(income != 0){
//					//写入账单表（类型：收益，子类型：收益）
//					BillVO bill = new BillVO();
//					bill.setId(KeyUtil.getKey());
//					bill.setType(Const.billType_income);
//					bill.setActionType(Const.actionType_income);
//					bill.setUserId(userId);
//					bill.setPrice(income);
//					bill.setTime(dateStr);
//					billDAO.addBill(bill);						
//				}
//				
//				if(reward != 0){
//					//写入账单表（类型：奖励，子类型：团队奖励）
//					BillVO bill2 = new BillVO();
//					bill2.setId(KeyUtil.getKey());
//					bill2.setType(Const.billType_reward);
//					bill2.setActionType(Const.actionType_teamReward);
//					bill2.setUserId(userId);
//					bill2.setPrice(reward);
//					bill2.setTime(dateStr);
//					billDAO.addBill(bill2);						
//				}
//			}
		}
		logger.info("userIncome任务执行完毕："+DateUtils.dateToString(new Date()));
	}
	
	
	//获取一批用户的子用户
	private List<UserMachineDTO> getChild(List<UserMachineDTO> parentList,List<UserMachineDTO> allList){
		if(allList == null || parentList == null 
				|| allList.size() == 0 || parentList.size() == 0){
			return new ArrayList<>();
		}
		
		List<UserMachineDTO> retList = new ArrayList<>();
		//遍历所有用户，判断他是否属于parentList里面用户的孩子。
		Iterator<UserMachineDTO> ite = allList.iterator();
		while (ite.hasNext()){
			UserMachineDTO child = ite.next();
			String parentId = child.getParentId();
			//若父级id为空，直接跳过
			if(!StringUtils.isEmpty(parentId)){
				for(UserMachineDTO par : parentList){
					//如果外层用户的父级id等于该用户的id，说明是他们的孩子，添加到retList中，然后break掉
					if(parentId.equals(par.getUserId())){
						retList.add(child);
						break;
					}
				}
			}
		}
		return retList;
	}
}
