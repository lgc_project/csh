package com.vrc.bo.job;

public interface JobService {

	/**
	 * 矿机运行天数加一任务
	 * 每晚12：13分，矿机运行天数加一
	 */
	public void machineRunDay();
	
	/**
	 * 每天计算昨天的平均价，最高价，成交数量，写入到tb_tradesimple表中
	 * 每天凌晨12：10分执行
	 */
	public void tradeSimpleJob();
	
	/**
	 * 每天计算用户的收益（包括自己产生的币和团队的奖励）
	 * 每天凌晨12：01分执行
	 */
	public void userIncome();
}
