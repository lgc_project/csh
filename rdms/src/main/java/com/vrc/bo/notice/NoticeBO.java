package com.vrc.bo.notice;

import com.vrc.vo.common.PageInfo;
import com.vrc.vo.notice.CustomServiceVO;
import com.vrc.vo.notice.NoticeVO;

import net.sf.json.JSONObject;

/**
 * 公告BO
 * @author liboxing
 *
 */
public interface NoticeBO {

	public boolean saveNotice(NoticeVO vo);
	
	public boolean updateNotice(NoticeVO vo);
	
	public boolean deleteNotice(String id);
	
	/**
	 * 批量删除公告
	 * @param id   用“，”拼接的id
	 * @return
	 */
	public boolean deleteBatch(String id);
	
	/**
	 * 分页查询
	 * @param page
	 * @return
	 */
	public PageInfo getNoticePage(PageInfo page, String keyword);
	
	public NoticeVO getById(String id);
	
	/**
	 * 添加客服内容
	 * @return
	 */
	public boolean saveCustomService(CustomServiceVO vo);
	
	/**
	 * 更新客服内容
	 * @return
	 */
	public boolean updateCustomService(CustomServiceVO vo);
	
	/**
	 * 获取客服内容
	 * @return
	 */
	public CustomServiceVO getCustomService();
}
