package com.vrc.bo.notice.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.vrc.bo.notice.NoticeBO;
import com.vrc.dao.notice.NoticeDAO;
import com.vrc.util.DateUtils;
import com.vrc.util.KeyUtil;
import com.vrc.util.StringUtils;
import com.vrc.vo.common.PageInfo;
import com.vrc.vo.notice.CustomServiceVO;
import com.vrc.vo.notice.NoticeVO;

import net.sf.json.JSONObject;

/**
 * 公告BO
 * @author liboxing
 *
 */
@Service("noticeBO")
public class NoticeBOImpl implements NoticeBO {

	@Resource
	private NoticeDAO noticeDAO;
	
	@Override
	public boolean saveNotice(NoticeVO vo) {
		if(vo == null){
			return false;
		}
		String id = KeyUtil.getKey32();
		String time = DateUtils.dateToString(new Date());
		vo.setId(id);
		vo.setTime(time);
		
		boolean flag = noticeDAO.saveNotice(vo);
		return flag;
	}

	@Override
	public boolean updateNotice(NoticeVO vo) {
		if(vo == null){
			return false;
		}
		String time = DateUtils.dateToString(new Date());
		vo.setTime(time);
		
		boolean flag = noticeDAO.updateNotice(vo);
		return flag;
	}

	@Override
	public boolean deleteNotice(String id) {
		if(StringUtils.isEmpty(id)){
			return false;
		}
		boolean flag = noticeDAO.deleteNotice(id);
		return flag;
	}

	@Override
	public boolean deleteBatch(String id) {
		if(StringUtils.isEmpty(id)){
			return false;
		}
		String[] ids = id.split(",");
		boolean flag = noticeDAO.deleteNotice(ids);
		return flag;
	}

	@Override
	public PageInfo getNoticePage(PageInfo page, String keyword) {
		List<NoticeVO> list = noticeDAO.getNotice(page, keyword);
		page.setData(list);
		return page;
	}

	@Override
	public NoticeVO getById(String id) {
		if(StringUtils.isEmpty(id)){
			return null;
		}
		
		NoticeVO vo = noticeDAO.getById(id);
		return vo;
	}

	@Override
	public boolean saveCustomService(CustomServiceVO vo) {
		if(vo == null){
			return false;
		}
		String id = KeyUtil.getKey32();
		String time = DateUtils.dateToString(new Date());
		vo.setId(id);
		vo.setTime(time);
		
		boolean flag = noticeDAO.saveCustomService(vo);
		return flag;
	}

	@Override
	public boolean updateCustomService(CustomServiceVO vo) {
		if(vo == null){
			return false;
		}
		String time = DateUtils.dateToString(new Date());
		vo.setTime(time);
		
		boolean flag = noticeDAO.updateCustomService(vo);
		return flag;
	}

	@Override
	public CustomServiceVO getCustomService() {
		CustomServiceVO vo = noticeDAO.getCustomService();
		return vo;
	}

}
