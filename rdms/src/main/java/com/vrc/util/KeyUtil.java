package com.vrc.util;

import java.util.UUID;

/**
 * 主键生成工具
 * @author liboxing
 *
 */
public class KeyUtil {

    /**
     * 获取36位的主键
     * @return
     */
    public static String getKey(){
        String key = UUID.randomUUID().toString();
        return key;
    }
    
    /**
     * 获取32位主键
     * @return
     */
    public static String getKey32(){
        String key = UUID.randomUUID().toString();
        key = key.replaceAll("-", "");
        return key;
    }
}
