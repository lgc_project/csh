package com.vrc.util;

/**
 * 记录表名
 * @author liboxing
 *
 */
public class TableNameUtil {

    public static String TB_USER = "tb_user";   //用户表
    
    public static String TB_GUADAN = "tb_guadan";   //挂单表
    
    public static String TB_MACHINE_TYPE = "tb_machine_type";   //矿机类型表
    
    public static String TB_MACHINE = "tb_machine";   //矿机表
    
    public static String TB_BILL = "tb_bill";  //账单表
    
    public static String TB_TRADE = "tb_trade";  //交易表
    
    public static String TB_NOTICE = "tb_notice";  //公告表
    
    public static String TB_CUSTOMSERVICE = "tb_customservice";  //公告表
    
    public static String TB_BUYRECORD = "tb_buyrecord";   //购买矿机记录
    
    public static String TB_TRADESIMPLE = "tb_tradesimple";  //交易概况表（每天的平均价，最改价，成交数量）
    
    public static String TB_COMMODITY = "tb_commodity";   //商品信息表
    
    public static String TB_DEAL = "tb_deal";	//商品交易表
    
    public static String TB_COMMODITY_DEAL = "tb_commodity_deal"; //商品交易中间表
    
    public static String LB_PRIZE = "lb_prize"; // 奖品表
    
    public static String LB_AUCTION = "lb_auction"; // 竞拍活动表
    
    public static String LB_AUCTION_USER = "lb_auction_user"; // 竞拍信息表
    
    public static String LB_CAISHENGBI = "lb_caishenbi"; // 财神币发布表
    
    public static String LB_CSB_BILL = "lb_csb_bill"; // 财神币账单
    
    public static String LB_CSB_GUADAN = "lb_csb_guadan"; // 财神币挂单
    
    public static String LB_CSB_RECOVER = "lb_csb_recover"; // 财神币回收
    
    public static String LB_CSB_TRADE = "lb_csb_trade"; // 财神币交易表
    
    public static String LB_TAKECASH = "lb_takecash"; // 取现表
}
