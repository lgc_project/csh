package com.vrc.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 日期处理工具
 * 
 * @author libaixing
 */
public class DateUtils {

    /**
     * 默认的日期格式
     */
    public static final String FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss"; // 默认格式

    /**
     * 获取精确的日期
     * 
     * @param timestamps
     *            时间long集合
     * @return 日期
     */
    private static Date getAccurateDate(List<Long> timestamps) {
        Date date = null;
        long timestamp = 0;
        Map<Long, long[]> map = new HashMap<Long, long[]>();
        List<Long> absoluteValues = new ArrayList<Long>();

        if (timestamps != null && timestamps.size() > 0) {
            if (timestamps.size() > 1) {
                for (int i = 0; i < timestamps.size(); i++) {
                    for (int j = i + 1; j < timestamps.size(); j++) {
                        long absoluteValue = Math.abs(timestamps.get(i) - timestamps.get(j));
                        absoluteValues.add(absoluteValue);
                        long[] timestampTmp = { timestamps.get(i), timestamps.get(j) };
                        map.put(absoluteValue, timestampTmp);
                    }
                }

                // 有可能有相等的情况。如2012-11和2012-11-01。时间戳是相等的
                long minAbsoluteValue = -1;
                if (!absoluteValues.isEmpty()) {
                    // 如果timestamps的size为2，这是差值只有一个，因此要给默认值
                    minAbsoluteValue = absoluteValues.get(0);
                }
                for (int i = 0; i < absoluteValues.size(); i++) {
                    for (int j = i + 1; j < absoluteValues.size(); j++) {
                        if (absoluteValues.get(i) > absoluteValues.get(j)) {
                            minAbsoluteValue = absoluteValues.get(j);
                        } 
                        else {
                            minAbsoluteValue = absoluteValues.get(i);
                        }
                    }
                }

                if (minAbsoluteValue != -1) {
                    long[] timestampsLastTmp = map.get(minAbsoluteValue);
                    if (absoluteValues.size() > 1) {
                        timestamp = Math.max(timestampsLastTmp[0], timestampsLastTmp[1]);
                    } 
                    else if (absoluteValues.size() == 1) {
                        // 当timestamps的size为2，需要与当前时间作为参照
                        long dateOne = timestampsLastTmp[0];
                        long dateTwo = timestampsLastTmp[1];
                        if ((Math.abs(dateOne - dateTwo)) < 100000000000L) {
                            timestamp = Math.max(timestampsLastTmp[0], timestampsLastTmp[1]);
                        } 
                        else {
                            long now = new Date().getTime();
                            if (Math.abs(dateOne - now) <= Math.abs(dateTwo - now)) {
                                timestamp = dateOne;
                            } 
                            else {
                                timestamp = dateTwo;
                            }
                        }
                    }
                }
            } 
            else {
                timestamp = timestamps.get(0);
            }
        }

        if (timestamp != 0) {
            date = new Date(timestamp);
        }
        return date;
    }

    /**
     * 获取当前日期的年份
     * 
     * @return 当前年份
     */
    public static int getYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }

    /**
     * 获取给定日期的年份，若日期为空，返回-1
     * 
     * @param date
     *            给定的日期
     * 
     * @return 给定日期的年份
     */
    public static int getYear(Date date) {
        if (null == date) {
            return -1;
        }
        return date.getYear() + 1900;
    }

    /**
     * 获取当前日期的月份
     * 
     * @return 当前月份。 如：一月，返回 1
     */
    public static int getMonth() {
        return Calendar.getInstance().get(Calendar.MONTH) + 1;
    }

    /**
     * 获取给定日期的月份，若日期为空，返回-1
     * 
     * @param date
     *            给定的日期
     * 
     * @return 给定日期的月份。 如：一月，返回 1
     */
    public static int getMonth(Date date) {
        if (null == date) {
            return -1;
        }
        return date.getMonth() + 1;
    }

    /**
     * 获取当前日期的号数（即每月的几号），返回值是1~31的整数值。若给定日期为空，则返回-1。
     * 
     * @return
     */
    public static int getDate() {
        return Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
    }

    /**
     * 获取给定日期的号数（即每月的几号），返回值是1~31的整数值。若给定日期为空，则返回-1。
     * 
     * @param date
     *            给定的时间
     * @return
     */
    public static int getDate(Date date) {
        if (null == date) {
            return -1;
        }
        return date.getDate();
    }

    /**
     * 获取一星期中的某一天（当前日期）
     * 
     * @return 如：星期天，返回0 ， 星期一，返回1， . . . 星期六，返回6。
     */
    public static int getDay() {
        return Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1;
    }

    /**
     * 获取一星期中的某一天（给定日期）
     * 
     * @return 如：星期天，返回0 ， 星期一，返回1， . . . 星期六，返回6。
     */
    public static int getDay(Date date) {
        if (null == date) {
            return -1;
        }
        return date.getDay();
    }

    /**
     * 获取当前时间的小时（24小时制）
     * 
     * @return
     */
    public static int getHours() {
        return Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
    }

    /**
     * 获取给定时间的小时（24小时制）
     * 
     * @param date
     *            给定的时间
     * @return
     */
    public static int getHours(Date date) {
        if (null == date) {
            return -1;
        }
        return date.getHours();
    }

    /**
     * 获取当前时间的分钟
     * 
     * @return
     */
    public static int getMinutes() {
        return Calendar.getInstance().get(Calendar.MINUTE);
    }

    /**
     * 获取给定时间的分钟
     * 
     * @param date
     *            给定的时间
     * @return
     */
    public static int getMinutes(Date date) {
        if (null == date) {
            return -1;
        }
        return date.getMinutes();
    }

    /**
     * 获取当前时间的秒
     * 
     * @return
     */
    public static int getSeconds() {
        return Calendar.getInstance().get(Calendar.SECOND);
    }

    /**
     * 获取给定时间的秒
     * 
     * @param date
     *            给定的时间
     * @return
     */
    public static int getSeconds(Date date) {
        if (null == date) {
            return -1;
        }
        return date.getSeconds();
    }

    /**
     * 获取两个日期相差的天数，失败返回-1
     * 
     * @param date
     *            日期
     * @param otherDate
     *            另一个日期
     * @return 相差天数
     */
    public static int getIntervalDays(Date date, Date otherDate) {
        if (null == date || null == otherDate) {
            return -1;
        }
        long time = Math.abs(date.getTime() - otherDate.getTime());
        return (int) time / (24 * 60 * 60 * 1000);
    }

    /**
     * 将日期字符串转化为日期，失败返回null，格式为yyyy-MM-dd HH:mm:ss 。
     * 
     * @param date
     *            日期字符串
     * 
     * @return Date 转换后的日期
     */
    public static Date stringToDate(String date){
        Date myDate = null;
        if (null != date && !"".equals(date)) {
            SimpleDateFormat format = new SimpleDateFormat(DateUtils.FORMAT_PATTERN);
            try {
                myDate = format.parse(date);
            } 
            catch (Exception e) {
            }
        }
        return myDate;
    }
    
    /**
     * 将日期字符串转化为日期，失败返回null。
     * 
     * @param date
     *            日期字符串
     * @param parttern
     *            日期格式
     * 
     * @return Date 转换后的日期
     */
    public static Date stringToDate(String date, String parttern) {
        Date myDate = null;
        if (null != date && !"".equals(date)) {
            SimpleDateFormat format = new SimpleDateFormat(parttern);
            try {
                myDate = format.parse(date);
            } 
            catch (Exception e) {
            }
        }
        return myDate;
    }

    /**
     * 日期转换成字符串 ，若日期为空，则返回null
     * 
     * @param date
     *            要转换的日期
     * 
     * @return 日期字符串，格式为：2015-07-10 19:20:00
     */
    public static String dateToString(Date date) {
        if (null == date) {
            return null;
        }
        SimpleDateFormat format = new SimpleDateFormat(FORMAT_PATTERN);
        return format.format(date);
    }

    /**
     * 日期转换成字符串 ，若日期为空，则返回null
     * 
     * @param date
     *            要转换的日期
     * @param pattern
     *            日期格式，如：yyyy-MM-dd HH:mm
     * 
     * @return 日期字符串，格式为自定义格式， 转换失败返回默认格式（yyyy-MM-dd HH:mm:ss）
     */
    public static String dateToString(Date date, String pattern) {
        if (null == date) {
            return null;
        }
        try {
            SimpleDateFormat format = new SimpleDateFormat(pattern);
            return format.format(date);
        } 
        catch (IllegalArgumentException e) {
        }
        return new SimpleDateFormat(FORMAT_PATTERN).format(date);
    }

    public static long getTime(Date date){
        long time = 0;
        if(date != null){
            time = date.getTime();
        }
        return time;
    }
    
    public static long getTime(String dateStr){
        long time = 0;
        try {
            if(dateStr != null && dateStr != ""){
                Date date = stringToDate(dateStr);
                time = date.getTime();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return time;
    }
    
    /**
     * 获取数据库的时间戳（只精确到秒）
     * @param dateStr
     * @return
     */
    public static long getTimeX(String dateStr){
        long time = 0;
        try {
            if(dateStr != null && dateStr != ""){
                Date date = stringToDate(dateStr);
                time = date.getTime();
                time = time / 1000;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return time;
    }
    
    public static String getDateStr(long time){
        String dateStr = "";
        try {
            Date d = new Date(time);
            dateStr = dateToString(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateStr;
    }
    
    /**
     * 根据数据库时间戳获取日期字符（数据库时间戳只精确到秒，所以要乘以1000）
     * @param time
     * @return
     */
    public static String getDateStrX(long time){
        String dateStr = "";
        try {
            time = time * 1000;
            Date d = new Date(time);
            dateStr = dateToString(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateStr;
    }
    
    /**
     * 在给定的日期上增加 amount年
     * 
     * @param date
     *            给定的日期
     * @param amount
     *            要增加的年数
     * 
     * @return 增加年份后的日期
     */
    public static Date addYear(Date date, int amount) {
        Date myDate = null;
        if (null != date) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.YEAR, amount);
            myDate = calendar.getTime();
        }
        return myDate;
    }

    /**
     * 在给定的日期上增加 amount月
     * 
     * @param date
     *            给定的日期
     * @param amount
     *            要增加的月数
     * 
     * @return 增加月份后的日期
     */
    public static Date addMonth(Date date, int amount) {
        Date myDate = null;
        if (null != date) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MONTH, amount);
            myDate = calendar.getTime();
        }
        return myDate;
    }

    /**
     * 在给定的日期上增加 amount天
     * 
     * @param date
     *            给定的日期
     * @param amount
     *            要增加的天数
     * 
     * @return 增加天数后的日期
     */
    public static Date addDate(Date date, int amount) {
        Date myDate = null;
        if (null != date) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DATE, amount);
            myDate = calendar.getTime();
        }
        return myDate;
    }

    public static String formatDate(String dateStr, String fromPattern, String toPattern){
    	if(dateStr == null){
    		return null;
    	}
    	Date d = DateUtils.stringToDate(dateStr, fromPattern);
    	String str = DateUtils.dateToString(d, toPattern);
    	return str;
    }
    
    /**
     * 对比两个时间大小
     * @param date1
     * @param date2
     * @return
     */
    public static int compare(String date1, String date2){
    	Date d1 = stringToDate(date1);
    	Date d2 = stringToDate(date2);
    	
    	long time = d2.getTime() - d1.getTime();
    	if(time > 0){
    		return 1;
    	} else if(time == 0){
    		return 0;
    	} else if(time < 0){
    		return -1;
    	}
    	return 0;
    }
}
