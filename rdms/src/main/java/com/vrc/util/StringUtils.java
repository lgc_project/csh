package com.vrc.util;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Pattern;


/**
 * 字符串处理工具
 *
 * @author libx
 * @time 2016年8月23日 下午4:44:26
 * 
 */
public class StringUtils {

	/**
     * 检查字符串是否为空或无内容
     * 
     * @param str
     *            要检查的字符串
     * 
     * @return 若字符串为空或无字符，返回true，否则返回false
     */
    public static boolean isEmpty(String str) {
        return str == null || str.trim().length() == 0;
    }

    /**
     * 删除字符串开始和结尾的空白符
     * 
     * @param str
     *            源字符串
     * 
     * @return 若源字符串为空，返回null，否则返回去除首尾空白符的字符串
     */
    public static String trim(String str) {
        if (str == null){
            return null;
        }

        int start = 0;
        int end = str.length() - 1;
        while (start <= end && str.charAt(start) == ' ') {
            start++;
        }
        while (end >= start && str.charAt(end) == ' ') {
            end--;
        }
        return str.substring(start, end + 1);
    }

    /**
     * 比较两个字符串的值是否相等
     * 
     * @param str1
     * @param str2
     * 
     * @return 若两字符串的值相等，返回true，否则返回false
     */
    public static boolean equals(String str1, String str2) {
        if (str1 == null || str2 == null) {
            return false;
        }
        return str1.equals(str2);
    }

    /**
     * 比较两个字符串null安全（不考虑大小写）
     * 
     * @param str1
     * @param str2
     * 
     * @return 若两字符串的值相等（不考虑大小写），返回true，否则返回false
     */
    public static boolean equalsIgnoreCase(String str1, String str2) {
        if (str1 == null || str2 == null) {
            return false;
        }
        return str1.equalsIgnoreCase(str2);
    }

    /**
     * 检查源字符串中是否包含某一字符
     * 
     * @param str
     *            源字符串
     * @param checkChar
     *            要检查的字符
     * 
     * @return 若源字符串str 中包含要检查的字符checkChar，返回true， 否则返回false
     */
    public static boolean contains(String str, Character checkChar) {
        if (str == null || checkChar == null) {
            return false;
        }
        return str.contains(String.valueOf(checkChar));
    }

    /**
     * 检查源字符串中是否包含某一字符串
     * 
     * @param str
     *            源字符串
     * @param checkStr
     *            要检查的字符串
     * 
     * @return 若源字符串str 中包含要检查的字符checkStr，返回true， 否则返回false
     */
    public static boolean contains(String str, String checkStr) {
        if (str == null || checkStr == null) {
            return false;
        }
        return str.contains(checkStr);
    }

    /**
     * 查询某字符在源字符串中首次出现的位置，不存在则返回-1
     * 
     * @param str
     *            源字符串
     * @param searchChar
     *            要查询的字符
     * 
     * @return 要查询的字符searchChar 在源字符串str 中首次出现的位置， 不存在则返回-1
     */
    public static int indexOf(String str, Character searchChar) {
        if (str == null || searchChar == null) {
            return -1;
        }
        return str.indexOf(searchChar);
    }

    /**
     * 查询某字符串在源字符串中首次出现的位置，不存在则返回-1
     * 
     * @param str
     *            源字符串
     * @param searchStr
     *            要查询的字符串
     * 
     * @return 要查询的字符串searchStr 在源字符串str 中首次出现的位置， 不存在则返回-1
     */
    public static int indexOf(String str, String searchStr) {
        if (str == null || searchStr == null) {
            return -1;
        }
        return str.indexOf(searchStr);
    }

    /**
     * 查询某字符在源字符串中最后一次出现的位置，不存在则返回-1
     * 
     * @param str
     *            源字符串
     * @param searchChar
     *            要查询的字符
     * 
     * @return 要查询的字符searchChar 在源字符串str 中最后一次出现的位置， 不存在则返回-1
     */
    public static int lastIndexOf(String str, Character searchChar) {
        if (str == null || searchChar == null) {
            return -1;
        }
        return str.lastIndexOf(searchChar);
    }

    /**
     * 查询某字符串在源字符串中最后一次出现的位置，不存在则返回-1
     * 
     * @param str
     *            源字符串
     * @param searchStr
     *            要查询的字符串
     * 
     * @return 要查询的字符串searchStr 在源字符串str 中最后一次出现的位置， 不存在则返回-1
     */
    public static int lastIndexOf(String str, String searchStr) {
        if (str == null || searchStr == null) {
            return -1;
        }
        return str.lastIndexOf(searchStr);
    }

    /**
     * 字符串截取
     * 
     * @param str
     *            源字符串
     * @param start
     *            截取的开始位置
     * 
     * @return 截取后的字符串
     */
    public static String subString(String str, int start) {
        if (str == null) {
            return null;
        }

        int length = str.length();
        if (start >= 0 && start <= length) {
            return str.substring(start);
        }
        return "";
    }

    /**
     * 字符串截取
     * 
     * @param str
     *            源字符串
     * @param start
     *            截取开始位置
     * @param end
     *            截取结束位置
     * 
     * @return 截取后的字符串
     */
    public static String subString(String str, int start, int end) {
        if (str == null) {
            return null;
        }

        int length = str.length();
        if (start >= 0 && end <= length && start <= end) {
            return str.substring(start, end);
        }
        return "";
    }

    /**
     * 根据传入的字符分割源字符串
     * 
     * @param str
     *            源字符串
     * @param splitsign
     *            分隔符
     * 
     * @return
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static String[] split(String str, Character splitsign) {
        int index;
        if (str == null || splitsign == null) {
            return null;
        }
        ArrayList al = new ArrayList();
        while ((index = str.indexOf(splitsign)) != -1) {
            al.add(str.substring(0, index));
            str = str.substring(index + 1);
        }
        al.add(str);
        return (String[]) al.toArray(new String[0]);
    }

    /**
     * 根据传入的字符串分割源字符串
     * 
     * @param str
     *            源字符串
     * @param splitsigns
     *            分割字符串
     * @return
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public static String[] split(String str, String splitsigns) {
        if (str == null || splitsigns == null) {
            return null;
        }
        int index;
        ArrayList al = new ArrayList();
        while ((index = str.indexOf(splitsigns)) != -1) {
            al.add(str.substring(0, index));
            str = str.substring(index + splitsigns.length());
        }
        al.add(str);
        return (String[]) al.toArray(new String[0]);
    }

    /**
     * 返回字符串的长度，若为空，则返回0
     * 
     * @param str
     * @return 字符串长度
     */
    public static int length(String str) {
        if (str == null) {
            return 0;
        }
        return str.length();
    }

    /**
     * 获取源字符串某一位置的字符
     * 
     * @param str
     *            源字符串
     * @param index
     *            下标
     * 
     * @return index下标的字符
     */
    public static char charAt(String str, int index) {
        return str.charAt(index);
    }

    /**
     * 检查源字符串是否是某一前缀
     * 
     * @param str
     *            源字符串
     * @param prefix
     *            前缀字符串
     * 
     * @return 若源字符串str 以前缀字符串prefix 开始，返回true，否则返回false
     */
    public static boolean startsWith(String str, String prefix) {
        if (str == null || prefix == null) {
            return false;
        }
        return str.startsWith(prefix);
    }

    /**
     * 检查源字符串是否是某一前缀（忽略大小写）
     * 
     * @param str
     *            源字符串
     * @param prefix
     *            前缀字符串
     * 
     * @return 若源字符串str 以前缀字符串prefix 开始（不考虑大小写），返回true，否则返回false
     */
    public static boolean startsWithIgnoreCase(String str, String prefix) {
        if (str == null || prefix == null) {
            return false;
        }
        return str.toLowerCase().startsWith(prefix.toLowerCase());
    }

    /**
     * 检查源字符串是否是某一后缀
     * 
     * @param str
     *            源字符串
     * @param suffix
     *            后缀字符串
     * 
     * @return 若源字符串str 以后缀字符串suffix 结尾，返回true，否则返回false
     */
    public static boolean endsWith(String str, String suffix) {
        if (str == null || suffix == null) {
            return false;
        }
        return str.endsWith(suffix);
    }

    /**
     * 检查源字符串是否是某一后缀（忽略大小写）
     * 
     * @param str
     *            源字符串
     * @param suffix
     *            后缀字符串
     * 
     * @return 若源字符串str 以后缀字符串suffix 结尾（不考虑大小写），返回true，否则返回false
     */
    public static boolean endsWithIgnoreCase(String str, String suffix) {
        if (str == null || suffix == null) {
            return false;
        }
        return str.toLowerCase().endsWith(suffix.toLowerCase());
    }

    /**
     * 替换字符串
     * 
     * @param from
     *            String 原始字符串
     * @param to
     *            String 目标字符串
     * @param source
     *            String 母字符串
     * @return String 替换后的字符串
     */
    public static String replace(String from, String to, String source) {
        if (source == null || from == null || to == null){
            return null;
        }
        StringBuffer str = new StringBuffer("");
        int index = -1;
        while ((index = source.indexOf(from)) != -1) {
            str.append(source.substring(0, index) + to);
            source = source.substring(index + from.length());
            index = source.indexOf(from);
        }
        str.append(source);
        return str.toString();
    }

    /**
     * 把源字符串中的字母全部转为小写
     * 
     * @param str
     *            源字符串
     * 
     * @return 不包含大写字母的字符串
     */
    public static String toLowerCase(String str) {
        if (str == null) {
            return null;
        }
        return str.toLowerCase();
    }

    /**
     * 把源字符串中的字母全部转为大写
     * 
     * @param str
     *            源字符串
     * 
     * @return 不包含小写字母的字符串
     */
    public static String toUpperCase(String str) {
        if (str == null) {
            return null;
        }
        return str.toUpperCase();
    }

    /**
     * 判断是否为整数
     * 
     * @param str
     *            传入的字符串
     * @return 是整数返回true,否则返回false
     */
    public static boolean isInteger(String str) {
        if (str == null){
            return false;
        }
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]+$");
        return pattern.matcher(str).matches();
    }

    /**
     * 判断是否为浮点数，包括double和float
     * 
     * @param str
     *            传入的字符串
     * @return 是浮点数返回true,否则返回false
     */
    public static boolean isDouble(String str) {
        if (str == null){
            return false;
        }
        Pattern pattern = Pattern.compile("^[-\\+]?\\d+\\.\\d+$");
        return pattern.matcher(str).matches();
    }

    /**
     * 随机生成指定长度的字符串（0~9a~zA~Z）
     * 
     * @param num
     *            指定的长度
     * 
     * @return 指定长度的随机字符串
     */
    public static String randomStr(int num) {
        StringBuffer str = new StringBuffer("");
        if (num > 0) {
            Random r = new Random();
//            String codes = "0123456789abcdefghjkmnopqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ";
            String codes = "0123456789";
            for (int i = 0; i < num; i++) {
                int index = r.nextInt(codes.length());
                char c = codes.charAt(index);
                str.append(c);
            }
        }
       
        return str.toString();
    }

    /**
     * 随机生成ID
     * 
     * @return
     */
    public static String uuIdRandomStr(){
        
        return UUID.randomUUID().toString();
    }
    
    /**
     * null变双引号
     * @return
     */
    public static String nullToEmpty(String str){
        if(str == null){
            return "";
        }
        return str;
    }
}
