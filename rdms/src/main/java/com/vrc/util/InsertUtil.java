package com.vrc.util;

import java.util.ArrayList;

public class InsertUtil {

    private String destTable;
    private ArrayList<String> columnList = new ArrayList<>();
    private ArrayList<String> valueList = new ArrayList<>();
    private ArrayList<Boolean> quotList = new ArrayList<>();
    
    public InsertUtil(){
    }
    
    public InsertUtil(String destTable){
        this.setDestTable(destTable);
    }

    public String getDestTable() {
        return destTable;
    }
    public void setDestTable(String destTable) {
        this.destTable = destTable;
    }
    
    /**
     * 增加新列，如果该列已存在，那么该列将会被新列覆盖，默认在生成sql的时候，会在值得两端加上单引号
     * 如果不需要单引号，请调用
     * @param name
     * @param value
     */
    public void setColumn(String name, String value){
        if(StringUtils.isEmpty(name)){
            return ;
        }
        name = name.toUpperCase();
        if(value != null){
            int index = this.columnList.indexOf(name);
            if(index >= 0){
                this.valueList.set(index, value);
                this.quotList.set(index, true);
            } else {
                this.columnList.add(name);
                this.valueList.add(value);
                this.quotList.add(true);
            }
        }
    }
    
    public void setColumn(String name, long value){
        this.setColumn(name, String.valueOf(value), false);
    }
    
    public void setColumn(String name, double value){
        this.setColumn(name, String.valueOf(value), false);
    }
    
    /**
     * 增加新列，如果该列已经存在，那么该列将会被新列覆盖
     * @param name
     * @param value
     * @param quot   表示在生成sql的时候，是否要在值的两边加上单引号
     */
    public void setColumn(String name, String value, boolean quot){
        if(StringUtils.isEmpty(name)){
            return ;
        }
        name = name.toUpperCase();
        if(!StringUtils.isEmpty(value)){
            int index = this.columnList.indexOf(name);
            if(index >= 0){
                this.valueList.set(index, value);
                this.quotList.set(index, quot);
            } else {
                this.columnList.add(name);
                this.valueList.add(value);
                this.quotList.add(quot);
            }
        }
    }
    
    /**
     * 获得由目标table所有列组成的字符串
     * @return  String
     */
    public String getColumns(){
        String result = "";
        for(int i = 0; i < columnList.size(); i++){
            result += "," + columnList.get(i);
        }
        return result.replaceFirst(",", "");
    }
    
    /**
     * 获得由valueList所组成的字符串
     * @return  String
     */
    public String getValues(){
        String result = "";
        for(int i = 0; i < valueList.size(); i++){
            if(quotList.get(i)){
                result += "," + addQuot(valueList.get(i));
            } else {
                result += "," + valueList.get(i);
            }
        }
        return result.replaceFirst(",", "");
    }
    
    /**
     * 创建insert SQL语句
     * @return
     */
    public String getInsertSql(){
        return "insert into " + this.getDestTable() + " (" + this.getColumns() +") "+
                "values (" + this.getValues() + ")";
    }
    
    /**
     * 创建update SQL语句，未添加where条件（若直接执行则会全表更新），所以要自己添加where条件
     * @return
     */
    public String getUpdateSql(){
        String result = "";
        
        String setString = "";
        for(int i = 0; i < valueList.size(); i++){
            String column = columnList.get(i);
            String value = valueList.get(i);
            if(quotList.get(i)){
                value = addQuot(value);
            }
            setString = setString + "," + column + "=" + value;
        }
        setString = setString.replaceFirst(",", "");
        
        result = "update " + this.getDestTable() + " set " + setString;
        return result;
    }
    
    /**
     * 自动在字符串两侧加上自定的标点符号
     * 当str==null，会返回空字符串。
     * @param str  需要在两侧加标点的字符串
     * @return
     */
    private String addQuot(String str){
        if(str == null){
            return "";
        }
        return "'"+str+"'";
    }
}
