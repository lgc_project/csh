package com.vrc.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.shiro.authc.LockedAccountException;

import com.google.gson.Gson;
import com.rdms.common.utils.R;
import com.rdms.common.utils.SpringContextUtils;
import com.rdms.modules.sys.entity.SysUserEntity;
import com.rdms.modules.sys.entity.SysUserTokenEntity;
import com.rdms.modules.sys.service.MemberTokenService;
import com.vrc.common.Const;
import com.vrc.controller.sys.UserController;
import com.vrc.dao.sys.UserDAO;
import com.vrc.dao.sys.VrcConfigDAO;
import com.vrc.vo.sys.UserVO;

/**
 * 会员请求校验，用来控制单终端登录
 * @author liboxing
 *
 */
public class TokenFilter implements Filter {

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		//获取请求token，如果token不存在，直接返回401
        String token = getRequestToken((HttpServletRequest) request);
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        
        if(StringUtils.isBlank(token)){
            String json = new Gson().toJson(R.error(HttpStatus.SC_UNAUTHORIZED, "invalid token"));
            httpResponse.getWriter().print(json);
        } else {
        	MemberTokenService service = (MemberTokenService) SpringContextUtils.getBean("memberTokenService");
    		//根据token，查询会员信息
    		SysUserTokenEntity tokenEntity = service.queryByToken(token);
    		//token失效
    		if(tokenEntity == null || tokenEntity.getExpireTime().getTime() < System.currentTimeMillis()){
    			String json = new Gson().toJson(R.error(HttpStatus.SC_UNAUTHORIZED, "invalid token"));
                httpResponse.getWriter().print(json);
    		} else {
    			//查询交易时间段，判断是否在交易时间内
    			VrcConfigDAO vrcConfigDAO = (VrcConfigDAO) SpringContextUtils.getBean("vrcConfigDAO");
    			String timeStart = vrcConfigDAO.getConfigByKey("tradetimestart");
            	String timeEnd = vrcConfigDAO.getConfigByKey("tradetimeend");
            	boolean f = UserController.isTradeTime(timeStart, timeEnd);
            	if(!f){
            		String json = new Gson().toJson(R.error(HttpStatus.SC_UNAUTHORIZED, "not in trade time"));
                    httpResponse.getWriter().print(json);
                    return;
            	}
    			
    			//查询用户，判断是否被冻结
    			UserDAO userDAO = (UserDAO) SpringContextUtils.getBean("userDAO");
    			UserVO u = userDAO.getUserById(String.valueOf(tokenEntity.getUserId()));
    			//如果用户不为空且没被冻结，直接放行
    			if(u != null && "1".equals(u.getIsvalid())){
    				chain.doFilter(request, response);
    			} else {
    				String json = new Gson().toJson(R.error(HttpStatus.SC_UNAUTHORIZED, "invalid token"));
                    httpResponse.getWriter().print(json);
    			}
    		}
        }
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

	/**
     * 获取请求的token
     */
    private String getRequestToken(HttpServletRequest httpRequest){
        //从header中获取token
        String token = httpRequest.getHeader("token");

        //如果header中不存在token，则从参数中获取token
        if(StringUtils.isBlank(token)){
            token = httpRequest.getParameter("token");
        }

        return token;
    }
}
