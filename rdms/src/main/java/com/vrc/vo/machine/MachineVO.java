package com.vrc.vo.machine;

import java.math.BigDecimal;

/**
 * 矿机VO
 * @author liboxing
 *
 */
public class MachineVO {

    private String id;
    private String userId;
    private String machineNo;
    private int status;        //运行状态0:停止，1：运行
    private String type;       //机器类型
    private int workDay;
    private int workSum;
    private String isvalid;
    private int mtDay;
    private int mtSum;
    private BigDecimal maintenance;
    private int mtStatus; // 维修状态0：需要维修 1：正常
    
    private MachineTypeVO machineType = new MachineTypeVO();   //机器类型
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getMachineNo() {
        return machineNo;
    }
    public void setMachineNo(String machineNo) {
        this.machineNo = machineNo;
    }
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public int getWorkDay() {
        return workDay;
    }
    public void setWorkDay(int workDay) {
        this.workDay = workDay;
    }
    public int getWorkSum() {
        return workSum;
    }
    public void setWorkSum(int workSum) {
        this.workSum = workSum;
    }
    public String getIsvalid() {
        return isvalid;
    }
    public void setIsvalid(String isvalid) {
        this.isvalid = isvalid;
    }
    public MachineTypeVO getMachineType() {
        return machineType;
    }
    public void setMachineType(MachineTypeVO machineType) {
        this.machineType = machineType;
    }
	public int getMtDay() {
		return mtDay;
	}
	public void setMtDay(int mtDay) {
		this.mtDay = mtDay;
	}
	public int getMtSum() {
		return mtSum;
	}
	public void setMtSum(int mtSum) {
		this.mtSum = mtSum;
	}
	public BigDecimal getMaintenance() {
		return maintenance;
	}
	public void setMaintenance(BigDecimal maintenance) {
		this.maintenance = maintenance;
	}
	public int getMtStatus() {
		return mtStatus;
	}
	public void setMtStatus(int mtStatus) {
		this.mtStatus = mtStatus;
	}
}
