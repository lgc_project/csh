package com.vrc.vo.machine;

import java.math.BigDecimal;

/**
 * 矿机类型vo
 * @author liboxing
 *
 */
public class MachineTypeVO {

    private String id;
    private String code;
    private String name;
    private int calculate;
    private int workSum;
    private double incomeSum;
    private double price;
    private String isvalid;
    private BigDecimal maintenance;
    private int mtSum;
    private String level;
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getCalculate() {
        return calculate;
    }
    public void setCalculate(int calculate) {
        this.calculate = calculate;
    }
    public int getWorkSum() {
        return workSum;
    }
    public void setWorkSum(int workSum) {
        this.workSum = workSum;
    }
    public double getIncomeSum() {
        return incomeSum;
    }
    public void setIncomeSum(double incomeSum) {
        this.incomeSum = incomeSum;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public String getIsvalid() {
        return isvalid;
    }
    public void setIsvalid(String isvalid) {
        this.isvalid = isvalid;
    }
	public BigDecimal getMaintenance() {
		return maintenance;
	}
	public void setMaintenance(BigDecimal maintenance) {
		this.maintenance = maintenance;
	}
	public int getMtSum() {
		return mtSum;
	}
	public void setMtSum(int mtSum) {
		this.mtSum = mtSum;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	
}
