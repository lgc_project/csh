package com.vrc.vo.job;

/**
 * 记录矿机的日产量，等级的实体（计算团队收益的定时任务中有用到）
 * @author liboxing
 *
 */
public class MachineDTO {

	private String id;
	private String userId;    //用户id
	private String machineNo;   //矿机编号
	private String type;      //矿机类型
	private double dayIncome;   //日收益
	private int level;          //等级
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getMachineNo() {
		return machineNo;
	}
	public void setMachineNo(String machineNo) {
		this.machineNo = machineNo;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getDayIncome() {
		return dayIncome;
	}
	public void setDayIncome(double dayIncome) {
		this.dayIncome = dayIncome;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	
}
