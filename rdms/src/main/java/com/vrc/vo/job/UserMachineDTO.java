package com.vrc.vo.job;

import java.util.ArrayList;
import java.util.List;

import com.vrc.vo.machine.MachineVO;

/**
 * 记录用户以及他拥有的矿机的实体
 * @author liboxing
 *
 */
public class UserMachineDTO {

	private String userId;   //用户id
	private String parentId;     //推荐人id
	private int maxLevel;        //最高的矿机等级（用于计算获取金币的代数）
	private List<MachineDTO> machineList = new ArrayList<>();   //拥有的矿机列表
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public int getMaxLevel() {
		return maxLevel;
	}
	public void setMaxLevel(int maxLevel) {
		this.maxLevel = maxLevel;
	}
	public List<MachineDTO> getMachineList() {
		return machineList;
	}
//	public void setMachineList(List<MachineDTO> machineList) {
//		this.machineList = machineList;
//	}
	
}
