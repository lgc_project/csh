package com.vrc.vo.trade;

/**
 * 
 * @ClassName: CommodityDealVO
 * @Description:商品交易记录中间表
 * @author: MDS
 * @date: 2018年5月29日 上午11:08:09
 */
public class CommodityDealVO {
	// 编号
	private Long id;
	// 商品编号
	private Long commodityId;
	// 商品交易编号
	private Long dealId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCommodityId() {
		return commodityId;
	}

	public void setCommodityId(Long commodityId) {
		this.commodityId = commodityId;
	}

	public Long getDealId() {
		return dealId;
	}

	public void setDealId(Long dealId) {
		this.dealId = dealId;
	}
}
