package com.vrc.vo.trade;

/**
 * 账单实体
 * @author liboxing
 *
 */
public class BillVO {

    public String id;
    public String type;
    public String actionType;
    public String actionTypeDesc;
    public String userId;
    public double price;
    public String time;
    private String billType; // 账单类型 0：矿石 1：财神币 2：百源币
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getActionType() {
        return actionType;
    }
    public void setActionType(String actionType) {
        this.actionType = actionType;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }
    public String getActionTypeDesc() {
        return actionTypeDesc;
    }
    public void setActionTypeDesc(String actionTypeDesc) {
        this.actionTypeDesc = actionTypeDesc;
    }
	public String getBillType() {
		return billType;
	}
	public void setBillType(String billType) {
		this.billType = billType;
	}
    
}
