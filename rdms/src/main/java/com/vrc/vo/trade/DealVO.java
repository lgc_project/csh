package com.vrc.vo.trade;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.vrc.util.DateUtils;

/**
 * 
 * @ClassName: DealVO
 * @Description:商品交易类
 * @author: MDS
 * @date: 2018年5月29日 上午8:36:21
 */
public class DealVO {
	// 交易记录编号
	private Long id;
	// 用户编号
	private Long userId;
	// 交易状态（1：交易进行中，2：交易已完成，3：交易取消。默认为：1）（数据库中的数据）
	private int state;
	// 交易状态（展示数据）
	private String stateView;
	// 商品创建时间（数据库中的数据）
	private long time;
	// 商品创建时间（展示给用户看的数据）
	private String timeView;
	// 收货人
	private String receiver;
	// 地址
	private String address;
	// 电话
	private String phone;
	
	// 数据结构思想
	public static final Integer DEAL_STATE_ONE = 1;
	public static final Integer DEAL_STATE_TWO = 2;
	public static final Integer DEAL_STATE_THREE = 3;
	public static final String DEAL_STATE_VIEW_ONE = "交易进行中";
	public static final String DEAL_STATE_VIEW_TWO = "交易已完成";
	public static final String DEAL_STATE_VIEW_THREE = "交易取消";
	public static final Map<Integer, String> STATE_MAP = new HashMap<Integer, String>();
	static {
		STATE_MAP.put(DEAL_STATE_ONE, DEAL_STATE_VIEW_ONE);
		STATE_MAP.put(DEAL_STATE_TWO, DEAL_STATE_VIEW_TWO);
		STATE_MAP.put(DEAL_STATE_THREE, DEAL_STATE_VIEW_THREE);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
		this.setStateView(STATE_MAP.get(state));
	}

	public String getStateView() {
		return stateView;
	}

	public void setStateView(String stateView) {
		this.stateView = stateView;
	}

	public String getTimeView() {
		return timeView;
	}

	public void setTimeView(String timeView) {
		this.timeView = timeView;
		this.time = DateUtils.getTime(timeView);
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
		this.timeView = DateUtils.dateToString(new Date(time));
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
}
