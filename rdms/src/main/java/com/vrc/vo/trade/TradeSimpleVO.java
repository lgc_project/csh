package com.vrc.vo.trade;

/**
 * 简要的交易记录（记录每天的平均价，最高价，成交数量）
 * @author liboxing
 *
 */
public class TradeSimpleVO {

	private String id;
	private double avg; // 矿石当前价
	private double max; // 矿石最高价
	private double count; // 矿石数量
	
	private double csbAvg; // 财神币当前价
	private double csbMax; // 财神币最高价
	private double csbCount; // 财神币数量
	
	private String time;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public double getAvg() {
		return avg;
	}
	public void setAvg(double avg) {
		this.avg = avg;
	}
	public double getMax() {
		return max;
	}
	public void setMax(double max) {
		this.max = max;
	}
	public double getCount() {
		return count;
	}
	public void setCount(double count) {
		this.count = count;
	}
	public double getCsbAvg() {
		return csbAvg;
	}
	public void setCsbAvg(double csbAvg) {
		this.csbAvg = csbAvg;
	}
	public double getCsbMax() {
		return csbMax;
	}
	public void setCsbMax(double csbMax) {
		this.csbMax = csbMax;
	}
	public double getCsbCount() {
		return csbCount;
	}
	public void setCsbCount(double csbCount) {
		this.csbCount = csbCount;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
}
