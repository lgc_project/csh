package com.vrc.vo.trade;

/**
 * 挂单表
 * @author liboxing
 *
 */
public class GuadanVO {

    private String id;
    private String oddId;
    private String type;   //类型：0：买单  1：卖单
    private String buyId;
    private String sellId;   //对方id，即购买此单的用户
    private double count;
    private double price;
    private double priceSum;
    private int status;  //交易状态：0：挂单中  1：卖家已确认  2：买家已确认  3：成功  4：撤单   5：取消交易
    private String guadanTime;    //挂单时间
    private String operateTime;   //最新操作时间
    private String isValid;       //是否有效  0:无效   1：有效
    private String level;         //挂单等级 1：新手  2：高手
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getOddId() {
        return oddId;
    }
    public void setOddId(String oddId) {
        this.oddId = oddId;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getBuyId() {
        return buyId;
    }
    public void setBuyId(String buyId) {
        this.buyId = buyId;
    }
    public String getSellId() {
        return sellId;
    }
    public void setSellId(String sellId) {
        this.sellId = sellId;
    }
    public double getCount() {
        return count;
    }
    public void setCount(double count) {
        this.count = count;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public double getPriceSum() {
        return priceSum;
    }
    public void setPriceSum(double priceSum) {
        this.priceSum = priceSum;
    }
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public String getGuadanTime() {
        return guadanTime;
    }
    public void setGuadanTime(String guadanTime) {
        this.guadanTime = guadanTime;
    }
    public String getOperateTime() {
        return operateTime;
    }
    public void setOperateTime(String operateTime) {
        this.operateTime = operateTime;
    }
    public String getIsValid() {
        return isValid;
    }
    public void setIsValid(String isValid) {
        this.isValid = isValid;
    }
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
    
}
