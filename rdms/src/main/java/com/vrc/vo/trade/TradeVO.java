package com.vrc.vo.trade;

/**
 * 交易记录实体
 * @author liboxing
 *
 */
public class TradeVO {

    private String id;
    private String oddNo;
    private String buyId;
    private String buyName;
    private String sellId;
    private String sellName;
    private double tradeCount;
    private double price;
    private double tradeMoney;
    private String tradeTime;
    private String status;
    private String isvalid;
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getOddNo() {
        return oddNo;
    }
    public void setOddNo(String oddNo) {
        this.oddNo = oddNo;
    }
    public String getBuyId() {
        return buyId;
    }
    public void setBuyId(String buyId) {
        this.buyId = buyId;
    }
    public String getSellId() {
        return sellId;
    }
    public void setSellId(String sellId) {
        this.sellId = sellId;
    }
    public double getTradeCount() {
        return tradeCount;
    }
    public void setTradeCount(double tradeCount) {
        this.tradeCount = tradeCount;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public double getTradeMoney() {
        return tradeMoney;
    }
    public void setTradeMoney(double tradeMoney) {
        this.tradeMoney = tradeMoney;
    }
    public String getTradeTime() {
        return tradeTime;
    }
    public void setTradeTime(String tradeTime) {
        this.tradeTime = tradeTime;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getIsvalid() {
        return isvalid;
    }
    public void setIsvalid(String isvalid) {
        this.isvalid = isvalid;
    }
	public String getBuyName() {
		return buyName;
	}
	public void setBuyName(String buyName) {
		this.buyName = buyName;
	}
	public String getSellName() {
		return sellName;
	}
	public void setSellName(String sellName) {
		this.sellName = sellName;
	}
    
}
