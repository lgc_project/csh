package com.vrc.vo.trade;

import java.util.Date;

import com.vrc.util.DateUtils;

/**
 * 
 * @ClassName: CommodityVO
 * @Description:商品实体类
 * @author: MDS
 * @date: 2018年5月29日 上午8:32:40
 */
public class CommodityVO {
	// 商品编号
	private Long id;
	// 商品名称
	private String commodityName;
	// 商品图片路径
	private String imgPath;
	// 商品价格
	private double price;
	// 商品介绍
	private String introduce;
	// 商品创建时间（数据库中的数据）
	private long time;
	// 商品创建时间（展示给用户看的数据）
	private String timeView;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCommodityName() {
		return commodityName;
	}

	public void setCommodityName(String commodityName) {
		this.commodityName = commodityName;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getIntroduce() {
		return introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	public String getTimeView() {
		return timeView;
	}

	public void setTimeView(String timeView) {
		this.timeView = timeView;
		this.time = DateUtils.getTime(timeView);
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
		this.timeView = DateUtils.dateToString(new Date(time));
	}
	
}
