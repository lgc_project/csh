package com.vrc.vo.sys;

/**
 * 用户实体
 * @author libx
 *
 */
public class UserVO {

    private String id;
    private String groupId;       //用户组id
    private String username;
    private String loginPassword;
    private String tradePassword;
    private String nickName;
    private String phone;
    private String parentId;     //推荐人id
    private String level;           //矿工等级
    private String levelDesc;       //等级描述
    private double moneySum;
//    private double moneyCanuse;
    private double moneyFreeze;
    private String realname;
    private String idcard;         //身份证号
    private String bankcard;       //银行卡号
    private String bankName;       //银行名称
    private String bankKind;       //银行类型
    private String bankcardType;   //银行卡类型
    private String bankCode;       //银行编码
    private String leavephone;
    private String alipayAccount;
    private String weixinAccount;
    private String isvalid;
    private String token;
    private String inviteCode;       //邀请码
    private boolean hasTradePsd;   //是否有交易密码
    
    private double oreSum; // 矿石数
    private double oreSumFreeze; // 冻结的矿石数
    private double yuanBaoSum; // 元宝数
    private double caiShenBiSum; // 财神币
    private double caiShenBiFreeze; // 冻结的财神币数
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getGroupId() {
        return groupId;
    }
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getLoginPassword() {
        return loginPassword;
    }
    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }
    public String getTradePassword() {
        return tradePassword;
    }
    public void setTradePassword(String tradePassword) {
        this.tradePassword = tradePassword;
    }
    public String getNickName() {
        return nickName;
    }
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getParentId() {
        return parentId;
    }
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
    public double getMoneySum() {
        return moneySum;
    }
    public void setMoneySum(double moneySum) {
        this.moneySum = moneySum;
    }
    public double getMoneyFreeze() {
        return moneyFreeze;
    }
    public void setMoneyFreeze(double moneyFreeze) {
        this.moneyFreeze = moneyFreeze;
    }
    public String getRealname() {
        return realname;
    }
    public void setRealname(String realname) {
        this.realname = realname;
    }
    public String getIdcard() {
        return idcard;
    }
    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }
    public String getBankcard() {
        return bankcard;
    }
    public void setBankcard(String bankcard) {
        this.bankcard = bankcard;
    }
    public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankKind() {
		return bankKind;
	}
	public void setBankKind(String bankKind) {
		this.bankKind = bankKind;
	}
	public String getBankcardType() {
		return bankcardType;
	}
	public void setBankcardType(String bankcardType) {
		this.bankcardType = bankcardType;
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getLeavephone() {
        return leavephone;
    }
    public void setLeavephone(String leavephone) {
        this.leavephone = leavephone;
    }
    public String getAlipayAccount() {
        return alipayAccount;
    }
    public void setAlipayAccount(String alipayAccount) {
        this.alipayAccount = alipayAccount;
    }
    public String getWeixinAccount() {
        return weixinAccount;
    }
    public void setWeixinAccount(String weixinAccount) {
        this.weixinAccount = weixinAccount;
    }
    public String getIsvalid() {
        return isvalid;
    }
    public void setIsvalid(String isvalid) {
        this.isvalid = isvalid;
    }
    public String getLevel() {
        return level;
    }
    public void setLevel(String level) {
        this.level = level;
    }
    public String getLevelDesc() {
        return levelDesc;
    }
    public void setLevelDesc(String levelDesc) {
        this.levelDesc = levelDesc;
    }
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public double getOreSum() {
		return oreSum;
	}
	public void setOreSum(double oreSum) {
		this.oreSum = oreSum;
	}
	public double getOreSumFreeze() {
		return oreSumFreeze;
	}
	public void setOreSumFreeze(double oreSumFreeze) {
		this.oreSumFreeze = oreSumFreeze;
	}
	public double getYuanBaoSum() {
		return yuanBaoSum;
	}
	public void setYuanBaoSum(double yuanBaoSum) {
		this.yuanBaoSum = yuanBaoSum;
	}
	public double getCaiShenBiSum() {
		return caiShenBiSum;
	}
	public void setCaiShenBiSum(double caiShenBiSum) {
		this.caiShenBiSum = caiShenBiSum;
	}
	public double getCaiShenBiFreeze() {
		return caiShenBiFreeze;
	}
	public void setCaiShenBiFreeze(double caiShenBiFreeze) {
		this.caiShenBiFreeze = caiShenBiFreeze;
	}
	public boolean isHasTradePsd() {
		return hasTradePsd;
	}
	public void setHasTradePsd(boolean hasTradePsd) {
		this.hasTradePsd = hasTradePsd;
	}
	public String getInviteCode() {
		return inviteCode;
	}
	public void setInviteCode(String inviteCode) {
		this.inviteCode = inviteCode;
	}
	
    
}
