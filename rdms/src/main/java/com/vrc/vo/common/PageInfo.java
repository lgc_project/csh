package com.vrc.vo.common;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import com.vrc.util.StringUtils;

/**
 * 分页查询实体
 * @author liboxing
 *
 */
public class PageInfo {

    private int pageNum=1;   //当前页码
    private int pageSize = 10;     //分页大小，默认10
    private int pageTotle = 1;     //总页数
    private int total;        //数据总条数
    private List<?> Data;    //数据列表
    
    public int getPageNum() {
        return pageNum;
    }
    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }
    public int getPageSize() {
        return pageSize;
    }
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
    public int getTotal() {
        return total;
    }
    public void setTotal(int total) {
        this.total = total;
    }
    public List<?> getData() {
		return Data;
	}
	public void setData(List<?> data) {
		Data = data;
	}
	public int getPageTotle() {
		return pageTotle;
	}
	public void setPageTotle(int pageTotle) {
		this.pageTotle = pageTotle;
	}
	/**
     * 拼装分页sql
     * @param sql
     * @return
     */
    public String decorateSql(String sql){
    	//若传入的页码等于0，设置为1
        if(pageNum == 0){
            pageNum = 1;
        }
        
        int start = (pageNum - 1) * pageSize;
        String s = "select * from ("+ sql +") tpage limit " + start + "," + pageSize;
        return s;
    }
    
    /**
     * 查询数据总条数
     * @param sql
     * @param template
     */
    public void pageTotal(String sql, JdbcTemplate template, Object... args){
        if(StringUtils.isEmpty(sql) || template == null){
            return ;
        }
        if(this.total == 0){
            String s = "select count(1) from ("+ sql +") tpage";
            this.total = template.queryForObject(s,Integer.class,args);
            if(this.total == 0){
            	this.pageTotle = 1;
            } else {
            	this.pageTotle = (this.total-1) / this.pageSize + 1;            	
            }
        }
    }
    
}
