package com.vrc.dao.machine.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.vrc.dao.machine.MachineDAO;
import com.vrc.util.DateUtils;
import com.vrc.util.InsertUtil;
import com.vrc.util.StringUtils;
import com.vrc.util.TableNameUtil;
import com.vrc.vo.common.PageInfo;
import com.vrc.vo.machine.MachineTypeVO;
import com.vrc.vo.machine.MachineVO;
import com.vrc.vo.trade.BuyrecordVO;

@Repository("machineDAO")
public class MachineDAOImpl implements MachineDAO{

    @Resource
    private JdbcTemplate template;
    
    @Override
    public boolean saveMachine(MachineVO vo) {
//        String sql = "insert into tb_machine (id,user_id,machine_no,status,type,work_day,work_sum,mt_day,mt_sum)"
//                + " values (?,?,?,?,?,?,?)";
//        
//        int count = template.update(sql, vo.getId(),vo.getUserId(),vo.getMachineNo(),
//                vo.getStatus(),vo.getType(),vo.getWorkDay(),vo.getWorkSum(),vo.getMtDay(),vo.getMtSum());
//        return count > 0;
        InsertUtil insUtil = new InsertUtil("tb_machine");
        insUtil.setColumn("id", vo.getId());
        insUtil.setColumn("user_id", vo.getUserId());
        insUtil.setColumn("machine_no", vo.getMachineNo());
        insUtil.setColumn("status", vo.getStatus());
        insUtil.setColumn("type", vo.getType());
        insUtil.setColumn("work_day", vo.getWorkDay());
        insUtil.setColumn("work_sum", vo.getWorkSum());
        insUtil.setColumn("mt_day", vo.getMtDay());
        insUtil.setColumn("mt_sum", vo.getMtSum());
        insUtil.setColumn("maintenance", vo.getMaintenance().doubleValue());
        insUtil.setColumn("mt_status", vo.getMtStatus());
        String sql = insUtil.getInsertSql();
        int count = template.update(sql);
        return count > 0;
    }
    
    @Override
    public long getMaxMachineNo() {
        String sql = "select max(machine_no) from tb_machine";
        Long lo = template.queryForObject(sql, Long.class);
        long l = 0;
        if(lo != null){
        	l = lo.longValue();
        }
        return l;
    }

    @Override
    public List<MachineTypeVO> getAllMachineType() {
        List<MachineTypeVO> list = new ArrayList<>();
        String sql = "select * from tb_machine_type where isvalid='1'";
        
        SqlRowSet rs = template.queryForRowSet(sql);
        if(rs != null){
            while(rs.next()){
                MachineTypeVO vo = creatrMachineType(rs);
                list.add(vo);
            }
        }
        return list;
    }
    
    @Override
    public MachineTypeVO getMachineTypeByCode(String machineCode) {
        MachineTypeVO mType = null;
        String sql = "select * from tb_machine_type where code = ?";
        SqlRowSet rs = template.queryForRowSet(sql, machineCode);
        if(rs != null && rs.next()){
            mType = creatrMachineType(rs);
        }
        return mType;
    }
    
    @Override
    public List<MachineVO> getMachine(String userId) {
        List<MachineVO> list = new ArrayList<>();
        String sql = "select a.*,b.name as type_name,b.calculate,b.income_sum,b.work_sum as work_day_sum, b.mt_sum as mt_day_sum,b.maintenance as mt "
                + "from tb_machine a,tb_machine_type b where a.type=b.code and a.user_id=? order by b.calculate";
        
        SqlRowSet rs = template.queryForRowSet(sql, userId);
        if(rs != null){
            while(rs.next()){
                MachineVO vo = createMachine(rs);
                list.add(vo);
            }
        }
        return list;
    }

    @Override
	public List<MachineTypeVO> getMachineTypePage(PageInfo page, String keyword) {
    	List<MachineTypeVO> list = new ArrayList<>();
    	String where = "";
    	if(!StringUtils.isEmpty(keyword)){
    		where = " and name like '%"+keyword+"%'";
    	}
		String sql = "select * from "+TableNameUtil.TB_MACHINE_TYPE + " where isvalid='1' " 
    	+ where + " order by calculate";
		
		page.pageTotal(sql, template);
		sql = page.decorateSql(sql);
		SqlRowSet rs = template.queryForRowSet(sql);
		if(rs != null){
			while(rs.next()){
				MachineTypeVO vo = creatrMachineType(rs);
				list.add(vo);
			}
		}
		return list;
	}
    
    @Override
	public boolean saveMachineType(MachineTypeVO vo) {
		InsertUtil insUtil = new InsertUtil(TableNameUtil.TB_MACHINE_TYPE);
		insUtil.setColumn("id", vo.getId());
		insUtil.setColumn("code", vo.getCode());
		insUtil.setColumn("name", vo.getName());
		insUtil.setColumn("calculate", vo.getCalculate());
		insUtil.setColumn("work_sum", vo.getWorkSum());
		insUtil.setColumn("income_sum", vo.getIncomeSum());
		insUtil.setColumn("price", vo.getPrice());
		insUtil.setColumn("isvalid", "1");
		insUtil.setColumn("maintenance", vo.getMaintenance() == null ?
				BigDecimal.ZERO.toPlainString() : vo.getMaintenance().toPlainString());
		insUtil.setColumn("mt_sum", vo.getMtSum());
		
		String sql = insUtil.getInsertSql();
		int count = template.update(sql);
		return count > 0;
	}

	@Override
	public boolean updateMachineType(MachineTypeVO vo) {
		InsertUtil insUtil = new InsertUtil(TableNameUtil.TB_MACHINE_TYPE);
		insUtil.setColumn("code", vo.getCode());
		insUtil.setColumn("name", vo.getName());
		insUtil.setColumn("calculate", vo.getCalculate());
		insUtil.setColumn("work_sum", vo.getWorkSum());
		insUtil.setColumn("income_sum", vo.getIncomeSum());
		insUtil.setColumn("price", vo.getPrice());
		insUtil.setColumn("maintenance", vo.getMaintenance().toPlainString());
		insUtil.setColumn("mt_sum", vo.getMtSum());
		
		String sql = insUtil.getUpdateSql();
		sql = sql + " where id = ?";
		int count = template.update(sql, vo.getId());
		return count > 0;
	}
	
	@Override
	public boolean updateMachine(MachineVO vo) {
		InsertUtil insUtil = new InsertUtil(TableNameUtil.TB_MACHINE);
		insUtil.setColumn("user_id", vo.getUserId());
		insUtil.setColumn("machine_no", vo.getMachineNo());
		insUtil.setColumn("status", vo.getStatus());
		insUtil.setColumn("type", vo.getType());
		insUtil.setColumn("work_sum", vo.getWorkSum());
		insUtil.setColumn("mt_sum", vo.getMtSum());
		insUtil.setColumn("mt_status", vo.getMtStatus());
		insUtil.setColumn("work_day", vo.getWorkDay());
		insUtil.setColumn("mt_day", vo.getMtDay());
		insUtil.setColumn("maintenance", vo.getMaintenance().doubleValue());
		insUtil.setColumn("isvalid", vo.getIsvalid());
		String sql = insUtil.getUpdateSql();
		sql += " where id = ?";
		int count = template.update(sql, vo.getId());
		return count > 0;
	}

	
	@Override
	public boolean deleteMachType(String[] id) {
		if(id == null || id.length == 0){
			return false;
		}
		
		String idWhere = "";
		for(String str : id){
			idWhere = idWhere + ",'"+str+"'";
		}
		idWhere = idWhere.replaceFirst(",", "");
		idWhere = "("+idWhere+")";
		
//		String sql = "delete from "+TableNameUtil.TB_MACHINE_TYPE+" where code in "+idWhere;
		String sql = "update "+TableNameUtil.TB_MACHINE_TYPE+" set isvalid='0' where code in "+idWhere;
		int count = template.update(sql);
		return count > 0;
	}
    
	@Override
	public boolean delMachineByUserId(String[] userId) {
		if(userId == null || userId.length == 0){
			return false;
		}
		
		String idWhere = "";
		for(String str : userId){
			idWhere = idWhere + ",'"+str+"'";
		}
		idWhere = idWhere.replaceFirst(",", "");
		idWhere = "("+idWhere+")";
		
		String sql = "delete from "+TableNameUtil.TB_MACHINE+" where user_id in "+idWhere;
		int count = template.update(sql);
		return count > 0;
	}
	
	@Override
	public List<MachineVO> getMachinePage(PageInfo page, String keyword) {
		List<MachineVO> list = new ArrayList<>();
		String where = "";
    	if(!StringUtils.isEmpty(keyword)){
    		where = " and (a.user_id like '%"+keyword+"%' or a.machine_no like '%"+keyword+"%')";
    	}
        String sql = "select a.*,b.name as type_name,b.calculate,b.income_sum,b.work_sum as work_day_sum, b.mt_sum as mt_day_sum, b.maintenance as mt "
                + "from tb_machine a,tb_machine_type b where a.type=b.code " + where +" order by a.user_id,b.calculate";
        
        page.pageTotal(sql, template);
        sql = page.decorateSql(sql);
        
        SqlRowSet rs = template.queryForRowSet(sql);
        if(rs != null){
        	while(rs.next()){
        		MachineVO vo = createMachine(rs);
        		list.add(vo);
        	}
        }
		return list;
	}

	@Override
	public boolean deleteMachine(String[] code) {
		if(code == null || code.length == 0){
			return false;
		}
		
		String idWhere = "";
		for(String str : code){
			idWhere = idWhere + ",'"+str+"'";
		}
		idWhere = idWhere.replaceFirst(",", "");
		idWhere = "("+idWhere+")";
		
		String sql = "delete from "+TableNameUtil.TB_MACHINE+" where machine_no in "+idWhere;
//		String sql = "update "+TableNameUtil.TB_MACHINE_TYPE+" set isvalid='0' where machine_no in "+idWhere;
		int count = template.update(sql);
		return count > 0;
	}
	
	@Override
	public boolean saveBuyrecordVO(BuyrecordVO vo) {
		InsertUtil insUtil = new InsertUtil(TableNameUtil.TB_BUYRECORD);
		insUtil.setColumn("id", vo.getId());
		insUtil.setColumn("user_id", vo.getUserId());
		insUtil.setColumn("machine_type", vo.getMachineType());
		insUtil.setColumn("machine_no", vo.getMachineNo());
		String time = vo.getTime();
		insUtil.setColumn("time", DateUtils.getTimeX(time));
		
		String sql = insUtil.getInsertSql();
		int count = template.update(sql);
		return count > 0;
	}

	@Override
	public List<BuyrecordVO> getBuyRecordVO(PageInfo page, String keyword, String startTime, String endTime) {
		List<BuyrecordVO> list = new ArrayList<>();
    	String where = "";
    	if(!StringUtils.isEmpty(keyword)){
    		where = " and user_id like '%"+keyword+"%' or machine_no like '%"+keyword+"%'";
    	}
    	if(!StringUtils.isEmpty(startTime)){
    		long start = DateUtils.getTimeX(startTime);
    		where = where + " and time>="+start;
    	}
    	if(!StringUtils.isEmpty(endTime)){
    		long end = DateUtils.getTimeX(endTime);
    		where = where + " and time<="+end;
    	}
    	
		String sql = "select * from "+TableNameUtil.TB_BUYRECORD + " where 1=1 " 
    	+ where + " order by time desc";
		
		sql = "select t1.*,t2.name from ("+sql+") t1 left join "
				+ "(select code,name from tb_machine_type) t2 on t1.machine_type=t2.code";
		
		page.pageTotal(sql, template);
		sql = page.decorateSql(sql);
		SqlRowSet rs = template.queryForRowSet(sql);
		if(rs != null){
			while(rs.next()){
				BuyrecordVO vo = new BuyrecordVO();
				vo.setId(rs.getString("id"));
				vo.setUserId(rs.getString("user_id"));
				vo.setMachineNo(rs.getString("machine_no"));
				vo.setMachineType(rs.getString("machine_type"));
				vo.setTime(DateUtils.getDateStrX(rs.getLong("time")));
				vo.setMachineTypeName(rs.getString("name"));
				list.add(vo);
			}
		}
		return list;
	}
	
    //根据sqlrowset创建机器类型实体
    private MachineTypeVO creatrMachineType(SqlRowSet rs){
        MachineTypeVO vo = new MachineTypeVO();
        vo.setId(rs.getString("id"));
        vo.setCode(rs.getString("code"));
        vo.setName(rs.getString("name"));
        vo.setPrice(rs.getDouble("price"));
        vo.setCalculate(rs.getInt("calculate"));
        vo.setIncomeSum(rs.getDouble("income_sum"));
        vo.setWorkSum(rs.getInt("work_sum"));
        vo.setMaintenance(rs.getBigDecimal("maintenance"));
        vo.setMtSum(rs.getInt("mt_sum"));
        vo.setLevel(rs.getString("level"));
        return vo;
    }
    
    //根据sqlrowset创建机器实体
    private MachineVO createMachine(SqlRowSet rs){
        MachineVO vo = new MachineVO();
        vo.setId(rs.getString("id"));
        vo.setUserId(rs.getString("user_id"));
        vo.setMachineNo(rs.getString("machine_no"));
        vo.setStatus(rs.getInt("status"));
        vo.setType(rs.getString("type"));
        vo.setWorkDay(rs.getInt("work_day"));
        vo.setWorkSum(rs.getInt("work_day_sum"));
        vo.setMtDay(rs.getInt("mt_day"));
        vo.setMtSum(rs.getInt("mt_day_sum"));
        vo.setMaintenance(rs.getBigDecimal("mt"));
        vo.setMtStatus(rs.getInt("mt_status"));
        vo.getMachineType().setName(rs.getString("type_name"));
        vo.getMachineType().setCalculate(rs.getInt("calculate"));
        vo.getMachineType().setIncomeSum(rs.getDouble("income_sum"));
        return vo;
    }

	@Override
	public MachineVO getMachineByUidAndNo(String userId, String machineNo) {
		String sql = String.format("SELECT * FROM tb_machine WHERE user_id = '%s' AND machine_no = '%s'", userId, machineNo);
		SqlRowSet rs = template.queryForRowSet(sql);
		if(rs != null && rs.next()) {
			MachineVO vo = new MachineVO();
	        vo.setId(rs.getString("id"));
	        vo.setUserId(rs.getString("user_id"));
	        vo.setMachineNo(rs.getString("machine_no"));
	        vo.setStatus(rs.getInt("status"));
	        vo.setType(rs.getString("type"));
	        vo.setWorkDay(rs.getInt("work_day"));
	        vo.setWorkSum(rs.getInt("work_sum"));
	        vo.setMtDay(rs.getInt("mt_day"));
	        vo.setMtSum(rs.getInt("mt_sum"));
	        vo.setMaintenance(rs.getBigDecimal("maintenance"));
	        vo.setMtStatus(rs.getInt("mt_status"));
	        return vo;
		}
		return null;
	}

	@Override
	public MachineVO getMachineByUidAndCode(String userId, String machineCode) {
		String sql = String.format("select * from tb_machine where user_id = '%s' and type = '%s'", userId, machineCode);
		SqlRowSet rs = template.queryForRowSet(sql);
		if(rs != null && rs.next()) {
			MachineVO vo = new MachineVO();
			vo.setId(rs.getString("id"));
	        vo.setUserId(rs.getString("user_id"));
	        vo.setMachineNo(rs.getString("machine_no"));
	        vo.setStatus(rs.getInt("status"));
	        vo.setType(rs.getString("type"));
	        vo.setWorkDay(rs.getInt("work_day"));
	        vo.setWorkSum(rs.getInt("work_sum"));
	        vo.setMtDay(rs.getInt("mt_day"));
	        vo.setMtSum(rs.getInt("mt_sum"));
	        vo.setMaintenance(rs.getBigDecimal("maintenance"));
	        vo.setMtStatus(rs.getInt("mt_status"));
	        return vo;
		}
		return null;
	}

	@Override
	public List<Map<String, Object>> getMachineByUid(String userId) {
		String sql = "SELECT tm.type type, tmt.level level "
				+ "FROM tb_machine tm, tb_machine_type tmt "
				+ "WHERE tm.type = tmt.code and tm.user_id = ? and tmt.level != 0"; 
		SqlRowSet rs = template.queryForRowSet(sql, userId);
		
		List<Map<String, Object>> list = new ArrayList<>();
		while(rs != null && rs.next()) {
			Map<String, Object> ret = new HashMap<String, Object>();
			ret.put("type", rs.getString("type"));
			ret.put("level", rs.getString("level"));
			list.add(ret);
		}
		return list;
	}
	
	@Override
	public List<Map<String, Object>> getMachine(PageInfo page, String keyword) {
		String whereSql = "";
		if(!StringUtils.isEmpty(keyword)) {
			whereSql += "(and a.user_id like '%"+keyword+"%' or u.realname like '%"+keyword+"%') \n";
		}
		
		String sql = "select a.user_id, u.realname, sum(b.income_sum/b.mt_sum) cl \n";
		sql += "from tb_machine a, tb_machine_type b, tb_user u \n";
		sql += "where a.type = b.code and u.id = a.user_id \n";
		sql += whereSql;
		sql += "group by a.user_id";
		
		page.pageTotal(sql, template);
		sql = page.decorateSql(sql);
		SqlRowSet rs = template.queryForRowSet(sql);
		List<Map<String, Object>> list = new ArrayList<>();
		if(rs != null) {
			while(rs.next()) {
				Map<String, Object> map = new HashMap<>();
				map.put("userId", rs.getString("user_id"));
				map.put("realName", rs.getString("realname"));
				map.put("canLiang", rs.getString("cl"));
				list.add(map);
			}
		}
			   
	    return list;
	}
}
