package com.vrc.dao.machine;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.vrc.vo.common.PageInfo;
import com.vrc.vo.machine.MachineTypeVO;
import com.vrc.vo.machine.MachineVO;
import com.vrc.vo.trade.BuyrecordVO;

/**
 * 矿机DAO
 * @author liboxing
 *
 */
public interface MachineDAO {

    /**
     * 添加矿机
     * @param vo
     * @return
     */
    public boolean saveMachine(MachineVO vo);
    
    /**
     * 获取最大的机器编号
     * @return
     */
    public long getMaxMachineNo();
    
    /**
     * 获取所有的矿机类型
     * @return
     */
    public List<MachineTypeVO> getAllMachineType();
    
    /**
     * 根据矿机类型编码获取矿机类型
     * @param machineCode
     * @return
     */
    public MachineTypeVO getMachineTypeByCode(String machineCode);
    
    /**
     * 根据userId获取矿机列表
     * @param userId
     * @return
     */
    public List<MachineVO> getMachine(String userId);
    
    /**
     * 获取所有用户的矿机产量
     * 
     * @return
     */
    public List<Map<String, Object>> getMachine(PageInfo page, String keyword);
    
    /**
     * 分页获取机器类型
     * @param page
     * @param keyword
     * @return
     */
    public List<MachineTypeVO> getMachineTypePage(PageInfo page, String keyword);
    
    /**
     * 添加矿机类型
     * @param vo
     * @return
     */
    public boolean saveMachineType(MachineTypeVO vo); 
    
    /**
     * 修改矿机类型
     * @param vo
     * @return
     */
    public boolean updateMachineType(MachineTypeVO vo);
    
    /**
     * 修改矿机
     * 
     * @param vo
     * @return
     */
    public boolean updateMachine(MachineVO vo);
    
    /**
     * 删除矿机类型（可同时删除多个）
     * @param id
     * @return
     */
    public boolean deleteMachType(String[] code);
    
    /**
     * 根据用户id删除矿机
     * @param id
     * @return
     */
    public boolean delMachineByUserId(String[] userId);
    
    /**
     * 分页获取矿机
     * @param page
     * @param keyword
     * @return
     */
    public List<MachineVO> getMachinePage(PageInfo page, String keyword);
    
    /**
     * 删除矿机（可同时删除多个）
     * @param id
     * @return
     */
    public boolean deleteMachine(String[] code);
    
    /**
     * 添加购买矿机记录
     * @return
     */
    public boolean saveBuyrecordVO(BuyrecordVO vo);
    
    /**
     * 获取矿机交易记录
     * @param page
     * @param keyword
     * @param startTime
     * @param endTime
     * @return
     */
    public List<BuyrecordVO> getBuyRecordVO(PageInfo page, String keyword,String startTime, String endTime);
    
    /**
     * 根据用户id和矿机编号获取矿机
     * 
     * @param userId
     * @param machineNo
     * @return
     */
    public MachineVO getMachineByUidAndNo(String userId, String machineNo);
    
    /**
     * 根据用户id和矿机类型获取用户矿机
     * 
     * @param userId
     * @param machineCode
     * @return
     */
    public MachineVO getMachineByUidAndCode(String userId, String machineCode);
    
    /**
     * 根据用户ID获取所拥有的矿机类型和等级（除了免费矿机）
     * 
     * @param userId
     * @return
     */
    public List<Map<String, Object>> getMachineByUid(String userId);
}
