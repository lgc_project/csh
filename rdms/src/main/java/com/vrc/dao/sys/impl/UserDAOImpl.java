package com.vrc.dao.sys.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.vrc.dao.sys.UserDAO;
import com.vrc.util.InsertUtil;
import com.vrc.util.StringUtils;
import com.vrc.util.TableNameUtil;
import com.vrc.vo.common.PageInfo;
import com.vrc.vo.sys.UserVO;

import net.sf.json.JSONObject;

/**
 * 用户管理DAO
 * 
 * @author libx
 *
 */
@Repository("userDAO")
public class UserDAOImpl implements UserDAO {

	@Resource
	private JdbcTemplate template;

    @Override
    public boolean hasId(String id) {
    	boolean flag = false;

        String sql = "select 1 from tb_user where id=?";
        List<Map<String, Object>> list = template.queryForList(sql, id);
        if (list != null && list.size() > 0) {
            flag = true;
        }
        return flag;
    }
    
    @Override
    public boolean hasPhone(String phone) {
        boolean flag = false;

		String sql = "select 1 from tb_user where phone=?";
		List<Map<String, Object>> list = template.queryForList(sql, phone);
		if (list != null && list.size() > 0) {
			flag = true;
		}
		return flag;
	}

	@Override
	public boolean hasIdCard(String idCard) {
		boolean flag = false;

		String sql = "select 1 from tb_user where idcard=?";
		List<Map<String, Object>> list = template.queryForList(sql, idCard);
		if (list != null && list.size() > 0) {
			flag = true;
		}
		return flag;
	}

	@Override
	public boolean hasBankCard(String bankCard) {
		boolean flag = false;

		String sql = "select 1 from tb_user where bankcard=?";
		List<Map<String, Object>> list = template.queryForList(sql, bankCard);
		if (list != null && list.size() > 0) {
			flag = true;
		}
		return flag;
	}
    
    @Override
    public long getMaxId(){
        String sql = "select max(id) from tb_user";
        Long max = template.queryForObject(sql, Long.class);
        long id = 0;
        if(max != null){
        	id = max.longValue();
        }
        return id;
    }
    
    @Override
    public boolean hasUsername(String username) {
        boolean flag = false;

        String sql = "select 1 from tb_user where username=?";
        List<Map<String, Object>> list = template.queryForList(sql, username);
        if (list != null && list.size() > 0) {
            flag = true;
        }
        return flag;
    }

    @Override
    public boolean insertUser(UserVO userVO) {
    	//TODO
        String sql = "insert into tb_user (id,group_id,phone,login_password,parent_id,level,isvalid) values(?,?,?,?,?,?,?)";
        
        String id = userVO.getId();
        String groupId = userVO.getGroupId();
        String phone = userVO.getPhone();
        String login_password = userVO.getLoginPassword();
        String parentId = userVO.getParentId();
        String level = userVO.getLevel();
        String isvaild = userVO.getIsvalid();
        int i = template.update(sql, id,groupId,phone,login_password,parentId,level,isvaild);
        return i > 0;
    }

    @Override
    public UserVO getUser(String phone, String password) {
        UserVO user = null;
        String sql = "select * from tb_user where (phone=? and login_password=?) or (id=? and login_password=?)";
        SqlRowSet rs = template.queryForRowSet(sql, phone, password, phone, password);
        
        if(rs != null && rs.next()){
            user = createUserVO(rs);
        }
        
        //获取最高的矿机等级
        if(user != null){
        	String userId = user.getId();
        	String levelSql = "select a.*,b.level from tb_machine a,tb_machine_type b "
        			+ "where a.type=b.code and a.user_id=? and a.status='1' order by b.level desc";
        	SqlRowSet r = template.queryForRowSet(levelSql, userId);
        	if(r != null && r.next()){
        		String level = r.getString("level");
        		user.setLevel(level);
        	} else {
        		user.setLevel("0");
        	}
        }
        return user;
    }

    @Override
    public List<UserVO> getUserByParentId(String parentId) {
        List<UserVO> list = new ArrayList<>();
        String sql= "select t1.*,t2.level as maxlevel from "
        		+ "(select * from tb_user where parent_id=?) t1 "
        		+ "left join "
        		+ "(select user_id,max(level) as level from tb_machine a,tb_machine_type b "
        		+ "where a.type=b.code and a.status='1' "
        		+ "group by user_id) t2 "
        		+ "on t1.id=t2.user_id ";
        SqlRowSet rs = template.queryForRowSet(sql,parentId);
        
        if(rs != null){
            while(rs.next()){
                UserVO u = new UserVO();
                u.setId(rs.getString("id"));
                String level = rs.getString("maxlevel");
                if(StringUtils.isEmpty(level)){
                	level = "0";
                }
                u.setLevel(level);
                u.setPhone(rs.getString("phone"));
                list.add(u);
            }
        }
        return list;
    }
    
    @Override
    public Map<String, JSONObject> getMachineNumByParentId(String parentId) {
        Map<String, JSONObject> map = new HashMap<>();
        String sql = "select a.user_id,count(1) machinenum,sum(calculate) calculate, sum(income) income from "
                + "(select user_id,type from tb_machine where user_id in (select id from tb_user where parent_id=?) and status='1') a "
                + "LEFT JOIN " 
                + "(select code,calculate, income_sum/mt_sum income from tb_machine_type) b on a.type=b.code "
                + "group by a.user_id ";
        
        SqlRowSet rs = template.queryForRowSet(sql, parentId);
        if(rs != null){
            while(rs.next()){
                String userId = rs.getString("user_id");
                int machineNum = rs.getInt("machinenum");
                double calculate = rs.getDouble("calculate");
                double income = rs.getDouble("income");
                BigDecimal b = new BigDecimal(income).setScale(2, RoundingMode.UP);
                
                
                JSONObject jo = new JSONObject();
                jo.put("machineNum", machineNum);
                jo.put("calculate", calculate);
                jo.put("income", b.doubleValue());
                map.put(userId, jo);
            }
        }
        return map;
    }
    
    @Override
    public Map<String, Integer> getChildPushNum(String parentId) {
        Map<String, Integer> map = new HashMap<>();
        String sql = "select a.parent_id,count(1) as pushnum from"
                + " (select parent_id from tb_user where parent_id in (select id from tb_user where parent_id=?)) a"
                + " group by a.parent_id";
        
        SqlRowSet rs = template.queryForRowSet(sql,parentId);
        if(rs != null && rs.next()){
            String id = rs.getString("parent_id");
            int pushnum = rs.getInt("pushnum");
            map.put(id, pushnum);
        }
        return map;
    }
    
    @Override
    public UserVO getUserById(String userId) {
        String sql = "select * from tb_user where id=?";
        SqlRowSet rs = template.queryForRowSet(sql, userId);
        
        UserVO vo = null;
        if(rs != null && rs.next()){
            vo = createUserVO(rs);
        }
        return vo;
    }
    
    @Override
    public UserVO getUserByPhone(String phone){
    	 String sql = "select * from tb_user where phone=?";
         SqlRowSet rs = template.queryForRowSet(sql, phone);
         
         UserVO vo = null;
         if(rs != null && rs.next()){
             vo = createUserVO(rs);
         }
         return vo;
    }
    
    @Override
    public boolean updateUserMoney(UserVO user) {
        if(user == null || user.getId() == null){
            return false;
        }
        InsertUtil insUtil = new InsertUtil(TableNameUtil.TB_USER);
        insUtil.setColumn("money_sum", user.getMoneySum());
//        insUtil.setColumn("money_canuse", user.getMoneyCanuse());
        insUtil.setColumn("money_freeze", user.getMoneyFreeze());
        String sql = insUtil.getUpdateSql();
        sql = sql + " where id=?";
        
        int count = template.update(sql, user.getId());
        return count > 0;
    }
    
    @Override
	public boolean addUserMoney(String userId, double money) {
		String sql = "update tb_user set money_sum = IFNULL(money_sum,0)+? where id=?";
		int count = template.update(sql, money, userId);
		return count > 0;
	}

	@Override
	public boolean updateTradePass(String userId, String password) {
		String sql = "update tb_user set trade_password = ? where id = ?";
		int count = template.update(sql, password, userId);
		return count > 0;
	}

	@Override
	public boolean updateLoginPass(String userId, String password) {
		String sql = "update tb_user set login_password = ? where id = ?";
		int count = template.update(sql, password, userId);
		return count > 0;
	}

	@Override
	public String getTradePassById(String userId) {
		String sql = "select trade_password from tb_user where id=?";
        SqlRowSet rs = template.queryForRowSet(sql, userId);

		String str = null;
		if (rs != null && rs.next()) {
			str = rs.getString("trade_password");
		}
		return str;
	}

	@Override
	public UserVO getUserInfo(String userId) {
		String sql = "select * from tb_user where id=?";
		SqlRowSet rs = template.queryForRowSet(sql, userId);

		UserVO vo = null;
		if (rs != null && rs.next()) {
			vo = createUserRealInfo(rs);
		}
        //获取最高的矿机等级
        if(vo != null){
        	String levelSql = "select a.*,b.level from tb_machine a,tb_machine_type b "
        			+ "where a.type=b.code and a.user_id=? and a.status='1' order by b.level desc";
        	SqlRowSet r = template.queryForRowSet(levelSql, userId);
        	if(r != null && r.next()){
        		String level = r.getString("level");
        		vo.setLevel(level);
        	} else {
        		vo.setLevel("0");
        	}
        }
		return vo;
	}

	@Override
	public UserVO getUserByReference(String reference) {
		UserVO vo = null;
		String sql = "select * from " + TableNameUtil.TB_USER
				+ " where id=? or phone=?";
		SqlRowSet rs = template.queryForRowSet(sql, reference, reference);
		if (rs != null && rs.next()) {
			vo = createUserVO(rs);
		}
		return vo;
	}

	@Override
	public boolean updateRealInfo(UserVO user) {
		InsertUtil insUtil = new InsertUtil(TableNameUtil.TB_USER);
		insUtil.setColumn("realname", user.getRealname());
		insUtil.setColumn("idcard", user.getIdcard());
		insUtil.setColumn("bankcard", user.getBankcard());
		insUtil.setColumn("bank_name", user.getBankName());
		insUtil.setColumn("bank_kind", user.getBankKind());
		insUtil.setColumn("bankcard_type", user.getBankcardType());
		insUtil.setColumn("bank_code", user.getBankCode());

		String sql = insUtil.getUpdateSql();
		sql = sql + " where id=?";
		int count = template.update(sql, user.getId());
		return count > 0;
	}

	@Override
	public List<UserVO> getUserPage(PageInfo page, String keyword) {
		List<UserVO> list = new ArrayList<>();

		// id，号码，姓名
		String where = "";
		if (!StringUtils.isEmpty(keyword)) {
			where = " where id like '%" + keyword + "%' or phone like '%"
					+ keyword + "%' or realname like '%" + keyword + "%' ";
		}

		String sql = "select * from " + TableNameUtil.TB_USER + where
				+ " order by id";

		page.pageTotal(sql, template);
		sql = page.decorateSql(sql);
		SqlRowSet rs = template.queryForRowSet(sql);
		if (rs != null) {
			while (rs.next()) {
				UserVO vo = createUserRealInfo(rs);
				list.add(vo);
			}
		}
		return list;
	}

	@Override
	public boolean saveUser(UserVO vo) {
		InsertUtil insUtil = new InsertUtil(TableNameUtil.TB_USER);
		insUtil.setColumn("id", vo.getId());
		insUtil.setColumn("phone", vo.getPhone());
		insUtil.setColumn("parent_id", vo.getParentId());
		insUtil.setColumn("login_password", vo.getLoginPassword());
		insUtil.setColumn("money_sum", vo.getMoneySum());
		insUtil.setColumn("money_freeze", vo.getMoneyFreeze());

		String sql = insUtil.getInsertSql();
		int count = template.update(sql);
		return count > 0;
	}

	@Override
	public boolean updateUser(UserVO vo) {
		InsertUtil insUtil = new InsertUtil(TableNameUtil.TB_USER);
		insUtil.setColumn("phone", vo.getPhone());
		insUtil.setColumn("parent_id", vo.getParentId());
		insUtil.setColumn("money_sum", vo.getMoneySum());
		insUtil.setColumn("money_freeze", vo.getMoneyFreeze());
		insUtil.setColumn("realname", vo.getRealname());
		insUtil.setColumn("idcard", vo.getIdcard());
		insUtil.setColumn("bankcard", vo.getBankcard());
		insUtil.setColumn("bank_name", vo.getBankName());
		insUtil.setColumn("ore_sum", vo.getOreSum());
		insUtil.setColumn("ore_sum_freeze", vo.getOreSumFreeze());
		insUtil.setColumn("csb_sum", vo.getCaiShenBiSum());
		insUtil.setColumn("csb_freeze", vo.getCaiShenBiFreeze());
		insUtil.setColumn("weixin_account", vo.getWeixinAccount());
		insUtil.setColumn("alipay_account", vo.getAlipayAccount());
		
		String sql = insUtil.getUpdateSql();
		sql = sql + " where id=?";
		int count = template.update(sql, vo.getId());
		return count > 0;
	}

	@Override
	public boolean deleteUser(String[] id) {
		if (id == null || id.length == 0) {
			return false;
		}

		String idWhere = "";
		for (String str : id) {
			idWhere = idWhere + ",'" + str + "'";
		}
		idWhere = idWhere.replaceFirst(",", "");
		idWhere = "(" + idWhere + ")";

		String sql = "delete from " + TableNameUtil.TB_USER + " where id in "
				+ idWhere;
		int count = template.update(sql);
		return count > 0;
	}

	// 把sqlrowset变成用户对象（只获取基础信息）
	private UserVO createUserVO(SqlRowSet rs) {
		UserVO vo = new UserVO();
		vo.setId(rs.getString("id"));
		vo.setGroupId(rs.getString("group_id"));
		vo.setUsername(rs.getString("username"));
		vo.setPhone(rs.getString("phone"));
		vo.setLoginPassword(rs.getString("login_password"));
		vo.setTradePassword(rs.getString("trade_password"));
		vo.setParentId(rs.getString("parent_id"));
		vo.setNickName(rs.getString("nickname"));
		vo.setLevel(rs.getString("level"));
		vo.setMoneySum(rs.getDouble("money_sum"));
		// vo.setMoneyCanuse(rs.getDouble("money_cansue"));
		vo.setMoneyFreeze(rs.getDouble("money_freeze"));
		
		vo.setAlipayAccount(rs.getString("alipay_account"));
		vo.setWeixinAccount(rs.getString("weixin_account"));
		
		vo.setOreSum(rs.getDouble("ore_sum"));
		vo.setOreSumFreeze(rs.getDouble("ore_sum_freeze"));
		vo.setCaiShenBiSum(rs.getDouble("csb_sum"));
		vo.setCaiShenBiFreeze(rs.getDouble("csb_freeze"));
		vo.setIsvalid(rs.getString("isvalid"));
		return vo;
	}

	// 获取用户真实信息
	private UserVO createUserRealInfo(SqlRowSet rs) {
		UserVO vo = new UserVO();
		vo.setId(rs.getString("id"));
		vo.setPhone(rs.getString("phone"));
		vo.setLoginPassword(rs.getString("login_password"));
		vo.setTradePassword(rs.getString("trade_password"));
		vo.setParentId(rs.getString("parent_id"));
		vo.setLevel(rs.getString("level"));
		vo.setMoneySum(rs.getDouble("money_sum"));
		vo.setMoneyFreeze(rs.getDouble("money_freeze"));

		vo.setRealname(rs.getString("realname"));
		vo.setIdcard(rs.getString("idcard"));
		vo.setBankcard(rs.getString("bankcard"));
		vo.setBankName(rs.getString("bank_name"));
		vo.setBankKind(rs.getString("bank_kind"));
		vo.setBankcardType(rs.getString("bankcard_type"));
		vo.setBankCode(rs.getString("bank_code"));
		vo.setAlipayAccount(rs.getString("alipay_account"));
		vo.setWeixinAccount(rs.getString("weixin_account"));
		vo.setOreSum(rs.getDouble("ore_sum"));
		vo.setOreSumFreeze(rs.getDouble("ore_sum_freeze"));
		vo.setCaiShenBiSum(rs.getDouble("csb_sum"));
		vo.setCaiShenBiFreeze(rs.getDouble("csb_freeze"));
		vo.setIsvalid(rs.getString("isvalid"));
		return vo;
	}

	@Override
	public boolean setWeixinAccount(String id, String account) {
		String sql = "update tb_user set weixin_account = ? where id = ?";
		int count = template.update(sql, account, id);
		return count > 0;
	}

	@Override
	public boolean setAlipayAccount(String id, String account) {
		String sql = "update tb_user set alipay_account = ? where id = ?";
		int count = template.update(sql, account, id);
		return count > 0;
	}

	@Override
	public boolean updateMoneySum(double money, Long userId) {
		String sql = "update tb_user set money_sum=money_sum+? where id = ?";
		int count = template.update(sql, money, userId);
		return count > 0;
	}

	@Override
	public boolean addOreSum(String userId, double oreSum) {
		String sql = "";
		sql += "UPDATE tb_user SET ore_sum = IFNULL(ore_sum, 0)+? WHERE id = ?";
		int count = template.update(sql, oreSum, userId);
		return count > 0;
	}

	@Override
	public boolean addCsbSum(String userId, BigDecimal csbSum) {
		String sql = "";
		sql += "UPDATE tb_user SET csb_sum = IFNULL(csb_sum, 0)+? WHERE id = ?";
		int count = template.update(sql, csbSum, userId);
		return count > 0;
	}

	@Override
	public boolean subCsbSum(String userId, BigDecimal csbSum) {
		String sql = "";
		sql += "UPDATE tb_user SET csb_sum = csb_sum - ? WHERE id = ? \n";
		int count = template.update(sql, csbSum, userId);
		return count > 0;
	}

	@Override
	public boolean subOreSum(String userId, BigDecimal oreSum) {
		String sql = "";
		sql += "UPDATE tb_user SET ore_sum = ore_sum - ? WHERE id = ? \n";
		int count = template.update(sql, oreSum, userId);
		return count > 0;
	}


	@Override
	public List<UserVO> getUserByParentIdPage(PageInfo page, String parentId) {
		List<UserVO> list = new ArrayList<>();
		String sql = String.format("select * from tb_user where parent_id = %s \n", parentId);
		page.pageTotal(sql, template);
		sql = page.decorateSql(sql);
		SqlRowSet rs = template.queryForRowSet(sql);
		if (rs != null) {
			while (rs.next()) {
				UserVO u = new UserVO();
				u.setId(rs.getString("id"));
				String level = rs.getString("level");
				u.setLevel("1".equals(level) ? "普通矿工" : "");
				u.setPhone(rs.getString("phone"));
				list.add(u);
			}
		}
		return list;
	}
	
	@Override
	public boolean freezeUser(String userId) {
		String sql = "update " + TableNameUtil.TB_USER+" set isvalid=0 where id=?";
		int count = template.update(sql,userId);
		return count > 0;
	}

	@Override
	public boolean reFreezeUser(String userId) {
		String sql = "update " + TableNameUtil.TB_USER+" set isvalid=1 where id=?";
		int count = template.update(sql,userId);
		return count > 0;
	}

}
