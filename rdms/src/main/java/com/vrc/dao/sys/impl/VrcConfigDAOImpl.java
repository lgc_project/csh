package com.vrc.dao.sys.impl;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.vrc.dao.sys.VrcConfigDAO;
import com.vrc.util.KeyUtil;

import net.sf.json.JSONObject;

/**
 * 系统参数设置DAO
 * @author liboxing
 *
 */
@Repository("vrcConfigDAO")
public class VrcConfigDAOImpl implements VrcConfigDAO {

	@Resource
	private JdbcTemplate template;
	
	@Override
	public JSONObject getAllConfig() {
		String sql = "select * from tb_sysconfig";
		SqlRowSet rs = template.queryForRowSet(sql);
		
		JSONObject jo = new JSONObject();
		if(rs != null){
			while(rs.next()){
				String key = rs.getString("code");
				String value = rs.getString("value");
				jo.put(key, value);
			}
		}
		return jo;
	}

	@Override
	public String getConfigByKey(String key) {
		String sql = "select * from tb_sysconfig where code=?";
		SqlRowSet rs = template.queryForRowSet(sql, key);
		
		String str = null;
		if(rs != null && rs.next()){
			str = rs.getString("value");
		}
		return str;
	}

	@Override
	public boolean saveOrUpdate(String key, String value) {
		String sql = "select * from tb_sysconfig where code=?";
		SqlRowSet rs = template.queryForRowSet(sql, key);
		
		int count = 0;
		if(rs != null && rs.next()){
			String rsValue = rs.getString("value");
			if(rsValue != null && rsValue.equals(value)){
				
			} else {
				sql = "update tb_sysconfig set value=? where code=?";
				count = template.update(sql, value, key);				
			}
		} else {
			String id = KeyUtil.getKey32();
			sql = "insert into tb_sysconfig (id,code,value) values(?,?,?)";
			count = template.update(sql, id, key, value);
		}
		return count > 0;
	}

}
