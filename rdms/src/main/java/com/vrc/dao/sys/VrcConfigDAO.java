package com.vrc.dao.sys;

import net.sf.json.JSONObject;

/**
 * 系统参数设置dao
 * @author liboxing
 *
 */
public interface VrcConfigDAO {

	/**
	 * 获取所有配置参数
	 * @return
	 */
	public JSONObject getAllConfig();
	
	/**
	 * 根据key值获取参数
	 * @param key
	 * @return
	 */
	public String getConfigByKey(String key);
	
	/**
	 * 跟新系统参数
	 * @param key
	 * @param value
	 */
	public boolean saveOrUpdate(String key, String value);
}
