package com.vrc.dao.sys;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.vrc.vo.common.PageInfo;
import com.vrc.vo.sys.UserVO;

import net.sf.json.JSONObject;

/**
 * 用户管理dao
 * @author libx
 *
 */
public interface UserDAO {

	/**
	 * 判断用户id书否存在
	 * 
	 * @param phone
	 * @return
	 */
	public boolean hasId(String id);
    /**
     * 判断号码是否已注册
     * @param phone
     * @return
     */
    public boolean hasPhone(String phone);
    
    /**
     * 判断邀请码是否存在
     * @param inviteCode
     * @return
     */
//    public boolean hasInviteCode(String inviteCode);
    
    /**
     * 身份证是否已存在
     * @param idCard
     * @return
     */
    public boolean hasIdCard(String idCard);
    
    /**
     * 银行卡是否已存在
     * @param bankCard
     * @return
     */
    public boolean hasBankCard(String bankCard);
    
    /**
     * 判断用户名是否已存在
     * @param username
     * @return
     */
    public boolean hasUsername(String username);
    
    /**
     * 获取最大的用户id
     * @return
     */
    public long getMaxId();
    
    /**
     * 添加用户
     * @param userVO
     * @return
     */
    public boolean insertUser(UserVO userVO);
    
    /**
     * 获取用户
     * @param phone
     * @param password
     * @return
     */
    public UserVO getUser(String phone, String password);
    
    /**
     * 根据推荐人id获取直推用户
     * @param parentId
     * @return
     */
    public List<UserVO> getUserByParentId(String parentId);
    
    /**
     * 根据parentId获取直推用户的矿机数量和总算力
     * @param parentId
     * @return
     */
    public Map<String, JSONObject> getMachineNumByParentId(String parentId);

	/**
	 * 获取每个子用户的直推数量（即我直推的人中，每个人又直推的人数）
	 * 
	 * @param parentId
	 * @return
	 */
	public Map<String, Integer> getChildPushNum(String parentId);

	/**
	 * 根据用户id获取用户
	 * 
	 * @param userId
	 * @return
	 */
	public UserVO getUserById(String userId);

	/**
	 * 根据用户手机获取用户
	 * 
	 * @param phone
	 * @return
	 */
	public UserVO getUserByPhone(String phone);

	/**
	 * 修改用户的总金额，可用金额，冻结金额（不传的值不进行更新）
	 * 
	 * @param user
	 * @return
	 */
	public boolean updateUserMoney(UserVO user);

	/**
	 * 增加用户的资产（收益和团队奖励专用）
	 * 
	 * @param userId
	 * @param money
	 * @return
	 */
	public boolean addUserMoney(String userId, double money);

	/**
	 * 修改交易密码
	 * 
	 * @param userId
	 * @param password
	 * @return
	 */
	public boolean updateTradePass(String userId, String password);

	/**
	 * 修改登录密码
	 * 
	 * @param userId
	 * @param password
	 * @return
	 */
	public boolean updateLoginPass(String userId, String password);

	/**
	 * 获取用户的交易密码
	 * 
	 * @param userId
	 * @return
	 */
	public String getTradePassById(String userId);

	/**
	 * 获取用户信息（包括实名信息）
	 * 
	 * @param userId
	 * @return
	 */
	public UserVO getUserInfo(String userId);

	/**
	 * 输入推荐人时可以是手机或用户id，所以返回手机或id等于该字符的用户
	 * 
	 * @param reference
	 * @return
	 */
	public UserVO getUserByReference(String reference);

	/**
	 * 实名认证后，更新用户的真实信息
	 * 
	 * @param user
	 * @return
	 */
	public boolean updateRealInfo(UserVO user);

	/**
	 * 分页查询用户（会员）
	 * 
	 * @param page
	 * @param keyword
	 * @return
	 */
	public List<UserVO> getUserPage(PageInfo page, String keyword);

	/**
	 * 手动添加用户
	 * 
	 * @param vo
	 * @return
	 */
	public boolean saveUser(UserVO vo);

	/**
	 * 手动更新用户
	 * 
	 * @param vo
	 * @return
	 */
	public boolean updateUser(UserVO vo);

	/**
	 * 手动删除用户
	 * 
	 * @param id
	 * @return
	 */
	public boolean deleteUser(String[] id);

	/**
	 * 设置微信账号
	 * 
	 * @param id
	 * @param account
	 * @return
	 */
	public boolean setWeixinAccount(String id, String account);

	/**
	 * 设置支付宝账号
	 * 
	 * @param id
	 * @param account
	 * @return
	 */
	public boolean setAlipayAccount(String id, String account);

	/**
	 * 
	 * @Title: updateMoneySum
	 * @Description:更新用户总资产
	 * @author: MDS
	 * @param money
	 *            一次交易的金额
	 * @param userId
	 *            用户编号
	 * @return
	 */
	public boolean updateMoneySum(double money, Long userId);
	
	/**
	 * 增加矿石数
	 * 
	 * @param userId
	 * @param oreSum
	 * @return
	 */
	public boolean addOreSum(String userId, double oreSum);
	
	/**
	 * 增加财神币
	 * 
	 * @param userId
	 * @param csbSum
	 * @return
	 */
	public boolean addCsbSum(String userId, BigDecimal csbSum);
	
	/**
	 * 减去财神币
	 * 
	 * @param userId
	 * @param csbSum
	 * @return
	 */
	public boolean subCsbSum(String userId, BigDecimal csbSum);
	
	/**
	 * 减去矿石数
	 * 
	 * @param userId
	 * @param oreSum
	 * @return
	 */
	public boolean subOreSum(String userId, BigDecimal oreSum);
	
	 /**
     *	 冻结用户
     * @param userId
     * @return
     */
    public boolean freezeUser(String userId);
    
    /**
     *	 解冻用户
     * @param userId
     * @return
     */
    public boolean reFreezeUser(String userId);
    
    /**
     * 获取用户直推列表
     * 
     * @param page
     * @param parentId
     * @return
     */
    public List<UserVO> getUserByParentIdPage(PageInfo page, String parentId);
}
