package com.vrc.dao.notice.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.vrc.dao.notice.NoticeDAO;
import com.vrc.util.DateUtils;
import com.vrc.util.InsertUtil;
import com.vrc.util.StringUtils;
import com.vrc.util.TableNameUtil;
import com.vrc.vo.common.PageInfo;
import com.vrc.vo.notice.CustomServiceVO;
import com.vrc.vo.notice.NoticeVO;

/**
 * 公告dao
 * @author liboxing
 *
 */
@Repository("noticeDAO")
public class NoticeDAOImpl implements NoticeDAO {

	@Resource
	private JdbcTemplate template;
	
	@Override
	public boolean saveNotice(NoticeVO notice) {
		InsertUtil insUtil = new InsertUtil(TableNameUtil.TB_NOTICE);
		insUtil.setColumn("id", notice.getId());
		insUtil.setColumn("title", notice.getTitle());
		insUtil.setColumn("content", notice.getContent());
		insUtil.setColumn("creater", notice.getCreater());
		insUtil.setColumn("time", DateUtils.getTimeX(notice.getTime()));
		
		String sql = insUtil.getInsertSql();
		int count = template.update(sql);
		return count > 0;
	}

	@Override
	public boolean updateNotice(NoticeVO notice) {
		InsertUtil insUtil = new InsertUtil(TableNameUtil.TB_NOTICE);
		insUtil.setColumn("title", notice.getTitle());
		insUtil.setColumn("content", notice.getContent());
		insUtil.setColumn("creater", notice.getCreater());
		insUtil.setColumn("time", DateUtils.getTimeX(notice.getTime()));
		
		String sql = insUtil.getUpdateSql();
		sql = sql + " where id = ?";
		int count = template.update(sql, notice.getId());
		return count > 0;
	}

	@Override
	public boolean deleteNotice(String id) {
		String sql = "delete from "+ TableNameUtil.TB_NOTICE+" where id=?";
		int count = template.update(sql, id);
		return count > 0;
	}

	@Override
	public boolean deleteNotice(String[] id) {
		if(id == null || id.length == 0){
			return false;
		}
		
		String idWhere = "";
		for(String str : id){
			idWhere = idWhere + ",'"+str+"'";
		}
		idWhere = idWhere.replaceFirst(",", "");
		idWhere = "("+idWhere+")";
		
		String sql = "delete from "+TableNameUtil.TB_NOTICE+" where id in "+idWhere;
		int count = template.update(sql);
		return count > 0;
	}

	@Override
	public List<NoticeVO> getNotice() {
		List<NoticeVO> list = new ArrayList<>();
		String sql = "select * from "+TableNameUtil.TB_NOTICE +" order by time desc";
		SqlRowSet rs = template.queryForRowSet(sql);
		if(rs != null){
			while(rs.next()){
				NoticeVO vo = createNoticeVO(rs);
				list.add(vo);
			}
		}
		return list;
	}

	@Override
	public List<NoticeVO> getNotice(PageInfo page, String keyword) {
		List<NoticeVO> list = new ArrayList<>();
		String where = "";
		if(!StringUtils.isEmpty(keyword)){
			where = " where title like '%"+keyword+"%' or content like '%"+keyword+"%'";
		}
		
		String sql = "select * from "+TableNameUtil.TB_NOTICE + where +" order by time desc";
		
		page.pageTotal(sql, template);
		sql = page.decorateSql(sql);
		
		SqlRowSet rs = template.queryForRowSet(sql);
		if(rs != null){
			while(rs.next()){
				NoticeVO vo = createNoticeVO(rs);
				list.add(vo);
			}
		}
		return list;
	}

	@Override
	public NoticeVO getById(String id) {
		NoticeVO vo = null;
		String sql = "select * from "+TableNameUtil.TB_NOTICE +" where id=?";
		
		SqlRowSet rs = template.queryForRowSet(sql, id);
		if(rs != null && rs.next()){
			vo = createNoticeVO(rs);
		}
		return vo;
	}
	
	@Override
	public boolean saveCustomService(CustomServiceVO vo) {
		InsertUtil insUtil = new InsertUtil(TableNameUtil.TB_CUSTOMSERVICE);
		insUtil.setColumn("id", vo.getId());
		insUtil.setColumn("content", vo.getContent());
		insUtil.setColumn("creater", vo.getCreater());
		insUtil.setColumn("time", DateUtils.getTimeX(vo.getTime()));
		
		String sql = insUtil.getInsertSql();
		int count = template.update(sql);
		return count > 0;
	}

	@Override
	public boolean updateCustomService(CustomServiceVO vo) {
		InsertUtil insUtil = new InsertUtil(TableNameUtil.TB_CUSTOMSERVICE);
		insUtil.setColumn("content", vo.getContent());
		insUtil.setColumn("creater", vo.getCreater());
		insUtil.setColumn("time", DateUtils.getTimeX(vo.getTime()));
		
		String sql = insUtil.getUpdateSql();
		sql = sql + " where id = ?";
		int count = template.update(sql, vo.getId());
		return count > 0;
	}

	@Override
	public CustomServiceVO getCustomService() {
		String sql = "select * from " + TableNameUtil.TB_CUSTOMSERVICE;
		SqlRowSet rs = template.queryForRowSet(sql);
		
		CustomServiceVO vo = null;
		if(rs != null && rs.next()){
			vo = createCustomSericeVO(rs);
		}
		return vo;
	}
	
	//创建notice实体
	private NoticeVO createNoticeVO(SqlRowSet rs){
		NoticeVO vo = new NoticeVO();
		vo.setId(rs.getString("id"));
		vo.setTitle(rs.getString("title"));
		vo.setContent(rs.getString("content"));
		vo.setTime(DateUtils.getDateStrX(rs.getLong("time")));
		vo.setCreater(rs.getString("creater"));
		return vo;
	}
	
	//创建customservice实体
	private CustomServiceVO createCustomSericeVO(SqlRowSet rs){
		CustomServiceVO vo = new CustomServiceVO();
		vo.setId(rs.getString("id"));
		vo.setContent(rs.getString("content"));
		vo.setTime(DateUtils.getDateStrX(rs.getLong("time")));
		vo.setCreater(rs.getString("creater"));
		return vo;
	}

}
