package com.vrc.dao.notice;

import java.util.List;

import com.vrc.vo.common.PageInfo;
import com.vrc.vo.notice.CustomServiceVO;
import com.vrc.vo.notice.NoticeVO;

/**
 * 公告dao
 * @author liboxing
 *
 */
public interface NoticeDAO {

	/**
	 * 添加公告
	 * @return
	 */
	public boolean saveNotice(NoticeVO notice);
	
	/**
	 * 更新公告
	 * @return
	 */
	public boolean updateNotice(NoticeVO notice);
	
	/**
	 * 删除公告
	 * @param id
	 * @return
	 */
	public boolean deleteNotice(String id);
	
	/**
	 * 批量删除公告
	 * @param id
	 */
	public boolean deleteNotice(String[] id);
	
	/**
	 * 查询公告
	 * @return
	 */
	public List<NoticeVO> getNotice();
	
	/**
	 * 分页查询公告
	 * @param page
	 * @return
	 */
	public List<NoticeVO> getNotice(PageInfo page, String keyword);
	
	/**
	 * 根据id获取公告
	 * @param id
	 * @return
	 */
	public NoticeVO getById(String id);
	
	/**
	 * 添加客服内容
	 * @return
	 */
	public boolean saveCustomService(CustomServiceVO vo);
	
	/**
	 * 更新客服内容
	 * @return
	 */
	public boolean updateCustomService(CustomServiceVO vo);
	
	/**
	 * 获取客服内容
	 * @return
	 */
	public CustomServiceVO getCustomService();
}
