package com.vrc.dao.trade.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.vrc.dao.trade.CommodityDAO;
import com.vrc.util.DateUtils;
import com.vrc.util.InsertUtil;
import com.vrc.util.TableNameUtil;
import com.vrc.vo.common.PageInfo;
import com.vrc.vo.trade.CommodityVO;

@Repository("commodityDAO")
public class CommodityDAOImpl implements CommodityDAO {
	@Resource
	private JdbcTemplate template;

	@Override
	public List<CommodityVO> getCommodityPage(PageInfo page, String keyword) {
		String sql = "select * from tb_commodity \n";
		
		String whereSql = "";
		if(StringUtils.isNotBlank(keyword)) {
			whereSql += "WHERE id like '%" + keyword + "%' or commodity_name like '%" + "%' \n";
		}
		sql += whereSql;
		sql += "ORDER BY id \n";
		page.pageTotal(sql, template);
		sql = page.decorateSql(sql);
		SqlRowSet rs  = template.queryForRowSet(sql);
		
		List<CommodityVO> list = new ArrayList<>();
		if(rs != null) {
			while(rs.next()) {
				CommodityVO vo = createCommodity(rs);
				list.add(vo);
			}
		}
		
		return list;
	}
	
	@Override
	public boolean saveCommodity(CommodityVO vo) {
		InsertUtil insUtil = new InsertUtil(TableNameUtil.TB_COMMODITY);
		insUtil.setColumn("id", vo.getId());
		insUtil.setColumn("commodity_name", vo.getCommodityName());
		insUtil.setColumn("price", vo.getPrice());
		insUtil.setColumn("introduce", vo.getIntroduce());
		insUtil.setColumn("time", vo.getTime());
		int count = template.update(insUtil.getInsertSql());
		return count > 0;
	}

	@Override
	public boolean updateCommodity(CommodityVO vo) {
		InsertUtil insUtil = new InsertUtil(TableNameUtil.TB_COMMODITY);
		insUtil.setColumn("commodity_name", vo.getCommodityName());
		insUtil.setColumn("price", vo.getPrice());
		insUtil.setColumn("introduce", vo.getIntroduce());
		
		String sql = insUtil.getUpdateSql();
		sql += " WHERE id = ?";
		int count = template.update(sql, vo.getId());
		return count > 0;
	}

	@Override
	public boolean deleteCommodity(String[] ids) {
		if(ids == null || ids.length == 0) {
			return false;
		}
		String whereSql = "";
		for(int i = 0; i < ids.length; i++) {
			if(i == 0) {
				whereSql += String.format("%s", ids[i]);
			} else {
				whereSql += String.format(", %s", ids[i]);
			}
		}
		
		String delSql = "";
		delSql += String.format("DELETE FROM %s \n", TableNameUtil.TB_COMMODITY);
		delSql += String.format("WHERE id IN (%s) \n", whereSql);
		int count = template.update(delSql);
		
		return count > 0;
	}
	
	@Override
	public List<CommodityVO> getCommodityAll() {
		List<CommodityVO> list = new ArrayList<>();
		String sql = "select * from tb_commodity";

		SqlRowSet rs = template.queryForRowSet(sql);
		if (rs != null) {
			while (rs.next()) {
				CommodityVO vo = createCommodity(rs);
				list.add(vo);
			}
		}
		return list;
	}

	private CommodityVO createCommodity(SqlRowSet rs) {
		CommodityVO vo = new CommodityVO();
		vo.setId(rs.getLong("id"));
		vo.setCommodityName(rs.getString("commodity_name"));
		vo.setImgPath(rs.getString("img_path"));
		vo.setIntroduce(rs.getString("introduce"));
		vo.setPrice(rs.getDouble("price"));
		vo.setTimeView(DateUtils.getDateStr(rs.getLong("time")));
		return vo;
	}

	@Override
	public CommodityVO getCommodity(String commodityId) {
		String sql = "";
		sql += String.format("SELECT * FROM %s \n", TableNameUtil.TB_COMMODITY);
		sql += String.format("WHERE id = '%s' \n", commodityId);
		SqlRowSet rs = template.queryForRowSet(sql);
		CommodityVO vo = new CommodityVO();
		if(rs != null && rs.next()) {
			vo = createCommodity(rs);
		}
		
		return vo;
	}

	@Override
	public Long getMaxId() {
		String sql = String.format("SELECT MAX(id) FROM %s \n", TableNameUtil.TB_COMMODITY);		
		Long max = template.queryForObject(sql, Long.class);
		if(max == null) {
			return 0L;
		}
		return max;
	}

	@Override
	public boolean updateCommodityImg(String commodityId, String path) {
		InsertUtil insUtil = new InsertUtil(TableNameUtil.TB_COMMODITY);
		insUtil.setColumn("img_path", path);
		
		String sql = insUtil.getUpdateSql();
		sql += " where id = ?";
		int count = template.update(sql, commodityId);
		return count > 0;
	}
}
