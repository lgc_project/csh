package com.vrc.dao.trade;

import com.vrc.vo.trade.CommodityDealVO;

/**
 * 
 * @ClassName:  CommodityDealDao   
 * @Description:商品交易中间表dao接口
 * @author: MDS
 * @date:   2018年5月29日 上午11:37:08
 */
public interface CommodityDealDao {
	/**
	 * 
	 * @Title: saveCommodityDealInfo
	 * @Description:保存信息
	 * @author: MDS
	 * @param vo 商品交易中间表实体类对象
	 * @return
	 */
	public boolean saveCommodityDealInfo(CommodityDealVO vo);
	/**
	 * 获取最大的商品交易中间表id
	 * 
	 * @return
	 */
	public Long getMaxId();
}
