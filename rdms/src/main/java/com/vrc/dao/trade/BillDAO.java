package com.vrc.dao.trade;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.vrc.vo.common.PageInfo;
import com.vrc.vo.trade.BillVO;

/**
 * 账单操作dao
 * @author liboxing
 *
 */
public interface BillDAO {

    /**
     * 添加一个账单
     * @param bill
     * @return
     */
    public boolean addBill(BillVO bill);
    
    /**
     * 获取账单列表
     * @param userId   用户id
     * @param type     类型（全部，收益，奖励，支出，冻结）
     * @param page     分页实体
     * @param billType 账单类型0：矿石 1：财神币 2：百源币
     * @return
     */
    public List<BillVO> getBillList(String userId, String type, PageInfo page, String billType);
}
