package com.vrc.dao.trade;

import java.util.List;

import com.vrc.vo.common.PageInfo;
import com.vrc.vo.trade.CommodityVO;

/**
 * 
 * @ClassName:  CommodityDAO   
 * @Description:商品信息dao
 * @author: MDS
 * @date:   2018年5月29日 上午8:55:30
 */
public interface CommodityDAO {
	
	List<CommodityVO> getCommodityPage(PageInfo page, String keyword);
	
	boolean saveCommodity(CommodityVO vo);
	
	boolean updateCommodity(CommodityVO vo);
	
	boolean deleteCommodity(String[] ids);
	
	Long getMaxId();
	
	/**
	 * 
	 * @Title: getCommodityAll
	 * @Description:获取所有的商品信息
	 * @author: MDS
	 * @return
	 */
	public List<CommodityVO> getCommodityAll();
	
	/**
	 * 获取商品的信息
	 * 
	 * @param commodityId
	 * @return
	 */
	public CommodityVO getCommodity(String commodityId);
	
	public boolean updateCommodityImg(String commodityId, String path);
}
