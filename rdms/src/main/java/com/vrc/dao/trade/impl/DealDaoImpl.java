package com.vrc.dao.trade.impl;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.vrc.dao.trade.DealDao;
import com.vrc.util.InsertUtil;
import com.vrc.util.TableNameUtil;
import com.vrc.vo.trade.DealVO;

/**
 * 
 * @ClassName: DealDaoImpl
 * @Description:商品交易daoimpl类
 * @author: MDS
 * @date: 2018年5月29日 上午11:02:04
 */
@Repository("dealDao")
public class DealDaoImpl implements DealDao {
	@Resource
	private JdbcTemplate template;

	@Override
	public boolean saveDealInfo(DealVO vo) {
		InsertUtil insertUtil = new InsertUtil(TableNameUtil.TB_DEAL);
		insertUtil.setColumn("id", vo.getId());
		insertUtil.setColumn("user_id", vo.getUserId());
		insertUtil.setColumn("state", vo.getState());
		insertUtil.setColumn("time", vo.getTime());
		String sql = insertUtil.getInsertSql();
		int count = template.update(sql);
		return count > 0;
	}

	@Override
	public Long getMaxId() {
		String sql = "select max(id) from " + TableNameUtil.TB_DEAL;
		Long max = template.queryForObject(sql, Long.class);
		return max;
	}
}
