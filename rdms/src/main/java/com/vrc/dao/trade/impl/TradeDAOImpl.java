package com.vrc.dao.trade.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.vrc.common.Const;
import com.vrc.dao.trade.TradeDAO;
import com.vrc.util.DateUtils;
import com.vrc.util.InsertUtil;
import com.vrc.util.KeyUtil;
import com.vrc.util.StringUtils;
import com.vrc.util.TableNameUtil;
import com.vrc.vo.common.PageInfo;
import com.vrc.vo.trade.GuadanVO;
import com.vrc.vo.trade.TradeSimpleVO;
import com.vrc.vo.trade.TradeVO;

/**
 * 交易相关dao
 * @author liboxing
 *
 */
@Repository("tradeDAO")
public class TradeDAOImpl implements TradeDAO {

    @Resource
    private JdbcTemplate template;
    
    @Override
    public long getMaxGuadanId() {
        String sql = "select max(odd_no) from " + TableNameUtil.TB_GUADAN;
        Long max = template.queryForObject(sql,Long.class);
        long id = 0;
        if(max != null){
        	id = max.longValue();
        }
        return id;
    }

    @Override
    public boolean addGuadan(GuadanVO vo) {
        InsertUtil insUtil = new InsertUtil(TableNameUtil.TB_GUADAN);
        insUtil.setColumn("id", vo.getId());
        insUtil.setColumn("odd_no", vo.getOddId()); 
        insUtil.setColumn("buy_id", vo.getBuyId());
        insUtil.setColumn("sell_id", vo.getSellId());
        insUtil.setColumn("count", vo.getCount());
        insUtil.setColumn("price", vo.getPrice());
        insUtil.setColumn("price_sum", vo.getPriceSum());
        insUtil.setColumn("status", vo.getStatus());
        insUtil.setColumn("level", vo.getLevel());
        insUtil.setColumn("type", vo.getType());
        
        String guadanTime = vo.getGuadanTime();
        insUtil.setColumn("guadan_time", DateUtils.getTimeX(guadanTime));
        String oprTime = vo.getOperateTime();
        insUtil.setColumn("operate_time", DateUtils.getTimeX(oprTime));
        
        String sql = insUtil.getInsertSql();
        int count = template.update(sql);
        return count > 0;
    }

    @Override
    public List<GuadanVO> getGuadanList(String userId,String level, String type) {
        List<GuadanVO> list = new ArrayList<>();
        if(userId == null){
            userId = "";
        }
        
        String levelWhere = "";
        if(!StringUtils.isEmpty(level)){
        	levelWhere = " and level='"+level+"' ";
        }
        
        String idWhere = "";
        int oddStatus = 0;
        if(!StringUtils.isEmpty(type)) {
        	if("0".equals(type)) { // 买家挂单
        		idWhere = " and buy_id != ? ";
        		oddStatus = Const.oddPost;
        	} else if("1".equals(type)) {
        		idWhere = " and sell_id != ? ";
        		oddStatus = Const.oddSellSure; // 卖家挂单没有第一步
        	}
        }
        
        String sql = "select * from " + TableNameUtil.TB_GUADAN + " where status = ? and type = ? " + idWhere + levelWhere + " order by price desc, count desc";
        SqlRowSet rs = template.queryForRowSet(sql, oddStatus, type, userId);
        if(rs != null){
            while(rs.next()){
                GuadanVO vo = createGuadanVO(rs);
                list.add(vo);
            }
        }
        return list;
    }

    @Override
    public GuadanVO getGuadanByIdAndStau(String oddId, int stutas) {
        String sql = "select * from "+TableNameUtil.TB_GUADAN+" where odd_no=? and status=?";
        SqlRowSet rs = template.queryForRowSet(sql,oddId,stutas);
        
        GuadanVO vo = null;
        if(rs != null && rs.next()){
            vo = createGuadanVO(rs);
        }
        return vo;
    }
    
    @Override
    public boolean updateGuadan(GuadanVO vo) {
        InsertUtil insUtil = new InsertUtil(TableNameUtil.TB_GUADAN);
        insUtil.setColumn("sell_id", vo.getSellId());
        insUtil.setColumn("buy_id", vo.getBuyId());
        insUtil.setColumn("status", vo.getStatus());
        
        String oprTime = vo.getOperateTime();
        if(!StringUtils.isEmpty(oprTime)){
            insUtil.setColumn("operate_time", DateUtils.getTimeX(oprTime));            
        }
        
        String sql = insUtil.getUpdateSql();
        sql = sql + " where odd_no=?";
        int count = template.update(sql, vo.getOddId());
        return count > 0;
    }
    
    @Override
    public List<GuadanVO> getGuadanByBuyOrSell(String userId, String level, String type) {
    	String levelWhere = "";
        if(!StringUtils.isEmpty(level)){
        	levelWhere = " and level='"+level+"' ";
        } else {
        	levelWhere = " and level is null ";
        }
    	
        //查询状态为0，1，2。且买家或卖家是该用户的挂单
        String sql = "select * from " + TableNameUtil.TB_GUADAN + " where status in ('0','1','2') "
                + " and (buy_id=? or sell_id=?) "+levelWhere+" and type = ? order by operate_time desc";
        SqlRowSet rs = template.queryForRowSet(sql, userId, userId, type);
        
        List<GuadanVO> list = new ArrayList<>();
        if(rs != null){
            while(rs.next()){
                GuadanVO vo = createGuadanVO(rs);
                list.add(vo);
            }
        }
        return list;
    }
    
    @Override
    public GuadanVO getGuadanByOddId(String oddId) {
        String sql = "select * from "+TableNameUtil.TB_GUADAN+" where odd_no=?";
        SqlRowSet rs = template.queryForRowSet(sql,oddId);
        
        GuadanVO vo = null;
        if(rs != null && rs.next()){
            vo = createGuadanVO(rs);
        }
        return vo;
    }
    
    @Override
    public Map<String, Object> getLastSimpleTrade() {
    	Map<String, Object> map = new HashMap<>();
    	String sql = "select * from "+TableNameUtil.TB_TRADESIMPLE+" where time = "
    			+ "(select max(time) from "+TableNameUtil.TB_TRADESIMPLE+")";
    	SqlRowSet rs = template.queryForRowSet(sql);
		if(rs != null && rs.next()){
			String id = rs.getString("id");
			double avg = rs.getDouble("avg");
			double max = rs.getDouble("max");
			double count = rs.getDouble("count");
			
			double csbAvg = rs.getDouble("csb_avg");
			double csbMax = rs.getDouble("csb_max");
			double csbCount = rs.getDouble("csb_count");
			
			String time = rs.getString("time");
			map.put("id", StringUtils.nullToEmpty(id));
			map.put("avg", avg);
			map.put("max", max);
			map.put("count", count);
			
			map.put("csbAvg", csbAvg);
			map.put("csbMax", csbMax);
			map.put("csbCount", csbCount);
			
			map.put("time", StringUtils.nullToEmpty(time));
		}
        return map;
    }
    
    @Override
    public List<Map<String, Object>> getChartDate(String startTime, String endTime) {
    	List<Map<String, Object>> list = new ArrayList<>();
    	String sql = "select time as day,avg,csb_avg,csb_count from tb_tradesimple "
    			+ "where time>? and time<=? order by time";
    	startTime = DateUtils.formatDate(startTime, "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd");
    	endTime = DateUtils.formatDate(endTime, "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd");
    	
    	SqlRowSet rs = template.queryForRowSet(sql,startTime,endTime);
    	if(rs != null){
    		while(rs.next()){
    			String day = rs.getString("day");
    			double price = rs.getDouble("avg");
    			double csbPrice = rs.getDouble("csb_avg");
    			double csbCount = rs.getDouble("csb_count");
    			Map<String, Object> map = new HashMap<>();
                map.put("day", day);
                map.put("price", price);
                map.put("csbPrice", csbPrice);
                map.put("csbCount", csbCount);
                list.add(map);
    		}
    	}
        return list;
    }
    
    @Override
    public boolean addTrade(TradeVO trade) {
        InsertUtil insUtil = new InsertUtil(TableNameUtil.TB_TRADE);
        insUtil.setColumn("id", trade.getId());
        insUtil.setColumn("buy_id", trade.getBuyId());
        insUtil.setColumn("sell_id", trade.getSellId());
        insUtil.setColumn("price", trade.getPrice());
        insUtil.setColumn("trade_count", trade.getTradeCount());
        insUtil.setColumn("trade_money", trade.getTradeMoney());
        insUtil.setColumn("trade_time", DateUtils.getTimeX(trade.getTradeTime()));
        insUtil.setColumn("odd_no", trade.getOddNo());
        
        String sql = insUtil.getInsertSql();
        int count = template.update(sql);
        return count > 0;
    }
    
    @Override
	public int getSellCount(String userId, long startTime, long endTime) {
    	String sql = "select count(1) from "+TableNameUtil.TB_GUADAN+" where sell_id=? "
    			+ "and guadan_time>? and guadan_time<? and status not in ('4','5')";
    	
    	int count = template.queryForObject(sql, Integer.class, userId,startTime,endTime);
		return count;
	}
    
    @Override
	public boolean saveTradeSimple(TradeSimpleVO vo) {
		InsertUtil insUtil = new InsertUtil(TableNameUtil.TB_TRADESIMPLE);
		insUtil.setColumn("id", vo.getId());
		insUtil.setColumn("avg", vo.getAvg());
		insUtil.setColumn("max", vo.getMax());
		insUtil.setColumn("count", vo.getCount());
		insUtil.setColumn("csb_avg", vo.getCsbAvg());
		insUtil.setColumn("csb_max", vo.getCsbMax());
		insUtil.setColumn("csb_count", vo.getCsbCount());
		insUtil.setColumn("time", vo.getTime());
		
		String sql = insUtil.getInsertSql();
		int count = template.update(sql);
		return count > 0;
	}
    
    @Override
	public boolean updateTradeSimple(TradeSimpleVO vo) {
    	InsertUtil insUtil = new InsertUtil(TableNameUtil.TB_TRADESIMPLE);
		insUtil.setColumn("avg", vo.getAvg());
		insUtil.setColumn("max", vo.getMax());
		insUtil.setColumn("count", vo.getCount());
		insUtil.setColumn("csb_avg", vo.getCsbAvg());
		insUtil.setColumn("csb_max", vo.getCsbMax());
		insUtil.setColumn("csb_count", vo.getCsbCount());
		insUtil.setColumn("time", vo.getTime());
		
		String sql = insUtil.getUpdateSql();
		sql = sql + " where id=?";
		int count = template.update(sql, vo.getId());
		return count > 0;
	}

    private GuadanVO createGuadanVO(SqlRowSet rs){
        GuadanVO vo = new GuadanVO();
        vo.setId(rs.getString("id"));
        vo.setOddId(rs.getString("odd_no"));
        vo.setBuyId(rs.getString("buy_id"));
        vo.setSellId(rs.getString("sell_id"));
        vo.setCount(rs.getDouble("count"));
        vo.setPrice(rs.getDouble("price"));
        vo.setPriceSum(rs.getDouble("price_sum"));
        vo.setStatus(rs.getInt("status"));
        vo.setGuadanTime(DateUtils.getDateStrX(rs.getLong("guadan_time")));
        vo.setOperateTime(DateUtils.getDateStrX(rs.getLong("operate_time")));
        return vo;
    }

	@Override
	public List<TradeVO> getTradePage(PageInfo page, String keyword, String startTime, String endTime) {
		List<TradeVO> list = new ArrayList<>();
    	String where = "";
    	if(!StringUtils.isEmpty(keyword)){
    		where = " and buy_id like '%"+keyword+"%' or sell_id like '%"+keyword+"%'";
    	}
    	if(!StringUtils.isEmpty(startTime)){
    		long start = DateUtils.getTimeX(startTime);
    		where = where + " and trade_time>="+start;
    	}
    	if(!StringUtils.isEmpty(endTime)){
    		long end = DateUtils.getTimeX(endTime);
    		where = where + " and trade_time<="+end;
    	}
    	
		String sql = "select a.*,(select realname from tb_user b where a.buy_id=b.id) as buy_name,"
				+ "(select realname from tb_user c where a.sell_id=c.id) as sell_name from tb_trade a where 1=1 "
				+ where + " order by trade_time desc";
		
		page.pageTotal(sql, template);
		sql = page.decorateSql(sql);
		SqlRowSet rs = template.queryForRowSet(sql);
		if(rs != null){
			while(rs.next()){
				TradeVO vo = new TradeVO();
				vo.setId(rs.getString("id"));
				vo.setBuyId(rs.getString("buy_id"));
				vo.setBuyName(rs.getString("buy_name"));
				vo.setSellId(rs.getString("sell_id"));
				vo.setSellName(rs.getString("sell_name"));
				vo.setOddNo(rs.getString("odd_no"));
				vo.setPrice(rs.getDouble("price"));
				vo.setTradeCount(rs.getDouble("trade_count"));
				vo.setTradeMoney(rs.getDouble("trade_money"));
				vo.setTradeTime(DateUtils.getDateStrX(rs.getLong("trade_time")));
				list.add(vo);
			}
		}
		return list;
	}

	@Override
	public List<TradeSimpleVO> getSimpleTradePage(PageInfo page) {
		List<TradeSimpleVO> list = new ArrayList<>();
		String sql = "select * from "+TableNameUtil.TB_TRADESIMPLE+" order by time desc";
		
		page.pageTotal(sql, template);
		sql = page.decorateSql(sql);
		SqlRowSet rs = template.queryForRowSet(sql);
		if(rs != null){
			while(rs.next()){
				TradeSimpleVO vo = new TradeSimpleVO();
				vo.setId(rs.getString("id"));
				vo.setAvg(rs.getDouble("avg"));
				vo.setMax(rs.getDouble("max"));
				vo.setCount(rs.getDouble("count"));
				vo.setCsbAvg(rs.getDouble("csb_avg"));
				vo.setCsbMax(rs.getDouble("csb_max"));
				vo.setCsbCount(rs.getDouble("csb_count"));
				vo.setTime(rs.getString("time"));
				list.add(vo);
			}
		}
		return list;
	}
	
	@Override
	public boolean updateTradeSimpleCount(String date, double count,double price) {
		String sql = "select * from "+TableNameUtil.TB_TRADESIMPLE+" where time=?";
		SqlRowSet rs = template.queryForRowSet(sql,date);
		
		//如果有今天的数据，则矿石交易数量加一，然后处理最高价格
		if(rs != null && rs.next()){
			sql = "update "+TableNameUtil.TB_TRADESIMPLE+" set count = ifnull(count, 0) + ?, avg = ? where time=?";
			template.update(sql, count, price, date);
			
			//判断最大值
			double max = rs.getDouble("max");
			if(price >= max){
				sql = "update "+TableNameUtil.TB_TRADESIMPLE+" set max=? where time=?";
				template.update(sql,price,date);
			}
			
			
		} else {
			String id = KeyUtil.getKey32();
			sql = "insert into "+TableNameUtil.TB_TRADESIMPLE+" (id,avg,max,count,time) values(?,?,?,?,?)";
			template.update(sql,id,price,price,count,date);
		}
		
		return true;
	}

	@Override
	public boolean updateTradeSimpleCsbCount(String date, double csbCount, double csbPrice) {
		String sql = "select * from "+TableNameUtil.TB_TRADESIMPLE+" where time=?";
		SqlRowSet rs = template.queryForRowSet(sql,date);
		
		//如果有今天的数据，则财神币交易数量加一，然后处理最高价格
		if(rs != null && rs.next()){
			sql = "update "+TableNameUtil.TB_TRADESIMPLE+" set csb_count = ifnull(csb_count, 0) + ?, csb_avg = ? where time=?";
			template.update(sql, csbCount, csbPrice,date);
			
			//判断最大值
			double max = rs.getDouble("csb_max");
			if(csbPrice >= max){
				sql = "update "+TableNameUtil.TB_TRADESIMPLE+" set csb_max=? where time=?";
				template.update(sql,csbPrice,date);
			}
			
			
		} else {
			String id = KeyUtil.getKey32();
			sql = "insert into "+TableNameUtil.TB_TRADESIMPLE+" (id,csb_avg,csb_max,csb_count,time) values(?,?,?,?,?)";
			template.update(sql,id,csbPrice,csbPrice,csbCount,date);
		}
		
		return true;
	}

	@Override
	public boolean updateTradeSimpleAvg(String date, double avg) {
		String sql = "select * from "+TableNameUtil.TB_TRADESIMPLE+" where time=?";
		SqlRowSet rs = template.queryForRowSet(sql, date);
		
		if(rs != null && rs.next()){
			sql = "update "+TableNameUtil.TB_TRADESIMPLE+" set avg=? where time=?";
			template.update(sql,avg,date);
		} else {
			String id = KeyUtil.getKey32();
			sql = "insert into "+TableNameUtil.TB_TRADESIMPLE+" (id,avg,max,count,time) values(?,?,?,?,?)";
			template.update(sql,id,avg,0,0,date);
		}
		return true;
	}

	@Override
	public boolean updateTradeSimpleCsbAvg(String date, double csbAvg) {
		String sql = "select * from "+TableNameUtil.TB_TRADESIMPLE+" where time=?";
		SqlRowSet rs = template.queryForRowSet(sql, date);
		
		if(rs != null && rs.next()){
			sql = "update "+TableNameUtil.TB_TRADESIMPLE+" set csb_avg=? where time=?";
			template.update(sql,csbAvg,date);
		} else {
			String id = KeyUtil.getKey32();
			sql = "insert into "+TableNameUtil.TB_TRADESIMPLE+" (id,csb_avg,csb_max,csb_count,time) values(?,?,?,?,?)";
			template.update(sql,id,csbAvg,0,0,date);
		}
		return true;
	}

	@Override
	public boolean deleteTrade(String[] ids) {
		if(ids == null || ids.length == 0) {
			return false;
		}
		String whereSql = "";
		for(int i = 0; i < ids.length; i++) {
			if(i == 0) {
				whereSql += String.format("'%s'", ids[i]);
			} else {
				whereSql += String.format(", '%s'", ids[i]);
			}
		}
		
		String delSql = "";
		delSql += String.format("delete from %s \n", "tb_trade");
		delSql += String.format("where id IN (%s) \n", whereSql);
		int count = template.update(delSql);
		return count > 0;
	}
	
	
}
