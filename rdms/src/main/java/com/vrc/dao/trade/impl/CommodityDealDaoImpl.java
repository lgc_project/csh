package com.vrc.dao.trade.impl;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.vrc.dao.trade.CommodityDealDao;
import com.vrc.util.InsertUtil;
import com.vrc.util.TableNameUtil;
import com.vrc.vo.trade.CommodityDealVO;

/**
 * 
 * @ClassName: CommodityDealDaoImpl
 * @Description:商品交易中间表daoimpl类
 * @author: MDS
 * @date: 2018年5月29日 上午11:39:01
 */
@Repository("commodityDealDao")
public class CommodityDealDaoImpl implements CommodityDealDao {
	@Resource
	private JdbcTemplate template;

	@Override
	public boolean saveCommodityDealInfo(CommodityDealVO vo) {
		InsertUtil insertUtil = new InsertUtil(TableNameUtil.TB_COMMODITY_DEAL);
		insertUtil.setColumn("id", vo.getId());
		insertUtil.setColumn("commodity_id", vo.getCommodityId());
		insertUtil.setColumn("deal_id", vo.getDealId());
		String sql = insertUtil.getInsertSql();
		int count = template.update(sql);
		return count > 0;
	}

	@Override
	public Long getMaxId() {
		String sql = "select max(id) from " + TableNameUtil.TB_COMMODITY_DEAL;
		Long max = template.queryForObject(sql, Long.class);
		return max;
	}
}
