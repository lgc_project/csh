package com.vrc.dao.trade.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.vrc.dao.trade.BillDAO;
import com.vrc.util.DateUtils;
import com.vrc.util.InsertUtil;
import com.vrc.util.StringUtils;
import com.vrc.util.TableNameUtil;
import com.vrc.vo.common.PageInfo;
import com.vrc.vo.trade.BillVO;

/**
 * 账单操作dao
 * @author liboxing
 *
 */
@Repository("billDAO")
public class BillDAOImpl implements BillDAO {

	private Logger logger = Logger.getLogger(getClass());
	
    @Resource
    private JdbcTemplate template;
    
    @Override
    public boolean addBill(BillVO bill) {
        InsertUtil insUtil = new InsertUtil(TableNameUtil.TB_BILL);
        insUtil.setColumn("id", bill.getId());
        insUtil.setColumn("type", bill.getType());
        insUtil.setColumn("action_type", bill.getActionType());
        insUtil.setColumn("user_id", bill.getUserId());
        insUtil.setColumn("price", bill.getPrice());
        insUtil.setColumn("time", DateUtils.getTimeX(bill.getTime()));
        insUtil.setColumn("bill_type", bill.getBillType());
        
        String sql = insUtil.getInsertSql();
        int count = template.update(sql);
        return count > 0;
    }

    @Override
    public List<BillVO> getBillList(String userId, String type, PageInfo page, String billType) {
        if(userId == null){
            return new ArrayList<>();
        }
        List<BillVO> list = new ArrayList<>();
        
        String typeWhere = "";
        if(!StringUtils.isEmpty(type)){
            typeWhere = " and type='"+type+"'";
        }
        
        String billTypeWhere = "";
        if(!StringUtils.isEmpty(billType)) {
        	billTypeWhere = " and bill_type = '" + billType + "'";
        }
        String sql = "select * from "+TableNameUtil.TB_BILL+" where user_id=? " + typeWhere + billTypeWhere + " order by time desc";
        
        //关联账单子类型的名称（jzcode=actionType的就是账单子类型的翻译）
        sql = "select t1.*,t2.codedesc as action_type_desc from ("+sql+") t1 "
                + "left JOIN (select code,codedesc from tb_s_jzcode where jzcode='actionType') t2 "
                + "on t1.action_type = t2.code order by t1.time desc"; 
        
        logger.info(sql);
        //如果分页对象不为空，进行分页查询
        if(page != null){
            page.pageTotal(sql, template, userId);
            sql = page.decorateSql(sql);
        }
        
        SqlRowSet rs = template.queryForRowSet(sql, userId);
        if(rs != null){
            while(rs.next()){
                BillVO vo = createBillVO(rs);
                list.add(vo);
            }
        }
        return list;
    }

    
    private BillVO createBillVO(SqlRowSet rs){
        BillVO vo = new BillVO();
        vo.setId(rs.getString("id"));
        vo.setType(rs.getString("type"));
        vo.setActionType(rs.getString("action_type"));
        vo.setActionTypeDesc(rs.getString("action_type_desc"));
        vo.setUserId(rs.getString("user_id"));
        vo.setPrice(rs.getDouble("price"));
        vo.setTime(DateUtils.getDateStrX(rs.getLong("time")));
        vo.setBillType(rs.getString("bill_type"));
        return vo;
    }
}
