package com.vrc.dao.trade;

import com.vrc.vo.trade.DealVO;

/**
 * 
 * @ClassName:  DealDao   
 * @Description:商品交易dao接口
 * @author: MDS
 * @date:   2018年5月29日 上午10:57:59
 */
public interface DealDao {
	/**
	 * 
	 * @Title: saveDealInfo
	 * @Description:保存交易信息
	 * @author: MDS
	 * @param commodityId 商品编号
	 * @return
	 */
	public boolean saveDealInfo(DealVO vo);
	/**
	 * 获取最大的商品交易id
	 * 
	 * @return
	 */
	public Long getMaxId();
}
