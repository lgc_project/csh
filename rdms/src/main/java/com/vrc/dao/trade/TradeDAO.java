package com.vrc.dao.trade;

import java.util.List;
import java.util.Map;

import com.vrc.vo.common.PageInfo;
import com.vrc.vo.trade.GuadanVO;
import com.vrc.vo.trade.TradeSimpleVO;
import com.vrc.vo.trade.TradeVO;

public interface TradeDAO {

    /**
     * 获取挂单表的最大单号
     * @return
     */
    public long getMaxGuadanId();
    
    /**
     * 添加一份挂单
     * @param vo
     * @return
     */
    public boolean addGuadan(GuadanVO vo);
    
    /**
     * 获取挂单列表（用户看不到自己挂的单）
     * @param userId
     * @param level
     * @param type
     * @return
     */
    public List<GuadanVO> getGuadanList(String userId, String level, String type);
    
    /**
     * 根据单号和挂单状态获取挂单
     * @param oddId     单号
     * @param stutas     挂单状态
     * @return
     */
    public GuadanVO getGuadanByIdAndStau(String oddId, int stutas);
    
    /**
     * 根据odd_no更新挂单信息（只修改卖家id，状态，和操作时间）
     * @param vo
     * @return
     */
    public boolean updateGuadan(GuadanVO vo);
    
    /**
     * 查询状态为0，1，2。且买家或卖家是该用户的挂单
     * 查询跟用户相关的挂单（买家或卖家是该用户）
     * @param userId
     * @param level
     * @param type
     * @return
     */
    public List<GuadanVO> getGuadanByBuyOrSell(String userId, String level, String type);
    
    /**
     * 根据单号查询挂单
     * @param oddId   单号
     * @return
     */
    public GuadanVO getGuadanByOddId(String oddId);
    
    /**
     * 添加到交易表（交易成功后，添加到交易表，好作计算）
     * @param trade
     * @return
     */
    public boolean addTrade(TradeVO trade);
    
    /**
     * 获取前N天每天的平均价（用于构造矿市中的折线图）
     * @param startTime
     * @param endTime
     * @return
     */
    public List<Map<String, Object>> getChartDate(String startTime, String endTime);
    
    /**
     * 获取最新的平均价，最高价和交易数量
     * @param startTime
     * @param endTime
     * @return
     */
    public Map<String, Object> getLastSimpleTrade();
    
    /**
     * 查询卖家一天卖了多少单（一天最多卖3次）
     * @param userId
     * @return
     */
    public int getSellCount(String userId, long startTime, long endTime);
    
    /**
     * 修改交易概况
     * @param vo
     * @return
     */
    public boolean updateTradeSimple(TradeSimpleVO vo); 
    
    /**
     * 添加交易概况
     * @param vo
     * @return
     */
    public boolean saveTradeSimple(TradeSimpleVO vo);
    
    /**
     * 获取交易记录
     * @param page
     * @param keyword
     * @param startTime
     * @param endTime
     * @return
     */
    public List<TradeVO> getTradePage(PageInfo page, String keyword, String startTime, String endTime);
    
    /**
     * 获取简易交易记录
     * @param page
     * @return
     */
    public List<TradeSimpleVO> getSimpleTradePage(PageInfo page);
    
    /**
     * 矿石
     * 修改当天简易交易记录的交易数量，如果价格大于最大价格，还要重新设置最大价格
     * 每次交易成功后都会调用该方法
     * @param date   2018-05-23
     * @param price    成交价格
     * @return
     */
    public boolean updateTradeSimpleCount(String date, double count, double price);
    
    /**
     * 财神币
     * 修改当天简易交易记录的交易数量，如果价格大于最大价格，还要重新设置最大价格
     * 每次交易成功后都会调用该方法
     * @param date
     * @param csbCount
     * @param csbPrice
     * @return
     */
    public boolean updateTradeSimpleCsbCount(String date, double csbCount, double csbPrice);
    
    /**
     * 修改当天的矿石价格(根据涨幅修改)
     * @param date   2018-05-23
     * @param avg      当前价格
     * @return
     */
    public boolean updateTradeSimpleAvg(String date, double avg);
    
    /**
     * 修改当天的财神币价格(根据涨幅修改)
     * 
     * @param date
     * @param csbAvg
     * @return
     */
    public boolean updateTradeSimpleCsbAvg(String date, double csbAvg);
    
    /**
     * 删除交易记录
     * 
     * @param ids
     * @return
     */
    public boolean deleteTrade(String[] ids);
}
