package com.vrc.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vrc.bo.sys.UserBO;
import com.vrc.common.SysConfig;

import net.sf.json.JSONObject;
import qiniu.happydns.util.Hex;

/**
 * 基础控制类
 * @author liboxing
 *
 */
@RequestMapping("common")
@Controller
public class CommonController {
	
	private Logger logger = Logger.getLogger(getClass());

	@Resource
	private UserBO userBO;
	
//	@RequestMapping("sendCode")
//	@ResponseBody
//	public JSONObject sendCode(HttpSession session, String phone){
//		JSONObject jo = new JSONObject();
//		SendSmsResponse response = null;
//		
//		//随机生成验证码
//		String verifCode =(int) ((Math.random()*9+1)*100000)+"";
//		System.out.println("verifCode : "+verifCode);
//		System.out.println(templateCode);
//		try {
//			response = SendCode.sendSms(phone, verifCode, templateCode);
//		} catch (ClientException e) {
//			e.printStackTrace();
//		}
//		if(response != null){
//			if("OK".equals(response.getCode())){
//				jo.put("code", Const.success);
//				jo.put("codeDesc", "发送成功");
////				jo.put("verifCode", verifCode);
//				
//				//存到session中。以号码作为key值，否则若用户换了号码访问就可以修改别人的密码。
//				session.setAttribute(phone, verifCode);
//				session.setAttribute(phone+"Time", new Date());  //存着时间，记录有效时间
//			} else {
//				jo.put("code", Const.failed);
//				jo.put("codeDesc", "发送失败");
//			}
//		} else {
//			jo.put("code", Const.failed);
//			jo.put("codeDesc", "发送失败");
//		}
//	    return jo;
//	}
	private static final String WSSE_HEADER_FORMAT = "UsernameToken Username=\"%s\",PasswordDigest=\"%s\",Nonce=\"%s\",Created=\"%s\"";
	
	private static final String AUTH_HEADER_VALUE = "WSSE realm=\"SDP\",profile=\"UsernameToken\",type=\"Appkey\"";
	
	@RequestMapping("sendCode")
	@ResponseBody
	public JSONObject sendCode(HttpSession session, String phone){
		JSONObject jo = new JSONObject();
		
		//随机生成验证码
		String verifCode =(int) ((Math.random()*9+1)*100000)+"";
	    
		String url = SysConfig.messUrl;
		String appKey = SysConfig.messAppKey;
		String appSecret = SysConfig.messAppSecret;
		String sender = SysConfig.messSender;
		String statusCallBack = SysConfig.messStatusCallBack;
		String templateId = SysConfig.messTemplateId;
		// 传入的参数必须是["你的验证码"];
		String templateParas = "[\"" + verifCode + "\"]";
		
		String body = buildRequestBody(sender, phone, templateId, templateParas, statusCallBack);
		String wsseHeader = buildWsseHeader(appKey, appSecret);
		try {
			CloseableHttpClient client = HttpClients.custom()
                .setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {
                    @Override
                    public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                        return true;
                    }
                }).build()).setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).build();
        
			HttpResponse response = client.execute(RequestBuilder.create("POST")
			        .setUri(url)
			        .addHeader(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded")
			        .addHeader(HttpHeaders.AUTHORIZATION, AUTH_HEADER_VALUE)
			        .addHeader("X-WSSE", wsseHeader)
			        .setEntity(new StringEntity(body)).build());
			if(response != null) {
				String res = EntityUtils.toString(response.getEntity());
				JSONObject resJo = JSONObject.fromObject(res);
				if(resJo.get("code").equals("000000")) {
					jo.put("code", 200);
					jo.put("codeDesc", "操作成功");
					
					//存到session中。以号码作为key值，否则若用户换了号码访问就可以修改别人的密码。
					session.setAttribute(phone, verifCode);
					session.setAttribute(phone+"Time", new Date());  //存着时间，记录有效时间
				} else {
					jo.put("code", 201);
					jo.put("codeDesc", "系统错误");
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			jo.put("code", 201);
			jo.put("codeDesc", "系统错误");
		}

		return jo;
		
	}
	
	private String buildRequestBody(String sender, String receiver, String templateId, String templateParas,
    		String statusCallbackUrl) {
    	List<NameValuePair> keyValues = new ArrayList<>();
    	keyValues.add(new BasicNameValuePair("from", sender));
    	keyValues.add(new BasicNameValuePair("to", receiver));
    	keyValues.add(new BasicNameValuePair("templateId", templateId));
    	keyValues.add(new BasicNameValuePair("templateParas", templateParas));
    	keyValues.add(new BasicNameValuePair("statusCallback", statusCallbackUrl));
    	return URLEncodedUtils.format(keyValues, StandardCharsets.UTF_8);
    }
	
	private String buildWsseHeader(String appKey, String appSecret) {
    	SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss'Z'");
    	String time = sdf.format(new Date());
    	String nonce = UUID.randomUUID().toString().replace("-", "");
    	
    	byte[] passwordDigest = DigestUtils.sha256(nonce + time + appSecret);
    	String hexDigest = Hex.encodeHexString(passwordDigest);
    	String passwordDigestBase64Str = Base64.encodeBase64String(hexDigest.getBytes(Charset.forName("utf-8")));
    	return String.format(WSSE_HEADER_FORMAT, appKey, passwordDigestBase64Str, nonce, time);
    }
	
	/**
	 * 银行卡四元素验证
	 * @param name
	 * @param cardNo
	 * @param idNo
	 * @param phoneNo
	 * @return
	 */
	@RequestMapping("bankAuthenticate")
	@ResponseBody
	public JSONObject bankAuthenticate(String userId, String name, String bankcard, String bankName,String idcard,String phone){
		JSONObject jo = userBO.authenUser(userId, name, bankcard, bankName, idcard, phone);
		return jo;
	}
	
	/**
	 * 下载app
	 * @param type   类型（android，ios）
	 * @param userId  用户id
	 */
	@RequestMapping(value="downloadAndroid",produces="application/vnd.android.package-archive")
	public ResponseEntity<byte[]> downloadAndroid(HttpServletResponse res, String type, String userId) {
		File path = null;

		String fileName = "";
		if ("ios".equals(type)) {
			fileName = SysConfig.iosName;
		} else {
			fileName = SysConfig.androidName;
		}

		// res.setHeader("content-type",`
		// "application/vnd.android.package-archive");
		// res.setHeader("Content-Disposition", "attachment;filename=" +
		// fileName);
		InputStream in = null;
		ResponseEntity<byte[]> response = null;
		try {
			path = new File(ResourceUtils.getURL("classpath:").getPath());
			String filePath = path + File.separator + fileName;

			in = new FileInputStream(filePath);
			byte[] b = new byte[in.available()];
			in.read(b);
			HttpHeaders headers = new HttpHeaders();
			// MediaType mt = new
			// MediaType("application/vnd.android.package-archive");
			// headers.setContentType(mt);
			// 以下的类型必须要设置，要不不能在android机上正常下载的
			headers.add("Content-Disposition", "attachment;filename=" + fileName);

			// resp.setContentType("application/vnd.android.package-archive");
			HttpStatus statusCode = HttpStatus.OK;
			response = new ResponseEntity<byte[]>(b, headers, statusCode);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return response;
	}
	
	/**
	 * 下载ios
	 * @param userId  用户id
	 */
	@RequestMapping(value="downloadIos",produces="application/vnd.iphone")
	public ResponseEntity<byte[]> downloadIos(HttpServletResponse res, String userId) {
		File path = null;

		String fileName = SysConfig.iosName;

		// res.setHeader("content-type",
		// "application/vnd.android.package-archive");
		// res.setHeader("Content-Disposition", "attachment;filename=" +
		// fileName);
		InputStream in = null;
		ResponseEntity<byte[]> response = null;
		try {
			path = new File(ResourceUtils.getURL("classpath:").getPath());
			String filePath = path + File.separator + fileName;
			logger.info(filePath);
			in = new FileInputStream(filePath);
			byte[] b = new byte[in.available()];
			in.read(b);
			HttpHeaders headers = new HttpHeaders();
			// MediaType mt = new
			// MediaType("application/vnd.android.package-archive");
			// headers.setContentType(mt);
			// 以下的类型必须要设置，要不不能在android机上正常下载的
			headers.add("Content-Disposition", "attachment;filename=" + fileName);

			// resp.setContentType("application/vnd.iphone");
			HttpStatus statusCode = HttpStatus.OK;
			response = new ResponseEntity<byte[]>(b, headers, statusCode);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return response;
	}
	
	/**
	 * 下载app
	 * @param type   类型（android，ios）
	 * @param userId  用户id
	 */
	@RequestMapping(value="download",produces="application/vnd.android.package-archive")
	public void download(HttpServletResponse res, String type, String userId) {
		File path = null;
		
		String fileName = "";
		if("ios".equals(type)){
			fileName = SysConfig.iosName;
		} else {
			fileName = SysConfig.androidName;
		}
		
		res.setHeader("content-type", "application/vnd.android.package-archive");
		res.setContentType("application/vnd.android.package-archive");
		res.setHeader("Content-Disposition", "attachment;filename=" + fileName);
		byte[] buff = new byte[1024];
		BufferedInputStream bis = null;
		OutputStream os = null;
		try {
			path = new File(ResourceUtils.getURL("classpath:").getPath());
			os = res.getOutputStream();
			String filePath = path + File.separator + fileName;
			bis = new BufferedInputStream(new FileInputStream(new File(filePath)));
			int i = bis.read(buff);
			while (i != -1) {
				os.write(buff, 0, buff.length);
				os.flush();
				i = bis.read(buff);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (bis != null) {
				try {
					bis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("success");
	}
	
	//处理ur
	private static String expandURL(String url, Set<?> keys) {
        StringBuilder sb = new StringBuilder(url);
        sb.append("?");

        for (Object key : keys) {
            sb.append(key).append("=").append("{").append(key).append("}").append("&");
        }
        return sb.deleteCharAt(sb.length() - 1).toString();
    }
}
