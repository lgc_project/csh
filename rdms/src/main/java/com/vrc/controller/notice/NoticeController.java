package com.vrc.controller.notice;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vrc.bo.notice.NoticeBO;
import com.vrc.common.Const;
import com.vrc.common.ResponseObject;
import com.vrc.vo.common.PageInfo;
import com.vrc.vo.notice.CustomServiceVO;
import com.vrc.vo.notice.NoticeVO;

import net.sf.json.JSONObject;

/**
 * 公告controller
 * @author liboxing
 *
 */
@Controller
@RequestMapping("notice")
public class NoticeController {

	@Resource
	private NoticeBO noticeBO;
	
	@RequestMapping("save")
	@ResponseBody
	public ResponseObject save(NoticeVO notice){
		boolean flag = noticeBO.saveNotice(notice);
		if(flag){
			return ResponseObject.editObject(Const.success, "操作成功", null);
		} else {
			return ResponseObject.editObject(Const.failed, "操作失败", null);
		}
	}
	
	@RequestMapping("update")
	@ResponseBody
	public ResponseObject update(NoticeVO notice){
		boolean flag = noticeBO.updateNotice(notice);
		if(flag){
			return ResponseObject.editObject(Const.success, "操作成功", null);
		} else {
			return ResponseObject.editObject(Const.failed, "操作失败", null);
		}
	}
	
	/**
	 * 分页获取消息列表
	 * @param page
	 * @return
	 */
	@RequestMapping("getNotice")
	@ResponseBody
	public ResponseObject getNotice(PageInfo page,String keyword){
		PageInfo p = noticeBO.getNoticePage(page, keyword);
		return ResponseObject.editObject(Const.success, "操作成功", p);
	}
	
	@RequestMapping("getById")
	@ResponseBody
	public NoticeVO getById(String id){
		NoticeVO vo = noticeBO.getById(id);
		return vo;
	}
	
	/**
	 * 批量删除用户
	 * @param ids
	 * @return
	 */
	@RequestMapping("deleteNotice")
	@ResponseBody
	public ResponseObject deleteNotice(String ids){
		boolean flag = noticeBO.deleteBatch(ids);
		if(flag){
			return ResponseObject.editObject(Const.success, "操作成功", null);
		} else {
			return ResponseObject.editObject(Const.failed, "操作失败", null);
		}
	}
	
	@RequestMapping("saveCustomService")
	@ResponseBody
	public ResponseObject saveCustomService(CustomServiceVO vo){
		boolean flag = noticeBO.saveCustomService(vo);
		if(flag){
			return ResponseObject.editObject(Const.success, "操作成功", null);
		} else {
			return ResponseObject.editObject(Const.failed, "操作失败", null);
		}
	}
	
	@RequestMapping("updateCustomService")
	@ResponseBody
	public ResponseObject updateCustomService(CustomServiceVO vo){
		boolean flag = noticeBO.updateCustomService(vo);
		if(flag){
			return ResponseObject.editObject(Const.success, "操作成功", null);
		} else {
			return ResponseObject.editObject(Const.failed, "操作失败", null);
		}
	}
	
	@RequestMapping("getCustomService")
	@ResponseBody
	public ResponseObject getCustomService(){
		CustomServiceVO vo = noticeBO.getCustomService();
		return ResponseObject.editObject(Const.success, "操作成功", vo);
	}
}
