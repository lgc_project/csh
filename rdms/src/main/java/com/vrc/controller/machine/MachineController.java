package com.vrc.controller.machine;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rdms.common.annotation.SysLog;
import com.vrc.bo.machine.MachineBO;
import com.vrc.common.Const;
import com.vrc.common.ResponseObject;
import com.vrc.util.StringUtils;
import com.vrc.vo.common.PageInfo;
import com.vrc.vo.machine.MachineTypeVO;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("machine")
public class MachineController {
    
    @Resource
    private MachineBO machineBO;

    /**
     * 获取所有矿机类型
     * @return
     */
    @RequestMapping("machineType")
    @ResponseBody
    public ResponseObject getMachineType(){
        JSONArray ja = machineBO.getAllMachineType();
        return ResponseObject.editObject(Const.success, "操作成功", ja);
    }
    
    /**
     * 获取所有矿机类型（由于machineType方法需要带有token，所以再写一个让后台调用）
     * @return
     */
    @RequestMapping("getAllMachineType")
    @ResponseBody
    public ResponseObject getAllMachineType(){
        JSONArray ja = machineBO.getAllMachineType();
        return ResponseObject.editObject(Const.success, "操作成功", ja);
    }
    
    /**
     * 获取用户的矿机列表
     * @param userId
     * @return
     */
    @RequestMapping("machineList")
    @ResponseBody
    public ResponseObject getMachine(String userId){
        JSONObject jo = machineBO.getMachine(userId);
        return ResponseObject.editObject(Const.success, "操作成功", jo);
    }
    
    /**
     * 购买矿机
     * @return
     */
    @RequestMapping("buyMachine")
    @ResponseBody
    public ResponseObject buyMachine(String userId, String machineTypeCode){
        JSONObject jo = machineBO.buyMachine(userId, machineTypeCode);
        return ResponseObject.editObject(jo.getString("code"), jo.getString("codeDesc"), null);
    }
    
    /**
     * 后台手动添加矿机
     * @param userId
     * @param type
     * @return
     */
    @RequestMapping("addMachine")
    @ResponseBody
    public ResponseObject addMachine(String userId, String type){
    	JSONObject jo = machineBO.addMachine(userId, type);
        return ResponseObject.editObject(jo.getString("code"), jo.getString("codeDesc"), null);
    }
    
    /**
     * 分页获取机器类型
     * @param page
     * @param keyword
     * @return
     */
    @RequestMapping("getMachineTypePage")
    @ResponseBody
    public PageInfo getMachineTypePage(PageInfo page,String keyword){
    	PageInfo p = machineBO.getMachineTypePage(page, keyword);
    	return p;
    }
    
    /**
     * 添加矿机类型
     * @return
     */
    @RequestMapping("saveMachineType")
    @ResponseBody
    @SysLog("添加矿机类型")
    public ResponseObject saveMachineType(MachineTypeVO vo){
    	boolean flag = machineBO.saveMachineType(vo);
    	if(flag){
			return ResponseObject.editObject(Const.success, "操作成功", null);
		} else {
			return ResponseObject.editObject(Const.failed, "操作失败", null);
		}
    }
    
    /**
     * 修改矿机类型
     * @return
     */
    @RequestMapping("updateMachineType")
    @ResponseBody
    @SysLog("编辑矿机类型")
    public ResponseObject updateMachineType(MachineTypeVO vo){
    	boolean flag = machineBO.updateMachineType(vo);
    	if(flag){
			return ResponseObject.editObject(Const.success, "操作成功", null);
		} else {
			return ResponseObject.editObject(Const.failed, "操作失败", null);
		}
    }
    
    /**
     * 删除矿机（可同时删除多个）
     * @param id
     * @return
     */
    @RequestMapping("deleteMachineType")
    @ResponseBody
    @SysLog("删除矿机类型")
    public ResponseObject deleteMachineType(String codes){
    	if(StringUtils.isEmpty(codes)){
    		return ResponseObject.editObject(Const.failed, "操作失败", null);
    	} else {
    		String[] idList = codes.split(",");
    		for(String str : idList){
    			if("A0".equals(str) || "A1".equals(str) || "A2".equals(str)
    					|| "A3".equals(str) || "A4".equals(str)){
    				return ResponseObject.editObject(Const.failed, "初始矿机，无法删除", null);
    			}
    		}
    	}
    	
    	boolean flag = machineBO.deleteMachType(codes);
    	if(flag){
			return ResponseObject.editObject(Const.success, "操作成功", null);
		} else {
			return ResponseObject.editObject(Const.failed, "操作失败", null);
		}
    }
    
    /**
     * 分页获取机器类型
     * @param page
     * @param keyword
     * @return
     */
    @RequestMapping("getMachinePage")
    @ResponseBody
    public PageInfo getMachinePage(PageInfo page,String keyword){
    	PageInfo p = machineBO.getMachinePage(page, keyword);
    	return p;
    }
    
    /**
     * 删除矿机（可同时删除多个）
     * @param id
     * @return
     */
    @RequestMapping("deleteMachine")
    @ResponseBody
    @SysLog("删除用户矿机")
    public ResponseObject deleteMachine(String codes){
    	boolean flag = machineBO.deleteMachine(codes);
    	if(flag){
			return ResponseObject.editObject(Const.success, "操作成功", null);
		} else {
			return ResponseObject.editObject(Const.failed, "操作失败", null);
		}
    }
    
    /**
     * 购买矿机记录
     * @return
     */
    @RequestMapping("getBuyrecord")
    @ResponseBody
    public PageInfo getBuyrecord(PageInfo page, String keyword, String startTime, String endTime){
    	PageInfo p = machineBO.getBuyRecordVO(page, keyword, startTime, endTime);
    	return p;
    }
    
    /**
     * 维修矿机
     * 
     * @param userId
     * @param machineNo
     * @return
     */
    @RequestMapping("maintenanceMachine")
    @ResponseBody
    public ResponseObject maintenanceMachine(String userId, String machineNo) {
    	JSONObject jo = machineBO.maintenanceMachine(userId, machineNo);
    	return ResponseObject.editObject(jo.getString("code"), jo.getString("codeDesc"), null);
    }
    
    /**
     * 
     * 
     * @param page
     * @param keyword
     * @return
     */
    @RequestMapping("getMachineCl")
    @ResponseBody
    public PageInfo getMachine(PageInfo page, String keyword) {
    	PageInfo p = machineBO.getMachine(page, keyword);
    	return p;
    }
}
