package com.vrc.controller.trade;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vrc.bo.trade.TradeBO;
import com.vrc.common.Const;
import com.vrc.common.ResponseObject;
import com.vrc.vo.common.PageInfo;
import com.vrc.vo.trade.TradeSimpleVO;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 交易相关controller
 * @author liboxing
 *
 */
@Controller
@RequestMapping("trade")
@Transactional
public class TradeController {

    @Resource
    private TradeBO tradeBO;
    
    /**
     * 挂单操作
     * @param userId
     * @param price
     * @param count
     * @return
     */
    @RequestMapping("postOdd")
    @ResponseBody
    public ResponseObject postOdd(String userId, double price, double count, String level){
        boolean flag = tradeBO.postOdd(userId, price, count, level);
        if(flag){
            return ResponseObject.editObject(Const.success, "挂单成功", null);
        } else {
            return ResponseObject.editObject(Const.failed, "挂单失败", null);
        }
    }
    
    /**
     * 卖家点击买单，进行交易
     * @param oddId
     * @param userId
     * @return
     */
    @RequestMapping("sell")
    @ResponseBody
    public ResponseObject sell(String oddId, String userId){
        JSONObject jo = tradeBO.sell(oddId, userId);
        return ResponseObject.editObject(jo.getString("code"), jo.getString("codeDesc"), null);
    }
    
    /**
     * 点对点交易
     * @param buyId
     * @param sellId
     * @param count
     * @param price
     * @return
     */
    @RequestMapping("pointSell")
    @ResponseBody
    public ResponseObject pointSell(String buyId, String sellId, double count, double price){
        JSONObject jo = tradeBO.pointSell(buyId, sellId, count, price);
        return ResponseObject.editObject(jo.getString("code"), jo.getString("codeDesc"), null);
    }
    
    /**
     * 查看挂单列表
     * @param userId
     * @return
     */
    @RequestMapping("oddList")
    @ResponseBody
    public ResponseObject oddList(String userId, String level) {
        JSONArray ja = tradeBO.getGuadanList(userId,level);
        return ResponseObject.editObject(Const.success, "操作成功", ja);
    }
    
    /**
     * 查看信箱
     * @param userId
     * @return
     */
    @RequestMapping("mailBox")
    @ResponseBody
    public ResponseObject mailBox(String userId, String level){
        JSONArray ja = tradeBO.mailBox(userId, level);
        return ResponseObject.editObject(Const.success, "操作成功", ja);
    }
    
    /**
     * 取消挂单
     * @param oddId
     * @return
     */
    @RequestMapping("cancelOdd")
    @ResponseBody
    public JSONObject cancelOdd(String oddId){
        JSONObject jo = tradeBO.cancelOdd(oddId);
        return jo;
    }
    
    /**
     * 确认交易
     * @param userId
     * @param oddId
     * @return
     */
    @RequestMapping("sureTrade")
    @ResponseBody
    public JSONObject sureTrade(String userId, String oddId){
        JSONObject jo = tradeBO.sureTrade(userId, oddId);
        return jo;
    }
    
    /**
     * 获取昨日交易的平均价，最高价，交易数量
     * @return
     */
    @RequestMapping("getTradeSimple")
    @ResponseBody
    public ResponseObject getTradeSimple(){
        JSONObject jo = tradeBO.getTradeSimple();
        return ResponseObject.editObject(Const.success, "操作成功", jo);
    }
    
    /**
     * 获取矿市页面的折线图数据
     * @return
     */
    @RequestMapping("getChartData")
    @ResponseBody
    public ResponseObject getChartData(){
        JSONObject jo = tradeBO.getChartData();
        return ResponseObject.editObject(Const.success, "操作成功", jo);
    }
    
    /**
     * 获取新手挂单，高手挂单的描述和数量范围
     * @return
     */
    @RequestMapping("postBillMsg")
    @ResponseBody
    public ResponseObject postBillMsg(String level){
        JSONObject jo = tradeBO.getPostBillMsg(level);
        return ResponseObject.editObject(Const.success, "操作成功", jo);
    }
    
    /**
     * 手续费提示信息
     * @return
     */
    @ResponseBody
    @RequestMapping("poundageMsg")
    public ResponseObject poundageMsg(){
    	JSONObject jo = tradeBO.getPoundageMsg();
        return ResponseObject.editObject(Const.success, "操作成功", jo);
    }
    
    /**
     * 获取账单列表
     * @param userId
     * @param type
     * @param page
     * @param billType
     * @return
     */
    @RequestMapping("getBillList")
    @ResponseBody
    public ResponseObject getBillList(String userId, String type, PageInfo page, String billType){
        JSONObject jo = tradeBO.getBillList(userId, type, page, billType);
        return ResponseObject.editObject(Const.success, "操作成功", jo);
    }
    
    /**
     * 添加简要交易记录
     * @param vo
     * @return
     */
    @RequestMapping("saveTradeSimple")
    @ResponseBody
    public ResponseObject saveTradeSimple(TradeSimpleVO vo){
    	boolean flag = tradeBO.saveTradeSimple(vo);
    	if(flag){
 			return ResponseObject.editObject(Const.success, "操作成功", null);
 		} else {
 			return ResponseObject.editObject(Const.failed, "操作失败", null);
 		}
    }
    
    /**
     * 修改简要交易记录
     * @param vo
     * @return
     */
    @RequestMapping("updateTradeSimple")
    @ResponseBody
    public ResponseObject updateTradeSimple(TradeSimpleVO vo){
    	boolean flag = tradeBO.updateTradeSimple(vo);
    	if(flag){
 			return ResponseObject.editObject(Const.success, "操作成功", null);
 		} else {
 			return ResponseObject.editObject(Const.failed, "操作失败", null);
 		}
    }
    
    /**
     * 分页查询交易记录
     * @param page
     * @param keyword
     * @return
     */
    @RequestMapping("getTradePage")
    @ResponseBody
    public PageInfo getTradePage(PageInfo page,String keyword, String startTime, String endTime){
    	PageInfo p = tradeBO.getTradePage(page, keyword, startTime, endTime);
    	return p;
    }
    
    @RequestMapping("deleteTrade")
    @ResponseBody
    public ResponseObject deleteTrade(String ids) {
    	if(tradeBO.deleteTrade(ids)) {
    		return ResponseObject.editObject(Const.success, "操作成功", null);
    	}
    	return ResponseObject.editObject(Const.failed, "系统错误", null);
    }
}
