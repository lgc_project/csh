package com.vrc.controller.trade;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.vrc.bo.trade.CommodityBO;
import com.vrc.common.Const;
import com.vrc.common.ResponseObject;
import com.vrc.vo.common.PageInfo;
import com.vrc.vo.trade.CommodityVO;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 
 * @ClassName: CommodityController
 * @Description:商品信息controller类
 * @author: MDS
 * @date: 2018年5月29日 上午10:06:34
 */
@Controller
@RequestMapping("commodity")
public class CommodityController {
	@Resource
	private CommodityBO commodityBO;
	@Autowired
	private HttpServletRequest request;

	@RequestMapping("getCommodityPage")
	@ResponseBody
	public PageInfo getCommodityPage(PageInfo page, String keyword) {
		PageInfo p = commodityBO.getCommodityPage(page, keyword);
		return p;
	}

	@RequestMapping("saveCommodity")
	@ResponseBody
	public ResponseObject saveCommodity(CommodityVO vo) {
		boolean flag = commodityBO.saveCommodity(vo);
		if (flag) {
			return ResponseObject.editObject(Const.success, "操作成功", null);
		}
		return ResponseObject.editObject(Const.failed, "系统错误", null);
	}

	@RequestMapping("updateCommodity")
	@ResponseBody
	public ResponseObject updateCommodity(CommodityVO vo) {
		boolean flag = commodityBO.updateCommodity(vo);
		if (flag) {
			return ResponseObject.editObject(Const.success, "操作成功", null);
		}
		return ResponseObject.editObject(Const.failed, "系统错误", null);
	}

	@RequestMapping("deleteCommodity")
	@ResponseBody
	public ResponseObject deleteCommodity(String ids) {
		boolean flag = commodityBO.deleteCommodity(ids);
		if (flag) {
			return ResponseObject.editObject(Const.success, "操作成功", null);
		}
		return ResponseObject.editObject(Const.failed, "系统错误", null);
	}

	/**
	 * 
	 * @Title: getCommodityAll
	 * @Description:查询商品信息
	 * @author: MDS
	 * @return
	 */
	@RequestMapping("getCommodityAll")
	@ResponseBody
	public ResponseObject getCommodityAll() {
		JSONArray ja = commodityBO.getCommodityAll();
		return ResponseObject.editObject(Const.success, "操作成功", ja);
	}

	/**
	 * 抽奖接口
	 * 
	 * @param userId
	 * @param price 奖品数量
	 * @param bool 是否中奖
	 * @param priceType 奖品类型（0：矿石 1：百源币 2：财神币）
	 * @param pay 支付数目
	 * @param payType 支付类型（0：矿石 1：百源币 2：财神币）
	 * @return
	 */
	@RequestMapping("updateMoneySum")
	@ResponseBody
	public ResponseObject updateUserMoneySum(String userId, double price, boolean bool, Integer priceType, double pay, Integer payType) {
		JSONObject jo = commodityBO.updateUserMoneySum(userId, price, bool, priceType, pay, payType);
		return ResponseObject.editObject(jo.getString("code"), jo.getString("codeDesc"), null);
	}

	@RequestMapping("uploadImg")
	@ResponseBody
	public ResponseObject uploadImg(@RequestParam("file") MultipartFile file, String commodityId) {
		if (!file.isEmpty()) {
			try {
				StringBuffer rp = request.getRequestURL();
				String fileName = UUID.randomUUID().toString()
						+ file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
				boolean flag = commodityBO.updateCommodityImg(commodityId,
						rp.toString().replace("commodity/uploadImg", "") + "images/" + fileName);
				if (flag) {
					// 获取项目根目录
					File rootPath = new File(ResourceUtils.getURL("classpath:").getPath());
					File imagesPath = new File(rootPath.getAbsolutePath(), "static/images");
					Path path = Paths.get(imagesPath.getAbsolutePath(), fileName);
					Files.copy(file.getInputStream(), path);
					return ResponseObject.editObject(Const.success, "上传成功", null);
				}

			} catch (IOException e) {
				e.printStackTrace();
				return ResponseObject.editObject(Const.failed, "系统错误", null);
			}

		}
		return ResponseObject.editObject(Const.failed, "系统错误", null);
	}

}
