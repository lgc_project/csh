package com.vrc.controller.trade;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vrc.bo.trade.DealBO;
import com.vrc.common.ResponseObject;

import net.sf.json.JSONObject;

/**
 * 
 * @ClassName: DealController
 * @Description:商品交易controller类
 * @author: MDS
 * @date: 2018年5月29日 下午12:46:57
 */
@Controller
@RequestMapping("deal")
public class DealController {
	@Resource
	private DealBO dealBO;
	/**
	 * 
	 * @Title: saveDealInfo
	 * @Description:购买商品
	 * @author: MDS
	 * @param userId 用户编号
	 * @param commodityId 商品编号
	 * @return
	 */
	@RequestMapping("saveDealInfo")
	@ResponseBody
	public ResponseObject saveDealInfo(Long userId, Long commodityId, String receiver, String address, String phone) {
		JSONObject jo = dealBO.saveDealInfo(userId, commodityId, receiver, address, phone);
		return ResponseObject.editObject(jo.getString("code"), jo.getString("codeDesc"), null);
	}
}
