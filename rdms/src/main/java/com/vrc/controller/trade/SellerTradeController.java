package com.vrc.controller.trade;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vrc.bo.trade.SellerTradeBo;
import com.vrc.common.Const;
import com.vrc.common.ResponseObject;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
* @ClassName 卖家挂单
* @Description
* @author 
* @date 2018年8月13日 下午9:39:30
*/
@Controller
@RequestMapping("sellerTrade")
@Transactional
public class SellerTradeController {

	@Resource
	private SellerTradeBo sellerTradeBo;
	
	/**
	 * 卖家挂单
	 * 
	 * @param userId
	 * @param price
	 * @param count
	 * @param level
	 * @return
	 */
	@RequestMapping("postOdd")
	@ResponseBody
	public ResponseObject postOdd(String userId, double price, double count, String level) {
		JSONObject jo = sellerTradeBo.postOdd(userId, price, count, level);
        return ResponseObject.editObject(jo.getString("code"), jo.getString("codeDesc"), null);
	}
	
	/**
	 * 买家确认购买
	 * 
	 * @param oddId
	 * @param userId
	 * @return
	 */
	@RequestMapping("buy")
	@ResponseBody
	public ResponseObject buy(String oddId, String userId) {
		JSONObject jo = sellerTradeBo.buy(oddId, userId);
		return ResponseObject.editObject(jo.getString("code"), jo.getString("codeDesc"), null);
	}
	
	/**
	 * 卖家第二次确认，交易成功
	 * 
	 * @param userId
	 * @param oddId
	 * @return
	 */
	@RequestMapping("sureTrade")
	@ResponseBody
	public JSONObject sureTrade(String userId, String oddId) {
		JSONObject jo = sellerTradeBo.sureTrade(userId, oddId);
		return jo;
	}
	
	/**
	 * 撤销挂单
	 * 
	 * @param oddId
	 * @return
	 */
	@RequestMapping("cancelOdd")
	@ResponseBody
	public JSONObject cancelOdd(String oddId) {
		JSONObject jo = sellerTradeBo.cancelOdd(oddId);
		return jo;
	}
	
	/**
	 * 查看卖家挂单列表
	 * 
	 * @param userId
	 * @param level
	 * @return
	 */
	@RequestMapping("oddList")
	@ResponseBody
	public ResponseObject oddList(String userId, String level) {
		JSONArray ja = sellerTradeBo.getGuadanList(userId, level);
		return ResponseObject.editObject(Const.success, "操作成功", ja);
	}
	
	/**
	 * 查看信箱
	 * 
	 * @param userId
	 * @param level
	 * @return
	 */
	@RequestMapping("mailBox")
	@ResponseBody
	public ResponseObject mailBox(String userId, String level) {
		JSONArray ja = sellerTradeBo.mailBox(userId, level);
		return ResponseObject.editObject(Const.success, "操作成", ja);
	}
}
