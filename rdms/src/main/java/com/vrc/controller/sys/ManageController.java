package com.vrc.controller.sys;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 系统管理控制类
 * @author liboxing
 *
 */
@Controller
@RequestMapping("manage")
public class ManageController {
    
    /**
     * 打开管理页面
     * @return
     */
    @RequestMapping("openManage")
    public ModelAndView openManage(){
        ModelAndView mv = new ModelAndView("jsp/manage");
        return mv;
    }
    
    /**
     * 管理员列表
     * @return
     */
    @RequestMapping("manageView")
    public ModelAndView manageView(){
        ModelAndView mv = new ModelAndView("jsp/sys/manageview");
        return mv;
    }
}
