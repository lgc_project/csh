package com.vrc.controller.sys;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vrc.bo.sys.VrcConfigBO;
import com.vrc.common.Const;
import com.vrc.common.ResponseObject;

import net.sf.json.JSONObject;

/**
 * vrc系统参数配置
 * @author liboxing
 *
 */
@Controller
@RequestMapping("vrcConfig")
public class VrcConfigController {

	@Resource
	private VrcConfigBO vrcConfigBO;
	
	@ResponseBody
	@RequestMapping("getAllConfig")
	public ResponseObject getAllConfig(){
		JSONObject jo = vrcConfigBO.getAllConfig();
		return ResponseObject.editObject(Const.success, "操作成功", jo);
	}
	
	@ResponseBody
	@RequestMapping("saveOrUpdate")
	public ResponseObject saveOrUpdate(String params){
		JSONObject jo = JSONObject.fromObject(params);
		
		Map<String, String> map = new HashMap<>();
		Set<String> keys = jo.keySet();
		for(String str : keys){
			map.put(str, jo.getString(str));
		}
		vrcConfigBO.saveOrUpdate(map);
		return ResponseObject.editObject(Const.success, "操作成功", null);
	}
}
