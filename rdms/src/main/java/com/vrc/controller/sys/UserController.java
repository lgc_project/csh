package com.vrc.controller.sys;


import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rdms.common.annotation.SysLog;
import com.rdms.modules.sys.service.MemberTokenService;
import com.vrc.bo.sys.UserBO;
import com.vrc.common.Const;
import com.vrc.common.ResponseObject;
import com.vrc.dao.sys.UserDAO;
import com.vrc.dao.sys.VrcConfigDAO;
import com.vrc.util.DateUtils;
import com.vrc.util.StringUtils;
import com.vrc.vo.common.PageInfo;
import com.vrc.vo.sys.UserVO;

import net.sf.json.JSONObject;

/**
 * 用户处理请求类
 * @author libx
 *
 */
@RequestMapping("user")
@Controller
public class UserController {

    @Resource
    private UserBO userBO;
    
    @Resource
    private UserDAO userDAO;
    
    @Resource
	private MemberTokenService memberTokenService;
    
    @Resource
    private VrcConfigDAO vrcConfigDAO;
    
    /**
     * 用户注册请求
     * @param phone
     * @param password
     * @param reference
     * @return
     */
    @RequestMapping("register")
    @ResponseBody
    public ResponseObject register(HttpSession session,String phone,String password,String reference,String verifCode){
        //校验验证码
    	String sessVerifCode = (String)session.getAttribute(phone);
    	//没有验证码，直接退出
        if(sessVerifCode == null){
        	return ResponseObject.editObject(Const.failed, "验证码错误", null);
        } else {
        	if(sessVerifCode.equals(verifCode)){
        		Date d = (Date) session.getAttribute(phone+"Time");
        		Date now = new Date();
        		long second = now.getTime() - d.getTime();
        		//验证码超时
        		if(second > Const.verifCodeTime.longValue()){
        			//移除验证码
        	        session.removeAttribute(phone);
        	        session.removeAttribute(phone+"Time");
        			return ResponseObject.editObject(Const.failed, "验证码错误", null);
        		}
        	} else {
        		//验证码不匹配
        		return ResponseObject.editObject(Const.failed, "验证码错误", null);
        	}
        }
        //移除验证码
        session.removeAttribute(phone);
        session.removeAttribute(phone+"Time");
        
        if(StringUtils.isEmpty(reference)){
        	return ResponseObject.editObject(Const.failed, "推荐人不能为空", null);
        } else {
        	UserVO u = userDAO.getUserByReference(reference);
        	if(u == null){
        		return ResponseObject.editObject(Const.failed, "不存在该推荐人，注册失败", null);
        	}
        }
        
    	boolean flag = userBO.hasPhone(phone);
        if(flag){
            return ResponseObject.editObject(Const.failed, "手机已被注册", null);
        } else {
            UserVO userVO = new UserVO();
            userVO.setPhone(phone);
            userVO.setLoginPassword(password);
            userVO.setParentId(reference);
            flag = userBO.register(userVO);
            if(flag){
                return ResponseObject.editObject(Const.success, "注册成功", null);                
            } else {
                return ResponseObject.editObject(Const.failed, "注册失败，请联系管理员", null);
            }
        }
    }
    
    /**
     * 用户登录请求
     * @param httpSession
     * @param phone
     * @param password
     * @return
     */
    @RequestMapping("login")
    @ResponseBody
    public JSONObject login(HttpSession httpSession, String phone, String password){
        UserVO u = null;
        try {
            u = userBO.getUser(phone, password);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        
        JSONObject retObj = new JSONObject();
        if(u != null){
        	//判断交易时间
        	String timeStart = vrcConfigDAO.getConfigByKey("tradetimestart");
        	String timeEnd = vrcConfigDAO.getConfigByKey("tradetimeend");
        	boolean f = isTradeTime(timeStart, timeEnd);
        	if(!f){
        		retObj.put("code", Const.failed);
            	retObj.put("codeDesc", "当前不在交易时间内");
            	return retObj;
        	}
        	
        	//判断是否冻结
        	if("1".equals(u.getIsvalid())){
        		String userId = u.getId();
            	Long id = Long.valueOf(userId);
            	String token = memberTokenService.createToken(id);  //创建token
            	
            	String tradePass = u.getTradePassword();
            	if(StringUtils.isEmpty(tradePass)){
            		u.setHasTradePsd(false);
            	} else {
            		u.setHasTradePsd(true);
            	}
            	retObj.put("code", Const.success);
            	retObj.put("codeDesc", "登录成功");
//            	retObj.put("token", token);
            	u.setToken(token);
            	retObj.put("data", u);
            	return retObj;
        	} else {
        		retObj.put("code", Const.failed);
             	retObj.put("codeDesc", "用户已被冻结，请联系管理员");
             	return retObj;
        	}
        } else {
            retObj.put("code", Const.failed);
        	retObj.put("codeDesc", "用户名或密码错误，登录失败");
        	return retObj;
        }
    }
    
    /**
     * 设置交易密码
     * @param userId
     * @param password
     * @return
     */
    @RequestMapping("setTradePassword")
    @ResponseBody
    public ResponseObject setTradePassword(String userId, String password){
        boolean flag = userBO.updateTradePass(userId, password);
        if(flag){
            return ResponseObject.editObject(Const.success, "设置成功", null);
        } else {
            return ResponseObject.editObject(Const.failed, "设置失败", null);
        }
    } 
    
    /**
     * 校验交易密码
     * @param userId
     * @param password
     * @return
     */
    @RequestMapping("checkTradePassword")
    @ResponseBody
    public ResponseObject checkTradePassword(String userId, String password){
        boolean flag = userBO.checkTradePass(userId, password);
        if(flag){
            return ResponseObject.editObject(Const.success, "校验通过", null);
        } else {
            return ResponseObject.editObject(Const.failed, "密码错误", null);
        }
    }
    
    /**
     * 获取用户信息
     * @param userId
     * @return
     */
    @RequestMapping("getUserInfo")
    @ResponseBody
    public ResponseObject getUserInfo(String userId){
        JSONObject jo = userBO.getUserInfo(userId);
        if(jo == null){
            return ResponseObject.editObject(Const.failed, "查询失败", null);
        } else {
            return ResponseObject.editObject(Const.success, "查询成功", jo);
        }
    }
    
    /**
     * 修改交易密码
     * @param userId
     * @param oldPass
     * @param newPass
     */
    @RequestMapping("changeTradePass")
    @ResponseBody
    public JSONObject changeTradePass(String userId, String oldPass, String newPass){
        JSONObject jo = userBO.changeTradePass(userId, oldPass, newPass);
        return jo;
    }
    
    /**
     * 修改登录密码
     * @param userId
     * @param oldPass
     * @param newPass
     * @return
     */
    @RequestMapping("changeLoginPass")
    @ResponseBody
    public JSONObject changeLoginPass(String userId, String oldPass, String newPass){
        JSONObject jo = userBO.changeLoginPass(userId, oldPass, newPass);
        return jo;
    }
    
    @RequestMapping("resetLoginPass")
    @ResponseBody
    public JSONObject resetLoginPass(HttpSession session,String verifCode, String phone, String password){
    	JSONObject jo = new JSONObject();
    	//校验验证码
    	String sessVerifCode = (String)session.getAttribute(phone);
    	//没有验证码，直接退出
        if(sessVerifCode == null){
        	jo.put("code", Const.failed);
        	jo.put("codeDesc", "验证码错误");
        	return jo;
        } else {
        	if(sessVerifCode.equals(verifCode)){
        		Date d = (Date) session.getAttribute(phone+"Time");
        		Date now = new Date();
        		long second = now.getTime() - d.getTime();
        		//验证码超时
        		if(second > Const.verifCodeTime.longValue()){
        			//移除验证码
        	        session.removeAttribute(phone);
        	        session.removeAttribute(phone+"Time");
        			jo.put("code", Const.failed);
                	jo.put("codeDesc", "验证码错误");
                	return jo;
        		}
        	} else {
        		//验证码不匹配
        		jo.put("code", Const.failed);
            	jo.put("codeDesc", "验证码错误");
            	return jo;
        	}
        }
        //移除验证码
        session.removeAttribute(phone);
        session.removeAttribute(phone+"Time");
    	
    	jo = userBO.resetLoginPass(phone, password);
    	return jo;
    }
    
    @RequestMapping("resetTradePass")
    @ResponseBody
    public JSONObject resetTradePass(HttpSession session,String verifCode, String phone, String password){
    	JSONObject jo = new JSONObject();
    	//校验验证码
    	String sessVerifCode = (String)session.getAttribute(phone);
    	//没有验证码，直接退出
        if(sessVerifCode == null){
        	jo.put("code", Const.failed);
        	jo.put("codeDesc", "验证码错误");
        	return jo;
        } else {
        	if(sessVerifCode.equals(verifCode)){
        		Date d = (Date) session.getAttribute(phone+"Time");
        		Date now = new Date();
        		long second = now.getTime() - d.getTime();
        		//验证码超时
        		if(second > Const.verifCodeTime.longValue()){
        			//移除验证码
        	        session.removeAttribute(phone);
        	        session.removeAttribute(phone+"Time");
        			jo.put("code", Const.failed);
                	jo.put("codeDesc", "验证码错误");
                	return jo;
        		}
        	} else {
        		//验证码不匹配
        		jo.put("code", Const.failed);
            	jo.put("codeDesc", "验证码错误");
            	return jo;
        	}
        }
        //移除验证码
        session.removeAttribute(phone);
        session.removeAttribute(phone+"Time");
        
    	jo = userBO.resetTradePass(phone, password);
    	return jo;
    }
    
    /**
     * 分页查询会员
     * @param page
     * @param keyword
     * @return
     */
    @RequestMapping("getUserPage")
    @ResponseBody
    public PageInfo getUserPage(PageInfo page,String keyword){
    	PageInfo p = userBO.getUserPage(page, keyword);
    	return p;
    }
    
    /**
     * 手动添加用户
     * @param vo
     * @return
     */
    @RequestMapping("saveUser")
    @ResponseBody
    @SysLog("添加会员")
    public ResponseObject saveUser(UserVO vo){
    	if(vo == null){
    		return ResponseObject.editObject(Const.failed, "操作失败", null);
    	}
    	
    	String phone = vo.getPhone();
    	boolean flag = userBO.hasPhone(phone);
        if(flag){
            return ResponseObject.editObject(Const.failed, "手机已被注册", null);
        }
        
        flag = userBO.saveUser(vo);
        if(flag){
			return ResponseObject.editObject(Const.success, "操作成功", null);
		} else {
			return ResponseObject.editObject(Const.failed, "操作失败", null);
		}
    }
    
    /**
     * 手动修改用户
     * @param vo
     * @return
     */
    @RequestMapping("updateUser")
    @ResponseBody
    @SysLog("编辑会员")
    public ResponseObject updateUser(UserVO vo){
    	boolean flag = userBO.updateUserInfo(vo);
    	if(flag){
			return ResponseObject.editObject(Const.success, "操作成功", null);
		} else {
			return ResponseObject.editObject(Const.failed, "操作失败", null);
		}
    }
    
    /**
     * 手动删除用户
     * @param ids
     * @return
     */
    @RequestMapping("deleteUser")
    @ResponseBody
    @SysLog("删除会员")
    public ResponseObject deleteUser(String ids){
    	boolean flag = userBO.deleteUser(ids);
    	if(flag){
			return ResponseObject.editObject(Const.success, "操作成功", null);
		} else {
			return ResponseObject.editObject(Const.failed, "操作失败", null);
		}
    }
    
    /**
     * 获取用户矿池
     * @param userId
     * @return
     */
    @RequestMapping("getUserPool")
    @ResponseBody
    public ResponseObject getUserPool(String userId){
    	JSONObject jo = userBO.getUserPool(userId);
    	return ResponseObject.editObject(Const.success, "操作成功", jo);
    }
    
    /**
     * 设置微信账号
     * @param userId
     * @param account
     * @return
     */
    @RequestMapping("setWeixinAccount")
    @ResponseBody
    public ResponseObject setWeixinAccount(String userId, String account){
    	boolean flag = userBO.setWeixinAccount(userId, account);
    	if(flag){
			return ResponseObject.editObject(Const.success, "操作成功", null);
		} else {
			return ResponseObject.editObject(Const.failed, "操作失败", null);
		}
    }
    
    /**
     * 设置支付宝账号
     * @param userId
     * @param account
     * @return
     */
    @RequestMapping("setAlipayAccount")
    @ResponseBody
    public ResponseObject setAlipayAccount(String userId, String account){
    	boolean flag = userBO.setAlipayAccount(userId, account);
    	if(flag){
			return ResponseObject.editObject(Const.success, "操作成功", null);
		} else {
			return ResponseObject.editObject(Const.failed, "操作失败", null);
		}
    }
    /**
     * 
     * @Title: updateMoneySum
     * @Description:更新总资产
     * @author: MDS
     * @param bool 判断用户输赢（true为用户赢，false为用户输）
     * @param money 交易金额
     * @param userId 用户编号
     * @return
     */
    @RequestMapping("updateMoneySum")
    @ResponseBody
    public ResponseObject updateMoneySum(boolean bool, double money, Long userId) {
    	if(userBO.updateMoneySum(bool, money, userId)){
    		return ResponseObject.editObject(Const.success, "操作成功", null);
		} else {
			return ResponseObject.editObject(Const.failed, "操作失败", null);
		}
    }
    
	    /**
     * 冻结/解冻
     * @param userId
     * @param isvalid
     * @return
     */
    @ResponseBody
    @RequestMapping("freeOrRefree")
    public ResponseObject freeOrRefree(String userId, String isvalid){
    	boolean flag = userBO.freOrUnfreUser(userId, isvalid);
    	if(flag){
			return ResponseObject.editObject(Const.success, "操作成功", null);
		} else {
			return ResponseObject.editObject(Const.failed, "操作失败", null);
		}
    }
	
    /**
     * 获取用户直推列表
     * 
     * @param page
     * @param parentId
     * @return
     */
    @RequestMapping("getUserChildList")
    @ResponseBody
    public PageInfo getChildUserPage(PageInfo page, String parentId) {
    	PageInfo p = userBO.getChildUserPage(page, parentId);
    	return p;
    }

	/**
     * 判断是否在交易时间内
     * @param timeStart  开始时间  09:00:00
     * @param timeEnd   结束时间  17:00:00
     */
    public static boolean isTradeTime(String timeStart,String timeEnd){
    	boolean flag = false;
    	
    	try {
			Date now = new Date();
			String year = DateUtils.dateToString(now, "yyyy-MM-dd");
			timeStart = year +" "+ timeStart;
			timeEnd = year +" "+ timeEnd;
			Date start = DateUtils.stringToDate(timeStart);
			Date end = DateUtils.stringToDate(timeEnd);
			if(now.getTime() >= start.getTime() && now.getTime() <= end.getTime()){
				flag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return flag;
    }
}
