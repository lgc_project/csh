$(function() {
	$("#jqGrid").jqGrid( {
		url:baseURL + 'ore/getOrePage',
		datatype: "json",
		colModel: [
			{label: 'ID', name: 'id', width:40 },
			{label: '用户ID', name: 'userId', width:40},
			{label: '用户名', name: 'userName', width:45},
			{label: '合成矿石数量', name: 'oreSum', width:40},
			{label: '合成财神币数量', name: 'csbSum', width:40},
			{label: '合成时间', name: 'createTime', width:45},
		],
		viewrecords: true,
		height: 385,
		rowNum: 10,
		rowList : [10, 30, 50],
		rownumbers: true, 
		rownumWidth: 25,
		autowidth: true,
		multiselect: true,
		pager: "#jqGridPager",
		footerrow: true, // 添加合计列
		jsonReader : {
			root: "data", // 数据
			page: "pageNum", // 当前页面
			total: "pageTotle", // 总页数
			records: "total", // 总数据数
		},
		prmNames : {
			page: "pageNum",
			rows: "pageSize",
			order: "order",
		},
		gridComplete:function() {
			// 隐藏grid底部滚动条
			$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
			var ids = $("#jqGrid").jqGrid('getDataIDs');
			for(var i = 0; i < ids.length; i++) {
				var rowData = $("#jqGrid").jqGrid('getRowData', ids[i]);
				vm.tableList.push(rowData);
			}
			var oreSum = $("#jqGrid").getCol('oreSum', false, 'sum');
			var csbSum = $("#jqGrid").getCol('csbSum', false, 'sum');
			$("#jqGrid").footerData('set', {'userId':'合计', 'oreSum':oreSum.toFixed(2), 'csbSum':csbSum.toFixed(2)}, false);
			
		}	
	});
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			keyword:null,
			startTime: null,
			endTime: null
		},
		showNum: "0",
		title:null,
		tableList: [{
			id:0,
			userId:'',
			userName:'',
			oreSum:0,
			csbSum:0,
			createTime:''
		}]
	},
	methods:{
		query: function() {
			var start = $('#startInput').datetimebox('getValue');
			var end = $('#endInput').datetimebox('getValue');
			vm.q.startTime = start;
			vm.q.endTime = end;
			vm.reload();
		},
		reload: function() {
			vm.showNum = "0";
			var page = $('#jqGrid').jqGrid('getGridParam', 'page');
			$('#jqGrid').jqGrid('setGridParam', {
				postData:{
					'keyword': vm.q.keyword,
                	'startTime': vm.q.startTime,
                	'endTime': vm.q.endTime
				},
				page:page
			}).trigger("reloadGrid");
		}
		
	},
	
});