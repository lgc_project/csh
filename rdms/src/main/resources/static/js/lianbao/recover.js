var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			keyword: null,
			startTime: null,
			endTime: null
		},
		showNum: "0",
		tableList: [{}]
	},
	mounted: function(){
		$(function () {
		    $("#jqGrid").jqGrid({
		        url: baseURL + 'caiShenBi/getCsbRecoverInfo',
		        datatype: "json",
		        colModel: [	
		            { label: 'id', name: 'id', width: 40, hidden: true },
		            { label: '用户ID', name: 'userId', width: 40 },
		            { label: '回收财神币数', name: 'csbSum', width: 40},
		            { label: '获得百源币数', name: 'moneySum', width: 40},
		            { label: '手续费率', name: 'poundage', width: 40},
		            { label: '回收扣除手续费', name: 'moneyPd', width: 40},
		            { label: '回收时间', name: 'createTime', width: 45},
		        ],
				viewrecords: true,
		        height: 385,
		        rowNum: 10,
				rowList : [10,30,50],
		        rownumbers: true, 
		        rownumWidth: 25, 
		        autowidth:true,
		        multiselect: true,
		        pager: "#jqGridPager",
		        footerrow: true,
		        jsonReader : {
		            root: "data",       //数据
		            page: "pageNum",     //当前页数
		            total: "pageTotle",    //总页数
		            records: "total"     //总数据条数
		        },
		        prmNames : {
		        	page:"pageNum", 
		            rows:"pageSize", 
		            order: "order",
//		            totalrows:"total"
		        },
		        gridComplete:function(){
		        	vm.tableList = [];
		        	//隐藏grid底部滚动条
		        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
		        	var ids = $("#jqGrid").jqGrid('getDataIDs');
		        	for(var i = 0; i < ids.length; i++) {
						var rowData = $("#jqGrid").jqGrid('getRowData',ids[i]);
						vm.tableList.push(rowData);
//			        	Vue.set(vm.tableList, i, rowData);
		        	}
		        	var csbSum = $("#jqGrid").getCol('csbSum', false, 'sum');
		        	var moneySum = $("#jqGrid").getCol('moneySum', false, 'sum');
		        	var moneyPdSum = $("#jqGrid").getCol('moneyPd', false, 'sum');
		        	$("#jqGrid").footerData('set', {'userId':'合计', 'csbSum':csbSum.toFixed(2), 'moneySum':moneySum.toFixed(2), 'moneyPd':moneyPdSum.toFixed(2)}, false);
					console.log(vm.tableList);
		        }
		    });
		    
		});
	},
	methods: {
		query: function(){
			console.info("query");
			console.info($("#startInput").val());
			var start = $('#startInput').datetimebox('getValue');
			var end = $('#endInput').datetimebox('getValue');
			vm.q.startTime = start;
			vm.q.endTime = end;
			vm.reload();
		},
		reload: function () {
			vm.showNum = "0";
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                postData:{
                	'keyword': vm.q.keyword,
                	'startTime': vm.q.startTime,
                	'endTime': vm.q.endTime
                },
                page:page
            }).trigger("reloadGrid");
		}
	}
});