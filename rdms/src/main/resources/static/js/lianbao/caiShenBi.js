$(function() {
	$("#jqGrid").jqGrid( {
		url:baseURL + 'caiShenBi/getCaiShenBisPage',
		datatype: "json",
		colModel: [
			{label: '财神币ID', name: 'id', width:40 },
			{label: '总数量', name: 'amount', width:40 },
			{label: '剩余量', name: 'surplus', width:40},
			{label: '单位价格', name: 'unitPrice', width:40 },
			{label: '财神币购买及交易手续费', name: 'poundage', width:40 },
			{label: '开始时间', name: 'beginTime', width:45 },
			{label: '结束时间', name: 'endTime', width:45 },
		],
		viewrecords: true,
		height: 385,
		rowNum: 10,
		rowList : [10, 30, 50],
		rownumbers: true, 
		rownumWidth: 25,
		autowidth: true,
		multiselect: true,
		pager: "#jqGridPager",
		jsonReader : {
			root: "data", // 数据
			page: "pageNum", // 当前页面
			total: "pageTotle", // 总页数
			records: "total", // 总数据数
		},
		prmNames : {
			page: "pageNum",
			rows: "pageSize",
			order: "order",
		},
		gridComplete:function() {
			// 隐藏grid底部滚动条
			$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
		}	
	});
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			keyword:null
		},
		showNum: "0",
		title:null,
		caiShenBi: {
			amount:0,
			unitPrice:0.0,
			poundage:0.0,
			beginTime:new Date(),
			endTime:new Date(),
		},
	},
	methods:{
		query: function() {
			vm.reload();
		},
		add: function() {
			vm.showNum = "1";
			vm.title = "新增";
			vm.caiShenBi = {};
		},
		update: function() {
			var id = getSelectedRow();
			if(id == null) {
				return;
			}
			
			vm.caiShenBi = {};
			var rowData = $("#jqGrid").getRowData(id); // 对象字面量
			vm.caiShenBi = rowData;
			vm.showNum = "2";
			vm.title = "修改";
		},
		del: function() {
			var idArray = getSelectedRows();
			if(idArray == null) {
				return;
			}
			var ids = idArray.join(",");
			confirm('确定删除选中的记录？', function() {
				$.ajax({
					type: "POST",
					url: baseURL + "caiShenBi/deleteCaiShenBi",
					data : {
						ids: ids
					},
					success: function(data) {
						if(data.code == "200") {
							alert('操作成功', function() {
								vm.reload();
							});
						} else {
							alert(data.codeDesc);
						}
					}
				});
			});
		},
		saveOrUpdate: function() {
			var url = vm.caiShenBi.id == null ? "caiShenBi/saveCaiShenBi" : "caiShenBi/updateCaiShenBi";
			if(vm.caiShenBi.id == null) {
				var begin = $('#beginInput').datetimebox('getValue');
				var end = $('#endInput').datetimebox('getValue');
				vm.caiShenBi.beginTime = begin;
				vm.caiShenBi.endTime = end;
			} else {
				var begin = $('#modBeginInput').datetimebox('getValue');
				var end = $('#modEndInput').datetimebox('getValue');
				vm.caiShenBi.beginTime = begin;
				vm.caiShenBi.endTime = end;
			}
			
			$.ajax({
				type: "POST",
				url: baseURL + url,
				data: vm.caiShenBi,
				success: function(data) {
					if(data.code == "1") {
						alert(data.codeDesc, function() {
							vm.reload();
						});
					} else {
						alert(data.codeDesc);
					}	
				}
			});
		},
		reload: function() {
			vm.showNum = "0";
			var page = $('#jqGrid').jqGrid('getGridParam', 'page');
			$('#jqGrid').jqGrid('setGridParam', {
				postData:{'keyword': vm.q.keyword},
				page:page
			}).trigger("reloadGrid");
		}
		
	},
	
});