$(function() {
	$("#jqGrid").jqGrid( {
		url:baseURL + 'prize/getPrizesPage',
		datatype: "json",
		colModel: [
			{label: '奖品ID', name: 'id', width:40 },
			{label: '奖品名称', name: 'prizeName', width:45 },
			{label: '抽奖数量', name: 'prizeCount', width:40 },
			{label: '奖品总数', name: 'prizeSum', width:40 },
			{label: '抽奖概率', name: 'prizeProb', width:40 },
			{label: '抽奖开始时间', name: 'beginTime', width:45 },
			{label: '抽奖结束时间', name: 'endTime', width:45 },
		],
		viewrecords: true,
		height: 385,
		rowNum: 10,
		rowList : [10, 30, 50],
		rownumbers: true, 
		rownumWidth: 25,
		autowidth: true,
		multiselect: true,
		pager: "#jqGridPager",
		jsonReader : {
			root: "data", // 数据
			page: "pageNum", // 当前页面
			total: "pageTotle", // 总页数
			records: "total", // 总数据数
		},
		prmNames : {
			page: "pageNum",
			rows: "pageSize",
			order: "order",
		},
		gridComplete:function() {
			// 隐藏grid底部滚动条
			$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
		}	
	});
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			keyword:null
		},
		showNum: "0",
		title:null,
		prize: {
			prizeNo:0,
			prizeName:"",
			prizeCount:0,
			prizeSum:0,
			prizeProb:0.0,
			beginTime:new Date(),
			endTime:new Date(),
		},
		prizeNameList : [],
	},
	methods:{
		query: function() {
			vm.reload();
		},
		add: function() {
			vm.showNum = "1";
			vm.title = "新增";
			vm.prize = {};
			vm.prizeNameList = [];
			vm.prizeNameList.push({
				value: "0",
				name: "矿石"
			},{
				value: "1",
				name: "元宝"
			},{
				value: "2", 
				name: "财神币"
			});
		},
		update: function() {
			var id = getSelectedRow();
			if(id == null) {
				return;
			}
			
			vm.prize = {};
			var rowData = $("#jqGrid").getRowData(id); // 对象字面量
			vm.prize = rowData;
			vm.showNum = "2";
			vm.title = "修改";
			vm.prizeNameList = [];
			vm.prizeNameList.push({
				value: "0",
				name: "矿石"
			},{
				value: "1",
				name: "元宝"
			},{
				value: "2", 
				name: "财神币"
			});
		},
		del: function() {
			var idArray = getSelectedRows();
			if(idArray == null) {
				return;
			}
			var ids = idArray.join(",");
			confirm('确定删除选中的记录？', function() {
				$.ajax({
					type: "POST",
					url: baseURL + "prize/deletePrize",
					data : {
						ids: ids
					},
					success: function(data) {
						if(data.code == "200") {
							alert('操作成功', function() {
								vm.reload();
							});
						} else {
							alert(data.codeDesc);
						}
					}
				});
			});
		},
		saveOrUpdate: function() {
			var url = vm.prize.id == null ? "prize/savePrize" : "prize/updatePrize";
//			var begin = $('#beginInput').datetimebox('getValue');
//			var end = $('#endInput').datetimebox('getValue');
			var begin = vm.prize.id == null ? $('#beginInput').datetimebox('getValue') : $('#modBeginInput').datetimebox('getValue');
			var end = vm.prize.id == null ? $('#endInput').datetimebox('getValue') : $('#modEndInput').datetimebox('getValue');
			vm.prize.beginTime = begin;
			vm.prize.endTime = end;
			
			var selectValue = $("#selectValue").val(); // prizeNo
			var selectName = $("#selectValue").find("option:selected").text(); // prizeName
			vm.prize.prizeNo = selectValue;
			vm.prize.prizeName = selectName;
			
			$.ajax({
				type: "POST",
				url: baseURL + url,
				data: vm.prize,
				success: function(data) {
					if(data.code == "1") {
						alert(data.codeDesc, function() {
							vm.reload();
						});
					} else {
						alert(data.codeDesc);
					}	
				}
			});
		},
		reload: function() {
			vm.showNum = "0";
			var page = $('#jqGrid').jqGrid('getGridParam', 'page');
			$('#jqGrid').jqGrid('setGridParam', {
				postData:{'keyword': vm.q.keyword},
				page:page
			}).trigger("reloadGrid");
		}
		
	},
	
});