var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			keyword: null,
			startTime: null,
			endTime: null
		},
		showNum: "0",
		tableList:[{}]
	},
	mounted: function(){
		$(function () {
		    $("#jqGrid").jqGrid({
		        url: baseURL + 'poundageDetails/getPdPage',
		        postData: {'type': "0"},
		        datatype: "json",
		        colModel: [	
		            { label: '用户ID', name: 'userId', width: 40 },
		            { label: '交易总金额', name: 'tradePrice', width: 40},
		            { label: '手续费率', name: 'tradePoundage', width: 40},
					{ label: '矿石交易扣除手续费', name: 'pdPrice', width: 45},
					{ label: '挂单类型', name: 'guadanType', width: 40, formatter: function(value, options, row) {
						if(value === "0") {
							return '<span class = "label label-success">挂买单</span>';
						} else if(value === "1") {
							return '<span class = "label label-success">挂卖单</span>';
						}
					}},
					{ label: '扣费时间', name: 'createTime', width: 45}
		        ],
				viewrecords: true,
		        height: 385,
		        rowNum: 10,
				rowList : [10,30,50],
		        rownumbers: true, 
		        rownumWidth: 25, 
		        autowidth:true,
		        multiselect: true,
		        pager: "#jqGridPager",
		        footerrow: true, // 添加合计行
		        jsonReader : {
		            root: "data",       //数据
		            page: "pageNum",     //当前页数
		            total: "pageTotle",    //总页数
		            records: "total"     //总数据条数
		        },
		        prmNames : {
		        	page:"pageNum", 
		            rows:"pageSize", 
		            order: "order",
//		            totalrows:"total"
		        },
		        gridComplete:function(){
		        	vm.tableList = [];
		        	//隐藏grid底部滚动条
		        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
		        	var ids = $("#jqGrid").jqGrid('getDataIDs');
		        	for(var i = 0; i < ids.length; i++) {
						var rowData = $("#jqGrid").jqGrid('getRowData',ids[i]);
						vm.tableList.push(rowData);
//			        	Vue.set(vm.tableList, i, rowData);
		        	}
		        	
		        	// 合计数据
		        	var tradePriceSum = $("#jqGrid").getCol('tradePrice', false, 'sum');
		        	var pdPriceSum = $("#jqGrid").getCol('pdPrice', false, 'sum');
		        	$("#jqGrid").footerData('set', {'userId': '合计', 'tradePrice': tradePriceSum.toFixed(2), 'pdPrice': pdPriceSum.toFixed(2)}, false);
					console.log(vm.tableList);
		        },
		    });
		});
		
	},
	methods: {
		query: function(){
			console.info("query");
			console.info($("#startInput").val());
			var start = $('#startInput').datetimebox('getValue');
			var end = $('#endInput').datetimebox('getValue');
			vm.q.startTime = start;
			vm.q.endTime = end;
			vm.reload();
		},
		del: function() {
			var idArray = getSelectedRows();
			if(idArray == null) {
				return;
			}
			var ids = idArray.join(",");
			confirm('确定删除选中的记录？', function() {
				$.ajax({
					type: "POST",
					url: baseURL + "poundageDetails/deletePd",
					data : {
						ids : ids,
						type : "0"
					},
					success: function(data) {
						if(data.code == "200") {
							alert('操作成功', function() {
								vm.reload();
							})
						} else {
							alert(data.codeDesc);
						}
					}
					
				});
			});
			
		},
		reload: function () {
			vm.showNum = "0";
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{
                postData:{
                	'keyword': vm.q.keyword,
                	'startTime': vm.q.startTime,
                	'endTime': vm.q.endTime,
                	'type': "0"
                },
                page:page
            }).trigger("reloadGrid");
		}
	}
});