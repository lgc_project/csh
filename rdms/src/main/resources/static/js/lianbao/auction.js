$(function() {
	$("#jqGrid").jqGrid( {
		url:baseURL + 'auction/getAuctionsPage',
		datatype: "json",
		colModel: [
			{label: '竞拍活动ID', name: 'id', width:40 },
			{label: '竞拍活动名称', name: 'auctionName', width:45 },
			{label: '竞拍活动金额', name: 'auctionMoney', width:40 },
			{label: '最低金额', name: 'auctionLowMoney', width:40 },
			{label: '手续费', name: 'auctionPoundage', width:40 },
			{label: '开始时间', name: 'beginTime', width:45 },
			{label: '结束时间', name: 'endTime', width:45 },
		],
		viewrecords: true,
		height: 385,
		rowNum: 10,
		rowList : [10, 30, 50],
		rownumbers: true, 
		rownumWidth: 25,
		autowidth: true,
		multiselect: true,
		pager: "#jqGridPager",
		jsonReader : {
			root: "data", // 数据
			page: "pageNum", // 当前页面
			total: "pageTotle", // 总页数
			records: "total", // 总数据数
		},
		prmNames : {
			page: "pageNum",
			rows: "pageSize",
			order: "order",
		},
		gridComplete:function() {
			// 隐藏grid底部滚动条
			$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
		}	
	});
});

var vm = new Vue({
	el:'#rrapp',
	data: {
		q:{
			keyword:null
		},
		showNum: "0",
		title: null,
		auction : {
			auctionName: "",
			auctionMoney:0.0,
			auctionLowmoney:0.0,
			auctionPoundage:0.0,
			beginTime:new Date(),
			endTime:new Date(),
		},
	},
	methods:{
		query: function() {
			vm.reload();
		},
		add: function() {
			vm.showNum = "1";
			vm.title = "新增";
			vm.auction = {};
		},
		update: function() {
			var id = getSelectedRow();
			if(id == null) {
				return;
			}
			
			vm.auction = {};
			var rowData = $("#jqGrid").getRowData(id);
			vm.auction = rowData;
			vm.showNum = "2";
			vm.title = "修改";
		},
		del: function() {
			var idArray = getSelectedRows();
			if(idArray == null) {
				return;
			}
			var ids = idArray.join(",");
			confirm('确定删除选中的记录？', function() {
				$.ajax({
					type: "POST",
					url: baseURL + "auction/deleteAuction",
					data : {
						ids: ids
					},
					success: function(data) {
						if(data.code == "200") {
							alert('操作成功', function() {
								vm.reload();
							});
						} else {
							alert(data.codeDesc);
						}
					}
				});
				
			});
		},
		saveOrUpdate: function() {
			var url = vm.auction.id == null ? "auction/saveAuction" : "auction/updateAuction";
		
//			var begin = $('#beginInput').datetimebox('getValue');
//			var end = $('#endInput').datetimebox('getValue');
			var begin = vm.auction.id == null ? $('#beginInput').datetimebox('getValue') : $('#modBeginInput').datetimebox('getValue');
			var end = vm.auction.id == null ? $('#endInput').datetimebox('getValue') : $('#modEndInput').datetimebox('getValue');
			vm.auction.beginTime = begin;
			vm.auction.endTime = end;
			
			$.ajax({
				type: "POST",
				url: baseURL + url,
				data: vm.auction,
				success: function(data) {
					if(data.code == "1") {
						alert(data.codeDesc, function() {
							vm.reload();
						});
					} else {
						alert(data.codeDesc);
					}
				}
			});
		},
		reload: function() {
			vm.showNum = "0";
			var page = $('#jqGrid').jqGrid('getGridParam', 'page');
			$('#jqGrid').jqGrid('setGridParam', {
				postData:{'keyword': vm.q.keyword},
				page:page
			}).trigger("reloadGrid");
		}
	},
	
});