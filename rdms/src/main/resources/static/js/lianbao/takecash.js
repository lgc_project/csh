var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			keyword: null,
			startTime: null,
			endTime: null
		},
		showNum: "0",
		tableList:[{}]
	},
//	tableList:[{}],
	mounted: function(){
		$(function () {
		    $("#jqGrid").jqGrid({
		        url: baseURL + 'takeCash/getCashPage',
		        datatype: "json",
		        colModel: [	
		            { label: 'id', name: 'id', width: 40, hidden: true },
		            { label: '用户ID', name: 'userId', width: 40 },
		            { label: '银行卡号', name: 'bankCard', width: 50},
		            { label: '支付宝账号', name: 'alipayAccount', width: 45},
		            { label: '取现金额', name: 'cash', width: 40},
		            { label: '状态', name: 'status', width: 0, hidden:true},
		            { label: '状态', name: 'statusTip', width: 40, formatter: function(value, options, row) {
		            	if(value === "未审理") {
							return "<span class='label label-danger'>未审理</span>";
						} else if(value === "已审理，未汇款") {
							return "<span class='label label-danger'>已审理，未汇款</span>";
						} else {
							return "<span class='label label-success'>已汇款</span>";
						}
		            }},
		            { label: '申请时间', name: 'createTime', width: 45},
		            { label: '审批时间', name: 'updateTime', width: 45},
		        ],
				viewrecords: true,
		        height: 385,
		        rowNum: 10,
				rowList : [10,30,50],
		        rownumbers: true, 
		        rownumWidth: 25, 
		        autowidth:true,
		        multiselect: true,
		        pager: "#jqGridPager",
		        footerrow: true, // 添加合计行
		        jsonReader : {
		            root: "data",       //数据
		            page: "pageNum",     //当前页数
		            total: "pageTotle",    //总页数
		            records: "total"     //总数据条数
		        },
		        prmNames : {
		        	page:"pageNum", 
		            rows:"pageSize", 
		            order: "order",
//		            totalrows:"total"
		        },
		        gridComplete:function(){
		        	vm.tableList = [];
		        	//隐藏grid底部滚动条
		        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
		        	var ids = $("#jqGrid").jqGrid('getDataIDs');
		        	for(var i = 0; i < ids.length; i++) {
						var rowData = $("#jqGrid").jqGrid('getRowData',ids[i]);
						vm.tableList.push(rowData);
//			        	Vue.set(vm.tableList, i, rowData);
		        	}
		        	var cashSum = $("#jqGrid").getCol('cash', false, 'sum');
					$("#jqGrid").footerData('set', {'userId':'合计', 'cash':cashSum.toFixed(2)}, false);
		        	console.log(vm.tableList);
		        }
		    });
		});
	},
	methods: {
		query: function(){
			console.info("query");
			console.info($("#startInput").val());
			var start = $('#startInput').datetimebox('getValue');
			var end = $('#endInput').datetimebox('getValue');
			vm.q.startTime = start;
			vm.q.endTime = end;
			vm.reload();
		},
		reload: function () {
			vm.showNum = "0";
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                postData:{
                	'keyword': vm.q.keyword,
                	'startTime': vm.q.startTime,
                	'endTime': vm.q.endTime
                },
                page:page
            }).trigger("reloadGrid");
		},
		update: function() {
			var id = getSelectedRow();
			var status = $("#jqGrid").getCell(id, "status");		
			
			if(status === 2) {
				alert("已汇款，不需要重复确认");
				return;
			}
			
			if(id == null) {
				return;
			}
			confirm('确认选中的提现记录？', function() {
				$.ajax({
					type: "POST",
					url: baseURL + "takeCash/sureSubmit",
					data : {
						id : id,
						status : status
					},
					success: function(data) {
						if(data.code == "200") {
							alert('操作成功', function() {
								vm.reload();
							});
						} else {
							alert(data.codeDesc);
						}
					}
					
				});
			}); 
			
		},
	}
});