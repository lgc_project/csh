var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			keyword: null,
			startTime: null,
			endTime: null
		},
		showNum: "0",
		tableList:[{}]
	},
	mounted: function(){
		$(function () {
		    $("#jqGrid").jqGrid({
		        url: baseURL + 'machine/getMachineCl',
		        datatype: "json",
		        colModel: [	
		            { label: '用户ID', name: 'userId', width: 40 },
		            { label: '用户名称', name: 'realName', width: 45},
		            { label: '矿机产量', name: 'canLiang', width: 45},
				],
				viewrecords: true,
		        height: 385,
		        rowNum: 10,
				rowList : [10,30,50],
		        rownumbers: true, 
		        rownumWidth: 25, 
		        autowidth:true,
		        multiselect: true,
		        pager: "#jqGridPager",
		        footerrow: true, // 添加合计行
		        jsonReader : {
		            root: "data",       //数据
		            page: "pageNum",     //当前页数
		            total: "pageTotle",    //总页数
		            records: "total"     //总数据条数
		        },
		        prmNames : {
		        	page:"pageNum", 
		            rows:"pageSize", 
		            order: "order",
//		            totalrows:"total"
		        },
		        gridComplete:function(){
		        	vm.tableList = [];
		        	//隐藏grid底部滚动条
		        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
		        	var ids = $("#jqGrid").jqGrid('getDataIDs');
		        	for(var i = 0; i < ids.length; i++) {
						var rowData = $("#jqGrid").jqGrid('getRowData',ids[i]);
						vm.tableList.push(rowData);
//			        	Vue.set(vm.tableList, i, rowData);
		        	}
		        	// 合计数据
		        	var tradePriceSum = $("#jqGrid").getCol('canLiang', false, 'sum');
		        	$("#jqGrid").footerData('set', {'userId': '合计', 'canLiang': tradePriceSum}, false);
					console.log(vm.tableList);
		        },
		    });
		});
		
	},
	methods: {
		query: function(){
			vm.reload();
		},
		reload: function () {
			vm.showNum = "0";
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{
                postData:{
                	'keyword': vm.q.keyword
                },
                page:page
            }).trigger("reloadGrid");
		}
	}
});