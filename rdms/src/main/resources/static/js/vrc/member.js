$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'user/getUserPage',
        datatype: "json",
        colModel: [	
            { label: '用户ID', name: 'id', width: 40 },
            { label: '电话号码', name: 'phone', width: 40 },
			{ label: '推荐人ID', name: 'parentId', width: 45 },
			{ label: '元宝券总额', name: 'moneySum', width: 40 },
			{ label: '冻结元宝券', name: 'moneyFreeze', width: 40},
			{ label: '财神币总额', name: 'caiShenBiSum', width: 40},
			{ label: '矿石总额', name: 'oreSum', width: 40},
			{ label: '真实姓名', name: 'realname', width: 40},
			{ label: '身份证号', name: 'idcard', width: 60},
			{ label: '银行卡号', name: 'bankcard', width: 60},
			{ label: '开户银行', name: 'bankName', width: 40},
      { label: '微信', name: 'weixinAccount', width: 110},
			{ label: '支付宝', name: 'alipayAccount', width: 110},
			{ label: '状态', name: 'isvalid', width: 80, formatter: function(value, options, row){
				return value === '0' ? 
						'<span class="label label-danger">冻结</span>' : 
						'<span class="label label-success">正常</span>';
				}},
			{ label: '操作', name: 'operator', width: 60, formatter: function(value, option, row) {
				return "<a style='color:#f60', onclick='getChildList(" + row.id + ")'>获取用户直邀列表</a>";
			}}
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "data",       //数据
            page: "pageNum",     //当前页数
            total: "pageTotle",    //总页数
            records: "total"     //总数据条数
        },
        prmNames : {
        	page:"pageNum", 
            rows:"pageSize", 
            order: "order",
//            totalrows:"total"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "scroll" });
        }
    });
});


var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			keyword: null
		},
		showNum: "0",
		title:null,
		member: {
			id:"",
			phone:"",
			parentId: "",
			moneySum: 0,
			moneyFrezze: 0,
			caiShenBiSum: 0,
			oreSum: 0,
			realname: "",
			idcard: "",
			bankcard: "",
			bankName: "",
			weixinAccount: "",
			alipayAccount: ""
		},
		machineTypeArr:[],
		machine: {
			userId: "",
			type: "",
		}
	},
	methods: {
		query: function(){
			vm.reload();
		},
		add: function(){
			vm.showNum = "1";
			vm.title = "新增";
			
			vm.member = {};
		},
		update: function () {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			
			vm.member = {};
			var d = $("#jqGrid").getRowData(id);
			vm.member = d;
			vm.showNum = "2";
            vm.title = "修改";
		},
		del: function () {
			var idList = getSelectedRows();
			if(idList == null){
				return ;
			}
			var ids = idList.join(",");
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "user/deleteUser",
				    data: {
				    	ids: ids
				    },
				    success: function(data){
						if(data.code == '200'){
							alert('操作成功', function(){
                                vm.reload();
							});
						}else{
							alert(data.codeDesc);
						}
					}
				});
			});
		},
		saveOrUpdate: function () {
			var url = vm.member.id == null ? "user/saveUser" : "user/updateUser";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
			    data: vm.member,
			    success: function(data){
			    	if(data.code == "200"){
			    		alert(data.codeDesc, function(){
							vm.reload();
						});
			    	} else {
			    		alert(data.codeDesc);
			    	}
				}
			});
		},
		giveMachine: function(){
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			
			$.ajax({
				type: "POST",
			    url: baseURL + "machine/getAllMachineType",
			    data: {},
			    success: function(data){
			    	if(data.code == "200"){
			    		vm.machineTypeArr = data.data;
			    		if(vm.machineTypeArr != null && vm.machineTypeArr.length > 0){
			    			vm.machine.type = vm.machineTypeArr[0].code;
			    		}
			    	} else {
			    		alert(data.codeDesc);
			    	}
				}
			});
			
			var d = $("#jqGrid").getRowData(id);
			vm.machine.userId = d.id;
			vm.showNum = "3";
            vm.title = "赠送矿机";
		},
		addMachine: function(){
			console.info("addMachine");
			console.info(vm.machine);
			
			$.ajax({
				type: "POST",
			    url: baseURL + "machine/addMachine",
			    data: vm.machine,
			    success: function(data){
			    	if(data.code == "200"){
			    		alert(data.codeDesc, function(){
							vm.reload();
						});
			    	} else {
			    		alert(data.codeDesc);
			    	}
				}
			});
		},
		freeOrRefree: function(){
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			
			var d = $("#jqGrid").getRowData(id);
			console.info(d);
			$.ajax({
				type: "POST",
			    url: baseURL + "user/freeOrRefree",
			    data: {
			    	userId: d.id,
			    	isvalid: d.isvalid
			    },
			    success: function(data){
					if(data.code == '200'){
						alert('操作成功', function(){
                            vm.reload();
						});
			    	} else {
			    		alert(data.codeDesc);
			    	}
				}
			});
		},
		reload: function () {
			vm.showNum = "0";
//			var page = $("#jqGrid").jqGrid('getGridParam','page');
			var page = 1;
			$("#jqGrid").jqGrid('setGridParam',{ 
                postData:{'keyword': vm.q.keyword},
                page:page
            }).trigger("reloadGrid");
		}
	}
});

function getChildList(id) {
	vm.showNum = "3";
	var page = $("#userChildList").jqGrid('getGridParam', 'page');
	
	$("#userChildList").jqGrid({
		url: baseURL + 'user/getUserChildList',
		datatype: "json",
		colModel: [	
            { label: '用户ID', name: 'id', width: 40 },
            { label: '电话号码', name: 'phone', width: 40 },
			{ label: '推荐人ID', name: 'parentId', width: 45 },
			{ label: '元宝券总额', name: 'moneySum', width: 40 },
			{ label: '冻结元宝券', name: 'moneyFreeze', width: 40},
			{ label: '财神币总额', name: 'caiShenBiSum', width: 40},
			{ label: '矿石总额', name: 'oreSum', width: 40},
			{ label: '真实姓名', name: 'realname', width: 40},
			{ label: '身份证号', name: 'idcard', width: 60},
			{ label: '银行卡号', name: 'bankcard', width: 60},
			{ label: '开户银行', name: 'bankName', width: 40}
        ],
        viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#userChildListPager",
        jsonReader : {
            root: "data",       //数据
            page: "pageNum",     //当前页数
            total: "pageTotle",    //总页数
            records: "total"     //总数据条数
        },
        prmNames : {
        	page:"pageNum", 
            rows:"pageSize", 
            order: "order",
//	            totalrows:"total"
        },
        postData: {'parentId': id},
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#userChildList").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
	});
	
}