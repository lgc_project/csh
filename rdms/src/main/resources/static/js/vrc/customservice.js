
var vm = new Vue({
	el:'#rrapp',
	data:{
		customservice:{
			content: ""
		}
	},
	mounted: function(){
		this.query();
	},
	methods: {
		query: function(){
			var url = "notice/getCustomService";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
			    success: function(data){
			    	if(data.code == "200"){
			    		console.info(data.data);
			    		if(data.data == null){
			    			vm.customservice = {
			    					id : null,
			    					content: null
			    			}
			    		} else {
			    			vm.customservice = data.data;
			    		}
			    	} else {
			    		alert(data.codeDesc);
			    	}
				}
			});
		},
		saveOrUpdate: function () {
			var url = vm.customservice.id == null ? "notice/saveCustomService" : "notice/updateCustomService";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
			    data: vm.customservice,
			    success: function(data){
			    	alert(data.codeDesc);
				}
			});
		}
	}
});