$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'machine/getMachineTypePage',
        datatype: "json",
        colModel: [	
            { label: 'id', name: 'id', hidden: true },
            { label: 'code', name: 'code', key: true, hidden: true },
			{ label: '名称', name: 'name', width: 45 },
			{ label: '算力', name: 'calculate', width: 40 },
			{ label: '运行周期', name: 'workSum', width: 40},
			{ label: '收益总量', name: 'incomeSum', width: 40},
			{ label: '价格', name: 'price', width: 40},
			{ label: '维修费用', name: 'maintenance', width:40},
			{ label: '维修周期', name: 'mtSum', width:40},
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "data",       //数据
            page: "pageNum",     //当前页数
            total: "pageTotle",    //总页数
            records: "total"     //总数据条数
        },
        prmNames : {
        	page:"pageNum", 
            rows:"pageSize", 
            order: "order",
//            totalrows:"total"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});


var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			keyword: null
		},
		showList: true,
		title:null,
		machineType: {
			id:"",
			code:"",
			name: "",
			calculate: 0,
			workSum: 0,
			incomeSum: 0,
			price: 0,
			maintenance:0.0,
			mtSum:0,
			gradeReward:0.0,
		}
	},
	methods: {
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			
			vm.machineType = {};
		},
		update: function () {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			
			var d = $("#jqGrid").getRowData(id);
			vm.machineType = d;
			console.info(vm.machineType);
			vm.showList = false;
            vm.title = "修改";
		},
		del: function () {
			var codeList = getSelectedRows();
			if(codeList == null){
				return ;
			}
			var codes = codeList.join(",");
			console.info(codes);
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "machine/deleteMachineType",
				    data: {
				    	codes: codes
				    },
				    success: function(data){
						if(data.code == "200"){
							alert('操作成功', function(){
                                vm.reload();
							});
						}else{
							alert(data.codeDesc);
						}
					}
				});
			});
		},
		saveOrUpdate: function () {
			console.info();
			var url = vm.machineType.id == null ? "machine/saveMachineType" : "machine/updateMachineType";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
			    data: vm.machineType,
			    success: function(data){
			    	if(data.code == "1"){
			    		alert(data.codeDesc, function(){
							vm.reload();
						});
			    	} else {
			    		alert(data.codeDesc);
			    	}
				}
			});
		},
		reload: function () {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                postData:{},
                page:page
            }).trigger("reloadGrid");
		}
	}
});