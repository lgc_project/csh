var theme = "";   //echart主题
var myChart = "";   //echart初始化 
var chartData = "";   //图表数据

$(function () {
	initChart();
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			keyword: null
		},
		showList: true,
		title: "",
		avgShow: 0,
		maxShow: 0,
		countShow: 0,
		csbAvgShow: 0,
		csbMaxShow: 0,
		csbCountShow: 0,
		marker: {
			id: "",
			avg: 0,
			max: 0,
			count: 0,
			csbAvg: 0,
			csbMax: 0,
			csbCount: 0
		}
	},
	mounted: function(){
		this.getChartData();
		this.getSimpleData();
	},
	methods: {
		query: function(){
			
		},
		getSimpleData:function(){
			$.ajax({
				type: "POST",
			    url: baseURL + "trade/getTradeSimple",
			    data: {
			    	
			    },
			    success: function(data){
			    	da = data.data;
			    	vm.marker.id = da.id;
			    	vm.marker.avg = da.avg;
			    	vm.marker.max = da.max;
			    	vm.marker.count = da.count;
			    	
			    	vm.marker.csbAvg = da.csbAvg;
			    	vm.marker.csbMax = da.csbMax;
			    	vm.marker.csbCount = da.csbCount;
			    	
//			    	alert(data);
			    	console.info(data);
			    	vm.avgShow = da.avg;
			    	vm.maxShow = da.max;
			    	vm.countShow = da.count;
			    	
			    	vm.csbAvgShow = da.csbAvg;
			    	vm.csbMaxShow = da.csbMax;
			    	vm.csbCountShow = da.csbCount;
				}
			});
		},
		getChartData: function(){
			console.info("getChartData...");
			$.ajax({
				type: "POST",
			    url: baseURL + "trade/getChartData",
			    data: {
			    	
			    },
			    success: function(data){
			    	createOption(data.data);
				}
			});
		},
		edit: function(){
			vm.marker.avg = vm.avgShow;
	    	vm.marker.max = vm.maxShow;
	    	vm.marker.count = vm.countShow;
	    	
	    	vm.marker.csbAvgShow = vm.csbAvgShow;
	    	vm.marker.csbMaxShow = vm.csbMaxShow;
	    	vm.marker.csbCountShow = vm.csbCountShow;
			layer.open({
				type: 1,
				skin: 'layui-layer-molv',
				title: "编辑",
				area: ['550px', '290px'],
				shadeClose: false,
				content: jQuery("#simpleLayer"),
				btn: ['修改','取消'],
				btn1: function (index) {
					$.ajax({
						type: "POST",
					    url: baseURL + "trade/updateTradeSimple",
					    data: vm.marker,
					    success: function(data){
					    	if(data.code == "200"){
					    		alert(data.codeDesc, function(){
					    			layer.close(index);
					    			vm.getChartData();
					    			vm.getSimpleData();
								});
					    	} else {
					    		if(data.codeDesc == null){
					    			data.codeDesc = "系统错误！";
					    		}
					    		alert(data.codeDesc, function(){
					    			layer.close(index);
					    		});
					    	}
						}
					});
	            }
			});
		}
	}
});


//画图
function initChart() {
	myChart = echarts.init(document.getElementById('echarts'), theme);
}

//构建option
function createOption(chartData){
//	var legend = chartData.legend; // 图例数组
	var xdata = chartData.dayList; // x轴数组
	var ydata = chartData.priceList; // y轴数据数组（多个）
	var csbData = chartData.csbPriceList;

	var option = {
			tooltip : {
				trigger : 'axis',
				//设置提示框的位置 p:鼠标位置  params：数据数组  div:提示框
				position:function(p,params,div){
					var height = $(div).height();   //提示框的高
					var width = $(div).width();    //提示框的宽
					var allHeight = $("#echarts").height();   //echart图表的总高度
					var allWidth = $("#echarts").width();    //echart图表的总宽度
					if(p[1]+height > allHeight){
						p[1] = allHeight - height - 10;
					}
					
					if(p[0]+width > allWidth){
						p[0] = p[0]-width-20;
					} else {
						p[0] = p[0]+10;
					}
					return [p[0],p[1]];
				},
//				formatter : function(params) {}
			},
			legend : {
				data : ['矿石价格','财神币价格']
			},
			xAxis : [ {
			    data : xdata
			} ],
			yAxis : [ {
				type : 'value',
			} ],
			series : [{
				name:'矿石价格',
	            type:'line',
	            itemStyle: {normal: {areaStyle: {type: 'default'}}},
	            data: ydata
			}, {
				name:'财神币价格',
				type:'line',
				itemStyle: {normal: {areaStyle: {type: 'default' , color: '#4169E1'}}},
				data: csbData
			}]
		};
	myChart.clear();
	myChart.setOption(option);   //设置图标
	myChart.hideLoading();       //取消Loading
	return option;
}
