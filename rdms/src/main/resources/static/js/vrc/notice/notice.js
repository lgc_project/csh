$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'notice/getNotice',
        datatype: "json",
        colModel: [	
            { label: 'id', name: 'id', key: true, hidden: true },
			{ label: '标题', name: 'title', width: 45 },
			{ label: '内容', name: 'content', width: 90, formatter: function(value, options, row){
				value = value.replace(/\n/g," ");
				return value;
			}},
			{ label: '操作时间', name: 'time', width: 40}
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
        	root: "data",       //数据
            page: "pageNum",     //当前页数
            total: "pageTotle",    //总页数
            records: "total"     //总数据条数
        },
        prmNames : {
        	page:"pageNum", 
            rows:"pageSize", 
            order: "order",
//            totalrows:"total"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});


var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			keyword: null
		},
		showList: true,
		title:null,
		notice:{
			title: "",
			content: ""
		}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			
			vm.notice = {id:null, title:null, content:null};
		},
		update: function () {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			
			vm.showList = false;
            vm.title = "修改";
			
			vm.getNoticeById(id);
		},
		del: function () {
			var idList = getSelectedRows();
			if(idList == null){
				return ;
			}
			var ids = idList.join(",");
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "notice/deleteNotice",
				    data: {
				    	ids: ids
				    },
				    success: function(data){
						if(data.code == 1){
							alert('操作成功', function(){
                                vm.reload();
							});
						}else{
							alert(data.codeDesc);
						}
					}
				});
			});
		},
		saveOrUpdate: function () {
			var url = vm.notice.id == null ? "notice/save" : "notice/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
			    data: vm.notice,
			    success: function(data){
			    	if(data.code == "1"){
			    		alert(data.codeDesc, function(){
							vm.reload();
						});
			    	} else {
			    		alert(data.codeDesc);
			    	}
				}
			});
		},
		getNoticeById: function(id){
			$.ajax({
				type: "POST",
			    url: baseURL + "notice/getById",
			    data: {
			    	id: id
			    },
			    success: function(data){
			    	vm.notice = data;
				}
			});
		},
		reload: function () {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                postData:{'keyword': vm.q.keyword},
                page:page
            }).trigger("reloadGrid");
		}
	}
});