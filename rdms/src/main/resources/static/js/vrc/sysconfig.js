
var vm = new Vue({
	el:'#rrapp',
	data:{
		config: {
			zeroguadanmin : 0,
			zeroguadanmax : 0,
			zeroguadantip : '普通用户可进行0-0币议价交易',
			firstguadanmin : 0,
			firstguadanmax : 0,
			firstguadantip : '认证用户可进行0-0币议价交易',
			secondguadanmin : 0,
			secondguadanmax : 0,
			secondguadantip : '认证用户可进行0-0币议价交易',
			thirdguadanmin : 0,
			thirdguadanmax : 0,
			thirdguadantip : '认证用户可进行0-0币议价交易',
			fouthguadanmin : 0,
			fouthguadanmax : 0,
			fouthguadantip : '认证用户可进行0-0币议价交易',
			fifthguadanmin : 0,
			fifthguadanmax : 0,
			fifthguadantip : '认证用户可进行0-0币议价交易',
			reward : 0,
			orePoundage : 0,
			upamount : 0,
			csbupamount: 0,
			tradetimestart: "",
			tradetimeend: "",
			firstreward: 0.0,
			secondreward: 0.0,
			thirdreward: 0.0,
			fourthreward: 0.0,
			fifthreward: 0.0,
			recoverpoundage: 0.0,
			recoverpoundagetip: ''
		}
	},
	mounted: function(){
		this.getConfig();
	},
	methods: {
		getConfig: function(){
			$.ajax({
				type: "POST",
			    url: baseURL + "vrcConfig/getAllConfig",
			    data: {
			    },
			    success: function(data){
					if(data.code == '200'){
						vm.config = data.data;
					}else{
						alert("查询系统参数失败");
					}
				}
			});
		},
		saveOrUpdate: function(){
			$.ajax({
				type: "POST",
			    url: baseURL + "vrcConfig/saveOrUpdate",
			    data: {
			    	params: JSON.stringify(vm.config)
			    },
			    success: function(data){
					if(data.code == '200'){
						alert('操作成功');
					} else {
						alert("操作失败");
					}
				}
			});
		}
	}
});