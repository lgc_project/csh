var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			keyword: null,
			startTime: null,
			endTime: null
		},
		showNum: "0",
		tableList: [{
//			id: 1,
//			buyId: '123456',
//			buyName: '小明',
//			sellId: '345345',
//			sellName: '小强',
//			price: '4.0',
//			tradeCount: '100',
//			tradeMoney: '400',
//			tradeTime: '2018-05-23 23:20:43'
		}]
	},
	mounted: function(){
		$(function () {
		    $("#jqGrid").jqGrid({
		        url: baseURL + 'machine/getBuyrecord',
		        datatype: "json",
		        colModel: [	
		            { label: 'id', name: 'id', width: 40, hidden: true },
		            { label: '用户ID', name: 'userId', width: 40 },
					{ label: '矿机编码', name: 'machineNo', width: 45 },
					{ label: '矿机类型', name: 'machineTypeName', width: 40 },
					{ label: '购买时间', name: 'time', width: 40},
		        ],
				viewrecords: true,
		        height: 385,
		        rowNum: 10,
				rowList : [10,30,50],
		        rownumbers: true, 
		        rownumWidth: 25, 
		        autowidth:true,
		        multiselect: true,
		        pager: "#jqGridPager",
		        jsonReader : {
		            root: "data",       //数据
		            page: "pageNum",     //当前页数
		            total: "pageTotle",    //总页数
		            records: "total"     //总数据条数
		        },
		        prmNames : {
		        	page:"pageNum", 
		            rows:"pageSize", 
		            order: "order",
//		            totalrows:"total"
		        },
		        gridComplete:function(){
		        	vm.tableList = [];
		        	//隐藏grid底部滚动条
		        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
		        	var ids = $("#jqGrid").jqGrid('getDataIDs');
		        	for(var i = 0; i < ids.length; i++) {
						var rowData = $("#jqGrid").jqGrid('getRowData',ids[i]);
						vm.tableList.push(rowData);
//			        	Vue.set(vm.tableList, i, rowData);
		        	}
					console.log(vm.tableList);
		        }
		    });
		    
		});
	},
	methods: {
		query: function(){
			console.info("query");
			console.info($("#startInput").val());
			var start = $('#startInput').datetimebox('getValue');
			var end = $('#endInput').datetimebox('getValue');
			vm.q.startTime = start;
			vm.q.endTime = end;
			vm.reload();
		},
		reload: function () {
			vm.showNum = "0";
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                postData:{
                	'keyword': vm.q.keyword,
                	'startTime': vm.q.startTime,
                	'endTime': vm.q.endTime
                },
                page:page
            }).trigger("reloadGrid");
		}
	}
});