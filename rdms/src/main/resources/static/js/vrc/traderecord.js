var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			keyword: null,
			startTime: null,
			endTime: null
		},
		showNum: "0",
		tableList: [{
//			id: 1,
//			buyId: '123456',
//			buyName: '小明',
//			sellId: '345345',
//			sellName: '小强',
//			price: '4.0',
//			tradeCount: '100',
//			tradeMoney: '400',
//			tradeTime: '2018-05-23 23:20:43'
		}]
	},
	mounted: function(){
		$(function () {
		    $("#jqGrid").jqGrid({
		        url: baseURL + 'trade/getTradePage',
		        datatype: "json",
		        colModel: [	
		            { label: 'id', name: 'id', width: 40, hidden: true },
		            { label: '买家ID', name: 'buyId', width: 40 },
					{ label: '买家名称', name: 'buyName', width: 45 },
					{ label: '卖家ID', name: 'sellId', width: 40 },
					{ label: '卖家名称', name: 'sellName', width: 40},
					{ label: '单价', name: 'price', width: 40},
					{ label: '交易数量', name: 'tradeCount', width: 40},
					{ label: '交易金额', name: 'tradeMoney', width: 40},
					{ label: '交易时间', name: 'tradeTime', width: 70},
		        ],
				viewrecords: true,
		        height: 385,
		        rowNum: 10,
				rowList : [10,30,50],
		        rownumbers: true, 
		        rownumWidth: 25, 
		        autowidth:true,
		        multiselect: true,
		        pager: "#jqGridPager",
		        footerrow: true,
		        jsonReader : {
		            root: "data",       //数据
		            page: "pageNum",     //当前页数
		            total: "pageTotle",    //总页数
		            records: "total"     //总数据条数
		        },
		        prmNames : {
		        	page:"pageNum", 
		            rows:"pageSize", 
		            order: "order",
//		            totalrows:"total"
		        },
		        gridComplete:function(){
		        	vm.tableList = [];
		        	//隐藏grid底部滚动条
		        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
		        	var ids = $("#jqGrid").jqGrid('getDataIDs');
		        	for(var i = 0; i < ids.length; i++) {
						var rowData = $("#jqGrid").jqGrid('getRowData',ids[i]);
						vm.tableList.push(rowData);
//			        	Vue.set(vm.tableList, i, rowData);
		        	}
		        	var priceSum = $("#jqGrid").getCol('price', false, 'sum');
		        	var tradeCountSum = $("#jqGrid").getCol('tradeCount', false, 'sum');
		        	var tradeMoneySum = $("#jqGrid").getCol('tradeMoney', false, 'sum');
		        	$("#jqGrid").footerData('set', {'buyId':'合计','price':priceSum.toFixed(2),'tradeCount':tradeCountSum.toFixed(2),'tradeMoney':tradeMoneySum.toFixed(2)}, false);
					console.log(vm.tableList);
		        },
		    });
		});
		
	},
	methods: {
		query: function(){
			console.info("query");
			console.info($("#startInput").val());
			var start = $('#startInput').datetimebox('getValue');
			var end = $('#endInput').datetimebox('getValue');
			vm.q.startTime = start;
			vm.q.endTime = end;
			vm.reload();
		},
		del : function() {
			var idArray = getSelectedRows();
			if(idArray == null) {
				return;
			}
			var ids = idArray.join(",");
			confirm('确定要删除选中的记录？', function() {
				$.ajax({
					type: "POST",
					url: baseURL + "trade/deleteTrade",
					data : {
						ids : ids
					},
					success: function(data) {
						if(data.code == "200") {
							alert("操作成功", function() {
								vm.reload();
							})
						} else {
							alert(data.codeDesc);
						}
					}
				});
			});
		},
		reload: function () {
			vm.showNum = "0";
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{
                postData:{
                	'keyword': vm.q.keyword,
                	'startTime': vm.q.startTime,
                	'endTime': vm.q.endTime
                },
                page:page
            }).trigger("reloadGrid");
		}
	}
});