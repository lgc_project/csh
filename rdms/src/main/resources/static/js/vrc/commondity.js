$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'commodity/getCommodityPage',
        datatype: "json",
        colModel: [	
            { label: '商品ID', name: 'id', width: 40 },
            { label: '商品名称', name: 'commodityName', width: 40 },
			{ label: '商品价格', name: 'price', width: 45 },
			{ label: '商品介绍', name: 'introduce', width: 40 },
			{ label: '创建时间', name: 'timeView', width: 40},
			{ label: '图片预览', name: 'imgPath', width: 50, formatter:imageFormatter},
			{ label: '操作', name: 'operator', width: 50, formatter:uploadImgFormatter},
		],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "data",       //数据
            page: "pageNum",     //当前页数
            total: "pageTotle",    //总页数
            records: "total"     //总数据条数
        },
        prmNames : {
        	page:"pageNum", 
            rows:"pageSize", 
            order: "order",
//            totalrows:"total"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

function imageFormatter(cellvalue, options, rowObject) {
	if(cellvalue == null) {
		return "<p style='color:#FF0000'>还没上传商品图片，请上传</p>";
	}
	
	return '<img src = "' + cellvalue + '" style = "width:50px; height:50px;"/>';
};

function uploadImgFormatter(cellvalue, options, rowObject) {
	return "<a href='#' style='color:#f60' onclick = 'uploadImg(" + rowObject.id + ")'>上传商品图片</a>";
};


var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			keyword: null
		},
		showNum: "0",
		title:null,
		commodity: {
			id: "",
			commodityName: "",
			price: 0.0,
			introduce: "",
		}
	
	},
	methods: {
		query: function(){
			vm.reload();
		},
		add: function(){
			vm.showNum = "1";
			vm.title = "新增";
			
			vm.commodity = {};
		},
		update: function () {
			var id = getSelectedRow();
			if(id == null) {
				return;
			}
			
			vm.commodity = {};
			var rowData = $("#jqGrid").getRowData(id);
			vm.commodity = rowData;
			vm.showNum = "2";
			vm.title = "修改";
		},
		del: function () {
			var idArray = getSelectedRows();
			if(idArray == null) {
				return;
			}
			var ids = idArray.join(",");
			confirm('确定删除选中的记录？', function() {
				$.ajax({
					type: "POST",
					url: baseURL + "commodity/deleteCommodity",
					data : {
						ids : ids
					},
					success: function(data) {
						if(data.code == "200") {
							alert('操作成功', function() {
								vm.reload();
							});
						} else {
							alert(data.codeDesc);
						}
					}
					
				});
			});
		},
		saveOrUpdate: function () {
			var url = vm.commodity.id == null ? "commodity/saveCommodity" : "commodity/updateCommodity";
			$.ajax({
				type: "POST",
				url : baseURL + url,
				data: vm.commodity,
				success: function(data) {
					if(data.code == "200") {
						alert(data.codeDesc, function() {
							vm.reload();
						});
					} else {
						alert(data.codeDesc);
					}
				}
			});
		},
		reload: function () {
			vm.showNum = "0";
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                postData:{'keyword': vm.q.keyword},
                page:page
            }).trigger("reloadGrid");
		}
	}
});

function uploadImg(id) {
	vm.showNum = "3";
	vm.title = "上传商品图片";
	vm.commodity.id = id;
};

function fileUpload() {
	var formData = new FormData();
	formData.append('file', $('#file')[0].files[0]);
	formData.append('commodityId', vm.commodity.id);
	$.ajax({
		url : baseURL + "commodity/uploadImg",
		type: 'POST',
		data: formData,
		processData: false,
		contentType: false,
		mimeType:"multipart/form-data",
		success: function(data) {
			if(data.code == "200") {
				alert('上传成功', function() {
					vm.reload();
				});
			} else {
				alert(data.codeDesc);
			}
		}
	});
}

