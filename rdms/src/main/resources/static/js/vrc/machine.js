$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'machine/getMachinePage',
        datatype: "json",
        colModel: [	
            { label: '用户ID', name: 'userId', width: 40 },
            { label: '状态', name: 'status', width: 40, formatter: function(value, options, row){
				return value === 0 ? 
						'<span class="label label-danger">停止</span>' : 
						'<span class="label label-success">运行</span>';
				}},
			{ label: '机号', name: 'machineNo', width: 45, key: true },
			{ label: '类型', name: 'typeName', width: 40 },
			{ label: '运行（天）', name: 'runDay', width: 40},
			{ label: '算力', name: 'calculate', width: 40},
			{ label: '产量', name: 'canliang', width: 60},
			{ label: '维修（天）', name: 'mtDay', width: 40},
			{ label: '维修费用', name: 'maintenance', width: 40},
			{ label: '维修状态', name: 'mtStatus', width: 40, formatter: function(value, options, row) {
				return value === 0 ?
						'<span class="label label-danger">损坏</span>' :
						'<span class="label label-success">正常</span>';
			}},
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "data",       //数据
            page: "pageNum",     //当前页数
            total: "pageTotle",    //总页数
            records: "total"     //总数据条数
        },
        prmNames : {
        	page:"pageNum", 
            rows:"pageSize", 
            order: "order",
//            totalrows:"total"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});


var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			keyword: null
		},
		showList: true
	},
	methods: {
		query: function(){
			vm.reload();
		},
		del: function () {
			var codeList = getSelectedRows();
			if(codeList == null){
				return ;
			}
			var codes = codeList.join(",");
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "machine/deleteMachine",
				    data: {
				    	codes: codes
				    },
				    success: function(data){
						if(data.code == "200"){
							alert('操作成功', function(){
                                vm.reload();
							});
						}else{
							alert(data.codeDesc);
						}
					}
				});
			});
		},
		reload: function () {
			vm.showNum = "0";
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                postData:{'keyword': vm.q.keyword},
                page:page
            }).trigger("reloadGrid");
		}
	}
});